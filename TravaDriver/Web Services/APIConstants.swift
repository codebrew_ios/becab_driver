//
//  APIConstants.swift

import Foundation
//added_with_ride
internal struct APIConstants {

   //static let socketBasePath =  "https://becab-api.royoapps.com"  // dev url

    static let socketBasePath = "https://api.becab.app"
    
    //"https://api.becab.app" //live
    
    //static let socketBasePath = "http://192.168.100.127:9015" //local
    static let basePathOnlyUser = "\(APIConstants.socketBasePath)/user/"
    static let basePath = "\(APIConstants.socketBasePath)/service/"
    static let basePathDriverService = "\(APIConstants.socketBasePath)/service/order/"
    static let basePathCommon = "\(APIConstants.socketBasePath)/common/"
    static let basePathChat = "\(APIConstants.socketBasePath)/user/other/"
    static let basePathWater = "\(APIConstants.socketBasePath)/service/water/"
    static let basePathOther = "\(APIConstants.socketBasePath)/service/other/"
    static let basePathUser = "\(APIConstants.socketBasePath)/user/service/"
    static let imageBasePath = "\(APIConstants.socketBasePath)/Resize/"
    static let appStoreUrl = "https://itunes.apple.com/app/id1467144815"
    static let privacyUrl = "https://admin.becab.app/privacy-policy/driver"
    static let TermsConditions = "https://admin.becab.app/terms-conditions/driver"
    static let AboutUs = "https://admin.becab.app/about-us/driver"
    static let imagesBasePath = "\(APIConstants.socketBasePath)/images/"
    //    static let googleApiKey = "AIzaSyCpXEyFf107YGDXVB2IfnuS5ugd96Euz8I"//"AIzaSyBJ--xhuftJS-r48lPXdvYN7nrvJnOpsk0"
    static let googleApiKey = "AIzaSyDWU1oTBLFSa_8fBym2-1LyVWv-c-SeLwo"
    
    //"AIzaSyB-AL6J0U0NV8Cps7s_lnUTZVI3x_TMVHE"
    //"AIzaSyBJ--xhuftJS-r48lPXdvYN7nrvJnOpsk0"
    ////MOhitAIzaSyBiBi-y-jreGQjQfu1M1Fnr8WYZxIZIrxw
    static let status = "success"
    static let statusCode = "statusCode"
    static let message = "msg"
    static var timerProgress = 45
    
    // Login Sign Up
    static let checkuserExists = "checkuserExists"
    static let socailLogin = "socailLogin"
    static let emailLogin = "emailLogin"
    static let sendOtp = "sendOtp"
    static let verifyOtp = "verifyOTP"
    static let addPersonalDetail = "addPersonalDetail"
    static let addServicesToOfferAndBankDetail = "addBankDetail"
    static let addMulkiyaDetail = "addMulkiyaDetail"
    static let brandProducts = "brandProducts"
    static let updateProfileSecondStep = "updateProfileSecondStep"
    static let updateProfileThirdStep = "updateProfileThirdStep"
    // Map or requests
    static let updateStop = "updateStop"
    static let accept = "accept"
    static let reject = "Reject"
    static let start = "Start"
    static let reached = "reached"
    static let cancel = "Cancel"
    static let end = "End"
    static let rate = "Rate"
    static let heatMap = "heatMap"
    static let confirm = "confirm"
    static let changeTurn = "change/turn"
    static let roadPickUpphoneNumber = "roadPickupRequestByDriver"
    static let halfWayStopping = "halfWayStop"
    static let breakdownRequest = "breakdownRequest"
    static let acceptRejectHalfWayStop = "acceptRejectHalfWayStop"
    static let acceptRejectBreakDown = "acceptRejectBreakDown"
    static let addStops = "addStops"
    static let updateOnline = "updateOnline"
     static let directionalHire = "directionalHire"
    static let onGoing = "Ongoing"
    static let updateData = "updateData"
    static let orgCodes = "Orgs"
    static let logout = "logout"
    static let emergencyContacts = "eContacts"
    static let contactUsMessage = "contactus"
    static let profileUpdate = "profileUpdate"
    static let chatList   = "chatlogs"
    static let getConversationList   = "getChat"
    static let getCreditPoints = "getCreditPoints"
    
    static let picUpdate = "picUpdate"
    
    // storyboard // tutorial
    static let loginSignUp = "LoginSignUp"
    static let main = "Main"
    static let first = "first"
    static let second = "second"
    static let third = "third"
    static let viewController = "ViewController"
    
    //support
    static let supportServicesList = "support/list"
    static let notificationSettings = "settingUpdate"
    
    //other endpoint
    static let orderHistory = "order/history"
    static let orderDetail =  "order/details"
    static let earnings =  "earnings"
    static let areaList =  "areas/list"
     static let areaCustomerList =  "area/customers/list"
    static let intiateEtokenOrder  = "initiate/order"
      static let endEtokenOrder  = "end/order"
    //        let endCorrdinate = CLLocationCoordinate2D.init(latitude:37.36553474, longitude: -122.13304395)
}

internal struct BuraqConstant{
    
    static let contactUsMailTo = "support@becab.app"
    static let contactUsCall = "+96898133781"
}

internal struct SocketConstants {
    
    // Emit Keys
    static let CommonEvent = "CommonEvent"
    static let OrderEvent = "OrderEvent"
    static let type = "type"
    static let confirmed = "Confirmed"
    static let reached = "Onreached"
    static let dPending = "DPending"
    static let Ongoing = "Ongoing"
    static let About2Start  = "About2Start"
    static let dApproved = "DApproved"
    static let DSchTimeout = "DSchTimeout"
    static let customerCancel = "CustCancel"
    static let driverCancel = "DriverCancel"
    static let Accepted = "Accepted"
    static let Searching = "Searching"
    static let Chat = "chat"
    
    //orderStatus
    
    static let orderCompleted = "SerComplete"
    static let serRequest =  "SerRequest"
    static let orderUpcoming = "Scheduled"
    static let orderTimeout = "CTimeout"
    static let customerCancelEtoken = "SerCustCancel"
     static let customerCompletedEtoken = "SerCustConfirm"
    static let serCustPending = "SerCustPending"
    static let SerPending = "Serpending"
    static let CustomerCancel = "CustCancel"
    
    //Keys Locally Used
    
    static let order = "order"
    static let order_id = "order_id"

}

internal struct LocalKeys{
    static let MontBold = "Mont-Bold"
    static let MontBlackItalic = "Mont-BlackItalic"
    static let MontserratAlternativesBold = "MontserratAlternates-Bold"
    static let MontBlack = "Mont-Black"

    static let distance = "distance"
    static let duration = "duration"
    static let type = "type"
    static let order = "order"
    static let sFUITextSemibold = ".SFUIText-Semibold"//"SFUIText-Semibold"
    
}

enum Keys : String{
    
    // Socket
    case type = "type"
    case UpdateData = "UpdateData"
    case DCurrentOrders = "DCurrentOrders"
    case service_detail = "service_detail"
    
    //Home
    case lat = "lat"
    case lng = "lng"
    
    // send OTP Keys
    case language_id  = "language_id"
    case phone_code  = "phone_code"
    case phone_number = "phone_number"
    case timezone   = "timezone"
    case Latitude = "Latitude"
    case Longitude = "Longitude"
    case socket_id = "socket_id"
    case fcm_id = "fcm_id"
    case device_type = "device_type"
    case otp = "otp"
    case pickup_address = "pickup_address"
   // case paymentType = "payment_type"
    case order_id = "order_id"
    case driver_charge
    case waiting_time
    case ride_stop_id = "ride_stop_id"
    case pickup_latitude = "pickup_latitude"
    case pickup_longitude = "pickup_longitude"
    case dropoff_address = "dropoff_address"
    case dropoff_latitude = "dropoff_latitude"
    case dropoff_longitude = "dropoff_longitude"
    case order_timings = "order_timings"
    case future = "future"
    case transaction_id = "transaction_id"
    case distance = "distance"
    case booking_type = "booking_type"
    // signUp Keys
    case access_token  = "access_token"
    case name = "name"
    case organisation_id   = "organisation_id"
    case profile_pic = "profile_pic"
    case mime_type = "image/jpg"
    case customerSignature = "customer_signature"
    
    // service offer and BankDetail
    
    case category_id  = "category_id"
    case category_brand_id = "category_brand_id"
    case bank_name = "bank_name"
    case bank_account_number = "bank_account_number"
    
    // Mulkiya Details
    case licenseNumber = "licenseNumber"
    case licensePicture = "licensePicture"
    case mulkiya_number  = "mulkiya_number"
    case mulkiya_validity = "mulkiya_validity"
    case category_brand_product_ids = "category_brand_product_ids"
    case mulkiya = "mulkiya"
    case details = "details"
    case stops = "stops"
    case material_details = "material_details"
    //Upload Step Two
    case vehicleRegistrationNumber = "vehicle_registration_number"
    case insurance_expiry = "insurance_expiry"
    case vehicle_rc_expiry = "vehicle_rc_expiry"
    case vehicleNumber = "vehicle_number"
    case final_charge = "final_charge"
    // Driver Service request
    case status = "status"
    case latitude  = "latitude"
    case longitude = "longitude"
   case payment_type = "payment_type"
    case order_distance = "order_distance"
    case online_status = "online_status"
    case directional_hire = "directional_hire"
    case cancel_reason = "cancel_reason"
    case status_value_on = "1"
    case status_value_off = "0"
    case ratings = "ratings"
    case comments = "comments"
    case approved = "approved"
    case bearing = "bearing"
    case polyline = "polyline"
    
    //    case speed = "speed"
    
    //MARK: Setting Panel
    
    case notifications = "notifications"
    case start_dt = "start_dt"
    case end_dt = "end_dt"
    
    //contacts us
    case message = "message"
    
    //other
    case skip = "skip"
    case take = "take"
    
    // water module
    case day = "day"
     case organisation_area_id   = "organisation_area_id"
    case organisation_coupon_user_id = "organisation_coupon_user_id"
    case coupon_id = "coupon_id"
    case product_weight = "product_weight"
    case productQuantity =  "product_quantity"
    case product_sq_mt = "product_sq_mt"
    case carModel = "vehicle_name"
    case carColor = "vehicle_color"
    case home_address = "home_address"
    case home_longitude = "home_longitude"
    case home_latitude = "home_latitude"
    case half_way_stop_reason = "half_way_stop_reason"
    case reason = "reason"
    case address = "address"
    case category_brand_product_id = "category_brand_product_id"
    case email = "email"
    case vehicle_make
    case vehicle_model
    case vehicle_year
    case order_time
     case receiver_id = "receiver_id"
     case limit
    case login_as
    case social_key
    case password
    case signup_as
    case referral_code
    
}

enum Validate : String {
    
    case none
    case success = "1"
    case failure = "0"
    case invalidAccessToken = "401"
    case fbLogin = "3"
    
    func map(response message : String?) -> String? {
        
        switch self {
        case .success:
            return message
        case .failure :
            return message
        case .invalidAccessToken :
            return message
        default:
            return nil
        }
    }
}

enum Response {
    case success(AnyObject?)
    case failure(String?)
}

typealias OptionalDictionary = [String : String]?

struct Parameters {
    static let updateStop :[Keys] = [.order_id,.ride_stop_id,.waiting_time, .type]
    static let checkuserExists : [Keys] = [.social_key, .login_as]
    static let socailLogin : [Keys] = [.language_id , .timezone , .Latitude , .Longitude , .socket_id , .fcm_id, .device_type, .social_key, .login_as]
    static let emailLogin : [Keys] = [.language_id , .timezone , .Latitude , .Longitude , .socket_id , .fcm_id, .device_type, .login_as, .email, .password ]
    static let signup : [Keys] = [.name, .organisation_id]
    static let sendOtp : [Keys]  = [.language_id, .phone_code, .phone_number, .timezone, .Latitude , .Longitude , .socket_id , .fcm_id, .device_type, .social_key, .signup_as, .email, .password]
    static let verifyOTP :[Keys] = [.otp]
//    static let  addServicesToOffer :[Keys] = [.category_id, .category_brand_id, .category_brand_product_ids ]
    static let  addServicesToOffer :[Keys] = [.service_detail]
    
    static let addMulkiyaDetails : [Keys] = [.mulkiya_number, .mulkiya_validity, .bank_name, .bank_account_number, .carModel, .carColor, .home_address, .home_longitude, .home_latitude]
    
    
   
    
    static let uploadProfileStep2 : [Keys] = [.vehicleNumber,.vehicleRegistrationNumber, .vehicle_rc_expiry, .insurance_expiry,.vehicle_make, .vehicle_model, .carColor, .vehicle_year]
    
    // Driver Services
    static let accept :[Keys] = [.order_id ,.latitude , .longitude]
    static let start :[Keys] = [.order_id ,.latitude , .longitude, .waiting_time]
    static let reject :[Keys] = [.order_id ,.latitude , .longitude]
    static let rate :[Keys] = [.order_id ,.ratings , .comments]
    static let cancel :[Keys] = [.order_id , .cancel_reason]
    static let end :[Keys] = [.order_id ,.order_distance,.latitude , .longitude, .payment_type,.order_time,.driver_charge]
    static let confirm :[Keys] = [.order_id ,.latitude , .longitude ,.approved]
    static let changeDropToAnotherRide:[Keys] = [.order_id]
    static let requestApi:[Keys] = [.category_id,.category_brand_id, .category_brand_product_id, .productQuantity, .dropoff_address,.dropoff_latitude,.dropoff_longitude,.pickup_address,.pickup_latitude,.pickup_longitude,.order_timings,.future,.payment_type,.distance,.coupon_id,.organisation_coupon_user_id,.product_weight,.product_sq_mt,.order_distance,.name,.phone_number,.phone_code,.booking_type,.transaction_id,.timezone,.final_charge,.material_details,.details,.stops]
    static let halfWayStop :[Keys] = [.order_id ,.payment_type , .order_distance ,.half_way_stop_reason,.latitude,.longitude,.driver_charge]
    static let breakDown :[Keys] = [.order_id , .address ,.reason,.latitude,.longitude,.order_distance]
    static let acceptRejectHalfWayStop :[Keys] = [.order_id , .status,.driver_charge]
    static let acceptRejectBreakDown :[Keys] = [.order_id , .status , .order_distance,.driver_charge]
    static let addStops :[Keys] = [.order_id , .stops]
    static let setOnline :[Keys] = [.online_status,.directional_hire]
    static let setDirectionalHireStatus :[Keys] = [.directional_hire]
    static let updateData: [Keys] = [.language_id, .timezone, .Latitude, .Longitude , .socket_id, .fcm_id]
    static let contactUsMessage: [Keys] = [.message]
    static let profileUpdate:[Keys] = [.name,.mulkiya_number ,.mulkiya_validity,.home_address,.home_longitude,.home_latitude,.email]
    
    static let updateHomeAddress:[Keys] = [.home_address,.home_longitude,.home_latitude]
    static let profileUpdate2:[Keys] = [.vehicleNumber, .vehicleRegistrationNumber,.vehicle_rc_expiry,.insurance_expiry]
    static let profileUpdate3:[Keys] = [.name,.email,.referral_code]
    static let orderDetails:[Keys] = [.order_id]
    static let notificationSettings:[Keys] = [.notifications]
    static let getBrandProdcuts:[Keys] = [.category_brand_id]
    static let getEarnings:[Keys] = [.start_dt, .end_dt]
    
    // water modules api
    
    static let getWaterDeliverylocation :[Keys] = [.day, .latitude, .longitude, .skip, .take]
    static let getAreaCustomers :[Keys] = [.organisation_area_id, .latitude, .longitude, .skip, .take]
    static let initiateOrderFromEtoken:[Keys] = [.organisation_coupon_user_id, .latitude, .longitude ]
    static let endOrderFromEtoken:[Keys] = [.order_id, .latitude, .longitude, .productQuantity, .order_distance]
    
    //other api
    static let getOrderHistory:[Keys] = [.skip, .take, .type]
    
    static let pssChatList :[Keys] = [.language_id,.receiver_id,.limit,.skip,.order_id]
       static let getchatList :[Keys] = [.language_id,.limit,.skip]
}

struct token {
    static var access_token = ""
    static var selectedLanguage = "1"
    static var fcmToken = ""
}

