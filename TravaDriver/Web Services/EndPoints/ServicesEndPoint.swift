//
//  ServicesEndPoint.swift
//  Buraq24Driver
//
//  Created by Apple on 17/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import Alamofire

enum ServicesEndPoint {
    case updateStop(orderId:Int,stopId:Int,waitingTime:String,type:String)
    case accept(orderId : Int? , latitude : Double? , longitude : Double?)
    case start(orderId : Int? , latitude : Double? , longitude : Double?, waitingTime: Int?)
    case reached(orderId : Int? , latitude : Double? , longitude : Double?)
    case reject(orderId : Int? , latitude : Double? , longitude : Double?)
    case cancel(orderId : Int? , cancelReason: String?)
    case confirm(orderId:Int?,latitude : Double? , longitude : Double? , approved: String? )
    case end(orderId : Int? , orderDistance:String , latitude : Double? , longitude : Double? , paymentType :String?, order_time: String,amount:String)
    case rate(orderId:Int?, rating:Int?, comments:String?)
    case setOnline(status : String?)
    case onGoing()
    case changeDropToAnotherRide(orderID:Int?)
    case requestApi( categoryId : Int?, categoryBrandId : Int?, categoryBrandProductId : Int? ,productQuantity : Int? , dropOffAddress : String? , dropOffLatitude : Double? , dropOffLongitude : Double?  , pickupAddress : String? , pickupLatitude : Double? , pickupLongitude : Double? , orderTimings : String? , future : String? , paymentType : String? , distance : Int? ,couponId:String?, organisationCouponUserId : Int? , productWeight : Double? , productDetail : String? , orderDistance :Float?, name:String?, phone_number:String?, phoneCode:String, bookingType:String, transactionId:String?, timezone:String,finalCharge:String, materialDetails:String, details:String?,Stops:String)
    case halfWayStopping(orderId:Int,paymentType:String,orderDistance:String,halfWayStopReason:String,latitude:Double,longitude:Double,amount:Double)
    case breakDown(orderId:Int,address:String,reason:String,latitude:Double,longitude:Double,orderDistance:String)
    case heatMap()
    case acceptRejectHalfWayStop(orderId:Int,status:String,amount:Double)
    case acceptRejectBreakDown(orderId:Int,status:String,orderDistance:String,amount:Double?)
    case addStops(orderId:Int,stops:String)
    case getCreditPoints
}



extension ServicesEndPoint : Router{
    
    var route : String  {
        
        switch self {
            case .updateStop(_): return APIConstants.updateStop
        case .accept(_): return APIConstants.accept
        case .start(_): return APIConstants.start
        case .reached(_): return APIConstants.reached
        case .reject(_): return APIConstants.reject
        case .cancel(_): return APIConstants.cancel
        case .end(_): return APIConstants.end
        case .rate(_): return APIConstants.rate
        case .setOnline(_): return APIConstants.updateOnline
        case .onGoing() : return APIConstants.onGoing
        case .confirm(_): return APIConstants.confirm
        case .changeDropToAnotherRide(_): return  APIConstants.changeTurn
        case .requestApi(_): return  APIConstants.roadPickUpphoneNumber
        case .halfWayStopping(_):  return  APIConstants.halfWayStopping
        case .breakDown(_): return  APIConstants.breakdownRequest
        case .heatMap(): return APIConstants.heatMap
        case .acceptRejectHalfWayStop: return APIConstants.acceptRejectHalfWayStop
        case .acceptRejectBreakDown(_): return APIConstants.acceptRejectBreakDown
        case .addStops(_): return APIConstants.addStops
        case .getCreditPoints : return APIConstants.getCreditPoints
            
        }
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
        case .updateStop(let orderId,let stopId,let waitingTime,let type):
            return  Parameters.updateStop.map(values: ["\(/orderId)","\(stopId)" , "\(/waitingTime)" , "\(/type)"])
        case .accept( let orderId, let latitude , let longitude ):
            return Parameters.accept.map(values: ["\(/orderId)" , "\(/latitude)" , "\(/longitude)"])
            
        case .start( let orderId, let latitude , let longitude, let waitingTime ):
            return Parameters.start.map(values: ["\(/orderId)" , "\(/latitude)" , "\(/longitude)", "\(/waitingTime)"])
            
        case .reached( let orderId, let latitude , let longitude ):
            return Parameters.accept.map(values: ["\(/orderId)" , "\(/latitude)" , "\(/longitude)"])
            
        case .reject( let orderId, let latitude , let longitude ):
            return Parameters.reject.map(values: ["\(/orderId)" , "\(/latitude)" , "\(/longitude)"])
            
        case .end( let orderId, let orderDistance, let latitude , let longitude, let paymentType , let orderTime,let amount):
            return Parameters.end.map(values: ["\(orderId!)" , "\(orderDistance)" , "\(/latitude)" , "\(/longitude)" , paymentType, orderTime, amount])
            
        case .cancel(let orderId, let cancelReason):
            return Parameters.cancel.map(values: ["\(/orderId)", "\(/cancelReason)"])
            
        case .rate(let orderId, let rating, let  comments):
            return Parameters.rate.map(values: ["\(/orderId)" , "\(/rating)", comments])
            
        case .setOnline(let status):
            return Parameters.setOnline.map(values: [status])
            
        case .onGoing():
            return [:]
            
        case .changeDropToAnotherRide(let orderID):
            return Parameters.changeDropToAnotherRide.map(values: ["\(/orderID)"])
            
        case .confirm(let orderId, let latitude, let  longitude, let  approved):
            return Parameters.confirm.map(values: ["\(/orderId)" , "\(/latitude)" , "\(/longitude)" , "\(/approved)"])
            
        case .requestApi(let categoryId , let categoryBrandId , let categoryBrandProductId , let productQuantity, let dropOffAddress , let dropOffLatitude ,let dropOffLongitude,let pickupAddress , let pickupLatitude,let pickupLongitude,let orderTimings ,let future, let paymentType ,let distance , let couponId , let organisationCouponUserId  , let productWeight ,let productDetail ,let orderDistance ,let name, let phone_number , let phoneCode , let bookingType  ,let transactionId , let timezone , let finalCharge ,let materialDetails , let details , let stops) :
            return Parameters.requestApi.map(values: ["\(/categoryId)" , "\(/categoryBrandId)" , "\(/categoryBrandProductId)" , "\(/productQuantity)" , "\(/dropOffAddress)" ,"\(/dropOffLatitude)" , "\(/dropOffLongitude)" , "\(/pickupAddress)" , "\(/pickupLatitude)", "\(/pickupLongitude)" , "\(/orderTimings)" , "\(/future)" , "\(/paymentType)" , "\(/distance)",  "\(/couponId)" , "\(/organisationCouponUserId)" , "\(/productWeight)" , "\(/productDetail)" , "\(/orderDistance)" , "\(/name)" , "\(/phone_number)" , "\(/phoneCode)" , "\(/bookingType)",  "\(/transactionId)" , "\(/timezone)" , "\(/finalCharge)" , "\(/materialDetails)" , "\(/details)", stops])
        case .halfWayStopping(let orderId, let paymentType, let orderDistance, let halfWayStopReason, let latitude, let longitude,let amount):
            return Parameters.halfWayStop.map(values: ["\(/orderId)" , "\(/paymentType)" , "\(/orderDistance)" , "\(/halfWayStopReason)" , "\(latitude)" , "\(longitude)","\(amount)"])
        case .breakDown(let orderId, let address, let reason, let latitude, let longitude,let orderDistance):
            return Parameters.breakDown.map(values: ["\(/orderId)" , "\(/address)" , "\(/reason)" , "\(latitude)" , "\(longitude)" , "\(orderDistance)"])
        case .heatMap:
            return [:]
        case .acceptRejectHalfWayStop(let orderId, let status,let amount):
            return Parameters.acceptRejectHalfWayStop.map(values: ["\(/orderId)" , "\(status)","\(amount)"])
        case .acceptRejectBreakDown(let orderId, let status, let orderDistance,let amount):
            return Parameters.acceptRejectBreakDown.map(values: ["\(/orderId)" , "\(status)" , "\(orderDistance)","\(amount)"])
        case .addStops(let orderId, let stops):
            return Parameters.addStops.map(values: ["\(/orderId)" , stops])
        case .getCreditPoints:
            return nil
        }
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
        default:
            return .post
        }
    }
    
    var baseURL: String{
        switch self {
        case .heatMap:
            return APIConstants.basePathOther
        case .addStops(_):
            return APIConstants.basePathUser
        case .getCreditPoints:
            return APIConstants.socketBasePath + "/user/"
        default :
            return APIConstants.basePathDriverService
            
        }
        
    }
}
