//
//  GeneralEndPoints.swift
//  Buraq24Driver
//
//  Created by Apple on 03/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import Alamofire

enum GeneralEndPoint {
    
    case getOrderHistory(skip : Int?, take:Int?, type:Int? )
    case orderDetails(orderId:String?)
    case getEarnings(startDate:String?, endDate:String?)
    case getWaterDeliverylocation( day:Int,latitude:Double, longitude:Double,skip:Int,take:Int)
     case getAreaCustomers( organisation_area_id:Int,latitude:Double, longitude:Double,skip:Int,take:Int)
    case initiateOrderFromEtoken (organisation_coupon_user_id: Int , latitude:Double, longitude:Double)
    case  endOrderFromEtoken (orderId: Int?, latitude:Double?, longitude:Double?, productQuantity:Int?, order_distance : Int?)
}

extension GeneralEndPoint : Router{
    
    var route : String  {
        switch self {
            
        case .getOrderHistory(_): return APIConstants.orderHistory
        case .orderDetails(_): return APIConstants.orderDetail
        case .getEarnings(_):  return APIConstants.earnings
        case .getWaterDeliverylocation(_) :  return APIConstants.areaList
        case .getAreaCustomers(_) : return APIConstants.areaCustomerList
        case .initiateOrderFromEtoken(_): return APIConstants.intiateEtokenOrder
        case .endOrderFromEtoken(_) : return APIConstants.endEtokenOrder
        }
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        switch self {
            
        case .getOrderHistory(let skip, let take, let type):
            
            return Parameters.getOrderHistory.map(values: ["\(/skip)", "\(/take)", "\(/type)"])
            
        case .orderDetails(let orderId ):
            return  Parameters.orderDetails.map(values: [orderId])
            
        case .getEarnings(let startDate, let endDate ):
            return Parameters.getEarnings.map(values: ["\(/startDate)", "\(/endDate)"])
            
        case .getWaterDeliverylocation(let day,let  latitude,let longitude,let skip,let take):
            return  Parameters.getWaterDeliverylocation.map(values: ["\(/day)", "\(/latitude)" ,"\(/longitude)", "\(/skip)", "\(/take)"])
            
        case .getAreaCustomers(let organisation_area_id,let  latitude,let longitude,let skip,let take):
            return Parameters.getAreaCustomers.map(values: ["\(/organisation_area_id)", "\(/latitude)" ,"\(/longitude)", "\(/skip)", "\(/take)"])
 
        case .initiateOrderFromEtoken(let organisation_coupon_user_id, let  latitude,let  longitude):
            return Parameters.initiateOrderFromEtoken.map(values: [ "\(organisation_coupon_user_id)", "\(/latitude)" ,"\(/longitude)"])
            
        case .endOrderFromEtoken(let orderId , let latitude, let longitude, let productQuantity, let order_distance):
            return Parameters.endOrderFromEtoken.map(values: ["\(/orderId)" , "\(/latitude)" ,"\(/longitude)" , "\(/productQuantity)" , "\(/order_distance)"])
        }
    }
    
    var method : Alamofire.HTTPMethod {
        
        switch self {
        default:
            return .post
        }
    }
    
    var baseURL: String{
        
        switch self {
            
        case .getWaterDeliverylocation(_), .getAreaCustomers(_), .initiateOrderFromEtoken(_), .endOrderFromEtoken(_):
            return APIConstants.basePathWater
        default:
              return APIConstants.basePathOther
        }
    }
}
