//
//  Login.swift
//  APISampleClass
//
//  Created by cbl20 on 2/23/17.
//  Copyright © 2017 Taran. All rights reserved.
//

import UIKit
import Alamofire

enum LoginEndpoint {
    case checkuserExists(social_key: String?, login_as: String?)
    case socailLogin(languageId : String , timeZone : String? , latitude : Double? , longitude : Double? , socketId : String? , fcmId : String?, device_type:String, social_key: String?, login_as: String?)
    case emailLogin(languageId : String , timeZone : String? , latitude : Double? , longitude : Double? , socketId : String? , fcmId : String?, device_type:String, login_as: String?, email: String?, password: String?)
    case signup(name : String? , organisationId : Int)
    case verifyOtp (otp: String?)
    case sendOtp(languageId : String , phoneCode: String? , phone_number : String? , timeZone : String? , latitude : Double? , longitude : Double? , socketId : String? , fcmId : String?, device_type:String, social_key: String? = nil, signup_as: String? = nil, email: String? = nil, password: String? = nil)
    case addServicesToOffer (serviceId : String? ,categoryBrandId: Int? ,category_brand_product_ids :String? )
    
    case addMulkiyaDetails (mulkiyaNumber: String?, mulkiyaValidity: String?, bankName: String?, bankAccountNumber: String?, carModel: String?, carColor: String?,home_address:String?,home_longitude:Double,home_latitude:Double)
    
    case profileUpdate(name:String, mulkiya_number:String, mulkiya_validity: String, carModel: String?, carColor: String?,home_address:String,home_longitude:Double,home_latitude:Double,email : String)
    case updateProfileImage()
    case uploadProfileStep2(vehicleNumber:String?,vehicleRegistrationNumber:String?,vehicle_rc_expiry:String?,insurance_expiry:String?, vehicleMake: String?, vehicleModel: String, vehicleColor: String, vehicleYear: String) //,vehicleRCDocs:[String]?,insauranceDocs:[String]?,vehicleImages:[String]?)
    case uploadProfileStep3(name:String,email: String,referral_code:String?)
    case updateHomeAddress(home_address:String?,home_longitude:Double,home_latitude:Double)
}

extension LoginEndpoint : Router{
    
    var route : String  {
        
        switch self {
        case .checkuserExists(_): return APIConstants.checkuserExists
        case .emailLogin(_): return APIConstants.emailLogin
        case .socailLogin(_): return APIConstants.socailLogin
        case .sendOtp(_) : return  APIConstants.sendOtp
        case .verifyOtp(_) : return APIConstants.verifyOtp
        case .signup(_): return APIConstants.addPersonalDetail
        case .addServicesToOffer(_): return APIConstants.addMulkiyaDetail
        case .addMulkiyaDetails(_): return APIConstants.addServicesToOfferAndBankDetail
        case .profileUpdate(_): return APIConstants.profileUpdate
        case .updateProfileImage(_): return APIConstants.picUpdate
        case .uploadProfileStep2(_):return APIConstants.updateProfileSecondStep
        case .uploadProfileStep3(_):return APIConstants.updateProfileThirdStep
        case .updateHomeAddress(_):return APIConstants.profileUpdate
            
        }
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
        
        case .checkuserExists(let social_key, let login_as ):
            return Parameters.checkuserExists.map(values: [social_key, login_as])
        case .socailLogin(let languageId, let timeZone, let latitude, let longitude, let socketId, let fcmId, let device_type, let social_key, let login_as):
            return Parameters.socailLogin.map(values: [languageId , timeZone , "\(latitude ?? 0)", "\(longitude ?? 0)", socketId, fcmId, device_type, social_key, login_as])
            
        case .emailLogin(let languageId, let timeZone, let latitude, let longitude, let socketId, let fcmId, let device_type, let login_as, let email, let password) :
            return Parameters.emailLogin.map(values: [ languageId , timeZone , "\(latitude ?? 0)", "\(longitude ?? 0)", socketId, fcmId, device_type, login_as, email, password])
        case .signup( let name, let organisationId):
            return Parameters.signup.map(values: [ name, "\(organisationId)"])
            
        case .sendOtp(let languageId, let phoneCode , let phone_number ,let timeZone, let latitude, let longitude, let socketId, let fcmId, let device_type, let social_key, let signup_as, let email, let password):
        return Parameters.sendOtp.map(values: [languageId ,phoneCode ,phone_number, timeZone , "\(latitude ?? 0)", "\(longitude ?? 0)", socketId, fcmId, device_type, social_key, signup_as, email, password ])
            
        case .verifyOtp(let otp):
            return Parameters.verifyOTP.map(values: [otp])
            
//        case .addServicesToOffer(let serviceId, let categoryBrandId,let category_brand_product_ids):
//            return Parameters.addServicesToOffer.map(values: [serviceId ,"\(categoryBrandId ?? 0)",category_brand_product_ids ])
            
            case .addServicesToOffer(let service_detail, let categoryBrandId,let category_brand_product_ids):
            return Parameters.addServicesToOffer.map(values: [service_detail])
            
        case .addMulkiyaDetails(let mulkiyaNumber, let  mulkiyaValidity, let bankName,  let bankAccountNumber, let carModel,  let carColor, let home_address, let home_longitude, let home_latitude):
            return Parameters.addMulkiyaDetails.map(values: [mulkiyaNumber , mulkiyaValidity ,bankName, bankAccountNumber, carModel, carColor,home_address,"\(home_longitude)","\(home_latitude)"])
            
        case .profileUpdate(let name, let mulkiya_number, let mulkiya_validity, let _, _, let home_address, let home_longitude, let home_latitude, let email):
            
            return Parameters.profileUpdate.map(values: ["\(name)", "\(mulkiya_number)", "\(mulkiya_validity)",home_address,"\(home_longitude)","\(home_latitude)","\(email)"])
            
        case .updateProfileImage():
            return nil
            
            
            
        case .uploadProfileStep2(let vehicleNumber, let vehicleRegistrationNumber , let vehicle_rc_expiry , let insurance_expiry, let vehicleMake, let vehicleModel, let vehicleColor, let vehicleYear): //, let vehicleRCDocs, let insauranceDocs, let vehicleImages):
            return Parameters.uploadProfileStep2.map(values: [vehicleNumber,vehicleRegistrationNumber, vehicle_rc_expiry, insurance_expiry, vehicleMake, vehicleModel, vehicleColor, vehicleYear])//, vehicleRCDocs.joined(separator: ","), insauranceDocs.joined(separator: ","), vehicleImages.joined(separator: ",")])
        
        case .uploadProfileStep3(let name,let email,let referral): //, let vehicleRCDocs, let insauranceDocs, let vehicleImages):
        return Parameters.profileUpdate3.map(values: [name,email,referral])//, vehicleRCDocs.joined(separator: ","), insauranceDocs.joined(separator: ","), vehicleImages.joined(separator: ",")])
        case .updateHomeAddress(let home_address, let home_longitude, let home_latitude):
            return Parameters.updateHomeAddress.map(values: [home_address,"\(home_longitude)","\(home_latitude)"])
        }
}

    var method : Alamofire.HTTPMethod {
        switch self {
        default:
            return .post
        }
    }
    
    var baseURL: String{
        switch self {
        case .checkuserExists(_),.socailLogin(_) , .emailLogin(_):
               return APIConstants.basePathOnlyUser
        default:
            return APIConstants.basePath
        }
    }
}
