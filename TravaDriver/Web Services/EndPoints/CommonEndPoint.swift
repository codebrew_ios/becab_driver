
//
//  ServicesEndPoint.swift
//  Buraq24Driver
//
//  Created by Apple on 17/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import Alamofire

enum CommonEndPoint {
    
    case setOnline(status : String?,directional_hire:String?)
    case updateData(newLanguageId: String?, timeZone: String?, latitude:Double?,longitude:Double?,socketId:String?, fcmId:String?)
    case getOrganisation
    case logout
    case emergencyContacts
    case contactUsMessage(message:String?)
    case supportServices()
    case notificationSettings(notifications: String?)
    case getBrandProdcuts(category_brand_id:Int?)
    case setDiretionalHireStatus(directional_hire : String?)
    case pssChatListing(_ language_id : String,_ receiver_id : String,_ limit : Int?,_ skip : Int?,_ orderId:String)
    case getChatList(_ language_id : String,_ limit : Int?,_ skip : Int?)
}

extension CommonEndPoint : Router{
    
    var route : String  {
        switch self {
        case .setOnline(_): return APIConstants.updateOnline
        case .updateData(_): return APIConstants.updateData
        case .getOrganisation: return APIConstants.orgCodes
        case .logout: return APIConstants.logout
        case .emergencyContacts: return APIConstants.emergencyContacts
        case .contactUsMessage(_): return APIConstants.contactUsMessage
        case  .supportServices: return APIConstants.supportServicesList
        case .notificationSettings(_): return APIConstants.notificationSettings
        case .getBrandProdcuts(_): return APIConstants.brandProducts
        case .setDiretionalHireStatus(_): return APIConstants.directionalHire
        case .pssChatListing(_): return APIConstants.getConversationList
        case .getChatList(_): return APIConstants.chatList
            
        }
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        switch self {
        case .setOnline(let status,let directional_hire):
            
            return Parameters.setOnline.map(values: [status,directional_hire])
            
        case .updateData(let newLanguageId,let timeZone,let latitude,let longitude,let socketId, let fcmId):
            
            return Parameters.updateData.map(values: [newLanguageId , timeZone , "\(/latitude )", "\(/longitude)", socketId, fcmId])
            
        case .contactUsMessage(let message):
            return Parameters.contactUsMessage.map(values: [message])
            
        case .supportServices , .getOrganisation ,.logout , .emergencyContacts :
            return [:]
            
        case .notificationSettings( let notifications):
            return Parameters.notificationSettings.map(values: [notifications])
            
        case .getBrandProdcuts( let category_brand_id):
            return Parameters.getBrandProdcuts.map(values: [/category_brand_id?.toString()])
        case .setDiretionalHireStatus(let directional_hire) :
            return Parameters.setDirectionalHireStatus.map(values: [directional_hire])
            
            
        case .getChatList(let language_id, let limit, let skip):
            return Parameters.getchatList.map(values: [/language_id,/limit?.toString(),/skip?.toString()])
            
        case .pssChatListing(let language_id, let receiver_id, let limit, let skip, let orderId):
            return Parameters.pssChatList.map(values: [/language_id,/receiver_id,/limit?.toString(),/skip?.toString(),/orderId])
        }
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
        case .pssChatListing(_):
            return .get
        default:
            return .post
        }
    }
    
    var baseURL: String{
        switch self {
        case .pssChatListing(_),.getChatList(_):
            return APIConstants.basePathChat
            
        default:
             return APIConstants.basePathCommon
        }
       
    }
}
