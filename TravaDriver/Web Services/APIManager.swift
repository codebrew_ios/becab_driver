//
//  APIManager.swift
//  BusinessDirectory
//
//  Created by Aseem 13 on 04/01/17.

import Foundation
import SwiftyJSON
import NVActivityIndicatorView
import ObjectMapper
import Alamofire

class APIManager : UIViewController {
    
    typealias Completion = (Response) -> ()
    static let shared = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    
    private  var api_request : Router?
    
    func request(with api : Router , completion : @escaping Completion )  {
        
            api_request = api
        
        if !NetworkConnection.isConnected{
            completion(Response.failure( "alert.connectionMessage".localized))
        }
        
        if isLoaderNeeded(api: api) {
        
            if api.baseURL == APIConstants.brandProducts {
                self.stopAnimating()
            }else {
              startAnimating(nil, message: nil, messageFont: nil, type: .lineScalePulseOutRapid, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
            }
            
        }
        
        httpClient.postRequest(withApi: api, success: {[weak self] (data) in
            self?.stopAnimating()
            
            guard let response = data else {
                completion(Response.failure(.none))
                return
            }
            
            let json = JSON(response)
//            print(json)
            
            if json[APIConstants.statusCode].stringValue == Validate.invalidAccessToken.rawValue{
                self?.tokenExpired()
                return
            }
            
            let responseType = Validate(rawValue: json[APIConstants.status].stringValue) ?? .failure
            if responseType == Validate.success{
                
                let object : AnyObject?
                object = api.handle(data: response)
                completion(Response.success(object))
                
                return
            }
            else{
                let str = json[APIConstants.message].stringValue
                completion(Response.failure(str))
            }
            
            }, failure: {[weak self] (message) in
                self?.stopAnimating()
                completion(Response.failure(message))
        })
    }
    
    func request (withImages api : Router , images : [UIImage]?  , completion : @escaping Completion )  {
        
        if isLoaderNeeded(api: api) {
            
            DispatchQueue.main.async {[weak self] in
                self?.startAnimating(nil, message: nil, messageFont: nil, type: .lineScalePulseOutRapid, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
            }
        }
        print(api.baseURL)
        print(api.route)
        print(api.method)
        print(api.parameters)
        
        httpClient.postRequestWithImages(withApi: api, images: images, success: {[weak self] (data) in
            
            DispatchQueue.main.async { [weak self] in
                self?.stopAnimating()
            }
            
            guard let response = data else {
                completion(Response.failure(.none))
                return
            }
            let json = JSON(response)
            print(json)
            
            if json[APIConstants.status].stringValue == Validate.invalidAccessToken.rawValue{
                self?.tokenExpired()
                return
            }
            
            let responseType = Validate(rawValue: json[APIConstants.status].stringValue) ?? .failure
            
            switch responseType {
            case .success:
                let object : AnyObject?
                object = api.handle(data: response)
                completion(Response.success(object))
                
            case .failure:
                let str = json[APIConstants.message].stringValue
                completion(Response.failure(str))
                
            default : break
                
            }
            
            }, failure: {[weak self] (message) in
                
                DispatchQueue.main.async { [weak self] in
                    self?.stopAnimating()
                }
                completion(Response.failure(message))
        })
    }
    
    func tokenExpired(){
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.logout()
    }
    
    func isLoaderNeeded(api : Router) -> Bool{
        
        switch api.route {
        case  APIConstants.updateOnline, APIConstants.onGoing,APIConstants.picUpdate : return false
        default: return true
            
        }
    }
    
    
    func request (withMultipleImages api : Router , images : [String:[UIImage]]?  , completion : @escaping Completion )  {
           
           if isLoaderNeeded(api: api) {
               
               DispatchQueue.main.async {[weak self] in
                   self?.startAnimating(nil, message: nil, messageFont: nil, type: .lineScalePulseOutRapid, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
               }
           }
           
           httpClient.postRequestWithMultipleImages(withApi: api, images: images, success: {[weak self] (data) in
               
               DispatchQueue.main.async { [weak self] in
                   self?.stopAnimating()
               }
               
               guard let response = data else {
                   completion(Response.failure(.none))
                   return
               }
               let json = JSON(response)
               print(json)
               
               if json[APIConstants.status].stringValue == Validate.invalidAccessToken.rawValue{
                   self?.tokenExpired()
                   return
               }
               
               let responseType = Validate(rawValue: json[APIConstants.status].stringValue) ?? .failure
               
               switch responseType {
               case .success:
                   let object : AnyObject?
                   object = api.handle(data: response)
                   completion(Response.success(object))
                   
               case .failure:
                   let str = json[APIConstants.message].stringValue
                   completion(Response.failure(str))
                   
               default : break
                   
               }
               
               }, failure: {[weak self] (message) in
                   
                   DispatchQueue.main.async { [weak self] in
                       self?.stopAnimating()
                   }
                   completion(Response.failure(message))
           })
       }
    
}

