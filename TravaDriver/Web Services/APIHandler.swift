////
////  APIHandler.swift
////  BusinessDirectory
////
////  Created by Aseem 13 on 04/01/17.

import Foundation
import SwiftyJSON
import ObjectMapper
//import RealmSwift

enum ResponseKeys : String {
    case user = "user"
    case countries = "countries"
}

extension LoginEndpoint {
    
    func handle(data : Any) -> AnyObject? {
        
        switch self {
        case .checkuserExists:
            let obj = Mapper<ApiSucessData<CheckUserExistModel>>().map(JSONObject: data)
            return obj?.object
        case  .sendOtp(_):
            
            let object  =  Mapper<OtpModel>().map(JSONObject: data)
            return object
            
        case .verifyOtp(_), .signup(_) , .addServicesToOffer(_), .socailLogin(_), .emailLogin(_)  :
            
            let object = Mapper<DriverModel>().map(JSONObject: data)
            return object
            
        case .addMulkiyaDetails(_):
            
            let object = Mapper<DriverModel>().map(JSONObject: data)
            // UserSingleton.shared.loggedInUser = object
            return object
            
        case .profileUpdate(_):
            return data as AnyObject
            
        case .updateProfileImage():
            return data as AnyObject
        case .uploadProfileStep2(_):
            let object = Mapper<DriverModel>().map(JSONObject: data)
            // UserSingleton.shared.loggedInUser = object
            return object
        case .uploadProfileStep3(_):
            let object = Mapper<DriverModel>().map(JSONObject: data)
            // UserSingleton.shared.loggedInUser = object
            return object
        case .updateHomeAddress(_):
            return data as AnyObject
        }
    }
}

extension ServicesEndPoint{
    func handle(data : Any) -> AnyObject? {
        
        switch self {
        case .updateStop(_):
            return nil
        case .accept(_):
            
            return data as AnyObject
            
        case .reject(_):
            
            return data as AnyObject
            
        case .end(_):
            
            let object = Mapper<CompletedOrderModel>().map(JSONObject: data)
            return object
            
        case .setOnline(_):
            
            return data as AnyObject
            
        case .onGoing():
            
            let object = Mapper<OnGoingOrders>().map(JSONObject: data)
            let JSONOrderObject = object?.orders?.last?.toJSON()
            
            var dic = [String:Any]()
            dic["type"] = "SerRequest"
            dic ["order"] = JSONOrderObject
            
            let val = Mapper<ServiceRequest>().map(JSONObject: dic)
            DataResponse.onGoingRequest = val
            
            return object
            
        case .start(_), .reached(_):
            
            let object = Mapper<StartedRequest>().map(JSONObject: data)
            return object
            
        case .rate(_):
            return data as AnyObject
            
        case .cancel(_):
            return data as AnyObject
            
        case .confirm(_):
            return data as AnyObject
            
        case .changeDropToAnotherRide(_):
            return data as AnyObject
            
        case .requestApi(_):
            return data as AnyObject
        case .addStops(_):
            return data as AnyObject
        case .halfWayStopping(_) :
            let object = Mapper<CompletedOrderModel>().map(JSONObject: data)
            return object
            
        case .breakDown(_) :
            let object = Mapper<CompletedOrderModel>().map(JSONObject: data)
            return object
            
        case .heatMap() :
            
            let object = Mapper<HeatMapData>().map(JSONObject: data)
            return object
            
        case .acceptRejectHalfWayStop(_) :
            let object = Mapper<CompletedOrderModel>().map(JSONObject: data)
            return object
            
        case .acceptRejectBreakDown(_) :
            let object = Mapper<CompletedOrderModel>().map(JSONObject: data)
            return object
        case .getCreditPoints:
            let obj = Mapper<ApiSucessData<User>>().map(JSONObject: data)
            return obj?.object
        }
        
    }
}

extension CommonEndPoint{
    
    func handle(data : Any) -> AnyObject? {
        
        switch self {
            
        case .setOnline(_),.setDiretionalHireStatus(_):
            return data as AnyObject
            
        case .updateData(_):
            
            let object = Mapper<DriverModel>().map(JSONObject: data)
            UserSingleton.shared.loggedInUser = object
            return object
            
        case .getOrganisation:
            
            let object = Mapper<organizations>().map(JSONObject: data)
            return object as AnyObject
            
        case .logout:
            
            return data as AnyObject
            
        case .emergencyContacts:
            
            let object = Mapper<EmergencyContacts>().map(JSONObject: data)
            return object as AnyObject
            
        case.contactUsMessage(_):
            
            return data as AnyObject
            
        case .supportServices(_):
            return data as AnyObject
            
        case .notificationSettings(_):
            
            return data as AnyObject
            
        case .getBrandProdcuts(_):
            
            let object = Mapper<BrandProducts>().map(JSONObject: data)
            
            return object as AnyObject
            
        case .pssChatListing(_):
                   let obj = Mapper<ApiSucessData<Chat>>().map(JSONObject: data)
                   return obj?.array as AnyObject
                   //            let obj =  Mapper<Chat>().map(JSONObject: data)
               //            return obj.arra as AnyObject
               case .getChatList(_):
                   let obj = Mapper<ApiSucessData<UserList>>().map(JSONObject: data)
                   return obj?.array as AnyObject
        }
    }
}

extension GeneralEndPoint{
    func handle(data : Any) -> AnyObject? {
        
        switch self {
        case .getOrderHistory(_):
            let object = Mapper<OrderHistory>().map(JSONObject: data)
            return object as AnyObject
            
        case .orderDetails(_):
            return data as AnyObject
            
        case .getEarnings(_):
            
            let object = Mapper<Earnings>().map(JSONObject: data)
            return object as AnyObject
            
        case .getWaterDeliverylocation(_):
            
            let object = Mapper<WaterDeliveryAreas>().map(JSONObject: data)
            return object as AnyObject
            
        case .getAreaCustomers(_):
            
            let object = Mapper<WaterEtokenClients>().map(JSONObject: data)
            return object as AnyObject
            
        case .initiateOrderFromEtoken(_):
            return data as AnyObject
            
        case .endOrderFromEtoken(_):
            let object = Mapper<CompletedOrderModel>().map(JSONObject: data)
            return object
        }
    }
}
