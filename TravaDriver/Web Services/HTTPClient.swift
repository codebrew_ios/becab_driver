//
//  HTTPClient.swift
//  BusinessDirectory

import Foundation
import Alamofire

typealias HttpClientSuccess = (Any?) -> ()
typealias HttpClientFailure = (String) -> ()

class HTTPClient {
    
    func JSONObjectWithData(data: NSData) -> Any? {
        
        do { return try JSONSerialization.jsonObject(with: data as Data, options: []) }
        catch { return .none }
    }
    
    func postRequest(withApi api : Router  , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure ){
        
        let params = api.parameters
        var fullPath = api.baseURL + api.route
        if api.route == APIConstants.roadPickUpphoneNumber {
           fullPath = APIConstants.socketBasePath + "/user/service/\(api.route)"
        }
        
        let method = api.method
        var headers = [String:String]()
        
        if api.route != APIConstants.sendOtp{
            headers  = ["language_id" : token.selectedLanguage,
                        "access_token": token.access_token]
        }
        
        Alamofire.request(fullPath, method: method, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {
            (response) in
            print(">>>>>>=========== Request ===========>")
            print(fullPath)
            print(method)
            print(headers)
            print(params ?? "No params")
            
            print(">>>>>>=========== response ===========")
            print(response)
            
            switch response.result {
            case .success(let data):
                success(data)
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    func postRequestWithImages(withApi api : Router,images : [UIImage]?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        let params = api.parameters
        
        let fullPath = api.baseURL + api.route
        let headers = [Keys.access_token.rawValue: "\(token.access_token)",
            Keys.language_id.rawValue : token.selectedLanguage]
        
        Alamofire.upload(multipartFormData:{
            multipartFormData in
            
            if let arrayImages = images {
                for (i, image) in arrayImages.enumerated() {
                    guard let imageData = UIImageJPEGRepresentation(image, 0.5) else { return }
                    
                    if (api.route ==  APIConstants.addPersonalDetail) || (api.route == APIConstants.picUpdate) {
                        
                        
                        //  parameter keys in case of profile picure
                        multipartFormData.append(imageData, withName: Keys.profile_pic.rawValue, fileName: "\(Keys.profile_pic.rawValue).jpg", mimeType: Keys.mime_type.rawValue)
                        
                    } else if (api.route ==  APIConstants.end) {
                        
                        //  parameter keys in case of profile picure
                        multipartFormData.append(imageData, withName: Keys.customerSignature.rawValue, fileName: "\(Keys.customerSignature.rawValue)\(Int(Date().timeIntervalSinceNow)).jpg", mimeType: Keys.mime_type.rawValue)
                        
                    }else if (api.route == APIConstants.addServicesToOfferAndBankDetail){
                        //parameter keys in case of addBankDetails api
                        multipartFormData.append(imageData, withName: Keys.licensePicture.rawValue, fileName: "\(Keys.licensePicture.rawValue)\(Int(Date().timeIntervalSinceNow)).jpg", mimeType: Keys.mime_type.rawValue)
                        
                        multipartFormData.append(imageData, withName: Keys.licenseNumber.rawValue, fileName: "\(Keys.licenseNumber.rawValue)\(Int(Date().timeIntervalSinceNow)).jpg", mimeType: Keys.mime_type.rawValue)
                    }
                    else {
                        // name in case of other mulkiya images
                        multipartFormData.append(imageData, withName: Keys.mulkiya.rawValue, fileName: "\(Keys.mulkiya.rawValue)[\(Int(Date().timeIntervalSinceNow))].jpg", mimeType: Keys.mime_type.rawValue)
                    }
                }
            }
            
            if params != nil {
                
                for (key, value) in params! {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, usingThreshold:UInt64.init(),
           to:fullPath,
           method:.post,
           headers:headers,
           encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    print(">>>>>>=========== Request ===========")
                    print(fullPath)
                    print("POST")
                    print(headers)
                    print(params ?? "No params")
                    
                    print(">>>>>>=========== response ===========")
                    print(response)
                    
                    let string = String(data: response.data ?? Data(), encoding: .utf8)
                    print("RESPONSE",string ?? "")
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    func postRequestWithMultipleImages(withApi api : Router,images : [String:[UIImage]]?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        let params = api.parameters
        
        let fullPath = api.baseURL + api.route
        let headers = [Keys.access_token.rawValue: "\(token.access_token)",
            Keys.language_id.rawValue : token.selectedLanguage]
        
        Alamofire.upload(multipartFormData:{
            multipartFormData in
            
            if let data = images {
                let keys = Array(data.keys)
              
                for (_,keyString) in keys.enumerated() {
                    if let arrayImages = data[keyString] {
                        for (_, image) in arrayImages.enumerated() {
                            guard let imageData = UIImageJPEGRepresentation(image, 0.5) else { return }
                            // name in case of other mulkiya images
                            multipartFormData.append(imageData, withName: keyString, fileName: "\(keyString)[\(Int(Date().timeIntervalSinceNow))].jpg", mimeType: Keys.mime_type.rawValue)
                        }
                    }
                }
            }
            
            if params != nil {
                
                for (key, value) in params! {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, usingThreshold:UInt64.init(),
           to:fullPath,
           method:.post,
           headers:headers,
           encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    print(">>>>>>=========== Request ===========")
                    print(fullPath)
                    print("POST")
                    print(headers)
                    print(params ?? "No params")
                    
                    print(">>>>>>=========== response ===========")
                    print(response)
                    
                    
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
}
