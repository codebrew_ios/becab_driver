//
//  AppDelegate.swift
//  Buraq24Driver
//
//  Created by OSX on 07/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import DropDown
import Crashlytics
import Fabric
import Firebase
import UserNotifications
import FirebaseMessaging
import AVKit
import Alamofire
import FirebaseMessaging
import Firebase
import FirebaseInstanceID

let googleKey = "AIzaSyAvkj1KOPpZpzwhsgMeDn1XGHsS2A9smW4"

struct NotificationRequest {
    static var orderId = 0
    static var isNotified = false
    static var orderIds = [Int]()
}

//@UIApplicationMain
//
//class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate{
//
//    var window: UIWindow?
//
//   // GMSPlacesClient.provideAPIKey("AIzaSyCjm_5GRo_aSr7m8Z2A_aUK7tRmVnUO094")
//  //  GMSServices.provideAPIKey("AIzaSyCjm_5GRo_aSr7m8Z2A_aUK7tRmVnUO094")
//
//
//    lazy var arrRequestIds = [Int]()
//    lazy var arrAboutToStart = [Int]()
//    lazy var arrAboutToStartPending = [Int]()
//    lazy var arrRequestCancelledOrOtherAccepted = [Int]()
//
//    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
//
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//
//        // App Custom appearance
//       // window?.tintColor = Colors.themeColor
//        if let font = UIFont(name: "Mont-Normal", size: 16) {
//            UINavigationBar.appearance().titleTextAttributes = [
//                NSAttributedStringKey.font: font]
//        }
//
//
//
//
//
//        //AIzaSyB-AL6J0U0NV8Cps7s_lnUTZVI3x_TMVHE
//        GMSPlacesClient.provideAPIKey(googleKey)
//        GMSServices.provideAPIKey(googleKey)
//
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#F59100")]
//
//
//        // google api  key
//        GMSServices.provideAPIKey(APIConstants.googleApiKey)
//
//        //Location initialiser
//        LocationManager.shared.updateUserLocation()
//
//        //To adjust Dropdown
//        DropDown.startListeningToKeyboard()
//
//        //listen for reachability
//        self.listenForReachability()
//
//        // Enabling keyboard manager
//        IQKeyboardManager.sharedManager().enable = true
//
//        //Crashlytics
//        Fabric.with([Crashlytics.self])
//
//
////        // Firebase Setup
////        FirebaseApp.configure()
////
////        //Messaging Delegate
////        Messaging.messaging().delegate = self
////
////        pushNotificationPermission(application: application)
//
//
//        // register for remote notifications
//    //    application.registerForRemoteNotifications()
//
//        //clear notifications and handle tap on notification
//
//
//
//
//        self.listenForReachability()
//
//        Messaging.messaging().delegate = self
//
//        FirebaseApp.configure()
//
//
////        pushNotificationPermission(application: application)
//
//        if let localNotificationInfo =  launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] {
//                        application.applicationIconBadgeNumber = 0
//                        let center = UNUserNotificationCenter.current()
//                        center.removeAllDeliveredNotifications() // To remove all delivered notifications
//
//                        let userInfo = localNotificationInfo as? NSDictionary
//                        let orderId = userInfo?.value(forKey: "order_id") as? String
//                        let eventType = userInfo?.value(forKey: "gcm.notification.push_type") as? String
//                        let expiringAt = userInfo?.value(forKey: "expiry") as? String
//
//                        let timeLeft = expiringAt?.timeDiffrenceBetweenUTCDates(date: /expiringAt)
//                        if (/timeLeft > 0) && (eventType == SocketConstants.OrderEvent){
//                            NotificationRequest.isNotified = true
//                            NotificationRequest.orderId = Int(orderId!)!
//                        }
//        }
//
//        // Remember login
//        token.access_token = /(UserSingleton.shared.loggedInUser?.data?.accessToken)
//
//        let selectedLanguage = UserDefaults.standard.value(forKey: "language") as? String
//        LanguageFile.shared.changeLanguage(type:/selectedLanguage)
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//
//
//        application.registerForRemoteNotifications()
//        return true
//    }
//
//    func getNotificationSettings() {
//
//        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
//            guard settings.authorizationStatus == .authorized else { return }
//
//            DispatchQueue.main.async {
//                UIApplication.shared.registerForRemoteNotifications()
//            }
//
//        }
//    }
//
//    func pushNotificationPermission(application:UIApplication, succcess: (() -> Void)? = nil){
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in
//                succcess?()
//                   self.getNotificationSettings()
//            }
//
//            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
//                switch settings.authorizationStatus {
//                case .authorized:
//                    succcess?()
//
//                default:
//                    break
//                }
//            }
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//    }
//
//    //MARK:- Listen Network reachablity
//
//    func listenForReachability() {
//        self.reachabilityManager?.listener = { status in
//            print("Network Status Changed: \(status)")
//            switch status {
//            case .notReachable, .unknown:
//                NetworkConnection.isConnected = false
//
//                NotificationCenter.default.post(name: Notification.Name(rawValue:NotificationNames.internetDisconnected.rawValue), object: nil)
//
//            //Show error state
//            case .reachable(_):
//
//                NetworkConnection.isConnected = true
//                NotificationCenter.default.post(name: Notification.Name(rawValue:NotificationNames.internetConnected.rawValue), object: nil)
//                //Hide error state
//            }
//        }
//
//        if !(/Alamofire.NetworkReachabilityManager()?.isReachable) {
//            NetworkConnection.isConnected = false
//        }
//
//        self.reachabilityManager?.startListening()
//    }
//
//
//    // Firebase delegate
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//
//
//        // Print full message.
//        print("tap on on forground app",userInfo)
//        completionHandler()
//    }
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("==========FCM Token===========")
//        print(fcmToken)
//        token.fcmToken = fcmToken
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name(NotificationNames.fcmTokenUpdated.rawValue), object: nil, userInfo: dataDict)
//    }
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
//        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//
//        print(token)
//    }
//
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//
//        application.applicationIconBadgeNumber = 0
//
////        let center = UNUserNotificationCenter.current()
////        center.removeAllDeliveredNotifications() // To remove all delivered notifications
////        center.removeAllPendingNotificationRequests()
//
//        print(userInfo)
//    }
//
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//
//
//           if let payload  = notification.request.content.userInfo  as? [String : Any] {
//
//               guard let type = payload["type"] as? String else { return }
//
//               switch type {
//
//
//               case "chat":
//
//
//                        NotificationCenter.default.post(name: Notification.Name(rawValue:"chat"), object: true)
//
//
//
//               default:
//                   break
//               }
//           }
//
//           //        let type = dic.value(forKey: "type") as? String
//           //
//           //        let future = dic.value(forKey: "future") as? String
//           //        let user_detail_id = dic.value(forKey: "user_detail_id") as? String
//           //        let google = dic.value(forKey: "google.c.a.e") as? String
//           //        let user_id = dic.value(forKey: "user_id") as? String
//           //        let aps = dic.value(forKey: "aps") as? NSDictionary
//           //        let gcmMessageId = dic.value(forKey: "gcm.message_id") as? String
//           //        debugPrint(eventType)
//
//          // completionHandler([.alert, .sound])
//           NSLog("Userinfo %@",notification.request.content.userInfo);
//       }
//
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//
//        application.applicationIconBadgeNumber = 0
//        let center = UNUserNotificationCenter.current()
//        center.removeAllDeliveredNotifications() // To remove all delivered notifications
//        center.removeAllPendingNotificationRequests()
//
//
//        let dic = userInfo as NSDictionary
//        let eventType = dic.value(forKey: "gcm.notification.push_type") as? String
//        let message = dic.value(forKey: "message") as? String
//        let orderId = dic.value(forKey: "order_id") as? String
//        let expiringAt = dic.value(forKey: "expiry") as? String
//
//        let _ = expiringAt?.timeDiffrenceBetweenUTCDates(date: /expiringAt)
//
//        switch eventType {
//
//        case SocketConstants.DSchTimeout:
//
//            Alerts.shared.show(alert: "alert.alert".localized, message: /message, type: .info)
//
//        case  SocketConstants.dPending:
//
//            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
//
//            if !self.arrAboutToStartPending.contains(/Int(/orderId)){
//                self.arrAboutToStartPending.append(/Int(/orderId))
//
//                NotificationCenter.default.post(name: Notification.Name(NotificationNames.confirmScheduled.rawValue), object: nil, userInfo: dataDict)
//            }
//
//        case SocketConstants.About2Start:
//
//            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
//
//            if !self.arrAboutToStart.contains(/Int(/orderId)){
//                self.arrAboutToStart.append(/Int(/orderId))
//                NotificationCenter.default.post(name: Notification.Name(NotificationNames.openRequestModal.rawValue), object: nil, userInfo: dataDict)
//            }
//
//        case OrderEventTypes.requestCancelled.rawValue:
//
//            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
//
//            NotificationCenter.default.post(name: Notification.Name(NotificationNames.requestCancelled.rawValue), object: nil, userInfo: dataDict)
//
//        case SocketConstants.customerCancel:
//
//            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
//            NotificationCenter.default.post(name: Notification.Name(NotificationNames.requestCancelled.rawValue), object: nil, userInfo: dataDict)
//
//        case  SocketConstants.OrderEvent:
//
//            if let orderId = dic.value(forKey: "order_id") as? String{
//
//                if !self.arrRequestIds.contains(/Int(/orderId)){
//                    self.arrRequestIds.append(/Int(/orderId))
//                    let dataDict:[String: String] = [SocketConstants.order_id: orderId]
//                    SocketIOManager.shared.isAuthenticated = false
//                    NotificationCenter.default.post(name: Notification.Name(NotificationNames.openRequestModal.rawValue), object: nil, userInfo: dataDict)
//                }
//            }
//
//        default:
//
//            break
//        }
//
//        completionHandler(UIBackgroundFetchResult.newData)
//    }
//
//    func applicationWillResignActive(_ application: UIApplication) {
//        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//    }
//
//    func applicationDidEnterBackground(_ application: UIApplication) {
//
//        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//
//    }
//
//    func applicationWillEnterForeground(_ application: UIApplication) {
//
//       self.checkUpdate()
//
//        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//    }
//
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//
//        let center = UNUserNotificationCenter.current()
//
//        var arrIds = [Int]()
//
//        center.getDeliveredNotifications { (arr) in
//
//            for item in arr{
//                let userInfo = item.request.content.userInfo as NSDictionary
//                let orderId = userInfo.value(forKey: "order_id") as? String
//                let eventType = userInfo.value(forKey: "gcm.notification.push_type") as? String
//                let expiringAt = userInfo.value(forKey: "expiry") as? String
//
//
//                if !self.arrRequestIds.contains(/Int(/orderId)){
//
//                    let timeLeft = expiringAt?.timeDiffrenceBetweenUTCDates(date: /expiringAt)
//                    if (/timeLeft > 0) && (eventType == SocketConstants.OrderEvent){
//                        NotificationRequest.isNotified = true
//                        arrIds.append(/Int(/orderId))
//                    }
//                }
//            }
//
//            NotificationRequest.orderIds = arrIds
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
//
//            if NotificationRequest.orderIds.count > 0{
//              NotificationCenter.default.post(name: Notification.Name(NotificationNames.readPendingNotifications.rawValue), object: nil, userInfo: nil)
//            }
//        }
//
//        // clear notification badge
//
//        application.applicationIconBadgeNumber = 0
//        center.removeAllDeliveredNotifications() // To remove all delivered notifications
//
//        NotificationCenter.default.post(name: Notification.Name(NotificationNames.didBecomeActive.rawValue), object: nil, userInfo: nil)
//    }
//
//    func applicationWillTerminate(_ application: UIApplication) {
//        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    }
//
//    //MARK: - Logout
//    func logout () {
//
//        guard let window = self.window else { return}
//
//        guard token.access_token != "" else {
//            return
//        }
//
//        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
////            guard  let vc = R.storyboard.loginSignUp.phoneSignUpViewController() else{ return}
//            guard  let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else{ return}
//            SocketIOManager.shared.closeConnection()
//            let nav = UINavigationController(rootViewController: vc)
//            nav.navigationBar.isHidden = true
//            socketTimer?.invalidate()
//            token.access_token = ""
//            NotificationCenter.default.removeObserver(self)
//            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
//            appDelegate.window?.rootViewController = nav
//            UserDefaults.standard.removeObject(forKey: SingletonKeys.user.rawValue)
//
//        }, completion: { completed in
//            // maybe do something here
//        })
//    }
//
//    func checkUpdate(){
//
//        guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
//            return
//        }
//
//        let force = DataResponse.driverData?.versionForce
//
//        if force != currentVersion{
//
//            if currentVersion.compare(/force, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending {
//
//                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertUpdateApp(), actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.window?.topMostWindowController()) {
//
//                    if let url = URL(string: APIConstants.appStoreUrl),
//                        UIApplication.shared.canOpenURL(url){
//                        UIApplication.shared.open(url, options: [:]) { (opened) in
//                            if(opened){
//                                print("App Store Opened")
//                            }
//                        }
//                    } else {
//                        Alerts.shared.show(alert: "Alert", message: "App store not available.", type: .info)
//                    }
//                }
//            }
//        }
//    }
//}
//
//var applicationStateString: String {
//    if UIApplication.shared.applicationState == .active {
//        return "active"
//    } else if UIApplication.shared.applicationState == .background {
//        return "background"
//    }else {
//        return "inactive"
//    }
//}
//
////extension AppDelegate  {
////    // iOS10+, called when presenting notification in foreground
////    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
////        let userInfo = notification.request.content.userInfo
////        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
////        //TODO: Handle foreground notification
////        completionHandler([.alert])
////    }
////
////    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
////    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
////        let userInfo = response.notification.request.content.userInfo
////        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) didReceiveResponse: \(userInfo)")
////        //TODO: Handle background notification
////        completionHandler()
////    }
////}
//
//extension AppDelegate {
//    var rootViewController: UINavigationController {
//        return window!.rootViewController as! UINavigationController
//    }
//}



@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate{
    
    var window: UIWindow?
    
    // GMSPlacesClient.provideAPIKey("AIzaSyCjm_5GRo_aSr7m8Z2A_aUK7tRmVnUO094")
    //  GMSServices.provideAPIKey("AIzaSyCjm_5GRo_aSr7m8Z2A_aUK7tRmVnUO094")
    
    
    
    lazy var arrRequestIds = [Int]()
    lazy var arrAboutToStart = [Int]()
    lazy var arrAboutToStartPending = [Int]()
    lazy var arrRequestCancelledOrOtherAccepted = [Int]()
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        // App Custom appearance
        // window?.tintColor = Colors.themeColor
        if let font = UIFont(name: "Mont-Normal", size: 16) {
            UINavigationBar.appearance().titleTextAttributes = [
                NSAttributedStringKey.font: font]
        }
        
        
        
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#F59100")]
        
        
        // google api  key
        
        //Location initialiser
        //        LocationManager.shared.updateUserLocation()
        
        //To adjust Dropdown
        DropDown.startListeningToKeyboard()
        
        //listen for reachability
        self.listenForReachability()
        
        // Enabling keyboard manager
        IQKeyboardManager.sharedManager().enable = true
        
        //Crashlytics
        Fabric.with([Crashlytics.self])
        
        // Firebase Setup
        
        
        FirebaseApp.configure()
        
        
        //Messaging Delegate
        Messaging.messaging().delegate = self
        
        GMSPlacesClient.provideAPIKey(googleKey)
        GMSServices.provideAPIKey(googleKey)
        
        
        
        
        
        // register for remote notifications
        //  application.registerForRemoteNotifications()
        
        //       registerForPushNotifications()
        
        //clear notifications and handle tap on notification
        
        if let localNotificationInfo =  launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] {
            application.applicationIconBadgeNumber = 0
            let center = UNUserNotificationCenter.current()
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
            
            let userInfo = localNotificationInfo as? NSDictionary
            let orderId = userInfo?.value(forKey: "order_id") as? String
            let eventType = userInfo?.value(forKey: "gcm.notification.push_type") as? String
            let expiringAt = userInfo?.value(forKey: "expiry") as? String
            
            let timeLeft = expiringAt?.timeDiffrenceBetweenUTCDates(date: /expiringAt)
            if (/timeLeft > 0) && (eventType == SocketConstants.OrderEvent){
                NotificationRequest.isNotified = true
                NotificationRequest.orderId = Int(orderId!)!
            }
        }
        
        // Remember login
        token.access_token = /(UserSingleton.shared.loggedInUser?.data?.accessToken)
        
        if !token.access_token.isEmpty{
            SocketIOManager.shared.createManager()
        }
        let selectedLanguage = UserDefaults.standard.value(forKey: "language") as? String
        LanguageFile.shared.changeLanguage(type:/selectedLanguage)
        
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            Messaging.messaging().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()
        return true
    }
    
    //MARK:- Listen Network reachablity
    
    func listenForReachability() {
        self.reachabilityManager?.listener = { status in
            print("Network Status Changed: \(status)")
            switch status {
            case .notReachable, .unknown:
                NetworkConnection.isConnected = false
                
                NotificationCenter.default.post(name: Notification.Name(rawValue:NotificationNames.internetDisconnected.rawValue), object: nil)
                
            //Show error state
            case .reachable(_):
                
                NetworkConnection.isConnected = true
                NotificationCenter.default.post(name: Notification.Name(rawValue:NotificationNames.internetConnected.rawValue), object: nil)
                //Hide error state
            }
        }
        
        if !(/Alamofire.NetworkReachabilityManager()?.isReachable) {
            NetworkConnection.isConnected = false
        }
        
        self.reachabilityManager?.startListening()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        
        // Print full message.
        print("tap on on forground app",userInfo)
        handle(userInfo: userInfo,fromTap:true)
        completionHandler()
    }
    
    func getNotificationSettings() {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else { return }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }
    }
    
    func pushNotificationPermission(application:UIApplication, succcess: (() -> Void)? = nil){
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in
                succcess?()
                self.getNotificationSettings()
            }
            
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                switch settings.authorizationStatus {
                case .authorized:
                    succcess?()
                    
                default:
                    break
                }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    
    
    
    // Firebase delegate
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        token.fcmToken = fcmToken
        let dataDict:[String: String] = ["token": fcmToken]
        
        print(dataDict)
        NotificationCenter.default.post(name: Notification.Name(NotificationNames.fcmTokenUpdated.rawValue), object: nil, userInfo: dataDict)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Token Value ",token)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        application.applicationIconBadgeNumber = 0
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
        
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        application.applicationIconBadgeNumber = 0
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
        
        
        handle(userInfo: userInfo)
        
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    
    
    func handle(userInfo:[AnyHashable: Any],fromTap:Bool = false){
        let dic = userInfo as NSDictionary
        //        let eventType = dic.value(forKey: "gcm.notification.push_type") as? String
        let eventType = dic.value(forKey: "type") as? String
        let message = dic.value(forKey: "message") as? String
        let orderId = dic.value(forKey: "order_id") as? String
        let expiringAt = dic.value(forKey: "expiry") as? String
        
        
        print(dic)
        
        if (UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int) == nil{
            
            return
        }
        
        let _ = expiringAt?.timeDiffrenceBetweenUTCDates(date: /expiringAt)
        print("EVENT",eventType)
        switch eventType {
            
        case SocketConstants.dApproved:
            if let orderId = dic.value(forKey: "order_id") as? String{
                
                if !self.arrRequestIds.contains(/Int(/orderId)){
                    self.arrRequestIds.append(/Int(/orderId))
                    let dataDict:[String: String] = [SocketConstants.order_id: orderId]
                    SocketIOManager.shared.isAuthenticated = false
                    NotificationCenter.default.post(name: Notification.Name(NotificationNames.refreshOrders.rawValue), object: nil, userInfo: dataDict)
                }
            }
        case SocketConstants.DSchTimeout:
            
            Alerts.shared.show(alert: "alert.alert".localized, message: /message, type: .info)
            
        case  SocketConstants.dPending:
            
            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
            
            if !self.arrAboutToStartPending.contains(/Int(/orderId)){
                self.arrAboutToStartPending.append(/Int(/orderId))
                
                NotificationCenter.default.post(name: Notification.Name(NotificationNames.confirmScheduled.rawValue), object: nil, userInfo: dataDict)
            }
            
        case SocketConstants.About2Start:
            
            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
            
            if !self.arrAboutToStart.contains(/Int(/orderId)){
                self.arrAboutToStart.append(/Int(/orderId))
                NotificationCenter.default.post(name: Notification.Name(NotificationNames.openRequestModal.rawValue), object: nil, userInfo: dataDict)
            }
            
        case OrderEventTypes.requestCancelled.rawValue:
            
            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
            
            NotificationCenter.default.post(name: Notification.Name(NotificationNames.requestCancelled.rawValue), object: nil, userInfo: dataDict)
            
        case SocketConstants.customerCancel:
            
            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
            NotificationCenter.default.post(name: Notification.Name(NotificationNames.requestCancelled.rawValue), object: nil, userInfo: dataDict)
            
        case  SocketConstants.OrderEvent:
            
            if let orderId = dic.value(forKey: "order_id") as? String{
                
//                if !self.arrRequestIds.contains(/Int(/orderId)){
                    self.arrRequestIds.append(/Int(/orderId))
                    let dataDict:[String: String] = [SocketConstants.order_id: orderId]
                    SocketIOManager.shared.isAuthenticated = false
                    NotificationCenter.default.post(name: Notification.Name(NotificationNames.openRequestModal.rawValue), object: nil, userInfo: dataDict)
//                }
            }
        case SocketConstants.Chat:
            let dataDict:[String: String] = [SocketConstants.order_id: /orderId, "status":"Pending"]
            NotificationCenter.default.post(name: Notification.Name(SocketConstants.Chat), object: nil, userInfo: dataDict)
        default:
            
            break
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        
        SocketIOManager.shared.establishConnection()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        let center = UNUserNotificationCenter.current()
        
        var arrIds = [Int]()
        
        center.getDeliveredNotifications { (arr) in
            
            for item in arr{
                let userInfo = item.request.content.userInfo as NSDictionary
                let orderId = userInfo.value(forKey: "order_id") as? String
                let eventType = userInfo.value(forKey: "gcm.notification.push_type") as? String
                let expiringAt = userInfo.value(forKey: "expiry") as? String
                
                
                if !self.arrRequestIds.contains(/Int(/orderId)){
                    
                    let timeLeft = expiringAt?.timeDiffrenceBetweenUTCDates(date: /expiringAt)
                    if (/timeLeft > 0) && (eventType == SocketConstants.OrderEvent){
                        NotificationRequest.isNotified = true
                        arrIds.append(/Int(/orderId))
                    }
                }
            }
            
            NotificationRequest.orderIds = arrIds
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            
            if NotificationRequest.orderIds.count > 0{
                NotificationCenter.default.post(name: Notification.Name(NotificationNames.readPendingNotifications.rawValue), object: nil, userInfo: nil)
            }
        }
        
        // clear notification badge
        
        application.applicationIconBadgeNumber = 0
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        
        NotificationCenter.default.post(name: Notification.Name(NotificationNames.didBecomeActive.rawValue), object: nil, userInfo: nil)
        
        SocketIOManager.shared.establishConnection()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        
        
        return false
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        let userInfo = notification.request.content.userInfo
        handle(userInfo: userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    //MARK: - Logout
    func logout () {
        
        guard let window = self.window else { return}
        
        guard token.access_token != "" else {
            return
        }
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            //            guard  let vc = R.storyboard.loginSignUp.phoneSignUpViewController() else{ return}
            guard  let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else{ return}
            SocketIOManager.shared.closeConnection()
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.isHidden = true
            socketTimer?.invalidate()
            token.access_token = ""
            NotificationCenter.default.removeObserver(self)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            appDelegate.window?.rootViewController = nav
            UserDefaults.standard.removeObject(forKey: SingletonKeys.user.rawValue)
            
        }, completion: { completed in
            // maybe do something here
        })
    }
    
    func checkUpdate(){
        
        guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return
        }
        
        let force = DataResponse.driverData?.versionForce
        
        if force != currentVersion{
            
            if currentVersion.compare(/force, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending {
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertUpdateApp(), actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.window?.topMostWindowController()) {
                    
                    if let url = URL(string: APIConstants.appStoreUrl),
                        UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.open(url, options: [:]) { (opened) in
                            if(opened){
                                print("App Store Opened")
                            }
                        }
                    } else {
                        Alerts.shared.show(alert: "Alert", message: "App store not available.", type: .info)
                    }
                }
            }
        }
    }
}

var applicationStateString: String {
    if UIApplication.shared.applicationState == .active {
        return "active"
    } else if UIApplication.shared.applicationState == .background {
        return "background"
    }else {
        return "inactive"
    }
}

//extension AppDelegate  {
//    // iOS10+, called when presenting notification in foreground
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
//        //TODO: Handle foreground notification
//        completionHandler([.alert])
//    }
//
//    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) didReceiveResponse: \(userInfo)")
//        //TODO: Handle background notification
//        completionHandler()
//    }
//}

extension AppDelegate {
    var rootViewController: UINavigationController {
        return window!.rootViewController as! UINavigationController
    }
}

