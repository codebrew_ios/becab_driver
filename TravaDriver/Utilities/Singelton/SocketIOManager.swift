//
//  SocketIOManager.swift
//  GasItUp
//
//  Created by cbl24_Mac_mini on 11/04/18.
//  Copyright © 2018 cbl24_Mac_mini. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import SocketIO
import ObjectMapper
typealias  chatResponse = (_ response : Chat?) -> ()


struct FullTrack {
    var dt : String = ""
    var latitude = 0.0
    var longitude = 0.0
}
struct LastEmittedLocation {
    
    static var lastEmittedLatitude:Double  = 0.0
    static var lastEmittedLongitude:Double = 0.0
}

class SocketIOManager: NSObject {
    
    static let shared = SocketIOManager()
    
    var socket: SocketIOClient?
    var isAuthenticated = false
    var socketConnectedInitially = false
    var socketId = ""
    var fullTrackList:Array<FullTrack> = []
    
    //    override init() {
    //        super.init()
    //
    //        setupListeners()
    //    }
    
    //    //Server Methods
    //    func establishConnection() {
    //
    //        socket = SocketIOClient(socketURL: URL(string: APIConstants.socketBasePath)!, config : [.connectParams([ "access_token" : token.access_token ]), .forceNew(false), .log(false)])
    //        setupListeners()
    //
    //        if (self.socket?.status == .disconnected || self.socket?.status == .notConnected ) {
    //            print("======= socket was disconnected =======)")
    //
    //            if (token.access_token != "") {
    //                print("======= socket.connect() with access token =======")
    //
    //                if NetworkConnection.isConnected {
    //
    //                socket?.connect()
    //                }
    //
    //            }else {
    //                print("Token is nill")
    //            }
    //        }
    //        else {
    //            print("======= Socket already connected =======")
    //        }
    //    }
    //
    //    func closeConnection() {
    //
    //        print("=======***** SocketClientEvent.disconnect called ****=======")
    //        socket?.disconnect()
    //
    //        //        let state = UIApplication.shared.applicationState
    //        //        if state == .active {
    //        //            establishConnection()
    //        //        }
    //    }
    //
    //    func setupListeners() {
    //
    //        socket?.on(SocketClientEvent.disconnect.rawValue) { (array, emitter) in
    //
    //            print("======= SocketClientEvent.disconnect listener=======")
    //            self.isAuthenticated = false
    //            self.socketConnectedInitially = false
    ////            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
    ////                Alerts.shared.show(alert: "ALert", message: "disconnected", type: .error)
    ////            }
    //
    //            //            self.establishConnection()
    //        }
    //
    //        socket?.on(SocketClientEvent.error.rawValue) { (array, emitter) in
    //            print("======= SocketClientEvent.error =======")
    //
    //            self.isAuthenticated = false
    //            self.establishConnection()
    //        }
    //
    //        socket?.on(SocketClientEvent.connect.rawValue) { (array, emitter) in
    //            print("======= SocketClientEvent.connect listener =======")
    //
    //            if self.socket?.status == .connected && !self.isAuthenticated {
    //
    //                for item:SocketEventHandler in (self.socket?.handlers)!{
    //                    if item.event == "connect"{
    //                        self.socketId = "\(item.id)"
    //                        print("Socket on connect id - \(item.id)")
    //
    //                    }
    //                }
    //                self.isAuthenticated = true
    //                self.socketConnectedInitially = true
    //
    //                self.listenErrorEvent({(resp) in
    //                    print(resp)
    //                })
    //            }
    //        }
    //    }
    
    
    
    
    private var manager: SocketManager?
    
    override init() {
        super.init()
        self.createManager()
        
    }
    
    func createManager(){
        let access_token = token.access_token
        
        guard let URL = URL(string: APIConstants.socketBasePath) else {
            return
        }
        manager = SocketManager(socketURL: URL , config: [.log(false), .connectParams(["access_token" : /access_token])])
        socket = manager?.defaultSocket
        setupListeners()
    }
    
    //Server Methods
    
    func establishConnection() {
        
        if (self.socket?.status == .disconnected || self.socket?.status == .notConnected ) {
            socket?.connect()
        }
        else {
            print("======= Socket already connected =======")
        }
    }
    
    func closeConnection() {
        print("=======***** SocketClientEvent.disconnect called ****=======")
        socket?.disconnect()
    }
    
    func setupListeners() {
        socket?.on(SocketClientEvent.disconnect.rawValue) { [weak self] (array, emitter) in
            print("======= SocketClientEvent.disconnect listener=======")
            
            // connect when logged in 
            if token.access_token != ""{
                //            self?.establishConnection()
            }
        }
        
        socket?.on(SocketClientEvent.error.rawValue) {[weak self] (array, emitter) in
            print("======= SocketClientEvent.error =======")
            self?.establishConnection()
        }
        
        socket?.on(SocketClientEvent.connect.rawValue) {  (array, emitter) in
            if self.socket?.status == .connected {
                print("======= userauth after connected =======")
                
                for item:SocketEventHandler in (self.socket?.handlers)!{
                    if item.event == "connect"{
                        self.socketId = "\(item.id)"
                        print("Socket on connect id - \(item.id)")
                        
                    }
                }
                self.isAuthenticated = true
                self.socketConnectedInitially = true
                
                self.listenErrorEvent({(resp) in
                    print(resp)
                })
                
            }
        }
    }
    
    
    // Emit methods with acknowledgement
    func sendCommonEventData(lat:Double,lng:Double){
        
        if self.socket?.status != .connected {
            self.createManager()
            self.establishConnection()
            return
        }
        
        let bearing = Double(String(format: "%.3f", /LocationManager.shared.heading))
        let data = [
            Keys.type.rawValue : Keys.UpdateData.rawValue ,
            Keys.access_token.rawValue : "\(token.access_token)" ,
            Keys.latitude.rawValue  : lat ,
            Keys.longitude.rawValue : lng ,
            Keys.timezone.rawValue  : HelperNames.timeZone.get(),
            Keys.fcm_id.rawValue    :    "\(token.fcmToken)",
            Keys.bearing.rawValue   : bearing ?? 0.0
            
            ] as [String : Any]
        
        self.socket?.emitWithAck(SocketConstants.CommonEvent, data).timingOut(after: 1, callback: { (response) in
            
            LastEmittedLocation.lastEmittedLatitude = Locations.lat.getLoc()
            LastEmittedLocation.lastEmittedLongitude = Locations.longitutde.getLoc()
            
        })
    }
    
    func sendCommonEventDataWithRide(lat:Double,lng:Double) {
        
        if self.socket?.status != .connected {
            self.createManager()
            self.establishConnection()
            return
        }
        
        let bearing = Double(String(format: "%.3f", /LocationManager.shared.heading))
        LocationManagers.shared.start()
        LocationManagers.shared.success = { address in
            
            Utility.shared.getDistanceBetweenAllPoints { (distanceValue) in
                let distanceExactValue = Double(distanceValue)
                //            if distanceExactValue.contains(" m") {
                //                distanceExactValue = "\((Double(distanceValue.replacingOccurrences(of: "m", with: "").trim()) ?? 0)/1000)"
                //            }else if distanceExactValue.contains("km") {
                //                distanceExactValue = distanceValue.replacingOccurrences(of: "km", with: "").trim()
                //            }
                let obj = ["points" : PolylineForCustomer.polyline,
                           "distanceText": PolylineForCustomer.distanceText,
                           "distanceValue": PolylineForCustomer.distanceValue,
                           "timeText" : PolylineForCustomer.timeText,
                           "timeValue" : PolylineForCustomer.timeValue
                    ] as [String : Any]
                
                let data = [
                    Keys.type.rawValue : Keys.DCurrentOrders.rawValue ,
                    Keys.access_token.rawValue : "\(token.access_token)" ,
                    Keys.latitude.rawValue : address.lat ?? 0.0 ,
                    Keys.longitude.rawValue : address.long ?? 0.0 ,
                    Keys.bearing.rawValue : bearing ?? 0.0,
                    Keys.polyline.rawValue : obj,
                    Keys.order_distance.rawValue : distanceExactValue
                    ] as [String : Any]
                
                self.socket?.emitWithAck(SocketConstants.CommonEvent, data).timingOut(after: 1, callback: { (response) in
                    if let responseData = response as? [[String:Any]] {
                        let firstResponse = responseData[0]
                        let firstResult = firstResponse["result"] as? [String:Any]
                        let orderList = firstResult?["orders"] as? [[String:Any]]
                        if orderList?.count ?? 0 > 0 {
                            let firstOrder = orderList?[0]
                            let crequest = firstOrder?["CRequest"] as? [String:Any]
                            // let payment = firstOrder?["payment"] as? [String:Any]
                            let finalCharge = firstOrder?["final_charge"] as? Double
                            let bookingType = firstOrder?["booking_type"] as? String
                            let status = firstOrder?["order_status"] as? String
                            UserDefaults.standard.set(finalCharge, forKey: "finalCharge")
                            let dict:[String: String] = ["finalCharge" : "\(finalCharge?.roundTo(places: 1) ?? 0.0)" ,"bookingType" : bookingType ?? "", "status" : status ?? ""]
                            let full_track = crequest?["full_track"] as? String
                            // convert String to NSData
                            if let data: Data = full_track?.data(using: String.Encoding.utf8)! {
                                do {
                                    let obj = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0))
                                    // convert 'AnyObject' to Array<Business>
                                    SocketIOManager.shared.fullTrackList.removeAll()
                                    SocketIOManager.shared.fullTrackList = self.parseJson(anyObj: obj as Any)
                                }catch {
                                    
                                }
                            }
                        }
                        //===============
                    }
                    LastEmittedLocation.lastEmittedLatitude = Locations.lat.getLoc()
                    LastEmittedLocation.lastEmittedLongitude = Locations.longitutde.getLoc()
                    
                })
            }
        }
    }
    
    func parseJson(anyObj:Any) -> Array<FullTrack>{
        
        var list:Array<FullTrack> = []
        var newList:Array<FullTrack> = []
        list.removeAll()
        newList.removeAll()
        if  anyObj is Array<AnyObject> {
            
            var b:FullTrack = FullTrack()
            if let data = anyObj as? [[String:Any]]{
                for json in data {
                    b.dt = json["Dt"] as? String ?? ""
                    b.latitude  = json["latitude"] as? Double ?? 0
                    b.longitude = json["longitude"] as? Double ?? 0
                    
                    list.append(b)
                }// for
            }
        } // if
        
        //MARK:-To remove more points from list.
        if list.count > 25 {
            var pickupCount = Int(list.count/25)
            if pickupCount == 0 {
                pickupCount = 1
            }
            for (index,_) in list.enumerated() {
                if index%pickupCount == 0 && newList.count < 25 {
                    newList.append(list[index])
                }
            }
            return newList
        }
        return list
        
    }//func
    
    // On Methods to listen
    func listenOrderEvent(_ completionHandler: @escaping ([String:Any]) -> Void){
        SocketIOManager.shared.socket?.on(SocketConstants.OrderEvent, callback: { (dataArray, socketAck) in
            
            let item = JSON(dataArray[0]).dictionaryObject
            print(item)
            completionHandler(item!)
        })
        
    }
    
    func listenErrorEvent(_ completionHandler: @escaping ([Any]) -> Void){
        
        socket?.on("Error", callback: { (dataArray, socketAck) in
            self.closeConnection()
            UserDefaults.standard.removeObject(forKey: SingletonKeys.user.rawValue)
        })
    }
    
    func sendChatsMessage(userDetailId: String, text: String,to : Int,sendAt : String,originalIMage : String?,thumbnailImage : String?,chat_type : String,orderId:String) {
              let dte = Date()
              let format = DateFormatter()
              format.timeZone = TimeZone.current
              format.dateFormat = "yyyy-MM-dd HH:mm:ss"
              let dteGet = format.string(from: dte) as AnyObject
              let dict: [String:AnyObject] = ["detail":["user_detail_id": userDetailId, "text" : text,"to": to,"sent_at" : dteGet,"original" : /originalIMage,"thumbnail": /thumbnailImage,"chat_type" : chat_type,"order_id":orderId] as AnyObject]
              
              print(dict)
              socket?.emit("msg", dict)
              
              //        let data = "{\"detail\":\"{\"text\":\"\(text)\",\"to\":\"\(to)\",\"access_token\":\"\(/UDSingleton.shared.userData?.userDetails?.accessToken)\",\"sent_at\":\"\(sendAt)\"}}"
              
          }
       
       func getChatsMessage(completionHandler: @escaping  chatResponse) {
           
           socket?.on("receive") { (dataArray, socketAck) -> Void in
               print(dataArray)
               NotificationCenter.default.post(name: Notification.Name(rawValue:"chat"), object: true)
               //var messageDictionary = [String: AnyObject]()
               //  let obj = Mapper<ApiSucessData<Chat>>().map(JSONObject: dataArray)
               let obj = Chat(cid: (dataArray[0] as! Dictionary<String,AnyObject>)["c_id"]! as? Int, conversationId: 0, send_to: (dataArray[0] as! Dictionary<String,AnyObject>)["send_to"]! as? Int, send_by: (dataArray[0] as! Dictionary<String,AnyObject>)["send_by"]! as? Int, text: (dataArray[0] as! Dictionary<String,AnyObject>)["text"]! as? String, sent_at: (dataArray[0] as! Dictionary<String,AnyObject>)["sent_at"]! as? String,original: (dataArray[0] as! Dictionary<String,AnyObject>)["original"]! as? String,thumbnail: (dataArray[0] as! Dictionary<String,AnyObject>)["thumbnail"]! as? String,chat_type: (dataArray[0] as! Dictionary<String,AnyObject>)["chat_type"]! as? String)
               
               //            messageDictionary["text"] = (dataArray[0] as! Dictionary<String,AnyObject>)["text"]!
               //            messageDictionary["send_to"] = (dataArray[0] as! Dictionary<String,AnyObject>)["send_to"]!
               //            messageDictionary["send_at"] = (dataArray[0] as! Dictionary<String,AnyObject>)["send_at"]!
               
               completionHandler(obj)
           }
       }
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
