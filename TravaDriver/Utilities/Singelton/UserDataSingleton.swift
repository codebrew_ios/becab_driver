//
//  UserDataSingleton.swift
//  Untap
//
//  Created by OSX on 23/02/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import Foundation
import ObjectMapper

enum SingletonKeys : String {
  case user = "BuraqDriverAccount"
}

//MARK: - User Singleton
class UserSingleton:NSObject{
    
    static let shared = UserSingleton()
    
    var loggedInUser : DriverModel? {
        
        get{
            
            guard let data = UserDefaults.standard.value(forKey: SingletonKeys.user.rawValue) else{
                
                let mappedModel = Mapper<DriverModel>().map(JSON: [:] )
                return mappedModel
            }
            
            let mappedModel = Mapper<DriverModel>().map(JSON: data as! [String : Any])
            return mappedModel
            
        }set{
            
            if let value = newValue {
                UserDefaults.standard.set(value.toJSON(), forKey: SingletonKeys.user.rawValue)
                
            } else {
                UserDefaults.standard.removeObject(forKey: SingletonKeys.user.rawValue)
            }
        }
    }
}
