
import UIKit
import CoreLocation
//import EZSwiftExtensions

protocol LocationManagerDelegate {
    
    func authorizationStatusChanged (status: CLAuthorizationStatus)
    func updateDriverMarker(coordinates:CLLocationCoordinate2D)
    func updateDriverMakerLessAccurate(coordinates: CLLocationCoordinate2D)
}

class LocationManager: NSObject,CLLocationManagerDelegate {
    
    var locationManager : CLLocationManager?
    var currentLoc : CLLocation?
    var snappedLoc :CLLocation?
    //    var heading : CLHeading?
    
    var heading : CLLocationDirection?
    
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    
    var delegate : LocationManagerDelegate?
    var currentCity : String?
    
    
    override init() {
        super.init()
        
        locationInitializer()
        updateLocation()
    }
    
    static let shared = LocationManager()
    
    func updateUserLocation() {
        locationInitializer()
        updateLocation()
    }
    
    func updateLocation() {
        //        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    func locationInitializer() {
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        if CLLocationManager.headingAvailable(){
            //            locationManager?.startUpdatingHeading()
        }
        if #available(iOS 11.0, *) {
            locationManager?.showsBackgroundLocationIndicator = false
        } else {
            // Fallback on earlier versions
            
        }
        
        locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        locationManager?.distanceFilter = 40
        
        //        locationManager?.activityType = .automotiveNavigation
        locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.pausesLocationUpdatesAutomatically = false
        
        
        
        locationManager?.allowsBackgroundLocationUpdates = true
        
        locationManager?.requestAlwaysAuthorization()
        locationManager?.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        delegate?.authorizationStatusChanged(status: status)
        
        switch status {
            
        case .authorizedWhenInUse,.authorizedAlways:
            locationManager?.startUpdatingLocation()
            
        case .notDetermined:
            locationManager?.requestAlwaysAuthorization()
            locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager?.distanceFilter = 40
            //            locationManager?.activityType = .automotiveNavigation
            
        case .restricted,.denied:
            settingsAlert()
        }
    }
    
    func settingsAlert() {
        
        //        Alerts.shared.showAlertView(alert: Alert.alert.getLocalised(), message: "alert.TurnOnLocationPermission".localized, buttonTitles: ["buttonTitle.settings".localized ], viewController: ez.topMostVC!)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // Check to filter less accurate location coordinate
        if /locations.last?.horizontalAccuracy > 10.0  && currentLoc != nil {
            
            // Check to update marker if accuracy is less than 14.0 otherwise ignore coordinate
            if /locations.last?.horizontalAccuracy < 12.0{
                if let coordinates = locations.last?.coordinate{
                      heading = locations.last?.course
                    delegate?.updateDriverMakerLessAccurate(coordinates: coordinates)
                }
            }
        }
            // update driver marker and update path using road api and direction api.
            
        else{
            currentLoc = locations.last
            
            if let lat = currentLoc?.coordinate.latitude ,let lng = currentLoc?.coordinate.longitude {
                
                latitude = lat
                longitude = lng
                
                    heading = currentLoc?.course
                
                delegate?.updateDriverMarker(coordinates: currentLoc!.coordinate)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        //        print("Heading -\(newHeading.trueHeading)")
        //        print("Course - \(newHeading.magneticHeading)")
        //        heading = newHeading
        
        
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
        //                self.locationManager?.stopUpdatingLocation()
        //            }
    }
    
    func stopUpdatingLocation () {
        
        locationManager?.stopUpdatingLocation()
        locationManager?.delegate = nil
    }
}



class LocationManagers : NSObject, CLLocationManagerDelegate {

    static let shared = LocationManagers()

    let locationManager = CLLocationManager()

    /// This is the clouser that will return the current Address through google login in
    var success: (_ addrees: Address) -> Void = { _ in }

    /// This clouser will return the error
    var failure:  (_ error: String) -> () = { _ in }
    
    //MARK: - Permission Checks
    private var isEnabled: Bool {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse: return true
        default: return false
        }

    }
    private var notDetermined: Bool {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined: return true
        default: return false
        }
    }

    func start() -> Void {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if isEnabled {
            locationManager.startUpdatingLocation()
        } else if notDetermined {
            request()
        } else {
            failure("error")
        }
    }
    func request() -> Void {
        locationManager.requestWhenInUseAuthorization()
    }

    //MARK:Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation: CLLocation = manager.location {
            getAddressFromLocation(location: currentLocation)
        } else {
            self.failure("error")
        }
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil

    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        failure("error")
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            request()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
            break
        case .restricted: failure("error")
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            failure("error")
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        }
    }

    private func getAddressFromLocation(location: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if error != nil {
                self.success(Address(location: location))
                return
            }
            if let placemark = placemarks?.first {
                self.success(Address(placemark: placemark))
            } else {
                self.success(Address(location: location))
            }




        })

    }

}
