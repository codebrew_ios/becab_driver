//
//  SpeechSynthesizer.swift
//  Buraq24Driver
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import AVFoundation

class SpeechSynthesizer: AVSpeechSynthesizer, AVSpeechSynthesizerDelegate {
    
    static let shared = SpeechSynthesizer()
    var speechUtterance:AVSpeechUtterance?
    let audioSession = AVAudioSession.sharedInstance()
    var player: AVAudioPlayer?
    
    override init() {
        
        super.init()
        initialSetup()
    }
    
    func initialSetup(){
        self.delegate = self
        
                print(AVSpeechSynthesisVoice.speechVoices())
    }
    
    func speakTextOverVoice(text:String){
        
        speechUtterance = AVSpeechUtterance(string: text)
        speechUtterance?.pitchMultiplier = 1.5
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic {
            speechUtterance?.voice =   AVSpeechSynthesisVoice(language: "ar-SA")
        }
        else if BundleLocalization.sharedInstance().language == Languages.Urdu{
            speechUtterance?.voice = AVSpeechSynthesisVoice(language: "en-US")

        }
        else if BundleLocalization.sharedInstance().language == Languages.Chinese{
             speechUtterance?.voice = AVSpeechSynthesisVoice(language: "zh-CN")
            
        }
        else {
             speechUtterance?.voice = AVSpeechSynthesisVoice(language: "en-US")
        }
        
        try? audioSession.setCategory(AVAudioSessionCategoryPlayback, with: .duckOthers)

        
        if speechUtterance != nil{
            self.speak(speechUtterance!)
        }
    }
    
    func stopSpeaking(){
        if self.isSpeaking{
            self.stopSpeaking(at: .immediate)
        }
    }
    
    func stopToneAudio(){
        if /player?.isPlaying{
            player?.stop()
        }
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        guard !synthesizer.isSpeaking else { return }
        self.playSound()
//        try? audioSession.setActive(false)
    }
    
    func playSound() {
        
        guard let url = Bundle.main.url(forResource: "custom_tone", withExtension: "wav") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            player.numberOfLoops = -1
            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
}
