//
//  TimeLeftView.swift
//  Nizcare
//
//  Created by cbl16 on 07/02/19.
//  Copyright © 2019 nizcare. All rights reserved.
//

import UIKit

class TimeLeftView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var timeLeftView: UIView!
    @IBOutlet weak var lblTimeLeft: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
