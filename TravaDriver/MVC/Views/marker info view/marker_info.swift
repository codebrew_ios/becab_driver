//
//  marker_info.swift
//  Buraq24Driver
//
//  Created by Apple on 01/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable


class marker_info: UIView {
    
    @IBOutlet weak var lblAddress: UILabel!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "marker_info", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
}
