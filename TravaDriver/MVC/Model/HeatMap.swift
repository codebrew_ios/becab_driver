//
//  HeatMap.swift
//  TravaDriver
//
//  Created by Apple on 16/12/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import Foundation
import ObjectMapper

class HeatMapData: NSObject , Mappable{
    
    var result:[HeatMapDataEntry]?
    var msg:String?
    var statusCode : Int?
    var success : Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        result <- map["result"]
        msg <- map ["msg"]
        statusCode <- map["statusCode"]
        success <- map["success"]
        
    }
}


class HeatMapDataEntry: NSObject , Mappable {
    
    var count:Int?
    var distance:Double?
    var pickup_latitude:Double?
    var pickup_longitude : Double?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        count <- map ["count"]
        distance <- map["distance"]
        pickup_latitude <- map["pickup_latitude"]
        pickup_longitude <- map["pickup_longitude"]
    }
}
