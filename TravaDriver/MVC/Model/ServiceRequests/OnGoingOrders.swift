//
//  OnGoingOrders.swift
//  Buraq24Driver
//
//  Created by Apple on 22/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class OnGoingOrders: NSObject, Mappable{
    
    var orders:[OrdersList]?
    var message : String?
    var statusCode : Int?
    var pendingOrders:[OrdersList]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        orders <- map ["result"]
        message <- map["msg"]
        statusCode <- map["statusCode"]
        pendingOrders <- map["pendingOrders"]
    }
}

class OrdersList: NSObject, Mappable {
    
    var admin_charge:String?
    var bottle_charge:String?
    var bottle_first_time_value:String?
    var bottle_returned:String?
    var bottle_returned_value:String?
    var brand:BrandsinOrder?
    var cancelled_by:String?
    var cancel_reason:String?
    var  category_brand_id:Int?
    var category_brand_product_id:Int?
    var  category_id:Int?
    var  category_type:String?
    var  continouos_startdt:String?
    var  continuous:String?
    var  continuous_enddt:String?
    var  continuous_order_id:Int?
    var continuous_time:String?
    var created_at:String?
    var created_by:String?
    var customer_organisation_id:Int?
    var customer_user_detail_id:Int?
    var  customer_user_id:Int?
    var  customer_user_type_id:Int?
    var  driver_organisation_id:Int?
    var  driver_user_detail_id:Int?
    var driver_user_id:Int?
    var  driver_user_type_id:Int?
    var  dropoff_address:String?
    var  dropoff_latitude:Double?
    var  dropoff_longitude:Double?
    var  final_charge:String?
    var  future:String?
    var initial_charge:String?
    var my_turn:String?                         // ride currently started and need to drop
    var order_distance:String?
    var order_id:Int?
    var  order_status:String?
    var   order_timings:String?
    var  order_token:String?
    var   order_type:String?
    var   order_weight:String?
    var   organisation_coupon_user_id:Int?   // for etoken rides
    var  payment_status:String?
    var   payment_type:String?
    var   pickup_address:String?
    var   pickup_latitude:Double?
    var  pickup_longitude:Double?
    var   product_actual_value:String?
    var  product_alpha_price:String?
    var  product_price_per_distance:String?
    var  product_price_per_quantity:Int?
    var  product_price_per_weight:String?
    var  product_quantity:Int?
    var  refund_id:String?
    var  refund_status:String?
    var  transaction_id:String?
    var  updated_at:String?
    var  user:UserInOrder?
    var  user_card_id:Int?
    var payment : PaymentOrder?
    var roadStops: [RideStops]?
    var distanceGoogleApi : String?
    var etaTimeGoogleApi : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        roadStops <- map["ride_stops"]
        admin_charge <- map["admin_charge"]
        bottle_charge <- map["bottle_charge"]
        bottle_first_time_value <- map["bottle_first_time_value"]
        bottle_returned <- map["bottle_returned"]
        bottle_returned_value <- map["bottle_returned_value"]
        brand <- map["brand"]
        cancelled_by <- map["cancelled_by"]
        cancel_reason <- map["cancel_reason"]
        category_brand_id <- map["category_brand_id"]
        category_brand_product_id <- map["category_brand_product_id"]
        category_id <- map["category_id"]
        category_type <- map["category_type"]
        continouos_startdt <- map["continouos_startdt"]
        continuous_enddt <- map["continuous_enddt"]
        continuous_order_id <- map["continuous_order_id"]
        continuous_time <- map["continuous_time"]
        created_at <- map["created_at"]
        created_by <- map["created_by"]
        customer_organisation_id <- map["customer_organisation_id"]
        customer_user_detail_id <- map["customer_user_detail_id"]
        customer_user_id <- map["customer_user_id"]
        customer_user_type_id <- map["customer_user_type_id"]
        driver_organisation_id <- map["driver_organisation_id"]
        driver_user_detail_id <- map["driver_user_detail_id"]
        driver_user_id <- map["driver_user_id"]
        driver_user_type_id <- map["driver_user_type_id"]
        dropoff_address <- map["dropoff_address"]
        dropoff_latitude <- map["dropoff_latitude"]
        dropoff_longitude <- map["dropoff_longitude"]
        final_charge <- map["final_charge"]
        future <- map["future"]
        initial_charge <- map["initial_charge"]
        order_distance <- map["order_distance"]
        category_id <- map["category_id"]
        order_id <- map["order_id"]
        order_status <- map["order_status"]
        order_timings <- map["order_timings"]
        order_token <- map["order_token"]
        order_type <- map["order_type"]
        order_weight <- map["order_weight"]
        organisation_coupon_user_id <- map["organisation_coupon_user_id"]
        payment_status <- map["payment_status"]
        payment_type <- map["payment_type"]
        pickup_address <- map["pickup_address"]
        pickup_latitude <- map["pickup_latitude"]
        pickup_longitude <- map["pickup_longitude"]
        product_actual_value <- map["product_actual_value"]
        product_alpha_price <- map["product_alpha_price"]
        product_price_per_distance <- map["product_price_per_distance"]
        product_price_per_quantity <- map["product_price_per_quantity"]
        product_price_per_weight <- map["product_price_per_weight"]
        product_quantity <- map["product_quantity"]
        refund_id <- map["refund_id"]
        refund_status <- map["refund_status"]
        transaction_id <- map["transaction_id"]
        updated_at <- map["updated_at"]
        user <- map["user"]
        user_card_id <- map["user_card_id"]
        payment <- map["payment"]
        my_turn <- map["my_turn"]
        
    }
}

class PaymentOrder :NSObject, Mappable{
    var admin_charge : String?
    var bank_charge : String?
    var bottle_charge : String?
    var bottle_returned_value : Int?
    
    var buraq_percentage : Any?
    var created_at : String?
    
    var customer_organisation_id : Int?
    var customer_user_detail_id : Int?
    var customer_user_id : Int?
    var customer_user_type_id : Int?
    var final_charge:String?
    
    var initial_charge : String?
    var order_distance : String?
    var order_id : Int?
    var payment_id : Int?
    var payment_status:String?
    
    var payment_type : String?
    var product_actual_value : Int?
    var product_alpha_charge : String?
    var product_per_distance_charge : String?
    var product_per_quantity_charge:String?
    var product_per_sq_mt_charge:String?
    var product_sq_mt :String?
    var product_per_hr_charge :String?
    
    
    var product_per_weight_charge : String?
    var product_quantity : Int?
    var product_weight : String?
    
    var refund_id:String?
    var refund_status:String?
    
    var seller_organisation_id : Int?
    var seller_user_detail_id:Int?
    var seller_user_id : Int?
    var seller_user_type_id:Int?
    
    var transaction_id : String?
    var updated_at:String?
    var user_card_id : Int?
    var order_time : Int?
    var package_distance :Double?
    var previous_driver_charges : Double?
    
    var price_per_km : String?
    var price_per_min : String?
    var extra_distance : String?
    var extra_time : String?
    var distance_price_fixed : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        order_time <- map["order_time"]
        admin_charge <- map["admin_charge"]
        bank_charge <- map["bank_charge"]
        bottle_charge <- map["bottle_charge"]
        bottle_returned_value <- map["bottle_returned_value"]
        buraq_percentage <- map["buraq_percentage"]
        created_at <- map["created_at"]
        customer_organisation_id <- map["customer_organisation_id"]
        customer_user_detail_id <- map["customer_user_detail_id"]
        customer_user_id <- map["customer_user_id"]
        customer_user_type_id <- map["customer_user_type_id"]
        final_charge <- map["final_charge"]
        initial_charge <- map["initial_charge"]
        order_distance <- map["order_distance"]
        order_id <- map["order_id"]
        payment_id <- map["payment_id"]
        payment_status <- map["payment_status"]
        payment_type <- map["payment_type"]
        product_actual_value <- map["product_actual_value"]
        product_alpha_charge <- map["product_alpha_charge"]
        product_per_distance_charge <- map["product_per_distance_charge"]
        product_per_quantity_charge <- map["product_per_quantity_charge"]
        product_per_weight_charge <- map["product_per_weight_charge"]
        product_per_sq_mt_charge <- map["product_per_sq_mt_charge"]
        product_per_hr_charge <- map["product_per_hr_charge"]
        product_sq_mt <- map["product_sq_mt"]
        product_quantity <- map["product_quantity"]
        product_weight <- map["product_weight"]
        refund_id <- map["refund_id"]
        refund_status <- map["refund_status"]
        seller_organisation_id <- map["seller_organisation_id"]
        seller_user_detail_id <- map["seller_user_detail_id"]
        seller_user_id <- map["seller_user_id"]
        seller_user_type_id <- map["seller_user_type_id"]
        transaction_id <- map["transaction_id"]
        updated_at <- map["updated_at"]
        user_card_id <- map["user_card_id"]
        previous_driver_charges <- map["previous_driver_charges"]
        price_per_km <- map["price_per_km"]
        price_per_min <- map["price_per_min"]
        extra_distance <- map["extra_distance"]
        extra_time <- map["extra_time"]
        distance_price_fixed <- map["distance_price_fixed"]
    }
    
}


class BrandsinOrder : Mappable {
    
    var actual_value : Int?
    var alpha_price : Int?
    var blocked : String?
    var category_brand_id : Int?
    var category_id : Int?
    var sort_order : Int?
    var image : String?
    var image_url : String?
    var name : String?
    var brand_name :String?
    var description : String?
    var multiple: String?
    var price_per_distance: String?
    var price_per_quantity: String?
    var price_per_weight: String?
    var si_unit: String?
    var updated_at: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        actual_value <- map["actual_value"]
        alpha_price <- map["alpha_price"]
        blocked <- map["blocked"]
        category_brand_id <- map["category_brand_id"]
        category_id <- map["category_id"]
        sort_order <- map["sort_order"]
        image <- map["image"]
        name <- map["name"]
        brand_name <- map["brand_name"]
        description <- map["description"]
        multiple <- map["multiple"]
        price_per_distance <- map["price_per_distance"]
        price_per_quantity <- map["price_per_quantity"]
        price_per_weight <- map["price_per_weight"]
        si_unit <- map["si_unit"]
        updated_at <- map["updated_at"]
    }
}


class UserInOrder : Mappable {
    
    var actual_value : Int?
    var alpha_price : Int?
    var blocked : String?
    var category_brand_id : Int?
    var category_id : Int?
    var sort_order : Int?
    var profile_pic : String?
    var profile_pic_url : String?
    var name : String?
    var phone_number :Int?
    var phone_code :String?
    var description : String?
    var multiple: String?
    var price_per_distance: String?
    var price_per_quantity: String?
    var price_per_weight: String?
    
    var ratings_count: Int?
    var ratings_avg: String?
    
    var si_unit: String?
    var updated_at: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        actual_value <- map["actual_value"]
        alpha_price <- map["alpha_price"]
        blocked <- map["blocked"]
        category_brand_id <- map["category_brand_id"]
        category_id <- map["category_id"]
        sort_order <- map["sort_order"]
        profile_pic_url <- map["profile_pic_url"]
        profile_pic <- map["profile_pic"]
        name <- map["name"]
        description <- map["description"]
        multiple <- map["multiple"]
        price_per_distance <- map["price_per_distance"]
        price_per_quantity <- map["price_per_quantity"]
        price_per_weight <- map["price_per_weight"]
        si_unit <- map["si_unit"]
        updated_at <- map["updated_at"]
        phone_number <- map ["phone_number"]
        phone_code <- map["phone_code"]
        ratings_count <- map["rating_count"]
        ratings_avg <- map["rating_avg"]
    }
}


class RideStops : Mappable {
    
    var added_with_ride : String?
    var address : String?
    var created_at : String?
    var customer_user_id : Int?
    var driver_user_id : Int?
    var deleted : Int?
    var latitude : Double?
    var longitude : Double?
    var order_id : Int?
    var priority : Int?
    var ride_stop_id : Int?
    var updated_at :String?
    var stop_status: String?
    var waiting_time:Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        stop_status <- map["stop_status"]
        added_with_ride <- map["added_with_ride"]
        address <- map["address"]
        created_at <- map["created_at"]
        customer_user_id <- map["customer_user_id"]
        driver_user_id <- map["driver_user_id"]
        deleted <- map["deleted"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        order_id <- map["order_id"]
        priority <- map["priority"]
        updated_at <- map["updated_at"]
        ride_stop_id <- map["ride_stop_id"]
        waiting_time <- map["waiting_time"]
        
        
    }
}
