//
//  CompletedOrderModel.swift
//  Buraq24Driver
//
//  Created by Apple on 28/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper



class CompletedOrderModel: NSObject, Mappable {
    
    var message : String?
    var statusCode : Int?
    var result :CompletedOrderDetail?
    var success:Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        message <- map["msg"]
        statusCode <- map["statusCode"]
        result <- map["result"]
        success <- map["success"]
    }
    
}


class CompletedOrderDetail: NSObject,Mappable{
    
    var admin_charge:String?
    var bottle_charge:String?
    var bottle_first_time_value:String?
    var bottle_returned:String?
    var bottle_returned_value:String?
    var cancelled_by:String?
    var cancel_reason:String?
    var  category_brand_id:Int?
    var category_brand_product_id:Int?
    var  category_id:Int?
    var  category_type:String?
    var  continouos_startdt:String?
    var  continuous:String?
    var  continuous_enddt:String?
    var  continuous_order_id:Int?
    var continuous_time:String?
    var  coupon_user_id:Int?
    var created_at:String?
    var created_by:String?
    
    var cRequest:CRequest?
    
    var Customer:Customer?
    
    var CustomerDetails: CustomerDetails?
    
    var BrandinCompleted :BrandinCompleted?
    
    var Payment:PaymentCompleted?
    
    var customer_organisation_id:Int?
    var customer_user_detail_id:Int?
    var  customer_user_id:Int?
    var  customer_user_type_id:Int?
    var  driver_organisation_id:Int?
    var  driver_user_detail_id:Int?
    var driver_user_id:Int?
    var  driver_user_type_id:Int?
    var  dropoff_address:String?
    var  dropoff_latitude:Double?
    var  dropoff_longitude:Double?
    var  final_charge:String?
    var  future:String?
    var initial_charge:String?
    var order_distance:String?
    var order_id:Int?
    var  order_status:String?
    var   order_timings:String?
    var  order_token:String?
    var   order_type:String?
    var   order_weight:String?
    
    var   organisation_coupon_user_id:Int?
    var  payment_status:String?
    var   payment_type:String?
    var   pickup_address:String?
    var   pickup_latitude:Double?
    var  pickup_longitude:Double?
    var   product_actual_value:String?
    var  product_alpha_price:String?
    var  product_price_per_distance:String?
    var  product_price_per_quantity:Int?
    var  product_price_per_weight:String?
    var  product_quantity:Int?
    var  refund_id:String?
    var  refund_status:String?
    var  transaction_id:String?
    var  updated_at:String?
    var user_card_id:Int?
    var booking_type:String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        
        admin_charge <- map["admin_charge"]
        bottle_charge <- map["bottle_charge"]
        bottle_first_time_value <- map["bottle_first_time_value"]
        bottle_returned <- map["bottle_returned"]
        bottle_returned_value <- map["bottle_returned_value"]
        cancelled_by <- map["cancelled_by"]
        cancel_reason <- map["cancel_reason"]
        category_brand_id <- map["category_brand_id"]
        category_brand_product_id <- map["category_brand_product_id"]
        category_id <- map["category_id"]
        category_type <- map["category_type"]
        continouos_startdt <- map["continouos_startdt"]
        continuous_enddt <- map["continuous_enddt"]
        continuous_order_id <- map["continuous_order_id"]
        continuous_time <- map["continuous_time"]
        created_at <- map["created_at"]
        created_by <- map["created_by"]
        customer_organisation_id <- map["customer_organisation_id"]
        customer_user_detail_id <- map["customer_user_detail_id"]
        customer_user_id <- map["customer_user_id"]
        customer_user_type_id <- map["customer_user_type_id"]
        driver_organisation_id <- map["driver_organisation_id"]
        driver_user_detail_id <- map["driver_user_detail_id"]
        driver_user_id <- map["driver_user_id"]
        driver_user_type_id <- map["driver_user_type_id"]
        dropoff_address <- map["dropoff_address"]
        dropoff_latitude <- map["dropoff_latitude"]
        dropoff_longitude <- map["dropoff_longitude"]
        final_charge <- map["final_charge"]
        future <- map["future"]
        initial_charge <- map["initial_charge"]
        order_distance <- map["order_distance"]
        category_id <- map["category_id"]
        order_id <- map["order_id"]
        order_status <- map["order_status"]
        order_timings <- map["order_timings"]
        order_token <- map["order_token"]
        order_type <- map["order_type"]
        order_weight <- map["order_weight"]
        organisation_coupon_user_id <- map["organisation_coupon_user_id"]
        payment_status <- map["payment_status"]
        payment_type <- map["payment_type"]
        pickup_address <- map["pickup_address"]
        pickup_latitude <- map["pickup_latitude"]
        pickup_longitude <- map["pickup_longitude"]
        product_actual_value <- map["product_actual_value"]
        product_alpha_price <- map["product_alpha_price"]
        product_price_per_distance <- map["product_price_per_distance"]
        product_price_per_quantity <- map["product_price_per_quantity"]
        product_price_per_weight <- map["product_price_per_weight"]
        product_quantity <- map["product_quantity"]
        refund_id <- map["refund_id"]
        refund_status <- map["refund_status"]
        transaction_id <- map["transaction_id"]
        updated_at <- map["updated_at"]
        user_card_id <- map["user_card_id"]
        cRequest <- map["CRequest"]
        Customer <- map["Customer"]
        CustomerDetails <- map["CustomerDetails"]
        BrandinCompleted <- map["brand"]
        Payment <- map["payment"]
        booking_type <- map["booking_type"]
    }
}


class CRequest: NSObject, Mappable {
    
    var   accepted_at:String?
    var  created_at:String?
    var   driver_confirmed_latitude:String?
    var   driver_confirmed_longitude:String?
    var   driver_current_latitude:Double?
    var  driver_current_longitude:Double?
    var   driver_organisation_id:Int?
    var  driver_request_latitude:Double?
    var  driver_request_longitude:Double?
    var  driver_started_latitude:Double?
    var  driver_started_longitude:Double?
    var  driver_user_detail_id:Int?
    var driver_user_id:Int?
    var full_track:String?
    var order_id:Int?
    var order_request_id: Int?
    var order_request_status:String?
    var rotation:String?
    var updated_at: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        accepted_at <- map["accepted_at"]
        created_at <- map["created_at"]
        driver_confirmed_latitude <- map["driver_confirmed_latitude"]
        driver_confirmed_longitude <- map["driver_confirmed_longitude"]
        driver_current_longitude <- map["driver_current_latitude"]
        driver_current_latitude <- map["cancelled_by"]
        driver_organisation_id <- map["driver_organisation_id"]
        driver_request_latitude <- map["driver_request_latitude"]
        driver_request_longitude <- map["driver_request_longitude"]
        driver_started_latitude <- map["driver_started_latitude"]
        driver_started_longitude <- map["driver_started_longitude"]
        driver_user_detail_id <- map["driver_user_detail_id"]
        driver_user_id <- map["driver_user_id"]
        full_track <- map["full_track"]
        order_id <- map["order_id"]
        order_request_id <- map["order_request_id"]
        order_request_status <- map["order_request_status"]
        rotation <- map["rotation"]
        
    }
    
}

class Customer: NSObject, Mappable {
    
    var address : String?
    var address_latitude : Double?
    var address_longitude : Double?
    var blocked : String?
    var created_at : String?
    var created_by : String?
    var email : String?
    var name : String?
    var organisation_id :Int?
    var phone_code :Int?
    var phone_number : Int?
    var stripe_connect_id: String?
    var stripe_connect_token: String?
    var stripe_customer_id: String?
    var updated_at: String?
    var user_id: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        address <- map["address"]
        address_latitude <- map["address_latitude"]
        address_longitude <- map["blocked"]
        blocked <- map["blocked"]
        created_at <- map["created_at"]
        created_by <- map["created_by"]
        email <- map["email"]
        name <- map["name"]
        organisation_id <- map["organisation_id"]
        phone_code <- map["phone_code"]
        phone_number <- map ["phone_number"]
        stripe_connect_id <- map["stripe_connect_id"]
        stripe_connect_token <- map["stripe_connect_token"]
        stripe_customer_id <- map["stripe_customer_id"]
        updated_at <- map["updated_at"]
        user_id <- map["user_id"]
    }
}


class BrandinCompleted: NSObject, Mappable{
    
    var actual_value : Int?
    var alpha_price : Int?
    var blocked : String?
    var brand_name : String?
    var category_brand_id : Int?
    var category_brand_product_id: Int?
    var category_name :String?
    var category_sub_type:String?
    var  created_at:String?
    var desc: String?
    var multiple: String?
    var name : String?
    var price_per_distance: String?
    var price_per_quantity: String?
    var price_per_weight: String?
    var si_unit: String?
    var sort_order : Int?
    var updated_at: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        actual_value <- map["actual_value"]
        alpha_price <- map["alpha_price"]
        blocked <- map["blocked"]
        brand_name <- map["brand_name"]
        category_brand_id <- map["category_brand_id"]
        sort_order <- map["sort_order"]
        name <- map["name"]
        desc <- map["description"]
        multiple <- map["multiple"]
        price_per_distance <- map["price_per_distance"]
        price_per_quantity <- map["price_per_quantity"]
        price_per_weight <- map["price_per_weight"]
        si_unit <- map["si_unit"]
        updated_at <- map["updated_at"]
    }
    
}



class CustomerDetails: NSObject, Mappable {
    
    var accessToken : String?
    var blocked : String?
    var category_brand_id : Int?
    var category_id : Int?
    var created_at : String?
    var created_by : String?
    var fcm_id: String?
    var forgot_token: String?
    var forgot_token_validity: String?
    var language_id : Int?
    var latitude : Double?
    var longitude : Double?
    var maximum_rides:Int?
    var mulkiya_front : String?
    var mulkiya_back : String?
    var mulkiya_number : String?
    var mulkiya_validity : String?
    var online_status:String?
    var organisation_id:Int?
    var profile_pic : String?
    var profile_pic_url :String?
    var timezone:String?
    var timezonez:String?
    var updated_at: String?
    var user_id: Int?
    var user_detail_id: Int?
    var user_type_id: Int?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        accessToken <- map["accessToken"]
        blocked <- map["blocked"]
        category_brand_id <- map["category_brand_id"]
        category_id <- map["category_id"]
        created_at <- map["created_at"]
        fcm_id <- map["fcm_id"]
        forgot_token <- map["forgot_token"]
        forgot_token_validity <- map["forgot_token_validity"]
        language_id <- map["language_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        maximum_rides <- map["maximum_rides"]
        mulkiya_number <- map["mulkiya_number"]
        mulkiya_front <- map["mulkiya_front"]
        mulkiya_back <- map["mulkiya_back"]
        mulkiya_validity <- map["mulkiya_validity"]
        online_status <- map["online_status"]
        organisation_id        <- map["organisation_id"]
        profile_pic <- map["profile_pic"]
        timezone        <- map["timezone"]
        updated_at <- map["updated_at"]
        user_id        <- map["user_id"]
        user_detail_id <- map["user_detail_id"]
        user_type_id <- map["user_type_id"]
        profile_pic_url <- map["profile_pic_url"]
        
    }
}


class PaymentCompleted :NSObject,Mappable {
    
    var payment_type : String?
    var product_weight : Int?
    
    var product_per_quantity_charge:String?
    var refund_status:String?
    var initial_charge : String?

    var admin_charge : String?
    var bottle_charge : Int?
    var refund_id:String?
    
    var transaction_id : String?
    var payment_id : Int?
    
    var bank_charge : Int?
    var bottle_returned_value : Int?
    
    var buraq_percentage : Int?
    var created_at : String?
    
    var customer_organisation_id : Int?
    var customer_user_detail_id : Int?
    var customer_user_id : Int?
    var customer_user_type_id : Int?
    var final_charge:String?
    
    var order_distance : String?
    var order_id : Int?
    var order_time : String?
    var payment_status:Int?
    
    var product_actual_value : String?
    var product_alpha_charge : String?
    var product_per_distance_charge : String?
    var product_per_hr_charge : String?
    var product_per_weight_charge : String?
    var product_quantity : Int?
    
    var seller_organisation_id : Int?
    var seller_user_detail_id:Int?
    var seller_user_id : Int?
    var seller_user_type_id:Int?
    
    var updated_at:String?
    var user_card_id : Int?
    var previous_driver_charges : Double?
    var coupon_charge: String?
    
    
    var price_per_km : String?
    var price_per_min : String?
    var extra_distance : String?
    var extra_time : String?
    var distance_price_fixed : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        previous_driver_charges <- map["previous_driver_charges"]
        admin_charge <- map["admin_charge"]
        bank_charge <- map["bank_charge"]
        bottle_charge <- map["bottle_charge"]
        bottle_returned_value <- map["bottle_returned_value"]
        buraq_percentage <- map["buraq_percentage"]
        created_at <- map["created_at"]
        customer_organisation_id <- map["customer_organisation_id"]
        customer_user_detail_id <- map["customer_user_detail_id"]
        customer_user_id <- map["customer_user_id"]
        customer_user_type_id <- map["customer_user_type_id"]
        final_charge <- map["final_charge"]
        initial_charge <- map["initial_charge"]
        order_distance <- map["order_distance"]
        order_id <- map["order_id"]
        order_time <- map["order_time"]
        payment_id <- map["payment_id"]
        payment_status <- map["payment_status"]
        payment_type <- map["payment_type"]
        product_actual_value <- map["product_actual_value"]
        product_alpha_charge <- map["product_alpha_charge"]
        product_per_distance_charge <- map["product_per_distance_charge"]
        product_per_hr_charge <- map["product_per_hr_charge"]
        product_per_quantity_charge <- map["product_per_quantity_charge"]
        product_per_weight_charge <- map["product_per_weight_charge"]
        product_quantity <- map["product_quantity"]
        product_weight <- map["product_weight"]
        refund_id <- map["refund_id"]
        refund_status <- map["refund_status"]
        seller_organisation_id <- map["seller_organisation_id"]
        seller_user_detail_id <- map["seller_user_detail_id"]
        seller_user_id <- map["seller_user_id"]
        seller_user_type_id <- map["seller_user_type_id"]
        transaction_id <- map["transaction_id"]
        updated_at <- map["updated_at"]
        user_card_id <- map["user_card_id"]
        price_per_km <- map["price_per_km"]
        price_per_min <- map["price_per_min"]
        extra_distance <- map["extra_distance"]
        extra_time <- map["extra_time"]
        distance_price_fixed <- map["distance_price_fixed"]
        coupon_charge <- map["coupon_charge"]
    }
    
}

/*
 
 
    "break_down_charges" = 0;
    "breakdown_order_distance" = 0;
    "buraq_percentage" = 2;
    "cancellation_charges" = 0;
    "coupon_charge" = 0;
    "coupon_id" = 0;
    "credit_point_used" = 0;
    "distance_price_fixed" = 0;
    "extra_distance" = 0;
    "final_charge" = "186.9456024169922";
    "initial_charge" = "143.28";
    "order_distance" = "3.082";
    "order_time" = 0;
    "organisation_coupon_user_id" = 0;
    "package_distance" = 0;
    "payment_id" = 6290;
    "payment_status" = Pending;
    "payment_type" = Cash;
    "previous_charges" = 0;
    "previous_driver_charges" = 0;
    "price_per_km" = 0;
    "price_per_min" = 0;
    "product_actual_value" = 40;
    "product_alpha_charge" = 20;
    "product_per_distance_charge" = 40;
    "product_per_hr_charge" = 5;
    "product_per_quantity_charge" = 1;
    "product_per_sq_mt_charge" = 1;
    "product_per_weight_charge" = 1;
    "product_quantity" = 1;
    "product_sq_mt" = 0;
    "product_weight" = 0;
    "refund_id" = "";
    "refund_status" = NoNeed;
    "time_fixed_price" = 0;
    tip = "<null>";
    "transaction_id" = "<null>";
    "updated_order_distance" = 0;
    "updated_order_time" = 0;
    "user_card_id" = 0;
}*/

