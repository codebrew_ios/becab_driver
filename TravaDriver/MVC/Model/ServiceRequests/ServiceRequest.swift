//
//  ServiceRequest.swift
//  Buraq24Driver
//
//  Created by Apple on 21/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class ServiceRequest: NSObject , Mappable{
    
    var order:Order?
    var orderNotification:Order?
    var type : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        order <- map ["order"]
        type <- map["type"]
        orderNotification <- map["result"]
    }
}

class Order : NSObject, Mappable {
    
    var product_weight : Int?
    var order_id : Int?
    var future: String?
    //var paymentType: String?
    var pickup_address : String?
    var pickup_longitude : Double?
    var dropoff_address : String?
    var rateComments: String?
    var rateValue :Int?
    var details :String?
    var category_id : Int?
    var category_brand_id : String?
    var dropoff_longitude : Double?
    var pickup_latitude : Double?
    var final_charge : Double?
    var final_charge_string : String?
    var order_timings : String?
    var order_status : String?
    var order_token : String?
    var dropoff_latitude : Double = 0.0
    var product_quantity : Int?
    var category_brand_product_id : String?
    var ratingByDriver: ratingByDriver?
    var ratingByUser: ratingByDriver?
    var product : ServiceRequestProduct?
    var user : ServiceRequestUser?
    var coupon_user:Coupon?
    var payment :Payment?
    var roadStops: [RideStops]?
    var bookingType :String?
    var order_images :[imagesArray]?
    var resquestArrivalDateApp = Date()
    var friendName:String?
    var friendPhoneCode:String?
    var friendPhoneNumber:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        roadStops <- map["ride_stops"]
        ratingByUser <- map["ratingByUser"]
        ratingByDriver <- map["ratingByDriver"]
        product_weight <- map["product_weight"]
        order_id <- map["order_id"]
        future <- map["future"]
        pickup_address <- map["pickup_address"]
        pickup_longitude <- map["pickup_longitude"]
        dropoff_address <- map["dropoff_address"]
        category_id <- map["category_id"]
        category_brand_id <- map["category_brand_id"]
        dropoff_longitude <- map["dropoff_longitude"]
        pickup_latitude <- map["pickup_latitude"]
        details <- map["details"]
        final_charge <- map["final_charge"]
        final_charge_string <- map["final_charge"]
        order_timings <- map["order_timings"]
        order_status <- map["order_status"]
        order_token <- map["order_token"]
        dropoff_latitude <- map["dropoff_latitude"]
        product_quantity <- map["product_quantity"]
        category_brand_product_id <- map["category_brand_product_id"]
        rateComments <- map ["ratingByUser.comments"]
        rateValue <- map["ratingByUser.ratings"]
        
        product <- map["brand"]
        user <- map ["user"]
        coupon_user <- map["coupon_user"]
        payment <- map["payment"]
        bookingType <- map["booking_type"]
        order_images <- map["order_images_url"]
        friendName <- map["friend_name"]
        friendPhoneCode <- map["friend_phone_code"]
        friendPhoneNumber <- map["friend_phone_number"]
      //  paymentType <- map["payment_type"]
    }
}

class imagesArray:NSObject,Mappable{
    
    var image:String?
    var order_id:Int?
    var image_url:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        order_id <- map["order_id"]
        image_url <- map["image_url"]
    }
}

class ratingByDriver:NSObject,Mappable {
    
    var comments:String?
    var ratings:Int?
    var phone_number :Int?
    var phone_code :String?
    var createdAt:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        comments <- map["comments"]
        ratings <- map["ratings"]
        phone_number <- map["phone_number"]
        phone_code <- map["phone_code"]
        createdAt <- map["created_at"]
        
    }
}


class PaymentFromOrderDetail:NSObject,Mappable{
    
    var product_per_quantity_charge:String?
    var user_card_id : Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        product_per_quantity_charge <- map["product_per_quantity_charge"]
    }
    
}

class Payment :NSObject, Mappable{
    
    var admin_charge : Int?
    var bank_charge : Int?
    var bottle_charge : Int?
    var bottle_returned_value : Int?
    
    var buraq_percentage : Any?
    var created_at : String?
    
    var customer_organisation_id : Int?
    var customer_user_detail_id : Int?
    var customer_user_id : Int?
    var customer_user_type_id : Int?
    var final_charge:Double?
    var final_charge_string:String?
    
    var initial_charge : Int?
    var order_distance : String?
    var order_distance_Int : Double?
    var order_id : Int?
    var payment_id : Int?
    var payment_status:Int?
    var order_time : Int?
    var payment_type : String?
    var product_actual_value : Int?
    var product_alpha_charge : String?
    var product_alpha_charge_int : Int?
    
    var product_per_distance_charge : Int?
    var product_per_quantity_charge:Int?
    var product_per_quantity_charge_str :String?
    
    var product_per_weight_charge : Int?
    var product_quantity : Int?
    var product_weight : Int?
    
    var refund_id:String?
    var refund_status:String?
    
    var seller_organisation_id : Int?
    var seller_user_detail_id:Int?
    var seller_user_id : Int?
    var seller_user_type_id:Int?
    
    var transaction_id : String?
    var updated_at:String?
    var user_card_id : Int?
    var package_distance :Double?
    var previous_driver_charges : Double?
    
    var price_per_km : String?
    var price_per_min : String?
    var extra_distance : String?
    var extra_time : String?
    var distance_price_fixed : String?
    var coupon_charge: String?
    
    
    required convenience init?(map: Map) {
        
        self.init()
        
    }
    
    func mapping(map: Map) {
        order_time <- map["order_time"]
        admin_charge <- map["admin_charge"]
        bank_charge <- map["bank_charge"]
        bottle_charge <- map["bottle_charge"]
        bottle_returned_value <- map["bottle_returned_value"]
        buraq_percentage <- map["buraq_percentage"]
        created_at <- map["created_at"]
        customer_organisation_id <- map["customer_organisation_id"]
        customer_user_detail_id <- map["customer_user_detail_id"]
        customer_user_id <- map["customer_user_id"]
        customer_user_type_id <- map["customer_user_type_id"]
        final_charge <- map["final_charge"]
        final_charge_string <- map["final_charge"]
        initial_charge <- map["initial_charge"]
        order_distance <- map["order_distance"]
        order_distance_Int <- map["order_distance"]
        order_id <- map["order_id"]
        payment_id <- map["payment_id"]
        payment_status <- map["payment_status"]
        payment_type <- map["payment_type"]
        product_actual_value <- map["product_actual_value"]
        product_alpha_charge <- map["product_alpha_charge"]
        product_alpha_charge_int <- map["product_alpha_charge"]
        product_per_distance_charge <- map["product_per_distance_charge"]
        product_per_quantity_charge <- map["product_per_quantity_charge"]
        product_per_quantity_charge_str <- map["product_per_quantity_charge"]
        product_per_weight_charge <- map["product_per_weight_charge"]
        product_quantity <- map["product_quantity"]
        product_weight <- map["product_weight"]
        refund_id <- map["refund_id"]
        refund_status <- map["refund_status"]
        seller_organisation_id <- map["seller_organisation_id"]
        seller_user_detail_id <- map["seller_user_detail_id"]
        seller_user_id <- map["seller_user_id"]
        seller_user_type_id <- map["seller_user_type_id"]
        transaction_id <- map["transaction_id"]
        updated_at <- map["updated_at"]
        user_card_id <- map["user_card_id"]
        previous_driver_charges <- map["previous_driver_charges"]
        package_distance <- map["package_distance"]
        
        price_per_km <- map["price_per_km"]
        price_per_min <- map["price_per_min"]
        extra_distance <- map["extra_distance"]
        extra_time <- map["extra_time"]
        distance_price_fixed <- map["distance_price_fixed"]
        coupon_charge <- map["coupon_charge"]
    }
    
}

/*
 
 let paymentInfo = completedRequest?.result?.Payment
 let baseFare = (Double(paymentInfo?.product_alpha_charge ?? ""))?.roundTo(places: 2) ?? 0.0
 let orderTime = Double(paymentInfo?.order_time ?? 0).roundTo(places: 2)
 baseFareLabel.text = "\(paymentInfo?.product_alpha_charge ?? "") LKR"
 timeHeadingLabel.text = "Time(\(paymentInfo?.order_time ?? 0) min)"
 rideTimeLabel.text = "\(paymentInfo?.product_per_hr_charge ?? "") LKR"
 distanceHeadingLabel.text = "Distance (\(paymentInfo?.order_distance ?? 0) KM)"
 let orderDistance = Double(paymentInfo?.order_distance ?? 0).roundTo(places: 2)
 let perHourCharges = (Double(paymentInfo?.product_per_hr_charge ?? ""))?.roundTo(places: 2) ?? 0.0
 rideDistanceLabel.text = "\((orderDistance * perHourCharges).roundTo(places: 2)) LKR"
 let normalCharges = (baseFare + orderTime + orderDistance).roundTo(places: 2)
 let actualValue = Double(paymentInfo?.product_actual_value ?? 0).roundTo(places: 2)
 normalFareLabel.text = "\(normalCharges) LKR"
 var serviceCharges = 0.0
 let totalPercentage = (Double(paymentInfo?.buraq_percentage ?? 0)/100).roundTo(places: 2)
 if normalCharges > actualValue {
 bookingFeeLabel.text = "\(normalCharges * totalPercentage) LKR"
 }else {
 bookingFeeLabel.text = "\(actualValue * totalPercentage) LKR"
 serviceCharges = actualValue - normalCharges
 }
 serviceChargeLabel.text = "\(serviceCharges.roundTo(places: 2)) LKR"
 subTotalLabel.text = "\((normalCharges + baseFare + serviceCharges).roundTo(places: 2)) LKR"
 totalAmountLabel.text = "\(paymentInfo?.final_charge ?? "") LKR"
 */
class Coupon:NSObject,Mappable{
    
    var coupon_user_id :Int?
    var coupon_value :String?
    var coupon_type :String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        coupon_user_id <- map["coupon_user_id"]
        coupon_value <- map["coupon_value"]
        coupon_type <- map["coupon_type"]
    }
}

class ServiceRequestUser: NSObject , Mappable{
    
    var ratings_count: Int?
    var ratings_count_str: String?
    
    var profile_pic : String?
    var name: String?
    var phone_code :Int?
    var phoneNum1: String?
    var phoneNum2: Int?
    var phoneNum3: Double?
    var phoneNumber: Int?
    
    var ratings_avg: String?
    var ratings_avg_int :Int?
    
    var email : String?
    var profile_pic_url: String?
    var user_id : Int?
    var user_detail_id: Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        ratings_count <- map ["rating_count"]
        ratings_count_str <- map ["rating_count"]
        profile_pic <- map["profile_pic"]
        name <- map["name"]
        ratings_avg <- map ["rating_avg"]
        ratings_avg_int <- map ["rating_avg"]
        email <- map["email"]
        profile_pic_url <- map["profile_pic_url"]
        user_id <- map["user_id"]
        user_detail_id <- map["user_detail_id"]
        phoneNum1 <- map["phone_number"]
        phoneNum2 <- map["phone_number"]
        phoneNum3 <- map["phone_number"]
        phoneNumber <- map["phone_number"]
        phone_code <- map["phone_code"]
    }
}


class ServiceRequestProduct: NSObject , Mappable{
    
    var name: String?
    var brand_name : String?
    var category_brand_product_id: String?
    var category_id :Int?
    var categoryName: String?
    var category_id_str :String?
    var product_name :String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name <- map ["name"]
        brand_name <- map["brand_name"]
        category_brand_product_id <- map["category_brand_product_id"]
        product_name <- map["product_name"]
        category_id <- map["category_id"]
        category_id_str <- map["category_id"]
        
        if let str = category_id_str, let int = Int(str) {
            category_id = int
        }
        categoryName <- map["category_name"]
    }
}

class StartedRequest : NSObject , Mappable{
    
    var dropoff_latitude : Double?
    var dropoff_longitude : Double?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        dropoff_latitude <- map ["result.dropoff_latitude"]
        dropoff_longitude <- map["result.dropoff_longitude"]
        
    }
    
    
}
