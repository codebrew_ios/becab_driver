//
//  Model.swift
//  TravaDriver
//
//  Created by Work on 29/10/20.
//  Copyright © 2020 OSX. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit


class Walkthrough {
    
    var image: UIImage?
    var title: String?
    var description: String?
    
    init(title:String?,description:String?,image:UIImage) {
        self.image = image
        self.title = title
        self.description = description
    }
    
}


class CheckUserExistModel: Mappable {
  
    var AppDetail: Appdet?
    
  required init?(map: Map) {
    mapping(map: map)
  }
  
  func mapping(map: Map) {
    AppDetail <- map["AppDetail"]
  }
}


class Appdet: Mappable {
      
  var userExists : Bool?

  required init?(map: Map) {
    mapping(map: map)
  }
  
  func mapping(map: Map) {
    userExists <- map["userExists"]
  }
}






class NewTemplateValidations {
  
  static let sharedInstance = NewTemplateValidations()
    
  func validateEmail(email: String) -> Bool {
    if email.isBlank {
     //   Alerts.shared.showOnTop(alert: "alert.alert".localizedString, message: "Please enter email." , type: .info )

      return true
    } else {
      let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
      let status = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
      if status {
        return true
      } else {
        Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "invalid_email".localized , type: .info )

        return false
      }
    }
  }
    
    
    
    func validateLoginUsernameAndPassword(usernameOrEmail: String, password: String) -> Bool {
        
        
        if usernameOrEmail.isEmpty {
            
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "username_email_validation".localized , type: .info )

          return false
        } else if password.isEmpty {
            
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "password_empty_validation_message".localized , type: .info )

          return false
        } else {
             return true
        }
      }
    
    func validateSignupUsernameAndPassword(usernameOrEmail: String, password: String, confirmPassword: String, phone: String) -> Bool {
        
        
        if usernameOrEmail.isEmpty {
            
            Alerts.shared.show(alert: "AppName".localized, message: "username_email_validation".localized , type: .error )

          return false
        } else if !validateSignupPassword(password: password) {
          return false
        } else if confirmPassword.isEmpty {
            
            Alerts.shared.show(alert: "AppName".localized, message: "confirm_password_empty_validation_message".localized , type: .error )

          return false
        } else if password != confirmPassword {
            
            Alerts.shared.show(alert: "AppName".localized, message: "password_confirmPassword_validation".localized , type: .error )
            return false
        } else if !validatePhoneNumber(phone: phone) {
          return false
        } else {
             return true
    //      let regEx = "^([a-zA-Z]{1,}\\s?[a-zA-z]{1,}'?-?[a-zA-Z]{1,}\\s?([a-zA-Z]{1,})?)"
    //      let emailTest = NSPredicate(format:"SELF MATCHES %@", regEx)
    //      let status = emailTest.evaluate(with: userName)
    //      if status {
    //        return true
    //      } else {
    //        Toast.show(text: "name_empty_validation_message".localizedString , type: .error)
    //        return false
    //      }
        }
      }
    
  
    func validateInstitutionalSignup(type: String, name: String) -> Bool {
        
        
        if type.isEmpty {
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "institution_type_validation".localized , type: .info )

          return false
        } else if name.isEmpty {
            
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "institution_name_validation".localized , type: .info )

          return false
        }
        return true
      }
    
    
    func validateInstitutionalSignupInfo(id: String, email: String, phone: String, password: String, image: UIImage?) -> Bool {
      
      
      if id.isEmpty {
          
          Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "institution_id_validation".localized , type: .info )

        return false
      } else if email.isEmpty {
          
          Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "empty_institutional_email_validation".localized , type: .info )

        return false
      }  else if !validateEmail(email: email) {
             return false
      } else if !validatePhoneNumber(phone: phone) {
        return false
      }else if !validateSignupPassword(password: password) {
        return false
      } else if image == nil {
        Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "institutional_cred_Image_validation".localized , type: .info )
     }
        
      return true
    }
    
  
    func validatePhoneNumber(phone: String) -> Bool {
        
        if phone.isEmpty {
            
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "phone_validation_message".localized , type: .info)
            
            return false
            
        } else {
            
            return true
            
           /* if (phone.count != 10 ) {
                
                Alerts.shared.showOnTop(alert: "alert.alert".localizedString, message: "phone_validation_message".localizedString , type: .info)
                
                return false
                
            } else {
                
                return true
                
            } */
            
        }
        
    }
  
    
    func validateSignupPassword(password: String) -> Bool {
        
        if password.isEmpty {
                        
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "password_empty_validation_message".localized , type: .info )

          return false
        } else if password.count < 6 {
                        
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "password_length_validation_message".localized , type: .info )

          return false
        }
        
        return true
    }
}



