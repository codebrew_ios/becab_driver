//
//  DriverModel.swift
//  Buraq24Driver
//
//  Created by Apple on 14/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//


import UIKit
import ObjectMapper

class DriverModel: NSObject , Mappable {
    
    var data : DriverData?
    var services:[Services]?
    var message : String?
    var statusCode : Int?
    var products :[Products]?
    var support :[Support]?
    var versionForce:String?
    var versionNormal:String?
    
    var seletedServices:[Services]? {
        return services?.filter({ $0.category_id == 7 })
    }
    
    var isCabSerive: Bool {
        return isCabSerive(idCat: data?.category_id)
    }
    
    func isCabSerive(idCat: Int?) -> Bool {
        if idCat == 7 {
            return true
        }
//        if let id = idCat, let objService = services?.first(where: { $0.category_id == id }), /objService.name?.lowercased().contains("cab") {
//            return true
//        }
        return false
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        data <- map["result.AppDetail"]
        services <- map ["result.services"]
        message <- map["msg"]
        statusCode <- map["statusCode"]
        products <- map["result.products"]
        support <- map["result.supports"]
        versionForce <- map["result.Versioning.IOS.driver.force"]
        versionNormal <- map["result.Versioning.IOS.driver.normal"]
    }
}

// class for driver detail

class DriverData : Mappable {
    
    var user_detail_id : Int?
    var user_id : Int?
    var user_type_id : Int?
    var language_id : Int?
    var category_id : Int?
    var category_brand_id : Int?
    var rating : Int?
    var accountStep : Int?
    
    var profile_pic : String?
    var accessToken : String?
    var latitude : Double?
    var longitude : Double?
    var mulkiya_number : String?
    var mulkiya_front : String?
    var mulkiya_back : String?
    var mulkiya_validity : String?
    var notifications  : String?
    var home_address : String?
    var home_latitude : Double?
    var home_longitude : Double?
    var mulkiya_front_url : String?
    var mulkiya_back_url : String?
    var profile_pic_url : String?
    var rating_count : Int?
    var approved :String?
    
    var cabModel: String?
    var cabColor: String?
    var qrCode :  String?
    var dataUser : User?
    var directional_hire : String?
    var online_status : String?
    var vehicle_number : String?
    var vehicle_make: String?
    var vehicle_registration_number: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        vehicle_make <- map["vehicle_make"]
        vehicle_registration_number <- map["vehicle_registration_number"]
        vehicle_number <- map["vehicle_number"]
        accountStep <- map["accountStep"]
        user_detail_id <- map["user_detail_id"]
        user_id <- map["user_id"]
        user_type_id <- map["user_type_id"]
        rating <- map["ratings_avg"]
        rating_count <- map["rating_count"]
        language_id <- map["language_id"]
        category_id <- map["category_id"]
        category_brand_id <- map["category_brand_id"]
        profile_pic <- map["profile_pic"]
        accessToken <- map["access_token"]
        directional_hire <- map["directional_hire"]
        online_status <- map["online_status"]
        home_address <- map["home_address"]
        home_latitude <- map["home_latitude"]
        home_longitude <- map["home_longitude"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        mulkiya_number <- map["mulkiya_number"]
        mulkiya_front <- map["mulkiya_front"]
        mulkiya_back <- map["mulkiya_back"]
        mulkiya_validity <- map["mulkiya_validity"]
        notifications <- map["notifications"]
        mulkiya_front_url <- map["mulkiya_front_url"]
        mulkiya_back_url <- map["mulkiya_back_url"]
        profile_pic_url <- map["profile_pic_url"]
        dataUser        <- map["user"]
        approved        <- map["approved"]
        
        cabModel    <-      map["vehicle_name"]
        cabColor    <-      map["vehicle_color"]
        qrCode      <-       map["qr_code"]
    }
}


class Support:Mappable {
    var category_id : Int?
    var category_type : String?
    
    var desc : String?
    var image : String?
    var image_url : String?
    var name : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        category_id <- map["category_id"]
        category_type <- map["category_type"]
        desc <- map["description"]
        image <- map["image"]
        image_url <- map["image_url"]
        name <- map["name"]
    }
}

class User : Mappable {
    
    var user_id : Int?
    var organisation_id : Int?
    var stripe_customer_id : String?
    var stripe_connect_id : String?
    
    var name : String?
    var email : String?
    var phone_code : String?
    var phone_number : Int?
    var stripe_connect_token : String?
    var active_bank : String?
    var referral_code: String?
    var credit_points:Float?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        referral_code <- map["referral_code"]
        user_id <- map["user_id"]
        organisation_id <- map["organisation_id"]
        stripe_customer_id <- map["stripe_customer_id"]
        stripe_connect_id <- map["stripe_connect_id"]
        name <- map["name"]
        email <- map["email"]
        phone_code <- map["phone_code"]
        phone_number <- map["phone_number"]
        stripe_connect_token <- map["stripe_connect_token"]
        active_bank <- map["active_bank"]
        credit_points <- map["credit_points"]
    }
}

// class for services and brands

class Services : Mappable {
    
    var category_id : Int?
    var category_type : String?
    var default_brands : String?
    var description : String?
    var name : String?
    var brands :[Brands]?
    var isOpen = true
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        category_id <- map["category_id"]
        category_type <- map["category_type"]
        default_brands <- map["default_brands"]
        description <- map["description"]
        name <- map["name"]
        brands <- map ["brands"]
    }
}

class Brands : Mappable {
    
    var category_brand_id : Int?
    var category_id : Int?
    var sort_order : Int?
    var image : String?
    var image_url : String?
    var name : String?
    var description : String?
    var products:[Products]?
    var isOpen = true
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        category_brand_id <- map["category_brand_id"]
        category_id <- map["category_id"]
        sort_order <- map["sort_order"]
        image <- map["image"]
        image_url <- map["image_url"]
        name <- map["name"]
        products <- map["products"]
        
    }
}

class BrandProducts:Mappable{
    
    var message : String?
    var statusCode : Int?
    var products :[Products]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        
        message <- map["msg"]
        statusCode <- map["statusCode"]
        products <- map["result"]
    }
    
}


// class for products

class Products : Mappable {
    
    var category_brand_product_id : Int?
    var name : String?
    var description : String?
    var sort_order : Int?
    var icon_image_url : String?
    var image_url : String?
    var isSelected = false
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        category_brand_product_id <- map["category_brand_product_id"]
        name <- map["name"]
        description <- map["description"]
        sort_order <- map["sort_order"]
        icon_image_url <- map["icon_image_url"]
        image_url <- map["image_url"]
    }
}


//class  Support :NSObject, Mappable{
//
//    var msg :String?
//    var result :[SupportServices]
//    var statusCode : Int?
//    var success :Int?
//
//
//    required convenience init?(map: Map) {
//        self.init()
//    }
//
//    func mapping(map: Map) {
//        msg <- map["msg"]
//        result <- map["result"]
//        statusCode <- map["statusCode"]
//        success <- map["success"]
//    }
//}
//
//
//
//class SupportServices : Mappable{
//
//    var category_id : Int?
//    var category_type : String?
//    var description : String?
//    var image :String?
//    var image_url :String?
//    var name : String?
//
//
//
//    required convenience init?(map: Map) {
//        self.init()
//    }
//
//    func mapping(map: Map) {
//        category_id <- map["category_id"]
//        category_type <- map["category_type"]
//        description <- map["description"]
//        image <- map["image"]
//        image_url <- map["image_url"]
//        name <- map["name"]
//    }
//
//
//}




