//
//  OtpModel.swift
//  Buraq24Driver
//
//  Created by Apple on 10/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class OtpModel: NSObject , Mappable {
    
    var data : OtpData?
    var message : String?
    var statusCode: Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        data <- map["result"]
        message <- map["msg"]
        statusCode <- map["statusCode"]
    }
}

class OtpData : NSObject, Mappable {
    
    var type : String?
    var acessToken : String?
    var otp = 0
    var versionNormal : String?
    var versionForce : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        type <- map["type"]
        acessToken <- map["access_token"]
        otp <- map["otp"]
        versionNormal <- map["Versioning.IOS.driver.normal"]
        versionForce <- map["Versioning.IOS.driver.force"]
    }
}

class organizations: NSObject , Mappable {
    
    var data : [organizationsData] = []
    var message : String?
    var statusCode: Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        data <- map["result"]
        message <- map["msg"]
        statusCode <- map["statusCode"]
    }
}

class organizationsData: NSObject , Mappable {
    
    var name : String?
    var image_url : String?
    var organisation_id: Int?
    var licence_number:String?
    var image:String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        image_url <- map["image_url"]
        organisation_id <- map["organisation_id"]
        licence_number <- map["licence_number"]
        image <- map["image"]
        
    }
}


