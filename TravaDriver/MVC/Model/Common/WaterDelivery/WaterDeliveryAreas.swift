//
//  WaterDeliveryAreas.swift
//  Buraq24Driver
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class WaterDeliveryAreas: NSObject,Mappable {
    
    var result:[Areas]?
    var statusCode : Int?
    var message : String?
    var areaData :AreaData?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        result <- map ["result"]
        areaData <- map ["result.data"]
        statusCode <- map["statusCode"]
        message <- map["msg"]
    }
}

class Areas:Mappable{
    
    var address:String?
    var address_latitude : Double?
    var address_longitude : Double?
    var distance :Double?
    var driver :AreaDriver?
    var label :String?
    var organisation_area_id :Int?
    var organisation_id:Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        address <- map ["address"]
        address_latitude <- map["address_latitude"]
        address_longitude <- map["address_longitude"]
        distance <- map["distance"]
        driver <- map["driver"]
        label <- map["label"]
        organisation_area_id <- map["organisation_area_id"]
        organisation_id <- map["organisation_id"]
    }
}

class AreaDriver :Mappable{
    
    var organisation_area_driver_id :Int?
    var organisation_area_id :Int?
    var user_detail_id:Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        organisation_area_driver_id <- map ["organisation_area_driver_id"]
        organisation_area_id <- map["organisation_area_id"]
        user_detail_id <- map["user_detail_id"]
    }
}

class AreaData : Mappable{
    
    var day :String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        day <- map ["day"]
    }
}


