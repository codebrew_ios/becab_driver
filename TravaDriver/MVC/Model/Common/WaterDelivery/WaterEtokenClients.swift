//
//  WaterEtokenClients.swift
//  Buraq24Driver
//
//  Created by Apple on 01/01/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class WaterEtokenClients: NSObject, Mappable {
    
    var result:[EtokenClients]?
    var statusCode : Int?
    var message : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        result <- map ["result.customers"]
        statusCode <- map["statusCode"]
        message <- map["msg"]
    }
}


class EtokenClients :NSObject, Mappable{
    
    var address : String?
    var address_latitude : Double?
    var address_longitude : Double?
    var bottle_quantity : Int?
    var brand_name: String?
    var category_brand_id : Int?
    var category_brand_product_id: Int?
    var category_id : Int?
    var distance: Double?
    var name: String?
    var nearest_area_id: Int?
    var organisation_coupon_id: Int?
    var organisation_coupon_user_id: Int?
    var product_name:String?
    var profile_pic_url: String?
    var quantity: Int?
    var quantity_left : Int?
    var user_detail_id: Int?
    var user_id: Int?
    var phone_code :String?
    var phone_number :Int?
    var todayOrderStatus : String?
    var todayMyOrderId: String?
    var rating_count: Int?
    var ratings_avg: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        address <- map ["address"]
        address_latitude <- map ["address_latitude"]
        address_longitude <- map["address_longitude"]
        bottle_quantity <- map["bottle_quantity"]
        
        brand_name <- map ["brand_name"]
        category_brand_id <- map ["category_brand_id"]
        category_brand_product_id <- map["category_brand_product_id"]
        category_id <- map["category_id"]
        
        distance <- map ["distance"]
        name <- map ["name"]
        nearest_area_id <- map["nearest_area_id"]
        organisation_coupon_id <- map["organisation_coupon_id"]
        
        product_name <- map ["product_name"]
        profile_pic_url <- map ["profile_pic_url"]
        quantity <- map["quantity"]
        quantity_left <- map["quantity_left"]
        
        user_detail_id <- map ["user_detail_id"]
        user_id <- map ["user_id"]
        phone_code <- map["phone_code"]
        phone_number <- map["phone_number"]
        
        organisation_coupon_user_id <- map["organisation_coupon_user_id"]
        todayOrderStatus <- map["todayOrderStatus"]
        todayMyOrderId <- map["todayMyOrderId"]
        rating_count <- map["rating_count"]
        ratings_avg <- map["ratings_avg"]
        
    }
}
