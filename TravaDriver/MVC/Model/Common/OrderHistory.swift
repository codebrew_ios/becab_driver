//
//  OrderHistory.swift
//  Buraq24Driver
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderHistory: NSObject, Mappable {
    
    var order:[Bookings]?
    var statusCode : Int?
    var message : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        order <- map ["result"]
        statusCode <- map["statusCode"]
        message <- map["message"]
    }
}

class Bookings: NSObject,Mappable {
    
    var cancelled_by:String?
    var booking_type:String?
    var cancel_reason:String?
    var category_brand_id:Int?
    var category_brand_product_id:Int?
    var category_id:Int?
    var continouos_startdt:String?
    var continuous:String?
    var continuous_enddt:String?
    var continuous_order_id:Int?
    var continuous_time:String?
    var coupon_user_id:Int?
    var created_at:String?
    var created_by:String?
    var customer_organisation_id:Int?
    var customer_user_detail_id:Int?
    var customer_user_id:Int?
    var customer_user_type_id:Int?
    var details :String?
    var driver_organisation_id:Int?
    var driver_user_detail_id:Int?
    var driver_user_id:Int?
    var driver_user_type_id:Int?
    var dropoff_address:String?
    var dropoff_latitude:Double?
    var dropoff_longitude:Double?
    var final_charge:String?
    var future:String?
    var order_id:Int?
    var order_status:String?
    var order_timings:String?
    var order_token:String?
    var order_type:String?
    var organisation_coupon_user_id:Int?
    var payment : PaymentCompleted?
    var pickup_address:String?
    var pickup_latitude:Double?
    var pickup_longitude:Double?
    var track_image:String?
    var track_image_url:String?
    var track_path:String?
    var updated_at:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        booking_type <- map["booking_type"]
        cancelled_by <- map["cancelled_by"]
        cancel_reason <- map["cancel_reason"]
        category_brand_id <- map["category_brand_id"]
        category_brand_product_id <- map["category_brand_product_id"]
        category_id <- map["category_id"]
        continouos_startdt <- map["continouos_startdt"]
        continuous_enddt <- map["continuous_enddt"]
        continuous_order_id <- map["continuous_order_id"]
        continuous_time <- map["continuous_time"]
        created_at <- map["created_at"]
        created_by <- map["created_by"]
        customer_organisation_id <- map["customer_organisation_id"]
        customer_user_detail_id <- map["customer_user_detail_id"]
        customer_user_id <- map["customer_user_id"]
        customer_user_type_id <- map["customer_user_type_id"]
        driver_organisation_id <- map["driver_organisation_id"]
        driver_user_detail_id <- map["driver_user_detail_id"]
        driver_user_id <- map["driver_user_id"]
        driver_user_type_id <- map["driver_user_type_id"]
        dropoff_address <- map["dropoff_address"]
        dropoff_latitude <- map["dropoff_latitude"]
        dropoff_longitude <- map["dropoff_longitude"]
        final_charge <- map["final_charge"]
        future <- map["future"]
        category_id <- map["category_id"]
        order_id <- map["order_id"]
        order_status <- map["order_status"]
        order_timings <- map["order_timings"]
        order_token <- map["order_token"]
        order_type <- map["order_type"]
        organisation_coupon_user_id <- map["organisation_coupon_user_id"]
        pickup_address <- map["pickup_address"]
        pickup_latitude <- map["pickup_latitude"]
        pickup_longitude <- map["pickup_longitude"]
        updated_at <- map["updated_at"]
        payment <- map["payment"]
    }
}
