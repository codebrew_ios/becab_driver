//
//  EmergencyContacts.swift
//  Buraq24Driver
//
//  Created by Apple on 01/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class EmergencyContacts: NSObject,Mappable{
    
    var contacts:[Contacts]?
    var statusCode : Int?
    var message : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        contacts <- map ["result"]
        statusCode <- map["statusCode"]
        message <- map["message"]
        
    }
}

class Contacts : NSObject, Mappable {
    
    var emergency_contact_id : Int?
    var name : String?
    var phone_number : Int?
    var sort_order : Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        emergency_contact_id <- map["emergency_contact_id"]
        name <- map["name"]
        phone_number <- map["phone_number"]
        sort_order <- map["sort_order"]
        
    }
}
