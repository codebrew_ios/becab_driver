//
//  Earnings.swift
//  Buraq24Driver
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

class Earnings: NSObject,Mappable {
    
    var result:EarningsInDateRange?
    var statusCode : Int?
    var message : String?
    var success : Int?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        result <- map ["result"]
        statusCode <- map["statusCode"]
        message <- map["msg"]
        success <- map["success"]
    }

}
class EarningsInDateRange: Mappable{
    
    var totalBooking : Int?
    var totalCancelledBooking : Int?
    var totalEarnings : Double?
    var totalUpcomingBooking : Int?
    var weeklyEarnings : Double?
    var rangeData:[RangeData]?

    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        totalBooking <- map["totalBooking"]
        totalCancelledBooking <- map["totalCancelledBooking"]
        totalEarnings <- map["totalEarnings"]
        totalUpcomingBooking <- map["totalUpcomingBooking"]
        weeklyEarnings <- map["weeklyEarnings"]
        rangeData <- map["daily"]
        
    }
}


class RangeData:Mappable  {
    
    var _id : String?
    var bookings : Int?
    var earning : Double?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        bookings <- map["bookings"]
        earning <- map["earning"]
    }
}
//class EarningsInDateRange: Mappable{
//
//    var admin_charge : Int?
//    var bank_charge : Int?
//    var bottle_charge : Int?
//    var bottle_returned_value : Int?
//    var brand :Brands?
//
//    var buraq_percentage : String?
//    var created_at : String?
//
//    var customer_organisation_id : Int?
//    var customer_user_detail_id : Int?
//    var customer_user_id : Int?
//    var customer_user_type_id : Int?
//    var final_charge:String?
//
//    var initial_charge : Int?
//    var order_distance : Int?
//    var order_id : Int?
//    var payment_id : Int?
//    var payment_status:Int?
//
//    var payment_type : String?
//    var product_actual_value : Int?
//    var product_alpha_charge : Int?
//    var product_per_distance_charge : Int?
//    var product_per_quantity_charge:Int?
//
//    var product_per_weight_charge : Int?
//    var product_quantity : Int?
//    var product_weight : Int?
//
//    var refund_id:String?
//    var refund_status:String?
//
//    var seller_organisation_id : Int?
//    var seller_user_detail_id:Int?
//    var seller_user_id : Int?
//    var seller_user_type_id:Int?
//
//    var transaction_id : String?
//    var updated_at:String?
//    var user_card_id : Int?
//
//    var order : OrderInEarnings?
//
//
//    required convenience init?(map: Map) {
//        self.init()
//    }
//
//    func mapping(map: Map) {
//
//        admin_charge <- map["admin_charge"]
//        bank_charge <- map["bank_charge"]
//        bottle_charge <- map["bottle_charge"]
//        brand <- map["brand"]
//        bottle_returned_value <- map["bottle_returned_value"]
//        buraq_percentage <- map["buraq_percentage"]
//        created_at <- map["created_at"]
//        customer_organisation_id <- map["customer_organisation_id"]
//        customer_user_detail_id <- map["customer_user_detail_id"]
//        customer_user_id <- map["customer_user_id"]
//        customer_user_type_id <- map["customer_user_type_id"]
//        final_charge <- map["final_charge"]
//        initial_charge <- map["initial_charge"]
//        order_distance <- map["order_distance"]
//        order_id <- map["order_id"]
//        payment_id <- map["payment_id"]
//        payment_status <- map["payment_status"]
//        payment_type <- map["payment_type"]
//        product_actual_value <- map["product_actual_value"]
//        product_alpha_charge <- map["product_alpha_charge"]
//        product_per_distance_charge <- map["product_per_distance_charge"]
//        product_per_quantity_charge <- map["product_per_quantity_charge"]
//        product_per_weight_charge <- map["product_per_weight_charge"]
//        product_quantity <- map["product_quantity"]
//        product_weight <- map["product_weight"]
//        refund_id <- map["refund_id"]
//        refund_status <- map["refund_status"]
//        seller_organisation_id <- map["seller_organisation_id"]
//        seller_user_detail_id <- map["seller_user_detail_id"]
//        seller_user_id <- map["seller_user_id"]
//        seller_user_type_id <- map["seller_user_type_id"]
//        transaction_id <- map["transaction_id"]
//        updated_at <- map["updated_at"]
//        user_card_id <- map["user_card_id"]
//        order <- map["Order"]
//
//    }
//}

class OrderInEarnings: Mappable{
    
    var product_weight : Int?
    var order_id : Int?
    var future: String?
    var pickup_address : String?
    var pickup_longitude : Double?
    var dropoff_address : String?
    var category_id : String?
    var category_brand_id : String?
    var dropoff_longitude : Double?
    var pickup_latitude : Double?
    var final_charge : Double?
    var order_timings : String?
    var order_status : String?
    var order_token : String?
    var dropoff_latitude : Double = 0.0
    var product_quantity : Int?
    var category_brand_product_id : String?
    var product : ServiceRequestProduct?
    var user : ServiceRequestUser?
    var coupon_user:Coupon?
    var payment :Payment?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        product_weight <- map["product_weight"]
        order_id <- map["order_id"]
        future <- map["future"]
        pickup_address <- map["pickup_address"]
        pickup_longitude <- map["pickup_longitude"]
        dropoff_address <- map["dropoff_address"]
        category_id <- map["category_id"]
        category_brand_id <- map["category_brand_id"]
        dropoff_longitude <- map["dropoff_longitude"]
        pickup_latitude <- map["pickup_latitude"]
        final_charge <- map["final_charge"]
        order_timings <- map["order_timings"]
        order_status <- map["order_status"]
        order_token <- map["order_token"]
        dropoff_latitude <- map["dropoff_latitude"]
        product_quantity <- map["product_quantity"]
        category_brand_product_id <- map["category_brand_product_id"]
        
        product <- map["brand"]
        user <- map ["user"]
        coupon_user <- map["coupon_user"]
        payment <- map["payment"]
        
    }
}

