//
//  RidePackageDetailController.swift
//  Buraq24Driver
//
//  Created by Apple on 26/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class RidePackageDetailController: UIViewController {
    
    
    //MARK:-IBOutlets.
    @IBOutlet weak var userImageview: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var packageDescLabel: UILabel!
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var dropLocationLabel: UILabel!
    @IBOutlet weak var ratinglabel: UILabel!
    
    //MARK:- Variables
    var serviceRequest : ServiceRequest?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        ratingView.filledBorderColor = .black
        ratingView.emptyBorderColor = .black
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK:-Custome Function.
    func setInitialData() {
        // update image profile
        let url = "\(/serviceRequest?.order?.user?.profile_pic_url)"
        userImageview.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        
        userNameLabel.text = "\(/serviceRequest?.order?.user?.name)"
        ratingView.rating = Double(/serviceRequest?.order?.user?.ratings_avg) ?? 0.0
        ratinglabel.text = "\(/serviceRequest?.order?.user?.ratings_avg)"
        pickUpLocationLabel.text = serviceRequest?.order?.pickup_address
        dropLocationLabel.text = serviceRequest?.order?.dropoff_address
        packageDescLabel.text = "Taxi Package for \(serviceRequest?.order?.payment?.package_distance ?? 35) KM"
    }
    
    //MARK:-IBActions.
    //MARK:-Function to hide controller.
    @IBAction func bckButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
