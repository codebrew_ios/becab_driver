////
////  StartDeliveryViewController.swift
////  Buraq24Driver
////
////  Created by OSX on 09/08/18.
////  Copyright © 2018 OSX. All rights reserved.
////
//
//import UIKit
//import IBAnimatable
//
//
////MARK::  Protocol including functions when order is order is started or cancelled
//
//protocol OnGoingDelegate: class {
//    func RequestOngoingProcessed()
//    func RequestOngoingCancelled()
//}
//
//class StartDeliveryViewController: UIViewController, reasonCancellation {
//    
//    
//    //MARK: ---------------Outlets-------------
//    @IBOutlet weak var customView: UIView!
//    
//    @IBOutlet weak var imgViewCustomer: UIImageView!
//    @IBOutlet weak var imgRating: UIImageView!
//    
//    @IBOutlet weak var lblCustomerName: UILabel!
//    @IBOutlet weak var lblProductDescription: UILabel!
//    @IBOutlet weak var lblCustomerRating: UILabel!
//    @IBOutlet weak var lblDistance: UILabel!
//    @IBOutlet weak var lblJourneyTime: UILabel!
//    @IBOutlet weak var lblTotalCost: UILabel!
//    @IBOutlet weak var lblCustomerNameKey: UILabel!
//    
//    @IBOutlet weak var btnStart: UIButton!
//    @IBOutlet weak var btnCancel: AnimatableButton!
//    
//    //MARK: ---------------variables-------------
//    
//    var serviceRequest : OrdersList?
//    weak var delegate: OnGoingDelegate?
//    
//    //MARK: ---------------ViewController LifeCycle Functions-------------
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.setupViewController()
//    }
//    
//    deinit {
//        
//        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(rawValue:  NotificationNames.updateDistanceTime.rawValue), object: nil)
//    }
//    
//    // MARK: CUSTOM METHODS
//    func setupViewController(){
//        
//        self.localiseController() // localise controller
//        self.addObserver() //to update distance and time
//        
//        // Update data on alert pop up
//        self.lblCustomerName.text = serviceRequest?.user?.name
//        self.lblProductDescription.text = "\(/serviceRequest?.brand?.name) , \(/serviceRequest?.brand?.brand_name) x \(/serviceRequest?.payment?.product_quantity)"
//        self.lblCustomerRating.text = "\(/serviceRequest?.user?.ratings_count)"
//                
//        // images loading
//        self.imgRating.getRatingImage(rating: /serviceRequest?.user?.ratings_avg)
//        let url = "\(/serviceRequest?.user?.profile_pic_url)"
//        self.imgViewCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
//        
//        // fair setup
//        let charge = calculateFair()
//        self.lblTotalCost.text = "\(charge) \(R.string.localizable.currency())"
//    }
//    
//    func localiseController(){
//        self.lblCustomerNameKey.text = R.string.localizable.requestCustomerName()
//        self.btnStart.setTitle(R.string.localizable.requestStart(), for: .normal)
//        self.btnCancel.setTitle(R.string.localizable.requestCancel(), for: .normal)
//    }
//    
//    //Observer for Distance and time update in poly line and map route
//    
//    func addObserver(){
//        
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(UpdateDistance),
//            name: NSNotification.Name.init(rawValue: NotificationNames.updateDistanceTime.rawValue),
//            object: nil)
//    }
//    
//    // Observer handler when polyline is created and distance and eta is updated
//    
//    @objc func UpdateDistance(notification:Notification){
//        guard let dic = notification.object as? NSDictionary? else{return}
//        
//        self.lblDistance.text = dic!.value(forKey: LocalKeys.distance) as? String
//        self.lblJourneyTime.text = dic!.value(forKey: LocalKeys.duration) as? String
//        self.lblCustomerName.text = serviceRequest?.user?.name
//        self.lblProductDescription.text = "\(/serviceRequest?.brand?.name) , \(/serviceRequest?.brand?.brand_name) x \(/serviceRequest?.payment?.product_quantity)"
//    }
//    
//    //MARK:-- CALCULATE FARE
//    
//    func calculateFair() -> String{
//        
//        var  perquantityCharge = 0.0
//        let product_distance_charge:Double = Double(/serviceRequest?.payment?.product_per_distance_charge) ?? 0.0
//        let product_weight_charge:Double = Double(/serviceRequest?.payment?.product_per_weight_charge) ?? 0.0
//        let product_alpha_charge:Double = Double(/serviceRequest?.payment?.product_alpha_charge) ?? 0.0
//        
//        guard let val = serviceRequest?.payment?.product_per_quantity_charge  else {
//            return ""
//        }
//        
//        perquantityCharge = Double(val) ??  0.0
//        
//        let base_price = Double(/serviceRequest?.payment?.product_quantity) * perquantityCharge
//        let total_charge = base_price + product_distance_charge + product_weight_charge + product_alpha_charge
//        
//        return "\(total_charge)"
//    }
//    
//    //MARK: --------------Btn's Action Functions-------------
//    
//    @IBAction func actionCallDriver(_ sender: UIButton) {
//
//    }
//    
//    @IBAction func actionStartRequest(_ sender: Any) {
//        
//        APIManager.shared.request(with: ServicesEndPoint.start(orderId: serviceRequest?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) {  [weak self] (response) in
//            switch response{
//            case .success(_):
//                
//                self?.dismiss(animated: true, completion: {
//                    self?.delegate?.RequestOngoingProcessed()
//                })
//                
//            case .failure(let err):
//                
//                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
//                self?.dismiss(animated: true, completion: nil)
//            }
//        }
//    }
//    
//    @IBAction func actionCancelRequest(_ sender: Any) {
//        
//       guard let vc = R.storyboard.main.cancellationViewController() else {return}
//        vc.reasonDelagate = self
//        self.present(vc, animated: false, completion: nil)
//    }
//    
//    //MARK: CUSTOM DELEGATE AFTER CANCEL REASON
//    
//    func cancelRide(reason: String) {
//     
//    }
//    
//}
