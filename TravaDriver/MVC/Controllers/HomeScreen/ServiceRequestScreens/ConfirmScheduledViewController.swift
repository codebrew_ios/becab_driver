//
//  ConfirmScheduledViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

//MARK::  Protocol including functions when request is confirmed

protocol confirmRequestDelegate: class {
    func confirmedRequest()
}

class ConfirmScheduledViewController: UIViewController {
    
    //MARK: OUTLETS

    @IBOutlet weak var imgCustomer: UIImageView!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var lblProductDesc: UILabel!
    @IBOutlet weak var imgRating: UIImageView!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: AnimatableButton!
    
     //MARK: PROPERTIES
    
    var serviceRequest :OrdersList?
    var delegate: confirmRequestDelegate?

    
     //MARK: VIEW CONTROLLER LIFE CYCLE METHODS
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        btnConfirm.applyGradient(colours: [UIColor(displayP3Red: 180, green: 0, blue: 72, alpha: 1.0),UIColor(displayP3Red: 232, green: 0, blue: 73, alpha: 1.0)])
        self.initialSetUp()
    }
    
    //MARK: Setup Controller
    
    func initialSetUp(){
        
        // localise button texts
        self.btnConfirm.setTitle(R.string.localizable.requestConfirm(), for: .normal)
        self.btnCancel.setTitle(R.string.localizable.alertCancel(), for: .normal)
        
        customerName.text = serviceRequest?.user?.name
        
        //product descriptio check for different categories
        if (serviceRequest?.category_id == 4 || serviceRequest?.category_id == 7){
            lblProductDesc.text = " \(/serviceRequest?.brand?.name)"
        }
        else{
            lblProductDesc.text = " \(/serviceRequest?.brand?.name) × \(/serviceRequest?.payment?.product_quantity)"
        }
      
        // rating setup
        imgRating.getRatingImage(rating: "\(/serviceRequest?.user?.ratings_avg)")
        lblRateCount.text = "\(/serviceRequest?.user?.ratings_avg)"
        
        // image view setup
        let url = "\(/serviceRequest?.user?.profile_pic_url)"
        imgCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        
    }

      //MARK: BUTTON ACTIONS
    
    @IBAction func actionConfirmScheduledBooking(_ sender: Any) {
        
        APIManager.shared.request(with: ServicesEndPoint.confirm(orderId: serviceRequest?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), approved: "1")) { (response) in
            switch response{
                
            case .success(_):
                self.delegate?.confirmedRequest()

                self.dismiss(animated: true, completion: nil)
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert,alert".localized, message: /err, type: .info)
            }
        }
    }
    
    @IBAction func actionCancelScheduledBooking(_ sender: Any) {
        
        APIManager.shared.request(with: ServicesEndPoint.confirm(orderId: serviceRequest?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), approved: "0")) { (response) in
            switch response{
            case .success(_):
                
                self.delegate?.confirmedRequest()
                self.dismiss(animated: true, completion: nil)
                
                
            case .failure(let err):
                
                Alerts.shared.show(alert: "alert.alert".localized, message: /err, type: .info)
            }
        }
    }
    
    @IBAction func actionCallDriver(_ sender: UIButton) {
        let str: String = "\(/serviceRequest?.user?.phone_code )\(/serviceRequest?.user?.phone_number)"
        self.callToNumber(number: str)
        
    }
}
