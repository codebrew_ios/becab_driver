
import UIKit
import Kingfisher
import IBAnimatable
import AVFoundation
import CoreLocation
import SwiftyJSON
import AVKit

protocol SeviceRequestDelegate: class {
    func acceptedRequest()
}

struct currentPopRequest {
    static var arrAllRequest = [ServiceRequest?]()
    static var requests = [String: ServiceRequest?]()
}

class RequestModelViewController: BaseController {
    //MARK: ---------------Outlets-------------
    
    @IBOutlet weak var lblPickuploc: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var labelPackage: UILabel!
    @IBOutlet weak var imgPackage: UIImageView!
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var leftImageViewLeading: NSLayoutConstraint!
    @IBOutlet weak var rightImageViewLeading: NSLayoutConstraint!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var lblDeliveryLocationKey: UILabel!
    @IBOutlet weak var lblDropOff: UILabel!

    //MARK:-  added by heeba
    
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var lblDeliveryLocationValue: UILabel!
    @IBOutlet weak var lblCapacityKey: UILabel!
    @IBOutlet weak var lbltotalKey: UILabel!
    @IBOutlet weak var lblCapacityValue: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblCustomerRating: UILabel!
    @IBOutlet weak var lblScheduledTime: UILabel!
    
    @IBOutlet weak var leftImageView: AnimatableImageView!
    @IBOutlet weak var lblDistanceLeft: UILabel!
    @IBOutlet weak var rightImageView: AnimatableImageView!
    @IBOutlet weak var lbldurationLeft: UILabel!
    @IBOutlet weak var lblScheduledTitle: UILabel!
    @IBOutlet weak var lblScheduledTimeTitle: UILabel!
    
    @IBOutlet weak var imgViewCustomer: UIImageView!
    @IBOutlet weak var imgRating: UIImageView!
    
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: AnimatableButton!
    @IBOutlet weak var viewScheduled: UIView!
    
    @IBOutlet weak var popHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var bottomButtonStackHeight: NSLayoutConstraint!
    
    //MARK: ---------------Variables-------------
    
    var serviceRequest : ServiceRequest?
    weak var delegate: SeviceRequestDelegate?
    
    var dismissController = false
    var count = 0
    var progress :Int?
    let formatter = DateFormatter()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var timer :Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    //MARK: ---------------ViewController LifeCycle Functions-------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK:-Handle Swipe Gesture.
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToRightSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.rightImageView.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToLeftSwipeGesture(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.leftImageView.addGestureRecognizer(swipeLeft)
        
        ratingView.filledBorderColor = .black
        ratingView.emptyBorderColor = .black
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        //self.StopTextSpeechAndInvalidateTimer()
        SpeechSynthesizer.shared.stopToneAudio()
    }
    
    @objc func respondToRightSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        let swipeGesture = gesture
        switch swipeGesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            UIView.animate(withDuration: 1.0, animations: {
                self.rightImageViewLeading.constant = self.btnAccept.frame.width - self.rightImageView.frame.width
                self.view.updateConstraintsIfNeeded()
                self.view.layoutIfNeeded()
            }) { (true) in
               self.accept()
            }
        case UISwipeGestureRecognizerDirection.down:
            print("Swiped down")
            
        case UISwipeGestureRecognizerDirection.left:
            print("Swiped left")
        case UISwipeGestureRecognizerDirection.up:
            print("Swiped up")
        default:
            break
        }
        
    }
    
    @objc func respondToLeftSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        let swipeGesture = gesture
        switch swipeGesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            print("Swiped right")
        case UISwipeGestureRecognizerDirection.down:
            print("Swiped down")
        case UISwipeGestureRecognizerDirection.left:
            UIView.animate(withDuration: 1.0, animations: {
                self.leftImageViewLeading.constant = -(self.btnReject.frame.width - self.leftImageView.frame.width)
                self.view.updateConstraintsIfNeeded()
                self.view.layoutIfNeeded()
            }) { (true) in
             self.reject()
            }
        case UISwipeGestureRecognizerDirection.up:
            print("Swiped up")
        default:
            break
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//         NotificationCenter.default.addObserver(self, selector: #selector(removeView), name: NSNotification.Name(rawValue: NotificationNames.halfWay.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: NSNotification.Name(rawValue: NotificationNames.cancelled.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissViewAcceptedOther), name: NSNotification.Name(rawValue: NotificationNames.OtherAccepted.rawValue), object: nil)
        
        localiseController() // localise controller strings
        setupViewController()
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name:  NSNotification.Name(rawValue: NotificationNames.cancelled.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name:  NSNotification.Name(rawValue: NotificationNames.OtherAccepted.rawValue), object: nil)
    }
    
    //MARK: ---------------Custom Methods-------------
    
    @objc func dismissView(notification:Notification){
        
        let dic = notification.userInfo as NSDictionary?
        let orderId = dic?.value(forKey: SocketConstants.order_id) as? Int
        
        self.removeOrder(orderId: /orderId)
    }
    
    @objc func dismissViewAcceptedOther(notification:Notification){
        
        let dic = notification.userInfo as NSDictionary?
        let orderId = dic?.value(forKey: SocketConstants.order_id) as? Int
        
        Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "alerts.orderAlreadyAccepted".localized, type: .info)
        self.removeOrder(orderId: /orderId)
    }
    
    func removeOrder(orderId:Int){
        
        //self.StopTextSpeechAndInvalidateTimer()
        SpeechSynthesizer.shared.stopToneAudio()
        
        currentPopRequest.arrAllRequest =   currentPopRequest.arrAllRequest.filter(){$0?.order?.order_id != /orderId}
        
        if currentPopRequest.arrAllRequest.count == 0 {
            
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        else{
            let id = currentPopRequest.arrAllRequest.first??.order?.order_id
            if let val = currentPopRequest.requests["\(/id)"]{
                self.serviceRequest = val
                self.setupViewController()
            }
        }
    }
    
     @objc func removeView() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setupViewController(){
        
        self.btnAccept.tag = /serviceRequest?.order?.order_id
        
        /** Check If Maximum Time limit of order for driver has passed or not. If passed remove order otherwise initialise a timer with remaining time left to expire order **/
        
        let timesinceRequestArrived =  Date().timeIntervalSince(serviceRequest?.order?.resquestArrivalDateApp ?? Date())
        
        if timesinceRequestArrived > 45{
            self.removeOrder(orderId: /serviceRequest?.order?.order_id)
            return
        }
        
        count =  Int(timesinceRequestArrived)
        
        let createdAt  = /serviceRequest?.order?.payment?.created_at
        let timePast = createdAt.timeDiffrenceBetweenUTCDates(date: /createdAt)
        
        let timeRemain = 45 - timePast
        
        if timePast < 45 && timeRemain > 0{
            APIConstants.timerProgress = timeRemain
            
        }
        
        self.registerBackgroundTask()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector( self.update(timeRemaining:)), userInfo: nil, repeats: true)
        
        self.updateDistanceAndPopupConstaints() // update request distance and pop up height constarints
        
        self.SetRequestedProductDetails()
        
        self.UpdateCustomerDetails()
        
        //kirti
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.00) {
            //self.StartTextSpeech()
            SpeechSynthesizer.shared.playSound()
        }
        
        
        
        
        
    }
    
    func StartTextSpeech(){
        
        let audioSession = AVAudioSession.sharedInstance()
        try? audioSession.setCategory(AVAudioSessionCategoryPlayback, with: .duckOthers)
        
        // Speech  request text over siri
        
        SpeechSynthesizer.shared.speakTextOverVoice(text: "\(/serviceRequest?.order?.user?.name) has requested \(/serviceRequest?.order?.product?.name) at \(/lblPickuploc.text) with payment mode \(/serviceRequest?.order?.payment?.payment_type)")
    }
    
    func StopTextSpeechAndInvalidateTimer(){
        
        SpeechSynthesizer.shared.stopSpeaking()
        SpeechSynthesizer.shared.stopToneAudio()
        
        if timer != nil{
            timer?.invalidate()
            self.count = 0
        }
    }
    
    func SetRequestedProductDetails(){
        
        if serviceRequest?.order?.payment?.final_charge_string == nil{
            lblTotalValue.text = "\(serviceRequest?.order?.payment?.final_charge ?? 0.0 ) \(R.string.localizable.currency())"
        }
        else{
            lblTotalValue.text = "\(serviceRequest?.order?.payment?.final_charge_string ?? "0.0" ) \(R.string.localizable.currency())"
        }
        self.lblCapacityKey.text =  R.string.localizable.requestCapacity()
        self.lblDeliveryLocationKey.text = "\(R.string.localizable.requestPickuplocation())"
        lblDropOff.text = R.string.localizable.bookingDropOff()
        
        if (serviceRequest?.order?.product?.category_id == 1 ||  serviceRequest?.order?.product?.category_id == 3) {
            lblCapacityValue.text = "\(/serviceRequest?.order?.product?.name) × \(/serviceRequest?.order?.payment?.product_quantity)"
        } else if (serviceRequest?.order?.product?.category_id == 7) {
            lblCapacityKey.text =  R.string.localizable.requestService()
            lblCapacityKey.text =  R.string.localizable.requestService()
            lblDeliveryLocationKey.text = R.string.localizable.requestPickuplocation()
            lblDropOff.text = R.string.localizable.bookingDropOff()
             
            lblCapacityValue.text = "\(/serviceRequest?.order?.product?.categoryName)"
        } else {
            lblCapacityValue.text = "\(/serviceRequest?.order?.product?.brand_name) , \(/serviceRequest?.order?.product?.name) × \(/serviceRequest?.order?.payment?.product_quantity)"
        }
    }
    
    func UpdateCustomerDetails(){
        if self.serviceRequest?.order?.bookingType == "Package" {
            self.btnViewDetail.isHidden = false
            self.btnViewDetail.isUserInteractionEnabled = true
            self.imgPackage.isHidden = false
            self.labelPackage.isHidden = false
        }else{
            self.btnViewDetail.isUserInteractionEnabled = false
            self.btnViewDetail.isHidden = true
            self.imgPackage.isHidden = true
            self.labelPackage.isHidden = true
        }
        // update image profile
        if self.serviceRequest?.order?.bookingType == "Friend" {
            let url = "\(/serviceRequest?.order?.friendPhoneNumber)"
            imgViewCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
            lblCustomerName.text = "\(/serviceRequest?.order?.friendName)"
            lblCustomerRating.text = "\(/serviceRequest?.order?.user?.ratings_avg)"
            imgRating.getRatingImage(rating: "\(/serviceRequest?.order?.user?.ratings_avg)")
            ratingView.rating = Double(serviceRequest?.order?.user?.ratings_avg ?? "0") ?? 0.0
            ratingView.isHidden = true
            lblCustomerRating.isHidden = true
            lblDeliveryLocationValue.text = serviceRequest?.order?.dropoff_address
            lblPickuploc.text = serviceRequest?.order?.pickup_address
            
        }else{
            let url = "\(/serviceRequest?.order?.user?.profile_pic_url)"
            imgViewCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
            ratingView.rating = Double(serviceRequest?.order?.user?.ratings_avg ?? "0") ?? 0.0
            ratingView.isHidden = false
             lblCustomerRating.isHidden = false
            lblCustomerName.text = "\(/serviceRequest?.order?.user?.name)"
            lblCustomerRating.text = "\(/serviceRequest?.order?.user?.ratings_avg)"
            imgRating.getRatingImage(rating: "\(/serviceRequest?.order?.user?.ratings_avg)")
            lblDeliveryLocationValue.text = serviceRequest?.order?.dropoff_address
            lblPickuploc.text = serviceRequest?.order?.pickup_address
            //MARK:-  added by heeba
            lblPaymentType.text = " ordenado vía: \(/serviceRequest?.order?.payment?.payment_type) "
            print("\(serviceRequest?.order?.payment?.payment_type) paymentTYOPe ")
        }
    }
    
    func updateDistanceAndPopupConstaints(){
        
        let destinationCoordinate = CLLocationCoordinate2D.init(latitude: /serviceRequest?.order?.dropoff_latitude, longitude: /serviceRequest?.order?.dropoff_longitude)
        
        self.calculateTimeAndDistance(from: LocationManager.shared.currentLoc?.coordinate, to: destinationCoordinate)
        
        //set localised streing
        
        if serviceRequest?.order?.future == "1"{
            self.lblScheduledTime.text = self.UTCToLocal(date: /serviceRequest?.order?.order_timings)
            viewScheduled.isHidden = false
            //   popHeightConstarint.constant = 450
            //    bottomButtonStackHeight.constant = 156
        }
        else{
            //      popHeightConstarint.constant = 385
            viewScheduled.isHidden = true
            //   bottomButtonStackHeight.constant = 115
        }
    }
    
    // localise strings
    
    func localiseController(){
        
        //        self.lblDeliveryLocationKey.text = R.string.localizable.requestLocation()
        //        self.lblCapacityKey.text =  R.string.localizable.requestCapacity()
        self.lbltotalKey.text = R.string.localizable.requestTotal()
        self.btnAccept.setTitle(R.string.localizable.requestAccept(), for: .normal)
        self.btnReject.setTitle(R.string.localizable.requestReject(), for: .normal)
        self.lblScheduledTitle.text = R.string.localizable.requestScheduledServiceTitle()
        self.lblScheduledTimeTitle.text = R.string.localizable.requestScheduledTime()
        
    }
    
    //MARK:--- Timer selector method
    
    @objc func update(timeRemaining:Int) {
        if(count < /APIConstants.timerProgress) {
            let fractionalProgress = Float(count) / Float(/APIConstants.timerProgress)
            progressView.setProgress(fractionalProgress, animated: true)
            count += 1
        }
        else{
            // remove order after (APIConstants.timerProgress) limit is crossed
            
            self.removeOrder(orderId: /self.serviceRequest?.order?.order_id)
        }
    }
    
    //MARK:-- Calculate fair
    
    func calculateFair() -> String{
        
        // Calculation of Base price
        var base_price = /Double(/serviceRequest?.order?.payment?.product_quantity) * /Double(/serviceRequest?.order?.payment?.product_per_quantity_charge)
        
        if base_price == 0{
            base_price = /Double(/serviceRequest?.order?.payment?.product_quantity) * /Double(/serviceRequest?.order?.payment?.product_per_quantity_charge_str)
        }
        
        // Additional Charges
        let product_distance_charge = /Double(/serviceRequest?.order?.payment?.product_per_distance_charge)
        let product_weight_charge = /Double(/serviceRequest?.order?.payment?.product_per_weight_charge)
        let product_alpha_charge = /Double(/serviceRequest?.order?.payment?.product_alpha_charge_int)
        let total_charge = base_price + product_distance_charge + product_weight_charge + product_alpha_charge
        
        let buraqPercentage = serviceRequest?.order?.payment?.buraq_percentage
        var share = 0.0
        
        if let anInt = buraqPercentage as? Int{
            share  = Double(anInt)/100 * total_charge
        }
        if let aDouble = buraqPercentage as? Double{
            share  = aDouble/100 * total_charge
        }
        if let aString = buraqPercentage as? String {
            share  = /Double(aString)/100 * total_charge
        }
        
        let Final = total_charge + share
        
        return "\(/Final.getTwoDecimalFloat())"
    }
    
    //MARK: ---------------Button Actions-------------
    
    @IBAction func viewPackageDetailScreen(_ sender: UIButton) {
        guard let vc = R.storyboard.main.ridePackageDetailController() else {return}
        vc.serviceRequest = self.serviceRequest
      self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func actionAcceptRequest(_ sender: UIButton) {
        self.accept()
    }
    
    func accept() {
        SpeechSynthesizer.shared.stopToneAudio()
        
        APIManager.shared.request(with: ServicesEndPoint.accept(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) { [weak self] (response) in
            
            switch response{
            case .success(let responseValue):
                
                let val = responseValue as? NSDictionary
                let result = val?.value(forKey: "result") as? NSDictionary
                let crequest = result?.value(forKey: "CRequest") as? NSDictionary
                let order_request_status = crequest?.value(forKey: "order_request_status") as? String
                
                let userDetailId = crequest?.value(forKey: "driver_user_detail_id") as? Int
                
                if order_request_status == SocketConstants.Accepted && userDetailId == UserSingleton.shared.loggedInUser?.data?.user_detail_id{
                    
                    if self?.serviceRequest?.order?.future == "1"{
                        self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
                        Alerts.shared.show(alert: "alert.alert".localized, message: R.string.localizable.requestScheduledSuccessFully(), type: .info)
                    }
                    else{
                        
                        self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
                        self?.delegate?.acceptedRequest()
                    }
                }
                else{
                    Alerts.shared.show(alert: "alert.alert".localized, message: R.string.localizable.alertsOrderAlreadyAccepted(), type: .info)
                    self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
                }
                
            case .failure(let err):
                
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
                self?.rightImageViewLeading.constant = 0
                //                self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
            }
        }
    }
    
    @IBAction func actionRejectRequest(_ sender: UIButton) {
        self.reject()
        
    }
    
    func reject() {
        APIManager.shared.request(with: ServicesEndPoint.reject(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) {[weak self] (response) in
            switch response{
            case .success(let responseValue):
                self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
                self?.leftImageViewLeading.constant = 0
            }
        }
    }
        //MARK: ---------------To dismiss service request view when tapped outside-------------
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view == self.view {
            //            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func calculateTimeAndDistance(from source: CLLocationCoordinate2D? , to destination: CLLocationCoordinate2D?){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let selectedLanguage = UserDefaults.standard.value(forKey: "language") as? String
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&language=\(/selectedLanguage)&mode=driving&key=\(APIConstants.googleApiKey)")!
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
            }else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        DispatchQueue.global(qos: .userInteractive).async { [weak self] () -> Void in
                            guard let routes = json["routes"] as? NSArray else {
                                return
                            }
                            
                            if (routes.count > 0) {
                                
                                guard let legs  = routes[0] as? NSDictionary , let legsJ = legs["legs"] as? NSArray , let lg = legsJ[0] as? NSDictionary else { return }
                                
                                let duration = lg["duration"] as? NSDictionary
                                let durationLeft1 = duration?.object(forKey: "text") as? String
                                
                                let distance = lg["distance"] as? NSDictionary
                                let distanceLeft1 = distance?.object(forKey: "text") as? String
                                DispatchQueue.main.async {
                                    self?.lbldurationLeft.text = "\(/durationLeft1)"
                                    self?.lblDistanceLeft.text = "\(/distanceLeft1)"
                                }
                            }
                        }
                    }
                }catch {
                    debugPrint("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
    }
}
