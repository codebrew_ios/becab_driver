
//
//  CancellationViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

//MARK::  Protocol including functions when request is cancelled

protocol reasonCancellation:class {
    func cancelRide(reason:String)
}

class CancellationViewController: UIViewController, UITextViewDelegate {
    
    //MARK: OUTLETS
    
    @IBOutlet weak var tvReason: AnimatableTextView!
    @IBOutlet weak var lblCancelInfo: UILabel!
    @IBOutlet weak var btnSubmit: AnimatableButton!
    @IBOutlet weak var viewContainer: AnimatableView!
    @IBOutlet weak var txtViewCencellationReason: AnimatableTextView!
    //MARK: PROPERTIES
    weak var reasonDelagate:reasonCancellation?
    var orderID:String?
    
    var cancellationReason = ["User never appeared".localized, "Impossible to contact the user".localized, "More than 5 minutes have passed".localized, "Suspicious activity".localized, "I had an accident".localized, "Other...".localized]
    
    var myPickerView : UIPickerView!
    
    //MARK: VIEW CONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainer.isHidden = true
        
        self.localiseController() // localise controller components
        self.pickUp(tvReason)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options:[.curveEaseOut], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            self.viewContainer.isHidden = false
            
        }, completion:nil)
    }
    
    //MARK: CUSTOM METHODS
    
    
    func localiseController(){
        
        self.btnSubmit.setTitle(R.string.localizable.contactusSubmit(), for: .normal)
        self.lblCancelInfo.text = "request.cancelTitle".localized
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic {
            tvReason.text = R.string.localizable.requestCancelCommentPlaceholder()
        }
        else if BundleLocalization.sharedInstance().language == Languages.Urdu{
            tvReason.text = R.string.localizable.requestCancelCommentPlaceholder()
        }
        else {
            tvReason.placeholderText = R.string.localizable.requestCancelCommentPlaceholder()
        }
    }
    
    //MARK: ---------------To dismiss service request view when tapped outside-------------
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view == self.view {
            self.viewContainer.isHidden = true
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    //MARK:: Textview Delegates
    
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text == R.string.localizable.requestCancelCommentPlaceholder() {
//            textView.text = ""
//        }
//    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text == ""{
//            
//            if BundleLocalization.sharedInstance().language == Languages.Arabic {
//                tvReason.text = R.string.localizable.requestCancelCommentPlaceholder()
//            }
//            else if BundleLocalization.sharedInstance().language == Languages.Urdu{
//                tvReason.text =  R.string.localizable.requestCancelCommentPlaceholder()
//            }
//            else {
//                tvReason.placeholderText = R.string.localizable.requestCancelCommentPlaceholder()
//            }
//        }
    }
    
    //MARK: ACTION METHODS
    @IBAction func actionCancel(_ sender: Any) {
        
        if tvReason.text == ""{
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "alert.cancellation".localized, type: .info)
            return
        }
        
        APIManager.shared.request(with: ServicesEndPoint.cancel(orderId: Int(/orderID), cancelReason: tvReason.text)) {
            [weak self] (response) in
            switch response{
                
            case .success(_):
                
                self?.reasonDelagate?.cancelRide(reason: /self?.tvReason.text)
                self?.view.backgroundColor = UIColor.clear
                self?.viewContainer.isHidden = true
                self?.dismiss(animated: false, completion: nil)
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
}


extension CancellationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickUp(_ textField : UITextView){

        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView

        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(CancellationViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(CancellationViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar

    }
    
    
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cancellationReason.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cancellationReason[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tvReason.text = cancellationReason[row]
        
        print("c",cancellationReason[row] )
        print("o","Other...".localized )
        
        if cancellationReason[row] == "Other...".localized {
            txtViewCencellationReason.isHidden = false
        } else {
            txtViewCencellationReason.isHidden = true
        }
        
    }
    //MARK:- TextFiled Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.pickUp(tvReason)
    }
    
    
    
    @objc func doneClick() {
         tvReason.resignFirstResponder()
     }
    @objc func cancelClick() {
         tvReason.resignFirstResponder()
     }
    
    
    
}


