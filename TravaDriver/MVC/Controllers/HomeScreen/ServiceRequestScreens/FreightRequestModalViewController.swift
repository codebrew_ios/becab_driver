//
//  FreightRequestModalViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 05/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import Kingfisher
import AVFoundation
import CoreLocation
import SwiftyJSON
import GoogleMaps

struct freightPolyLineHelper {
    
    static var orderID = 0
    static var polyline:GMSPolyline = GMSPolyline()
    
}

class FreightRequestModalViewController: BaseController {
    
    
    //MARK:: OUTLETS
    
    @IBOutlet weak var lblPickupLocationKey: UILabel!
    @IBOutlet weak var lblPickUpLocationValue: UILabel!
    @IBOutlet weak var lblDropOffLocationKey: UILabel!
    @IBOutlet weak var lblDropOffLocationValue: UILabel!
    
    @IBOutlet weak var lblCapacityKey: UILabel!
    @IBOutlet weak var lblCapacityValue: UILabel!
    @IBOutlet weak var lblTotalKey: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btReject: AnimatableButton!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNoImages: UILabel!
    
    //customer'
    @IBOutlet weak var lblCustomerNAme: UILabel!
    @IBOutlet weak var imgViewRating: UIImageView!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var imgCustomerImage: AnimatableImageView!
    
    // scheduled view
    @IBOutlet weak var lblScheduleTitle: UILabel!
    @IBOutlet weak var lblScheduleTitleTime: UILabel!
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var viewScheduled: AnimatableView!
    @IBOutlet weak var lblDistanceleft: UILabel!
    @IBOutlet weak var lblDurationLeft: UILabel!
    
    //constarint
    
    @IBOutlet weak var popHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var btnAcceptHeight: NSLayoutConstraint!
    @IBOutlet weak var btnRejectHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomConstaraintReject: NSLayoutConstraint!
    @IBOutlet weak var scheduledViewHeightCOnstaraint: NSLayoutConstraint!
    
    //MARK:: PROPRETIES
    
    var dataSource : CollectionViewDataSource? {
        didSet{
            collectionView.delegate = dataSource
            collectionView.dataSource = dataSource
            collectionView.reloadData()
        }
    }
    var serviceRequest : ServiceRequest?
    weak var delegate: SeviceRequestDelegate?
    var count = 0
    var progress :Int?
    let formatter = DateFormatter()
    var polyline: GMSPolyline?

    
    //MARK:: VIEW CONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SpeechSynthesizer.shared.stopSpeaking()
        SpeechSynthesizer.shared.stopToneAudio()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    
    //MARK: CUSTOM METHODS
    
    //MARK: ---------------Custom Methods-------------
    
    func setupViewController(){
        
        //schedule timer to dismiss  this controller after time
        var _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector( self.update), userInfo: nil, repeats: true)
        
        self.calculateDistanceTimeAndPopUpConstraints() // update distance,time and pop up height
        self.localiseController() // localise view controller content
        self.configureCollectionView()  // configure collection view
        
        // update  name and address details
        
        lblCapacityValue.text = "\(/serviceRequest?.order?.product?.brand_name) , \(/serviceRequest?.order?.product?.name)"
        lblCustomerNAme.text = /serviceRequest?.order?.user?.name
        lblRateCount.text = "\(/serviceRequest?.order?.user?.ratings_avg)"
        imgViewRating.getRatingImage(rating: "\(/serviceRequest?.order?.user?.ratings_avg)")
        self.lblPickUpLocationValue.text = "\(/serviceRequest?.order?.pickup_address)"
        self.lblDropOffLocationValue.text = "\(/serviceRequest?.order?.dropoff_address)"
        
        //Calculate fair
        
        let _ = self.calculateFair()
        
        if serviceRequest?.order?.payment?.final_charge == 0.0{
            
            let valDouble = Double(/serviceRequest?.order?.payment?.final_charge_string)?.getTwoDecimalFloat()
            self.lblTotalValue.text = "\(/valDouble) \(R.string.localizable.currency())"
        }
        else{
              self.lblTotalValue.text = "\(/serviceRequest?.order?.payment?.final_charge?.getTwoDecimalFloat()) \(R.string.localizable.currency())"
        }
        
        
        // Speech  request text over siri
        
//        SpeechSynthesizer.shared.speakTextOverVoice(text: "\(/lblCustomerNAme.text) has requested \(/serviceRequest?.order?.product?.name) at \(/lblPickUpLocationValue.text) to be dropped at \(/lblDropOffLocationValue.text)")
//
        // update image profile
        
        let url = "\(/serviceRequest?.order?.user?.profile_pic_url)"
        imgCustomerImage.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    //calculate distance time and update popup constarints
    
    func calculateDistanceTimeAndPopUpConstraints(){
        
        let serPickupCoordinate = CLLocationCoordinate2D(latitude: /serviceRequest?.order?.pickup_latitude, longitude: /serviceRequest?.order?.pickup_longitude)
        
        let serDropOffCoordinate = CLLocationCoordinate2D(latitude: /serviceRequest?.order?.dropoff_latitude, longitude: /serviceRequest?.order?.dropoff_longitude)
        
        calculateTimeAndDistance(from: serPickupCoordinate, to: serDropOffCoordinate)
        
        //set localised streing
        
        if serviceRequest?.order?.future == "1"{
            self.lblOrderTime.text = self.UTCToLocal(date: /serviceRequest?.order?.order_timings)
            viewScheduled.isHidden = false
            popHeightConstarint.constant = 540
            bottomConstaraintReject.constant = 8
            scheduledViewHeightCOnstaraint.constant = 45
        }
        else{
            popHeightConstarint.constant = 520
            btnAcceptHeight.constant = 48
            btnRejectHeight.constant = 48
            bottomConstaraintReject.constant = 28
            scheduledViewHeightCOnstaraint.constant = 0
            viewScheduled.isHidden = true
        }
    }
    
    // localise controller view
    func localiseController(){
        
        self.btnAccept.setTitle(R.string.localizable.requestAccept(), for: .normal)
        self.btReject.setTitle(R.string.localizable.requestReject(), for: .normal)
        self.lblPickupLocationKey.text = R.string.localizable.requestPickuplocation()
        self.lblDropOffLocationKey.text = "request.location".localized
        
        if serviceRequest?.order?.details == ""{
            self.lblDesc.text = R.string.localizable.requestDetailsNotAvailable()
        }
        else{
            self.lblDesc.text = /serviceRequest?.order?.details
        }
    }
    
    func calculateFair() -> String{
        
        // Calculation of Base price
        var base_price =  /Double(/serviceRequest?.order?.payment?.product_per_quantity_charge) * /Double(/serviceRequest?.order?.payment?.product_quantity)
        
        if base_price == 0{
            base_price = /Double(/serviceRequest?.order?.payment?.product_per_quantity_charge_str) * /Double(/serviceRequest?.order?.payment?.product_quantity)
        }
        
        // Additional Charges
        
        var product_distance_charge = /Double(/serviceRequest?.order?.payment?.product_per_distance_charge)
        
        var distance = ""
        if serviceRequest?.order?.payment?.order_distance == nil{
            distance = "\(/serviceRequest?.order?.payment?.order_distance_Int)"
        }
        else{
            distance = "\(/serviceRequest?.order?.payment?.order_distance)"
        }
        
        if distance != "0"{
            product_distance_charge = /Double(/serviceRequest?.order?.payment?.product_per_distance_charge) *  /Double(distance)
        }
        
        let product_weight_charge = /Double(/serviceRequest?.order?.payment?.product_per_weight_charge)
        var product_alpha_charge = /Double(/serviceRequest?.order?.payment?.product_alpha_charge)
        
        if product_alpha_charge == 0{
            product_alpha_charge = Double(/serviceRequest?.order?.payment?.product_alpha_charge_int)
        }
        
        let total_charge =   product_distance_charge + product_weight_charge + product_alpha_charge
        
        return "\(/total_charge)"
    }
    
    
    //MARK:--- Timer selector method
    
    @objc func update() {
        if(count < /APIConstants.timerProgress) {
            let fractionalProgress = Float(count) / Float(/APIConstants.timerProgress)
            progressView.setProgress(fractionalProgress, animated: true)
            count += 1
        }
        else{
            self.dismiss(animated: true) {
            }
        }
    }
    
    //MARK:-- CONFIGURE CELL
    
    func configureCollectionView(){
        
        dataSource = CollectionViewDataSource.init(items: serviceRequest?.order?.order_images, collectionView: collectionView, cellIdentifier: R.reuseIdentifier.freightCollectionViewCell.description, headerIdentifier: "", cellHeight: 48.0, cellWidth: 48.0 , configureCellBlock: {[weak self] (cell, item, indexPath) in
            
            let object = self?.serviceRequest?.order?.order_images?[indexPath.row]
            (cell as? FreightCollectionViewCell)?.item = object
            
            }, aRowSelectedListener: { (indexPath, item) in
                debugPrint(indexPath)
                
        }, willDisplayCell: { (indexpath) in
            debugPrint(indexpath)
        }, scrollViewDelegate: { (scrollView) in
            debugPrint(scrollView)
        },scrollViewDecelerate:{ (scrollView) in
            debugPrint(scrollView)
        })
        
        collectionView.reloadData()
        
        if serviceRequest?.order?.order_images?.count == 0{
            collectionView.isHidden = true
            lblNoImages.text = "Images are not available."
        }
    }
    
    //MARK:: Calculate disatance and time eta between two coordinates
    
    func calculateTimeAndDistance(from source: CLLocationCoordinate2D? , to destination: CLLocationCoordinate2D?){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving&key=\(APIConstants.googleApiKey)")!
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
            }else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        DispatchQueue.main.async {
                            [weak self] in
                            guard let routes = json["routes"] as? NSArray else {
                                return
                            }
                            
                            if (routes.count > 0) {
                                
                                let overview_polyline = routes[0] as? NSDictionary
                                let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                guard let points = dictPolyline?.object(forKey: "points") as? String else { return }
                                self?.getStartToEndPath(polyStr: points)
                                
                                guard let legs  = routes[0] as? NSDictionary , let legsJ = legs["legs"] as? NSArray , let lg = legsJ[0] as? NSDictionary else { return }
                                let duration = lg["duration"] as? NSDictionary
                                let durationLeft1 = duration?.object(forKey: "text") as? String
                                
                                let distance = lg["distance"] as? NSDictionary
                                let distanceLeft1 = distance?.object(forKey: "text") as? String
                                self?.lblDurationLeft.text = "\(/durationLeft1)"
                                self?.lblDistanceleft.text = "\(/distanceLeft1)"
                                
                            }
                        }
                    }
                }catch {
                    debugPrint("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    func getStartToEndPath(polyStr :String){
        let path = GMSMutablePath(fromEncodedPath: polyStr)
        polyline = GMSPolyline(path: path)
        freightPolyLineHelper.orderID = /self.serviceRequest?.order?.order_id
        if let val = polyline{
        freightPolyLineHelper.polyline = val
        }
        
    }
    
    //MARK: ACTION METHODS
    
    @IBAction func actionAcceptRequest(_ sender: Any) {
        SpeechSynthesizer.shared.stopToneAudio()
        
        APIManager.shared.request(with: ServicesEndPoint.accept(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) { [weak self] (response) in
            
            switch response{
                
            case .success(_):
                
                if self?.serviceRequest?.order?.future == "1"{
                    
                    self?.dismiss(animated: true, completion: nil)
                    self?.dismiss(animated: true, completion: {
                        Alerts.shared.show(alert: "alert.alert".localized, message: "Your ride has been successfully scheduled.", type: .info)
                    })
                    
                }
                else{
                    self?.dismiss(animated: true, completion: {
                        self?.delegate?.acceptedRequest()
                    })
                }
                
            case .failure(let err):
                
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
    
    @IBAction func actionCancelRequest(_ sender: Any) {
        SpeechSynthesizer.shared.stopToneAudio()

                APIManager.shared.request(with: ServicesEndPoint.reject(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) {[weak self] (response) in
            
            self?.dismiss(animated: true, completion: nil)
            
        }
    }
}
