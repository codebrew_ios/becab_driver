//
//  RateRideViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 28/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
protocol RateCompletedDeliveryOrderDelegate: class {
    func deliveryRatingCompleted(rating:Int,comment:String)
}
protocol RateCompletedDeliveryDelegate: class {
    func DeliveryRatingCompleted()
}

class RateRideViewController: UIViewController, UITextViewDelegate {
    
    // Properties
    
    var completedRequest : CompletedOrderModel?
    var delegate : RateCompletedDeliveryDelegate?
    var delegateOrder :  RateCompletedDeliveryOrderDelegate?
    var rateAtServer = 0
    
    //MARK:-- Outlets
    
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    
    @IBOutlet weak var imgViewCustomer: AnimatableImageView!
    @IBOutlet weak var tvComments: AnimatableTextView!
    
    @IBOutlet weak var btnSubmitRating: UIButton!
    @IBOutlet var rateButtons: [UIButton]!
    @IBOutlet var rateTexts: [UILabel]!
    
    @IBOutlet weak var btnRate1: UIButton!
    @IBOutlet weak var btnRate2: UIButton!
    @IBOutlet weak var btnRate3: UIButton!
    @IBOutlet weak var btnRate4: UIButton!
    @IBOutlet weak var btnRate5: UIButton!
    
    let arrRateTexts = ["rate.1".localized,"rate.2".localized,"rate.3".localized,"rate.4".localized,"rate.5".localized]
    
    //MARK:-- View controller life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initialSetup()
        
    }
    
    //MARK:-- CUSTOM METHODS
    
    
    func initialSetup(){
        
        self.lblRate.text = "rate.header".localized
        self.btnSubmitRating.setTitle("rate.submit".localized, for: .normal)
        let url = "\(/completedRequest?.result?.CustomerDetails?.profile_pic_url)"
        self.imgViewCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblCustomerName.text = "\(/completedRequest?.result?.Customer?.name)"
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic {
            tvComments.text = R.string.localizable.rateTvPlaceholder()
        }
        else if BundleLocalization.sharedInstance().language == Languages.Urdu{
            tvComments.text =  R.string.localizable.rateTvPlaceholder()
        }
        else {
            tvComments.placeholderText = R.string.localizable.rateTvPlaceholder()
        }
        
        for item in rateTexts{
            item.text = " "
        }
    }
    
    
    //MARK: Button Actions:--
    
    @IBAction func actionSubmitRating(_ sender: Any) {
        
        if rateAtServer == 0 {
            Alerts.shared.show(alert: "alert.alert".localized, message: "rate.first".localized, type: .info)
            return
        }
        
        var comment = tvComments.text!
        
        if comment  ==  "rate.tvPlaceholder".localized{
            comment = ""
        }
        
        APIManager.shared.request(with: ServicesEndPoint.rate(orderId: /(completedRequest?.result?.order_id), rating: rateAtServer, comments: comment)) { [weak self] (response) in
            switch response{
            case .success(_):
                
                self?.dismiss(animated: true, completion: {
                    self?.delegate?.DeliveryRatingCompleted()
                    self?.delegateOrder?.deliveryRatingCompleted(rating: self?.rateAtServer ?? 0, comment: comment)
                })
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .error)
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // deselect other Images /hide rate text
    
    func deselectOtherImagesExceptPassedIndex(indxSelected:Int){
        
        for (indx,item) in rateButtons.enumerated(){
            
            if indx <= indxSelected{
                rateButtons[indx].isSelected = true
                
                if indxSelected == indx {
                    rateTexts[indx].text  =  arrRateTexts[indxSelected]
                } else {
                    rateTexts[indx].text  =  " "
                }
                
            }
            else{
                rateTexts[indx].text =  " "
                rateButtons[indx].isSelected = false
            }
        }
    }
    
    //rate button actions
    @IBAction func actionRateButton(_ sender: UIButton) {
        
        rateAtServer = 0
        rateAtServer = sender.tag + 1
        
        btnRate1.isSelected = false
        btnRate2.isSelected = false
        btnRate3.isSelected = false
        btnRate4.isSelected = false
        btnRate5.isSelected = false
        rateTexts[0].text =  " "
        rateTexts[1].text =  " "
        rateTexts[2].text =  " "
        rateTexts[3].text =  " "
        rateTexts[4].text =  " "
        
        switch sender.tag {
        case 0:
            btnRate1.isSelected = true
            rateTexts[0].text =  arrRateTexts[0]
            //deselectOtherImagesExceptPassedIndex(indxSelected: 0)
        case 1:
            btnRate1.isSelected = true
            btnRate2.isSelected = true
            rateTexts[1].text =  arrRateTexts[1]
            //deselectOtherImagesExceptPassedIndex(indxSelected: 1)
            
        case 2:
            btnRate1.isSelected = true
            btnRate2.isSelected = true
            btnRate3.isSelected = true
            rateTexts[2].text =  arrRateTexts[2]
            //deselectOtherImagesExceptPassedIndex(indxSelected: 2)
            
        case 3:
            btnRate1.isSelected = true
            btnRate2.isSelected = true
            btnRate3.isSelected = true
            btnRate4.isSelected = true
            rateTexts[3].text =  arrRateTexts[3]
           // deselectOtherImagesExceptPassedIndex(indxSelected: 3)
            
        case 4:
            btnRate1.isSelected = true
            btnRate2.isSelected = true
            btnRate3.isSelected = true
            btnRate4.isSelected = true
            btnRate5.isSelected = true
            rateTexts[4].text =  arrRateTexts[4]
            //deselectOtherImagesExceptPassedIndex(indxSelected: 4)
            
        default:
            debugPrint("")
        }
    }
    
    //MARK::  TEXTVIEW DELEGATES
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == R.string.localizable.rateTvPlaceholder() {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            if BundleLocalization.sharedInstance().language == Languages.Arabic {
                tvComments.text = R.string.localizable.rateTvPlaceholder()
            }
            else if BundleLocalization.sharedInstance().language == Languages.Urdu{
                tvComments.text =  R.string.localizable.rateTvPlaceholder()
            }
            else {
                tvComments.placeholderText = R.string.localizable.rateTvPlaceholder()
            }
        }
    }
    
    //MARK:-IBOutlets.
    @IBAction func skipNowButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
