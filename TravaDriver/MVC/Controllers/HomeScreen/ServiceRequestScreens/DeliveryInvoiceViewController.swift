//
//  DeliveryInvoiceViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 28/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import ObjectMapper

//MARK::  Protocol including functions when orderinvoice in processes

protocol OnGoingInvoiceDelegate: class {
    func RequestInvoiceProcessed()
}

class DeliveryInvoiceViewController: BaseController {
    
    //MARK: -------------Outlets-------------
    //MARK:-Additional Charges Labels.
    @IBOutlet weak var productNameLabel: UILabel!
//    @IBOutlet weak var invoiceView: UIView!
    @IBOutlet weak var baseFareHeadingLabel: UILabel!
    @IBOutlet weak var lblServiceType: UILabel!
    @IBOutlet weak var baseFareLabel: UILabel!
//    @IBOutlet weak var rideTimeLabel: UILabel!
//    @IBOutlet weak var rideDistanceLabel: UILabel!
//    @IBOutlet weak var normalFareLabel: UILabel!
//    @IBOutlet weak var bookingFeeLabel: UILabel!
//    @IBOutlet weak var serviceChargeLabel: UILabel!
//    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
//    @IBOutlet weak var timeHeadingLabel: UILabel!
//    @IBOutlet weak var distanceHeadingLabel: UILabel!
    //MARK:- added by heeba
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var amountCollectedHeaderLabel: UILabel!
//    @IBOutlet weak var previousInvoiceView: UIView!
//    @IBOutlet weak var currentChargesLabel: UILabel!
//    @IBOutlet weak var previousDriverFareLabel: UILabel!
//    @IBOutlet weak var totalFareLabel: UILabel!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblBaseFair: UILabel!
    @IBOutlet weak var lblBasefairValue: UILabel!
    @IBOutlet weak var lblTaxType1: UILabel!
    @IBOutlet weak var lblTaxType1Value: UILabel!
    
    @IBOutlet weak var lbltaxType2: UILabel!
    @IBOutlet weak var lbltaxType2Value: UILabel!
    
    @IBOutlet weak var showInvoice: UIStackView!
    @IBOutlet weak var lbltaxType3: UILabel!
    @IBOutlet weak var lbltaxType3Value: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblInvoiceHeader: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    
    @IBOutlet weak var row1: UIView!
    @IBOutlet weak var row2: UIView!
    @IBOutlet weak var row3: UIView!
    @IBOutlet weak var row4: UIView!
    @IBOutlet weak var row5: UIView!
    
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTimeValue: UILabel!
    @IBOutlet weak var lblTimeHeading: UILabel!
    
    @IBOutlet weak var lblDistanceHeading: UILabel!
    @IBOutlet weak var lblDistanceValue: UILabel!
    @IBOutlet weak var lblNormalFare: UILabel!
    @IBOutlet weak var lblDicountStatic: UILabel!
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblWaitingCharges: UILabel!
    
    @IBOutlet weak var lblServiceCharge: UILabel!
    @IBOutlet weak var lblBookingFee: UILabel!
    
    
    
    
    @IBOutlet weak var lblWaitingChargeStatic: UILabel!
    @IBOutlet weak var lblserviceChargeStatic: UILabel!
    @IBOutlet weak var lblBookingFeeStatic: UILabel!
    
    var isWaterCouponInvoice : Bool?{
        didSet{
            //                row3.isHidden = /isWaterCouponInvoice
            //                row4.isHidden = /isWaterCouponInvoice
            //                row5.isHidden = /isWaterCouponInvoice
        }
    }
    
    var orderId  = 0
    
    //MARK: -------------Properties-------------
    
    var amountHeaderLabel = "Amount to be collected"
    var amountAnotherDriverHeaderLabel = "Amount to be collected by another driver"
    var breakDown:Bool = false
    var completedRequest : CompletedOrderModel?
    weak var delegate: OnGoingInvoiceDelegate?
    
    //MARK: -------------View controller life cycle-------------
    override func viewDidLoad() {
        amountCollectedHeaderLabel.text = "AmountTobe".localized //\(/completedRequest?.result?.payment_type)"
        //"AmountTobe".localized + "via:" +  "\(/completedRequest?.result?.payment_type)"
        super.viewDidLoad()
        self.initialSetupUI()
        
    }
    
    //MARK: -------------Custom Methods-------------
    func initialSetupUI(){
        lblPaymentType.text = /completedRequest?.result?.payment_type
        lblWaitingChargeStatic.text = "lblWaitingChargeStatic".localized
        lblserviceChargeStatic.text = "lblserviceChargeStatic".localized
        lblBookingFeeStatic.text = "lblBookingFeeStatic".localized
        lblDicountStatic.text = "discountvalue".localized
        lblDistanceValue.text = "\(R.string.localizable.currency()) 0.0"
        
//        if breakDown {
//            self.amountCollectedHeaderLabel.text = amountAnotherDriverHeaderLabel
//        }else{
//            self.amountCollectedHeaderLabel.text = amountHeaderLabel
//        }
        if completedRequest?.result?.organisation_coupon_user_id != 0 {
            completedRequest?.result?.payment_type
            isWaterCouponInvoice = true
            self.lblTotalValue.text = "$ 0.0"
            self.lbltaxType2Value.text = "\(R.string.localizable.currency()) 0.0"
            self.lblTaxType1Value.text = "\(R.string.localizable.currency()) 0.0"
            self.lbltaxType3Value.text = "\(R.string.localizable.currency()) 0.0"
            
            self.getOrderDetail(orderId: /completedRequest?.result?.order_id?.toString())
        }else{
            isWaterCouponInvoice = false
            let _ = calculateFair() // Calculate total fare and labels are also updates in methods
            
            self.lblTotalValue.text =  "$ \(/Int(/completedRequest?.result?.Payment?.final_charge))"
            //?.getTwoDecimalFloat())"
            if  completedRequest?.result?.category_id == 1 || completedRequest?.result?.category_id == 3{
                self.lblProduct.text = getServiceName(category: "\(completedRequest?.result?.category_id ?? 1)")
            }
            else{
                self.lblProduct.text = "\(/completedRequest?.result?.BrandinCompleted?.brand_name)"
            }
            // product description
            if (completedRequest?.result?.category_id == 4 || completedRequest?.result?.category_id == 7){
                self.lblQuantity.text = "\(/completedRequest?.result?.BrandinCompleted?.name)"
            }
            else{
                if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
                    self.lblQuantity.text = "\(/completedRequest?.result?.BrandinCompleted?.name) × \(/completedRequest?.result?.Payment?.product_quantity)"
                    
                }else {
                    self.lblQuantity.text = "\(/completedRequest?.result?.BrandinCompleted?.name) × \(/completedRequest?.result?.Payment?.product_quantity)"
                }
            }
            
        }
        let paymentInfo = completedRequest?.result?.Payment
        if paymentInfo?.previous_driver_charges ?? 0.0 > 0.0 {
            //self.previousInvoiceView.isHidden = false
            let finalCharge = Double(paymentInfo?.final_charge ?? "")?.roundTo(places: 2) ?? 0.0
            let previousCharge = paymentInfo?.previous_driver_charges?.roundTo(places: 2) ?? 0.0
//            totalFareLabel.text = "\(finalCharge)"
//            previousDriverFareLabel.text = "\(previousCharge)"
//            currentChargesLabel.text = "\(finalCharge - previousCharge)"
        }else{
            //self.previousInvoiceView.isHidden = true
        }
        self.localiseStrings() // localise text strings
        
        //MARK:-Add Fare Additional information.
        productNameLabel.text = completedRequest?.result?.BrandinCompleted?.name
        lblServiceType.text = /completedRequest?.result?.BrandinCompleted?.category_name
        if completedRequest?.result?.booking_type == "Package" {
            //Base Fare....
            //Package Price
            
            let baseFare = Double(paymentInfo?.distance_price_fixed ?? "") ?? 0.0
            baseFareLabel.text = " \(R.string.localizable.currency()) \(String(format: "%.2f",baseFare))"
            baseFareHeadingLabel.text = "Package Price"
            //Time
            let orderTime = Double(paymentInfo?.extra_time ?? "") ?? 0.0
            let timePerHourCharges = Double(paymentInfo?.price_per_min ?? "") ?? 0.0
            let timeCharges = orderTime*timePerHourCharges
           // timeHeadingLabel.text = "Extra Time(\((Int(paymentInfo?.extra_time ?? "") ?? 0)) min)"
            
            
            //rideTimeLabel.text = "\(String(format: "%.2f",timeCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Distance
            let orderDistance = Double(paymentInfo?.extra_distance ?? "") ?? 0.0
            let distancePerHourCharges = Double(paymentInfo?.price_per_km ?? "") ?? 0.0
            let distanceCharges = orderDistance * distancePerHourCharges
            //distanceHeadingLabel.text = "Extra Distance (\(orderDistance.roundTo(places: 2)) KM)"
           // rideDistanceLabel.text = "\(String(format: "%.2f",distanceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Normal Charges.
            let normalCharges = (baseFare + timeCharges + distanceCharges)
            // let actualValue = Double(paymentInfo?.product_actual_value ?? "") ?? 0.0
            //normalFareLabel.text = "\(String(format: "%.2f",normalCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            let serviceCharges = 0.0
            let bookingFee = 0.0
            
            //bookingFeeLabel.text = "\(String(format: "%.2f",bookingFee.roundTo(places: 2))) \(R.string.localizable.currency())"
            //serviceChargeLabel.text = "\(String(format: "%.2f",serviceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //SubTotal
            //subTotalLabel.text = "\(String(format: "%.2f",(normalCharges + bookingFee + serviceCharges).roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Total
            totalAmountLabel.text = "\(R.string.localizable.currency()) \(String(format: "%.2f",(Double(paymentInfo?.final_charge ?? "") ?? 0.0).roundTo(places: 2))) "
            
        }else{
            //Base Fare....
           
            let baseFare = Double(paymentInfo?.product_alpha_charge ?? "") ?? 0.0
            baseFareLabel.text = "\(R.string.localizable.currency()) \(String(format: "%.2f",baseFare))"
            
            //Time
            let orderTime = Double(paymentInfo?.order_time ?? "") ?? 0.0
            let timePerHourCharges = Double(paymentInfo?.product_per_hr_charge ?? "") ?? 0.0
            let timeCharges = orderTime*timePerHourCharges
            //timeHeadingLabel.text = "Time(\((Int(paymentInfo?.order_time ?? "") ?? 0)) min)"
            //rideTimeLabel.text = "\(String(format: "%.2f",timeCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Distance
            let orderDistance = Double(paymentInfo?.order_distance ?? "") ?? 0.0
            let distancePerHourCharges = Double(paymentInfo?.product_per_distance_charge ?? "") ?? 0.0
            let distanceCharges = orderDistance * distancePerHourCharges
           // distanceHeadingLabel.text = "Distance (\(orderDistance.roundTo(places: 2)) KM)"
            //rideDistanceLabel.text = "\(String(format: "%.2f",distanceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Normal Charges.
            let normalCharges = (baseFare + timeCharges + distanceCharges)
            let actualValue = Double(paymentInfo?.product_actual_value ?? "") ?? 0.0
            //normalFareLabel.text = "\(String(format: "%.2f",normalCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Booking fee and service charge
            var serviceCharges = 0.0
            let totalPercentage = Double(paymentInfo?.buraq_percentage ?? 0)/100
            var bookingFee = 0.0
            if normalCharges > actualValue {
                bookingFee = normalCharges * totalPercentage
            }else {
                bookingFee = actualValue * totalPercentage
                serviceCharges = actualValue - normalCharges
            }
            //bookingFeeLabel.text = "\(String(format: "%.2f",bookingFee.roundTo(places: 2))) \(R.string.localizable.currency())"
           // serviceChargeLabel.text = "\(String(format: "%.2f",serviceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //SubTotal
            //subTotalLabel.text = "\(String(format: "%.2f",(normalCharges + bookingFee + serviceCharges).roundTo(places: 2))) \(R.string.localizable.currency())"
            
            let timeValue = (/Float(/paymentInfo?.order_time)) * (/Float(/paymentInfo?.product_per_hr_charge))
            let distanceValue = (/Float(/paymentInfo?.order_distance)) * (/Float(/paymentInfo?.product_per_distance_charge))
            
            lblBaseFair.text = "\(R.string.localizable.currency()) \(Double(paymentInfo?.product_alpha_charge ?? "") ?? 0.0) "
            lblTimeHeading.text = "\("time".localized)(\(/paymentInfo?.order_time)) mins"
            lblTimeValue.text = "\(R.string.localizable.currency()) \(String(format: "%0.2f", timeValue)) "
            
            
            lblDistanceHeading.text = "\("distance".localized)(\(String(format: "%0.2f", orderDistance))) km"
            lblDistanceValue.text = "\(R.string.localizable.currency()) \(String(format: "%0.2f", distanceValue))"
            
            var service_Charges : Float = 0.0
             
            let normalFare = Float(/paymentInfo?.product_alpha_charge)! + timeValue + distanceValue
            if normalFare < Float(/paymentInfo?.product_actual_value)! {
                service_Charges = Float(/paymentInfo?.product_actual_value)! - normalFare
                lblServiceCharge.text = "\(R.string.localizable.currency()) \(String(format: "%0.2f",service_Charges))"
            } else {
                lblServiceCharge.text = "\(R.string.localizable.currency()) \(0.0)"
            }
            
            lblNormalFare.text = "\(R.string.localizable.currency()) \(String(format: "%0.2f", normalFare))"
            lblWaitingCharges.text = "\(R.string.localizable.currency()) \(0) "
            lblBookingFee.text = "\(R.string.localizable.currency()) \((/paymentInfo?.admin_charge))"
            lblSubTotal.text = "\(R.string.localizable.currency()) \((/paymentInfo?.final_charge))"
            lblDiscount.text = "\(R.string.localizable.currency()) \(0.0)"
            
            //Total
            totalAmountLabel.text = "\(R.string.localizable.currency()) \(String(format: "%.2f",(Double(paymentInfo?.final_charge ?? "") ?? 0.0).roundTo(places: 2)))"
        }
    }
    // localise strings for langugae
    
    func localiseStrings(){
        
        if completedRequest?.result?.organisation_coupon_user_id != 0{
            self.lblBaseFair.text =  "ETokens Used"
            self.lblBasefairValue.text = completedRequest?.result?.Payment?.product_quantity?.toString()
        }
        else{
            self.lblBaseFair.text = R.string.localizable.invoiceBaseFair()
        }
        
        self.lblTaxType1.text = R.string.localizable.invoiceTax1()
        self.lbltaxType2.text = R.string.localizable.invoiceTax2()
        self.lbltaxType3.text = R.string.localizable.invoiceTax3()
        self.lblTotal.text = R.string.localizable.invoiceTotal()
        self.btnRate.setTitle(R.string.localizable.invoiceRate(), for: .normal)
        self.lblInvoiceHeader.text = R.string.localizable.invoiceHeader()
    }
    
    // Return total fare
    
    func calculateFair() -> String{
        
        let base_price = Double(/completedRequest?.result?.Payment?.product_quantity) * /Double(/completedRequest?.result?.Payment?.product_per_quantity_charge)
        
        self.lblBasefairValue.text = "\(R.string.localizable.currency()) \(/Double(/completedRequest?.result?.Payment?.initial_charge)?.getTwoDecimalFloat()) "
        let _ = (/Double(/completedRequest?.result?.Payment?.buraq_percentage) / 100 ) * 100
        self.lblTaxType1Value.text = "\(R.string.localizable.currency()) \(/Double(/completedRequest?.result?.Payment?.admin_charge)?.getTwoDecimalFloat()) "
        self.lbltaxType2Value.text = "\(R.string.localizable.currency()) 0.0"
        
        let product_distance_charge = Double(/completedRequest?.result?.Payment?.product_per_distance_charge)
        let product_weight_charge = Double(/completedRequest?.result?.Payment?.product_per_weight_charge)
        let product_alpha_charge = Double(/completedRequest?.result?.Payment?.product_alpha_charge)
        let total_charge = base_price + /product_distance_charge + /product_weight_charge + /product_alpha_charge
        return "\(/total_charge)"
    }
    
    //MARK: Action Delivery Completed
    
    @IBAction func actionDeliveryCompleted(_ sender: Any) {
        if btnRate.tag == 10 {
            self.showInvoice.isHidden = true
            btnRate.tag = 0
            btnRate.setTitle("Completed", for: .normal)
        }else{
            self.dismiss(animated: true) {
                self.delegate?.RequestInvoiceProcessed()
            }
        }
    }
    
    func getOrderDetail(orderId:String){
        
        NotificationRequest.isNotified = false
        NotificationRequest.orderId = 0
        
        APIManager.shared.request(with: GeneralEndPoint.orderDetails(orderId: orderId)) { (response) in
            switch response{
                
            case .success(let responseValue):
                
                let object  =  Mapper<ServiceRequest>().map(JSONObject: responseValue)
                
                self.lblProduct.text = "\(/object?.order?.product?.brand_name)"
                self.lblQuantity.text = "\(/object?.order?.product?.name) × \(/object?.order?.payment?.product_quantity)"
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
    
    
    @IBAction func openInvoice(_ sender: UIButton) {
        //self.invoiceView.isHidden = false
        self.showInvoice.isHidden = false
        self.btnRate.tag = 10
        btnRate.setTitle("Finish", for: .normal)
    }
    
}
