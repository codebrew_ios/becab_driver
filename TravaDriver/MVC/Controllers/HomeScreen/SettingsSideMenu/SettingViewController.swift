//
//  SettingViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
//import SideMenu

enum selectedMenu: Int, CaseIterable {
   // case home = 0
    case myBooking = 0
    //case travelPackages = 2
    //case payment = 1
    case earnings = 1
   // case notifications = 4
    case settings = 2
    case heatMap = 3
    case contactUs = 4
    case referal = 5
}

class SettingViewController: BaseController {
    
    //MARK:--OUTLETS
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var vehicleNumberLabel: UILabel!
    @IBOutlet weak var tblViewSettings: UITableView!
    @IBOutlet weak var imgViewDriver: UIImageView!
    @IBOutlet weak var btnEditProfilePic: UIButton!
    @IBOutlet weak var imgRating: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblRatingValue: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    //MARK:-- Properties
    
    var dataSource : TableViewDataSource? {
        didSet{
            tblViewSettings.delegate = dataSource
            tblViewSettings.dataSource = dataSource
            tblViewSettings.reloadData()
        }
    }
    
    var emptyDriverData: Bool?{
        
        didSet{
            lblDriverName.text  = ""
            self.lblPhoneNumber.text = ""
            self.imgRating.getRatingImage(rating: "0")
            self.ratingView.rating = 0.0
            self.lblRatingValue.text = "0.0"
        }
    }
    
    var appVersion:String?
    
    //    private  lazy var titlesEtoken = ["sidemenu.setting.bookings".localized,"sidemenu.setting.earnings".localized,"sidemenu.setting.eTokenDeliveries".localized,"sidemenu.setting.referal".localized,"sidemenu.setting.contact".localized,"sidemenu.setting.emergency".localized,"sidemenu.setting.setting".localized, "sidemenu.setting.signout".localized]
    
    
    //private  lazy var titlesEtoken = ["Home".localized,"My Bookings".localized,"Travel packages".localized, "Payment Request".localized,"Earnings".localized,"Notifications".localized, "Settings".localized,"Heat Map".localized]
    
    private  lazy var titlesEtoken = [
                                      "My Bookings".localized,
                                      "Earnings".localized,
                                      "Settings".localized,
                                      "Heat Map".localized,
                                      "Contact Us".localized,
//                                      "Refer and Earn".localized
    ]
    
    
    //private  lazy var titlesNoEtoken = ["Home".localized,"My Bookings".localized,"Travel packages".localized ,"Payment Request".localized,"Earnings".localized, "Notifications".localized,"Settings".localized,"Heat Map".localized]
    
    private  lazy var titlesNoEtoken = ["My Bookings".localized,
                                        "Earnings".localized,
                                        "Settings".localized,
                                        "Heat Map".localized,
                                        "Contact Us".localized,
//                                        "Refer and Earn".localized
    ]
    //["sidemenu.setting.bookings".localized,"sidemenu.setting.earnings".localized,"sidemenu.setting.contact".localized,"sidemenu.setting.setting".localized, "sidemenu.setting.signout".localized]
    
    var titles = [String]()
    
    
    private(set) lazy var viewControllers: [UIViewController] = {
        
        return [R.storyboard.settingMenu.bookingsViewController(),R.storyboard.settingMenu.earningsViewController(),R.storyboard.settingMenu.deliveryAreasViewController(),R.storyboard.settingMenu.contactUsViewController(),R.storyboard.settingMenu.driverSettingsViewController(),R.storyboard.settingMenu.callController()]
        }() as! [UIViewController]
    
    private(set) lazy var viewControllersWithoutEtoken: [UIViewController] = {
        
        return [R.storyboard.settingMenu.bookingsViewController(),R.storyboard.settingMenu.earningsViewController(),R.storyboard.settingMenu.contactUsViewController(),R.storyboard.settingMenu.driverSettingsViewController(),R.storyboard.settingMenu.callController()]
        }() as! [UIViewController]
    
    
    //MARK:-- VIEW CONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = "Version: \(version)"
            if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                appVersion = "\(appVersion ?? "") & Build Number: \(build)"
                versionLabel.text = "Build Number: \(build)"
            }
        }
        
        
        ratingView.filledBorderColor = .black
        ratingView.emptyBorderColor = .black
    }
    
     
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        UIView.animate(withDuration: 2.0, animations: {
        }) { (true) in
            self.tblViewSettings.reloadData()
        }
        self.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    //MARK:-- CUSTOM METHODS
    
    func initialSetup(){
        
        self.navigationItem.title = R.string.localizable.back()
        
        if DataResponse.driverData?.data?.category_id != 2{
            
            titles = titlesNoEtoken
            
        }
        else{
            titles = titlesEtoken
        }
        
        if  let _ = DataResponse.driverData {
            
            lblDriverName.text = "\(/DataResponse.driverData?.data?.dataUser?.name)"
            vehicleNumberLabel.text = "\(/DataResponse.driverData?.data?.vehicle_number)"
            self.lblPhoneNumber.text = "\(/DataResponse.driverData?.data?.dataUser?.phone_code)-\(/DataResponse.driverData?.data?.dataUser?.phone_number)"
            imgViewDriver.kf.setImage(with: URL.init(string: /DataResponse.driverData?.data?.profile_pic_url), placeholder: #imageLiteral(resourceName: "ic_bg_menu"), options: nil, progressBlock: nil, completionHandler: nil)
            self.imgRating.getRatingImage(rating: "\(/DataResponse.driverData?.data?.rating)")
            // lblRatingValue.text = "\(/DataResponse.driverData?.data?.rating_count)"
            self.ratingView.rating = Double(DataResponse.driverData?.data?.rating ?? 0)
            lblRatingValue.text = "\(Double(/DataResponse.driverData?.data?.rating))"
            // Get driver service data
            
            let brandName = self.getBrandName(category: "\(/DataResponse.driverData?.data?.category_id)", BrandId: "\(/DataResponse.driverData?.data?.category_brand_id)")
            
            if DataResponse.driverData?.data?.category_id == 1 || DataResponse.driverData?.data?.category_id == 3 {
                
                let serviceName = self.getServiceName(category: "\(/DataResponse.driverData?.data?.category_id)")
                self.lblServiceName.text = "\(serviceName)"
                
            }
            else{
                let serviceName = self.getServiceName(category: "\(/DataResponse.driverData?.data?.category_id)")
                self.lblServiceName.text = "\(brandName)"
                //"\(serviceName) - \(brandName)"
            }
            
        }
        else{
            emptyDriverData = true
        }
        
        // configure  tableview
        
        self.configureTableView()
    }
    
    func configureTableView(){
        
        
        dataSource =  TableViewDataSource.init(items: titles, tableView: tblViewSettings, cellIdentifier: R.reuseIdentifier.settingMenuTableViewCell.description, cellHeight: UITableViewAutomaticDimension, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            print(self?.titles)
            let item = self?.titles[indexPath.row]
            (cell as? SettingMenuTableViewCell)?.title = item
            
            }, aRowSelectedListener: { (indexPath, cell) in
                // move to selected menu
                self.selectionHandlerSideMenuWithoutToken(indx: indexPath.row) // MYCHANGE
                
        }, willDisplayCell: { (indexPath, cell) in
            
        })
    }
    
    //MARK: Show Confirmation Alert
    
    func showConfirmationAlert(){
        
        
        Alerts.shared.showAlertWithActionBlock(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertTitleSureToLogout(), actionTitle: "alertTitle.ok".localized, viewController: self) {
            self.logoutDriver()
        }
    }
    
    //MARK: --------------------------------Logout ------------------------------
    
    func logoutDriver(){
        
        APIManager.shared.request(with: CommonEndPoint.logout, completion: { (response) in
            switch response{
                
            case .success(_):
                
                self.dismiss(animated: false, completion: {
                    
//                    guard  let vc = R.storyboard.loginSignUp.phoneSignUpViewController() else{ return}
                    SocketIOManager.shared.closeConnection()
                    guard  let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else{ return}
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    socketTimer?.invalidate()
                    token.access_token = ""
                    NotificationCenter.default.removeObserver(self)
                    UserDefaults.standard.removeObject(forKey: "online_status")
                    UserDefaults.standard.removeObject(forKey: "category_brand_product_id")
                    UserDefaults.standard.removeObject(forKey: "directionalHire_status")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = nav
                    
                    UserDefaults.standard.removeObject(forKey: SingletonKeys.user.rawValue)
                })
                
            case.failure(let err):
                Alerts.shared.showOnTop(alert: "alert.error".localized, message: /err, type: .info)
            }
        })
    }
    
    //MARK: Selection handler from side menu
    
    func selectionHandlerSideMenu(indx:Int){
        
        if let type = selectedMenu(rawValue: indx) {
            switch type {
//            case .home:
//                if MapViewController.showHeatMapData == true {
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.removeHeatMapNoti.rawValue), object: nil, userInfo: [:])
//                }
//
//                dismiss(animated: true, completion: nil)
//                return
            /*case .travelPackages :
                guard let vc =   R.storyboard.settingMenu.callController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)*/
            case .myBooking :
                
                guard let vc =   R.storyboard.settingMenu.bookingsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
//            case .payment :
//                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "Your request has been received. Admin will update you shortly.", actionTitle: "alertTitle.ok".localized, viewController: self) {
//                }
            case .earnings :

                guard let vc =   R.storyboard.settingMenu.earningsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
                
            case .settings :
                guard let vc =   R.storyboard.settingMenu.driverSettingsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
                
//            case .settings :
//                guard let vc =  R.storyboard.settingMenu.driverSettingsViewController() else{return}
//                self.navigationController?.pushViewController(vc, animated: true)
                
            case .heatMap :
                MapViewController.showHeatMapData = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.showHeatMapData.rawValue), object: nil, userInfo: [:])
                dismiss(animated: true, completion: nil)
                return
                
            case .contactUs:
                guard let vc =   R.storyboard.settingMenu.contactUsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
                return
            case .referal:
                guard let vc = R.storyboard.settingMenu.referalViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                dismiss(animated: true, completion: nil)
                return
            }
        }
    }
    
    func selectionHandlerSideMenuWithoutToken(indx:Int){
        
        if let type = selectedMenu(rawValue: indx) {
            switch type {
//            case .home:
//                if MapViewController.showHeatMapData == true {
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.removeHeatMapNoti.rawValue), object: nil, userInfo: [:])
//                }
//                dismiss(animated: true, completion: nil)
//                return
            /*case .travelPackages :
                break
                //guard let vc =   R.storyboard.settingMenu.callController() else{return}
                //self.navigationController?.pushViewController(vc, animated: true)*/
            case .myBooking :
              
                guard let vc =   R.storyboard.settingMenu.bookingsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
                  break
            case .earnings :
                guard let vc =   R.storyboard.settingMenu.earningsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
                break
                
//            case .payment :
//                break
                /*Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "Your request has been received. Admin will update you shortly.", actionTitle: "alertTitle.ok".localized, viewController: self) {
                }*/
                //            case .settings :
                //                guard let vc =   R.storyboard.settingMenu.driverSettingsViewController() else{return}
            //                self.navigationController?.pushViewController(vc, animated: true)
            case .settings :
                
                guard let vc =  R.storyboard.settingMenu.driverSettingsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
                break
                
            case .heatMap :
                
                MapViewController.showHeatMapData = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.showHeatMapData.rawValue), object: nil, userInfo: [:])
                dismiss(animated: true, completion: nil)
                break
                
            case .contactUs:
                guard let vc =   R.storyboard.settingMenu.contactUsViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
            case .referal:
                guard let vc = R.storyboard.settingMenu.referalViewController() else{return}
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                dismiss(animated: true, completion: nil)
                return
            }
        }
        
    }
    @IBAction func openroadPickupAction(_ sender: UIButton) {
        guard let vc = R.storyboard.main.roadPickUpController() else{ return }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signOutAction(_ sender: UIButton) {
        self.showConfirmationAlert()
    }
    //MARK:-- ACTIONS METHODS
    
    @IBAction func actionUpdateImage(_ sender: Any) {
        
        guard let vc = R.storyboard.settingMenu.profileViewController() else{return}
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
