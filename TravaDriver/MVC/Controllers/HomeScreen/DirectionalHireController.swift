//
//  DirectionalHireController.swift
//  TravaDriver
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import CoreLocation
import GooglePlaces

protocol DirectionalHireDelegate: class {
    func updateStatus(address:Address)
}

class DirectionalHireController: UIViewController {
    
    @IBOutlet weak var addressTextField: AnimatableTextField!
    @IBOutlet weak var lblDirectionalHire: UILabel!
    
    weak var delegate:DirectionalHireDelegate?
    var address:Address?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblDirectionalHire.text = "Directional Hire".localized
        self.view.isOpaque = false
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissControllerAction(_ sender: UIButton) {
          self.dismiss(animated: false, completion: nil)
    }
  
    @IBAction func submitButtonAction(_ sender: UIButton) {
        if addressTextField.text?.trim() == "" {
            self.alertBox(message: "Please enter address", title: "Alert!") {
            }
        }else{
            self.delegate?.updateStatus(address: address ?? Address())
            self.dismiss(animated: true) {
            }
        }
    }
    
    
    @IBAction func addAddressAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let country = UserDefaults.standard.value(forKey: "current_contry") as? String
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        //filter.type = .address
        //filter.country = country
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
}


extension DirectionalHireController:GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        address = Address(gmsPlace: place)
        addressTextField.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

