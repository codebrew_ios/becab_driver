//
//  SupportViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import SideMenu

class SupportViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    
    //MARK: Outlets
    
    @IBOutlet weak var lblSupportHeading: UILabel!
    @IBOutlet weak var lblSupportHeadingInfo: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: Properties
    
    var arr:[[String]] = []
    var cellheight = 168.0
    
    var dataSource : CollectionViewDataSource? {
        didSet{
            collectionView.delegate = dataSource
            collectionView.dataSource = dataSource
            collectionView.reloadData()
        }
    }
    
    var titles = ["sidemenu.support.warehouse".localized,"sidemenu.support.spareparts".localized,"sidemenu.support.tyrerepair".localized,"sidemenu.support.oilchange".localized,"sidemenu.support.general".localized,"sidemenu.support.carwash".localized,"sidemenu.support.backupvehicles".localized,"sidemenu.support.custom".localized, "sidemenu.support.ambulance".localized,"sidemenu.support.manpower".localized]
    
    var imageTitles = ["ic_warehouse","ic_spareparts","ic_tyre_repair","ic_oilchange","ic_general","ic_carwash","ic_backup","ic_customc","ic_ambulance","ic_manpower"]
    
    
    //MARK:-- VIEW CONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialSetup()

    }
    
    //MARK:-- CUSTOM METHODS
    
    func initialSetup(){
        lblSupportHeading.text = "sidemenu.support".localized
        lblSupportHeadingInfo.text = "sidemenu.supportInfo".localized
        arr.append(titles)
        arr.append(imageTitles)
        configureCollectionView()
    }
    
     //MARK:-- CONFIGURE CELL
    
    func configureCollectionView(){
        
        dataSource = CollectionViewDataSource.init(items: DataResponse.driverData?.support, collectionView: collectionView, cellIdentifier: R.reuseIdentifier.supportMenuCollectionViewCell.description, headerIdentifier: "", cellHeight: CGFloat(cellheight), cellWidth: SideMenuManager.default.menuWidth/2 - 20 , configureCellBlock: { (cell, item, indexPath) in
            
            let object = DataResponse.driverData?.support?[indexPath.row]
            (cell as? SupportMenuCollectionViewCell)?.item = object
            
            }, aRowSelectedListener: { (indexPath, item) in
                print(indexPath)
                
                Alerts.shared.show(alert: "alert.alert".localized, message: "alert.comingsoon".localized, type: .info)
        }, willDisplayCell: { (indexpath) in
            print(indexpath)
        }, scrollViewDelegate: { (scrollView) in
            print(scrollView)
        },scrollViewDecelerate:{ (scrollView) in
            print(scrollView)
        })
    }
    
}
