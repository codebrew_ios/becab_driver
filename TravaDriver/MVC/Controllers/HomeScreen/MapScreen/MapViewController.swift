//
//  MapViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 08/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//


import UIKit
import GoogleMaps
import ObjectMapper
import SocketIO
import SwiftyJSON
import IBAnimatable
import SideMenu
import Alamofire

enum HalfWayStopOption : String {
    case accept = "Accepted"
    case reject = "Rejected"
}

struct userInfo{
    static var userid = ""
}

var socketTimer: Timer?

class MapViewController: BaseController, GMSMapViewDelegate,LocationManagerDelegate, SeviceRequestDelegate, OnGoingInvoiceDelegate, RateCompletedDeliveryDelegate, UIScrollViewDelegate, UICollectionViewDelegate,DropViewActionDelegate,reasonCancellation,confirmRequestDelegate{
    
    
    //MARK: ------------------------------------------------------ OUTLETS------------------------------------------------------------------
    
    @IBOutlet weak var directionalHireButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var directionalHireButton: UIButton!
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet weak var driverExpiredView: AnimatableView!
    @IBOutlet weak var buttonSwitch: AnimatableButton!
    @IBOutlet weak var btnMyLocation: AnimatableButton!
    @IBOutlet weak var segmentMapStyle: UISegmentedControl!
    
    @IBOutlet weak var collectionView: UICollectionView!
    //ongoing order view
    @IBOutlet var DropAddressView: UIView!
    
    // ongoing collapsed view
    @IBOutlet weak var btnExpandCollapsedView: UIButton!
    @IBOutlet weak var lblCustomerNameInCollaspeView: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imageViewInCollaspView: UIImageView!
    @IBOutlet var CollapsedOngoingOrderView: UIView!
    @IBOutlet weak var mapViewbottomConstraint: NSLayoutConstraint!
    
    
    //MARK: ---------------------------------------------------VARIABLES ---------------------------------------------------------------------
    var dataSource : CollectionViewDataSource? {
        didSet {
            collectionView.delegate = dataSource
            collectionView.dataSource = dataSource
            collectionView.reloadData()
        }
    }
    
    var locationManagerVariable = CLLocationManager()
    // for service requests
    static var showHeatMapData:Bool = false
    private var heatmapLayer: GMUHeatmapTileLayer!
    var isRideOngoing = false
    var driverData :DriverModel?
    var serviceRequest : ServiceRequest?
    
    var mapMyLocationTimer: Timer?
    var menuRightNavigationController:  UISideMenuNavigationController?
    var menuLeftNavigationController: UISideMenuNavigationController?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var gradientColors = [UIColor.green, UIColor.red]
    private var gradientStartPoints = [0.2, 1.0] as? [NSNumber]
    // Constants
    
    var zoomInitial:Float = 15.0
    var heightDropView = 400.0
    var heightCollapsedDropView = 120
    var previousZoom:Float = 0.0
    var apiCounter = 0
    
    // marker pins
    var driverMarker = GMSMarker()
    var userMarker = GMSMarker()
    var dropOffMarker = GMSMarker()
    var dropOffMarkerHelper = GMSMarker()
    private var infoWindow = marker_info()
    var vehicleCurrentSize:CGSize = CGSize()
    
    //For Polylines Creation Module
    
    var deliveryStarted = false
    var routeCreatedOneTime = false
    var isFocus = true
    var polyline: GMSPolyline?
    var polylineSourceToDest: GMSPolyline?
    var isDoneCurrentPolyline = true
    var oldCordinates: CLLocationCoordinate2D?
    var waitingTimeValue = Int()
    var waitingTimer: Timer?
    var waitngTimerCounter = 0
    
    //MARK: ----------------------------------------- VIEW CONTROLLER LIFECYCLE -------------------------------------------------------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setData()
//        do {
//            // Set the map style by passing the URL of the local file.
//            if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
//                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
//                
//            } else {
//                debugPrint("Unable to find style.json")
//            }
//        } catch {
//            debugPrint("One or more of the map styles failed to load. \(error)")
//        }
        if MapViewController.showHeatMapData == false {
            LocationManager.shared.delegate = self
            self.locationManagerVariable.delegate = self
            self.locationManagerVariable.startUpdatingLocation()
        }else {
            self.removeHeatmap()
            self.fetchHeatMapDataFromBackend()
        }
        // Location Manager Delegate
        if LocationManager.shared.currentLoc != nil{
            self.zoomToCurrentLocation()
            self.setupController()
        }
        self.getCurrentContryCode()
        
        appDelegate.pushNotificationPermission(application: UIApplication.shared) {
            _ = LocationManager.shared
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if MapViewController.showHeatMapData == true {
            MapViewController.showHeatMapData = false
            self.removeHeatmap()
        }
        //  self.navigationItem.title = R.string.localizable.homeTitle()
        self.navigationController?.navigationBar.isHidden = true
        if let font = UIFont(name: LocalKeys.MontserratAlternativesBold, size: 16) {
            let lbl = UILabel()
            lbl.backgroundColor = UIColor.black
            lbl.text = R.string.localizable.homeTitle()
            lbl.textColor = UIColor.white
            lbl.font = font
            lbl.sizeToFit()
            navigationItem.titleView = lbl
            //            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: font]
        }
        self.setData()
        setupNavigationBarShadow()
    }
    
    func setData() {
        
        directionalHireButton.tag = Int(UserDefaults.standard.value(forKey: "directionalHire_status") as? String ?? "") ?? 0
        if directionalHireButton.tag == 1 {
            
            let langType = BundleLocalization.sharedInstance().language
            switch langType {
            case Languages.Spanish:
                directionalHireButton.setImage(UIImage(named:"ic_directionHire_on_s"), for: .normal)
            default:
                directionalHireButton.setImage(UIImage(named:"ic_direction_hire_on"), for: .normal)
                
                
            }
            
        }else {
            
            let langType = BundleLocalization.sharedInstance().language
            switch langType {
            case Languages.Spanish:
                directionalHireButton.setImage(UIImage(named:"ic_directionHire_off_s"), for: .normal)
            default:
                directionalHireButton.setImage(UIImage(named:"ic_direction_hire_off"), for: .normal)
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        //navigation bar customisation
    }
    
    
    func getCurrentContryCode() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
        if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
            let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{
            
            let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /code} as! [[String : AnyObject]]
            if let val = dict.first{
                let data = val as NSDictionary
                let code = data.value(forKey: "code") as! String
                UserDefaults.standard.set(code, forKey: "current_contry")
            }
        }
    }
    
    func fetchHeatMapDataFromBackend() {
        APIManager.shared.request(with: ServicesEndPoint.heatMap()) { [weak self] (response) in
            
            switch response{
            case .success(let responseValue):
                
                if let data =  responseValue as? HeatMapData {
                    print(data.toJSON())
                    if let dataEntry = data.result {
                        //Mark:-Show Map Entries.
                        self?.addHeatmap(data: dataEntry)
                        
                    }
                }
                
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
    
    func drawCircleAroundCurrentLocation() {
        let camera = GMSCameraPosition.camera(withLatitude: Locations.lat.getLoc(),
                                              longitude: Locations.longitutde.getLoc(), zoom: 6)
        //        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.mapView.frame.width, height: self.mapView.frame.height), camera: camera)
        mapView.isMyLocationEnabled = true
        //        self.view = mapView
        
        mapView.delegate = self
        
        let cirlce = GMSCircle(position: camera.target, radius: 1000000)
        cirlce.fillColor = UIColor.gray.withAlphaComponent(0.5)
        cirlce.map = mapView
    }
    
    @IBAction func directionalHireAction(_ sender: UIButton) {
        
        if sender.tag == 1 {
            self.setDirectionalStatus(directional_hire: Keys.status_value_off.rawValue)
        }
        else{
            guard let vc =   R.storyboard.main.directionalHireController() else{return}
            vc.delegate  = self
            self.navigationController?.present(vc, animated:false, completion: {
            })
        }
        DataResponse.driverData = DataResponse.driverData
    }
    
    //MARK: SHOW LEFT MENU ▶︎▶︎▶︎
    @IBAction func actionProfileSideMenu(_ sender: Any) {
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            
            if let val = menuRightNavigationController{
                self.present(val, animated: true, completion: nil)
            }
            
        }else{
            
            if let val = menuLeftNavigationController{
                self.present(val, animated: true, completion: nil)
            }
        }
    }
    
    func setupController(){
        self.addObserver()                                                                      // Add  Observer
        self.initialNetworkRequests()                                                           // Make intial api requests
        self.mapViewsetup()                                                                     // Map settings
        self.sideMenuInitialSetup()                                                             // Setup side menus according to language
    }
    
    func initialNetworkRequests(){
        
        if NotificationRequest.isNotified   {
            self.getOrderDetail(orderId: "\(NotificationRequest.orderId)")
        }
        
        self.connectToSocket()                                                                     //  Connect to socket
        self.updateDriverData()                                                                    //  Update driver location api
        self.getOngoingOrders()
        //  Get ongoing orders
        self.setOnlineStatus(status: (UserDefaults.standard.value(forKey: "online_status") as? String ?? "0" ), directionalHire: (UserDefaults.standard.value(forKey: "directionalHire_status") as? String ?? "1") )
        if let value = (UserDefaults.standard.value(forKey: "online_status") as? String) , value == "1" {
//            self.buttonSwitch.isSelected = true
        }else{
//            self.buttonSwitch.isSelected = false
            }
            
            self.setDirectionalStatus(directional_hire: (UserDefaults.standard.value(forKey: "directionalHire_status") as? String ?? "0") )
        //  Set online status to true initially
        //self.getSupportServices()                                                                //  Get support Services
    }
}


// MARK: ------------------------------------- SOCKET CONNECTION, EMITTERS AND LISTENER METHODS-------------------------------------------------

extension MapViewController{
    
    //MARK: CONNECTING SOCKETS  AND MARKER INITIAL POSITION ▶︎▶︎▶︎
    
    func connectToSocket(){
        
        SocketIOManager.shared.establishConnection()                                             //  Establish connection to socket
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in                      //  Setup mapview and add timer to update location
            self?.setupLocationPollingTimer(interval: 5.0)                                       // Setup  timer to update location to server via sockets
            self?.orderRequestListener()                                                         //  Listen to Order Status
            if CLLocationManager.locationServicesEnabled() { self?.addInitialDriverMarker() }    // Update driver location api
            else{self?.openSettingsScreenForLocationPermission()}
        }
    }
    
    //MARK: ORDER REQUEST SOCKET LISTENER ▶︎▶︎▶︎
    
    func orderRequestListener(){
        
        SocketIOManager.shared.listenOrderEvent({
            [weak self] (response) in
            
            let dic =  response[SocketConstants.order] as? NSDictionary
            let orderId = dic?.value(forKey: SocketConstants.order_id) as? Int
            let type =  response[SocketConstants.type] as? String
            
            switch type {
                
            case OrderEventTypes.resuestIncoming.rawValue:
                
                /** Check if pop up for this orderid is already presented either via socket or notification. Open request pop up according to category_id
                 **/
                
                let object  =  Mapper<ServiceRequest>().map(JSONObject: response)
                if /(self?.appDelegate.arrRequestIds.contains(Int(/object?.order?.order_id))){return}
                self?.appDelegate.arrRequestIds.append(/object?.order?.order_id)
                if let val = object{
                    self?.openRequestPopUp(object: val)
                }
                
            case OrderEventTypes.requestCancelled.rawValue:
                
                /** Dismiss  presented view Order cancelled by user.Also Check if pop up for this orderid is already cancelled either via socket or notification **/
                self?.directionalHireButton.isHidden = false
                self?.directionalHireButtonHeight.constant = 32
                self?.getOngoingOrders()
                
                let dataDict:[String: Int] = [SocketConstants.order_id: /orderId]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.cancelled.rawValue), object: nil, userInfo: dataDict)
                
            case OrderEventTypes.serReached.rawValue:
                break
                
            case OrderEventTypes.EtokenCustomerComplete.rawValue:
                
                /** Dismiss  presented view Order cancelled by user.Also Check if pop up for this orderid is already cancelled either via socket or notification **/
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.cancelled.rawValue), object: nil, userInfo: [:])
                let orderData = response[SocketConstants.order] as? NSDictionary
                let object = Mapper<CompletedOrderDetail>().map(JSONObject: orderData)
                let completedOrderModelObject = CompletedOrderModel()
                completedOrderModelObject.result = CompletedOrderDetail()
                completedOrderModelObject.result = object
                DataResponse.completedRequest = completedOrderModelObject
            // self?.openInvoiceView(completedOrder: DataResponse.completedRequest)
            case OrderEventTypes.SerHalfWayStopCancelled.rawValue :
                self?.dismiss(animated: true, completion: nil)
                return
                
            case OrderEventTypes.SerBreakdownCancelled.rawValue :
                self?.dismiss(animated: true, completion: nil)
                return
                
            case OrderEventTypes.SerBreakdown.rawValue :
                let orders = response[SocketConstants.order] as? [NSDictionary]
                if orders?.count == 0 {
                    return
                }
                let orderData = orders?.first
                let object = Mapper<CompletedOrderDetail>().map(JSONObject: orderData)
                let completedOrderModelObject = CompletedOrderModel()
                completedOrderModelObject.result = CompletedOrderDetail()
                completedOrderModelObject.result = object
                DataResponse.completedRequest = completedOrderModelObject
                self?.alertBoxWithDoubleAction(message: "User has requested to Break Down Ride. Are you sure you want to accept" , title: "Vehicle Breakdown" ,okTitle: "Accept",cancelTitle: "Reject", ok: {
                    self?.acceptRejectBreakDown(orderID: DataResponse.completedRequest?.result?.order_id ?? 0, status: HalfWayStopOption.accept.rawValue)
                }, cancel: {
                    self?.acceptRejectBreakDown(orderID: DataResponse.completedRequest?.result?.order_id ?? 0, status: HalfWayStopOption.reject.rawValue)
                })
                break
            case OrderEventTypes.SerHalfWayStop.rawValue:
                let orderData = response[SocketConstants.order] as? NSDictionary
                let object = Mapper<CompletedOrderDetail>().map(JSONObject: orderData)
                let completedOrderModelObject = CompletedOrderModel()
                completedOrderModelObject.result = CompletedOrderDetail()
                completedOrderModelObject.result = object
                DataResponse.completedRequest = completedOrderModelObject
                self?.alertBoxWithDoubleAction(message: "User has requested to stop Ride. Are you sure you want to accept" , title: "HalfWay Stop" ,okTitle: "Accept",cancelTitle: "Reject", ok: {
                    self?.acceptRejectHalfWayStop(orderID: DataResponse.completedRequest?.result?.order_id ?? 0, status: HalfWayStopOption.accept.rawValue)
                }, cancel: {
                    self?.acceptRejectHalfWayStop(orderID: DataResponse.completedRequest?.result?.order_id ?? 0, status: HalfWayStopOption.reject.rawValue)
                })
                break
            case OrderEventTypes.requestAcceptedOther.rawValue:
                
                /**Check if pop up for this orderid is already dismissed either via socket or notification.Then dismiss  presented view Order accepted by other driver.  **/
                
                if /(self?.appDelegate.arrRequestCancelledOrOtherAccepted.contains(Int(/orderId))){return}
                self?.appDelegate.arrRequestCancelledOrOtherAccepted.append(/orderId)
                
                let dataDict:[String: Int] = [SocketConstants.order_id: /orderId]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.OtherAccepted.rawValue), object: nil, userInfo: dataDict)
                
            case OrderEventTypes.requestRated.rawValue:
                
                debugPrint("")
                
            case OrderEventTypes.DPending.rawValue:
                
                //Check if confirm scheduled booking pop up for this orderid is already presented either via socket or notification
                
                if /(self?.appDelegate.arrAboutToStartPending.contains(/orderId)){return}
                self?.appDelegate.arrAboutToStartPending.append(orderId!)
                self?.getOngoingOrders()
                
            case OrderEventTypes.About2Start.rawValue:
                
                //Check if aboutTOSTart  pop up for scheduled booking  for this orderid is already presented either via socket or notification
                
                if (self?.appDelegate.arrAboutToStart.contains(/orderId))!{return}
                self?.appDelegate.arrAboutToStart.append(orderId!)
                self?.getOngoingOrders()
                
            case OrderEventTypes.EtokenTimeout.rawValue:
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.etokenEtoken(), message: R.string.localizable.alertTimeoutEToken(), actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                    self?.getOngoingOrders()
                    
                    self?.navigationController?.popToRootViewController(animated: true)
                }
                
            case OrderEventTypes.EtokenCustomerCancel.rawValue :
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.etokenEtoken(), message: R.string.localizable.alertCustomercancelEToken(), actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                    self?.getOngoingOrders()
                    self?.navigationController?.popToRootViewController(animated: true)
                }
                
            case OrderEventTypes.EtokenCustomerConfirm.rawValue:
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.etokenEtoken(), message: R.string.localizable.alertCustomerCOnfirmEToken(), actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                    
                    // remove orders ids which has been confirmed.
                    pendingApproval.arrOrderIds = pendingApproval.arrOrderIds.filter { $0 != /DataResponse.completedRequest?.result?.order_id}
                    
                    //refresh order list
                    self?.getOngoingOrders()
                    
                    // move to map view screen.
                    if !(/self?.navigationController?.visibleViewController?.isKind(of: MapViewController.self)){
                        if let nav = self?.navigationController{
                            for controller in nav.viewControllers.reversed(){
                                if controller.isKind(of: MapViewController.self) {
                                    self?.navigationController?.popToViewController(controller, animated: true)
                                    self?.openInvoiceView(completedOrder: DataResponse.completedRequest)
                                    break
                                }
                            }
                        }
                    }
                    else{
                        self?.openInvoiceView(completedOrder: DataResponse.completedRequest)
                    }
                }
                
            default:
                debugPrint("")
            }
        })
    }
    
    func acceptRejectHalfWayStop(orderID:Int,status:String) {
        
        switch status {
        case HalfWayStopOption.accept.rawValue:
            if let vc = R.storyboard.main.amountViewController(){
                vc.completion = { amount in
                    self.halfwayStop(orderID: orderID, status: status, amount: amount)
                }
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true)
            }
        default:
            self.halfwayStop(orderID: orderID, status: status, amount: 0)
        }
    }
    
    func halfwayStop(orderID:Int,status:String,amount:Double){
        APIManager.shared.request(with: ServicesEndPoint.acceptRejectHalfWayStop(orderId: orderID, status: status,amount: amount)) {
            [weak self] (response) in
            switch response{
                
            case .success(let responseValue):
                if status == HalfWayStopOption.accept.rawValue {
                    rideDistance.dictionaryAllRides = [:]
                    
                    if let items =  DataResponse.onGoingOrders?.orders{
                        if items.count > 0 {
                            self?.getOngoingOrders()
                        }
                    }else{
                        self?.completeOrderSetupUI()
                    }
                    
                    DataResponse.completedRequest = responseValue as? CompletedOrderModel
                    self?.openInvoiceView(completedOrder: DataResponse.completedRequest)
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
    
    
    func acceptRejectBreakDown(orderID:Int,status:String) {
        let distanceInKMeters = (self.getDistanceBetweenLatLongs(lat: Float(Locations.lat.getLoc()) , long: Float(Locations.longitutde.getLoc()) , latitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude) , longitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude))/1000)
        Utility.shared.getDistanceBetweenAllPoints { (distanceValue) in
            let distanceExactValue = distanceValue
            APIManager.shared.request(with: ServicesEndPoint.acceptRejectBreakDown(orderId: orderID, status: status, orderDistance: "\(distanceExactValue)",amount:0)) {
                [weak self] (response) in
                switch response{
                    
                case .success(let responseValue):
                    if status == HalfWayStopOption.accept.rawValue {
                        rideDistance.dictionaryAllRides = [:]
                        
                        if let items =  DataResponse.onGoingOrders?.orders{
                            if items.count > 0 {
                                self?.getOngoingOrders()
                            }
                        }else{
                            self?.completeOrderSetupUI()
                        }
                        
                        DataResponse.completedRequest = responseValue as? CompletedOrderModel
                        self?.openInvoiceView(completedOrder: DataResponse.completedRequest,breakdown: true)
                        //self?.RequestInvoiceProcessed()
                    }
                    
                case .failure(let err):
                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
                }
            }
        }
    }
    
    
    func StopTextSpeechAndInvalidateTimer(){
        SpeechSynthesizer.shared.stopSpeaking()
        SpeechSynthesizer.shared.stopToneAudio()
    }
    
    func removeOrder(orderId:Int) {
        
        self.StopTextSpeechAndInvalidateTimer()
        
        currentPopRequest.arrAllRequest =   currentPopRequest.arrAllRequest.filter(){$0?.order?.order_id != /orderId}
        
        if currentPopRequest.arrAllRequest.count == 0 {
            
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        else{
            let id = currentPopRequest.arrAllRequest.first??.order?.order_id
            if let val = currentPopRequest.requests["\(/id)"]{
                self.serviceRequest = val
            }
        }
    }
    
    func accept() {
        SpeechSynthesizer.shared.stopToneAudio()
        
        APIManager.shared.request(with: ServicesEndPoint.accept(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) { [weak self] (response) in
            
            switch response{
            case .success(let responseValue):
                
                let val = responseValue as? NSDictionary
                let result = val?.value(forKey: "result") as? NSDictionary
                let crequest = result?.value(forKey: "CRequest") as? NSDictionary
                let order_request_status = crequest?.value(forKey: "order_request_status") as? String
                
                let userDetailId = crequest?.value(forKey: "driver_user_detail_id") as? Int
                
                if order_request_status == SocketConstants.Accepted && userDetailId == UserSingleton.shared.loggedInUser?.data?.user_detail_id{
                    
                    if self?.serviceRequest?.order?.future == "1"{
                        self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
                        Alerts.shared.show(alert: "alert.alert".localized, message: R.string.localizable.requestScheduledSuccessFully(), type: .info)
                    }
                    else{
                        
                        self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
                        self?.acceptedRequest()
                    }
                }
                else{
                    Alerts.shared.show(alert: "alert.alert".localized, message: R.string.localizable.alertsOrderAlreadyAccepted(), type: .info)
                    self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
                }
                
            case .failure(let err):
                
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
                //                self?.removeOrder(orderId: /self?.serviceRequest?.order?.order_id)
            }
        }
    }
    
    func openRequestPopUp(object:ServiceRequest){
        
        
        self.serviceRequest = object
        
        if object.order?.bookingType == "RoadPickupPhone" {
            self.accept()
            return
        }
        
        
        if object.order?.product?.category_id_str == "4"{
            guard let vc = R.storyboard.main.freightRequestModalViewController() else {return}
            vc.serviceRequest = object
            vc.delegate = self
            self.openRequestVC(vc: vc)
            
        }
        else{
            
            guard let vc = R.storyboard.main.requestModelViewController() else {return}
            currentPopRequest.arrAllRequest.append(serviceRequest)
            currentPopRequest.requests["\(/serviceRequest?.order?.order_id)"] = serviceRequest
            
            if /(self.navigationController?.presentedViewController?.isKind(of: RequestModelViewController.self)){
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertNewRequestComing(), type: .info)
                return
            }
            vc.serviceRequest = object
            vc.delegate = self
            self.openRequestVC(vc: vc)
        }
    }
    
    func openRequestVC(vc:UIViewController){
        
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        
        // To present from any controller
        if self.navigationController?.visibleViewController != nil{
            self.navigationController?.visibleViewController?.present(vc, animated: true, completion: nil)
        }
        else{
            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)
            
        }
    }
    
    //MARK: Present Invoice View Controller
    
    func openInvoiceView(completedOrder:CompletedOrderModel?,breakdown:Bool = false){
        guard let vc = R.storyboard.main.deliveryInvoiceViewController() else {return}
        vc.delegate = self
        vc.breakDown = breakdown
        vc.completedRequest = DataResponse.completedRequest
        if self.navigationController?.viewControllers.contains(vc) ?? false {
            return
        }else{
            self.openRequestVC(vc: vc)
        }
    }
    
    func zoomToCurrentLocation(){
        let currentPosition = GMSCameraPosition.camera(withLatitude:Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), zoom: (self.mapView.camera.zoom))//(15.0) )
        CATransaction.begin()
        CATransaction.setAnimationDuration(3.0)
        self.mapView.animate(to: currentPosition)
        CATransaction.commit()
        //        self.drawCircleAroundCurrentLocation()
    }
    
}

// MARK: -------------------------------- MARKERS SETUPS AND (Distance Time Remaining) OBSERVERS -----------------------------------------------

extension MapViewController{
    
    func updateBearing(){
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        self.driverMarker.rotation = /LocationManager.shared.heading
        CATransaction.commit()
    }
    
    //MARK: Driver Initial Marker at homepage after opening the app
    
    func addInitialDriverMarker(){
        
        if LocationManager.shared.currentLoc != nil {
            
            let type =  DataResponse.driverData?.data?.category_id ??  /(UserSingleton.shared.loggedInUser?.data?.category_id)
            self.driverMarker.icon =  self.getVehicleImage(category: type)
            self.getSnappedRoadAPiCoordinates(lat: Locations.lat.getLoc(), lng: Locations.longitutde.getLoc()) { [weak self](snappedLat, snappedLong) in
                
                guard let lat = snappedLat else {return}
                guard let long = snappedLong else {return}
                CurrentSnappedLocation.latitude = lat
                CurrentSnappedLocation.longitude = long
                
                let currentPosition = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: (self?.mapView.camera.zoom ?? 15.0))//(15.0) )
                self?.driverMarker.isFlat = true
                self?.driverMarker.map = self?.mapView
                self?.driverMarker.position = currentPosition.target
                self?.driverMarker.rotation = /LocationManager.shared.heading
                
                CATransaction.begin()
                CATransaction.setAnimationDuration(2.0)
                self?.mapView.animate(to: currentPosition)
                CATransaction.commit()
                
            }
        }
    }
    
    //MARK: Show polyline  ▶︎▶︎▶︎
    func showPolyLineAndDestinationMarker() {
        
        if (DataResponse.onGoingRequest?.order?.category_id == 4 || DataResponse.onGoingRequest?.order?.category_id == 7) && freightPolyLineHelper.orderID == 0 {
            let serPickupCoordinate = CLLocationCoordinate2D(latitude: /DataResponse.onGoingRequest?.order?.pickup_latitude, longitude: /DataResponse.onGoingRequest?.order?.pickup_longitude)
            let serDropOffCoordinate = CLLocationCoordinate2D(latitude: /DataResponse.onGoingRequest?.order?.dropoff_latitude, longitude: /DataResponse.onGoingRequest?.order?.dropoff_longitude)
            self.calculateTimeAndDistancefreightHelper(from: serPickupCoordinate, to: serDropOffCoordinate)
        }
        
        let currentCoordinates = LocationManager.shared.currentLoc?.coordinate
        
        let pickup_latitude:Double = (DataResponse.onGoingRequest?.order?.pickup_latitude ?? 0.0)
        let pickup_longitude:Double = (DataResponse.onGoingRequest?.order?.pickup_longitude ?? 0.0)
        
        let dropOff_latitude:Double = (DataResponse.onGoingRequest?.order?.dropoff_latitude ?? 0.0)
        let dropOff_longitude:Double = (DataResponse.onGoingRequest?.order?.dropoff_longitude ?? 0.0)
        
        if (DataResponse.onGoingRequest?.order?.category_id == 4 || DataResponse.onGoingRequest?.order?.category_id == 7) {
            if (DataResponse.onGoingRequest?.order?.order_status == APIConstants.onGoing || DataResponse.onGoingRequest?.order?.order_status == APIConstants.reached) {
                
                if  currentCoordinates != nil {
                    
                    let  _ = CLLocationCoordinate2D(latitude: pickup_latitude, longitude:pickup_longitude)
                    let  destinationDropOffCoordinates = CLLocationCoordinate2D(latitude: dropOff_latitude, longitude:dropOff_longitude)
                    self.getPolylineRoute(from: currentCoordinates, to: destinationDropOffCoordinates, toDrop: destinationDropOffCoordinates, isFirst: deliveryStarted, orderID: DataResponse.onGoingRequest?.order?.order_id)
                    return
                }
            } else {
                if  currentCoordinates != nil {
                    let  destinationCoordinates = CLLocationCoordinate2D(latitude: pickup_latitude, longitude:pickup_longitude)
                    let  destinationDropOffCoordinates = CLLocationCoordinate2D(latitude: dropOff_latitude, longitude:dropOff_longitude)
                    self.getPolylineRoute(from: currentCoordinates, to: destinationCoordinates, toDrop: destinationDropOffCoordinates, isFirst: deliveryStarted, orderID: DataResponse.onGoingRequest?.order?.order_id)
                    return
                }
            }
        }
        
        if  currentCoordinates != nil {
            
            let  _ = CLLocationCoordinate2D(latitude: pickup_latitude, longitude:pickup_longitude)
            let  destinationDropOffCoordinates = CLLocationCoordinate2D(latitude: dropOff_latitude, longitude:dropOff_longitude)
            
            self.getPolylineRoute(from: currentCoordinates, to: destinationDropOffCoordinates, toDrop: destinationDropOffCoordinates, isFirst: deliveryStarted, orderID: DataResponse.onGoingRequest?.order?.order_id)
        }
    }
    
    func setRoadSnappedFocusedCameraView(snappedLat:Double, snappedLong:Double){
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(5.0)
        
        self.driverMarker.position = CLLocationCoordinate2D(latitude: snappedLat, longitude: snappedLong)
        self.driverMarker.rotation = /LocationManager.shared.heading
        self.driverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        if self.btnMyLocation == nil {
            return
        }
        if (self.btnMyLocation.isSelected){
            
            let position = GMSCameraPosition.camera(withLatitude: snappedLat, longitude: /snappedLong, zoom: (self.mapView.camera.zoom), bearing: /LocationManager.shared.heading, viewingAngle: 0)
            self.mapView.animate(to: position)
        }
        CATransaction.commit()
    }
    
    
    //MARK: To update distance and eta of ongoing ride ▶︎▶︎▶︎
    
    func updateDistanceAndDuration(Distance: Int?, Time:Int? , DistanceInMiles: String? , DurationinMinutes: String?, orderId:Int?) {
        
        DispatchQueue.main.async {
            
            // get current visible order from multiple orders to update time and distance
            if let items = DataResponse.onGoingOrders?.orders{
                let arr = Array(items.reversed())
                
                if let order = arr.enumerated().first(where: {$0.element.order_id == orderId}) {
                    order.element.distanceGoogleApi = DistanceInMiles ?? ""
                    order.element.etaTimeGoogleApi = DurationinMinutes ?? ""
                    if self.view.subviews.contains(self.DropAddressView){
                        self.collectionView.reloadItems(at: [IndexPath.init(row: order.offset, section: 0)])
                    }
                }
            }
            
            // update data customer name in collaspable view
            self.lblCustomerNameInCollaspeView.text = DataResponse.onGoingRequest?.order?.user?.name
            self.lblAddress.text = /DataResponse.onGoingRequest?.order?.dropoff_address
            self.imageViewInCollaspView.kf.setImage(with: URL.init(string: DataResponse.onGoingRequest?.order?.user?.profile_pic_url ?? ""), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}

// MARK: -------------------------------------------- RESQUEST POPUP HANDLERS -------------------------------------------------------------

extension MapViewController {
    
    //MARK: View to dispaly ongoing order ▶︎▶︎▶︎
    
    func showOngoingOrder(){
        if  DataResponse.onGoingOrders?.orders?.first?.roadStops?.count ?? 0 >= 1 {
            if DataResponse.onGoingOrders?.orders?.first?.order_status == "Confirmed" || DataResponse.onGoingOrders?.orders?.first?.order_status == "Onreached" {
                heightDropView = 300
            }else{
                heightDropView = 340
            }
            
        }else{
            if DataResponse.onGoingOrders?.orders?.first?.order_status == "Confirmed" || DataResponse.onGoingOrders?.orders?.first?.order_status == "Onreached" {
                heightDropView = 250
            }else{
                heightDropView = 290
            }
            
        }
        self.DropAddressView.frame = CGRect.init(x: 0 , y: ( self.view.frame.height) - CGFloat(self.heightDropView ), width: self.view.frame.width, height: CGFloat(self.heightDropView))
        self.view.addSubview(self.DropAddressView)
        DropAddressView.alpha = 0
        
        if isRideOngoing{
            self.isFocus = true
            if self.btnMyLocation != nil {
                self.btnMyLocation.isSelected  = true
            }
            self.mapViewbottomConstraint.constant = -250
        }
        self.configureCollectionView()
        UIView.animate(withDuration: 0.3) {
            self.DropAddressView.alpha = 1
        }
    }
    
    //MARK: Reset map after completing order ▶︎▶︎▶︎
    
    func completeOrderSetupUI(){
        self.mapView.clear()
        //      btnSwitch.isHidden = false
        isRideOngoing = false
        rideStatus.isRideOngoing = false
        self.DropAddressView.removeFromSuperview()
        self.DropAddressView.alpha = 1
        self.addInitialDriverMarker()
        UIView.animate(withDuration: 0.3) { [ unowned self] in
            self.DropAddressView.alpha = 0
            self.DropAddressView.removeFromSuperview()
            self.CollapsedOngoingOrderView.removeFromSuperview()
        }
    }
}












// MARK: ----------------------------------------------------------- API NETWORK REQUEST -------------------------------------------------------
extension MapViewController {
    
    // MARK: Set driver online/offline status ▶︎▶︎▶︎
    func setOnlineStatus(status : String,directionalHire:String){
        
        APIManager.shared.request(with: CommonEndPoint.setOnline(status: status, directional_hire: directionalHire)) {[weak self] (response) in
            //            if self?.buttonSwitch != nil {
            //            self?.buttonSwitch.isHidden = false
            //            }
            switch response{
            case .success(_):
                debugPrint("")
                
            case .failure(let err):
                //                if self?.buttonSwitch != nil {
                //                    self?.buttonSwitch.isSelected = status == "1" ? false : true
                //                }
                
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
    
    // MARK: Set driver online/offline status ▶︎▶︎▶︎
       func setDirectionalStatus(directional_hire:String){
           
        APIManager.shared.request(with: CommonEndPoint.setDiretionalHireStatus( directional_hire: directional_hire)) {[weak self] (response) in
            //            if self?.buttonSwitch != nil {
            //            self?.buttonSwitch.isHidden = false
            //            }
            switch response{
            case .success(_):
                let value = Int(directional_hire)
                self?.directionalHireButton.tag = value ?? 0
                if value == 0 {
                    let langType = BundleLocalization.sharedInstance().language
                    switch langType {
                    case Languages.Spanish:
                        self?.directionalHireButton.setImage(UIImage(named:"ic_directionHire_off_s"), for: .normal)
                    default:
                        self?.directionalHireButton.setImage(UIImage(named:"ic_direction_hire_off"), for: .normal)
                    }
                    UserDefaults.standard.set(Keys.status_value_off.rawValue, forKey: "directionalHire_status")
                }else{
                    UserDefaults.standard.set(Keys.status_value_on.rawValue, forKey: "directionalHire_status")
                    
                    let langType = BundleLocalization.sharedInstance().language
                    switch langType {
                    case Languages.Spanish:
                        self?.directionalHireButton.setImage(UIImage(named:"ic_directionHire_on_s"), for: .normal)
                    default:
                        self?.directionalHireButton.setImage(UIImage(named:"ic_direction_hire_on"), for: .normal)
                    }
                }
            case .failure(let err):
                //                if self?.buttonSwitch != nil {
                //                    self?.buttonSwitch.isSelected = status == "1" ? false : true
                   //                }
                
                   Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
               }
           }
       }
    
    //MARK::- Start ride after accepting
    func reachedRide(orderID:String, categoryId:Int) {
        
        APIManager.shared.request(with: ServicesEndPoint.reached(orderId: Int(/orderID), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) {
            [weak self] (response) in
            switch response{
                
            case .success(let _):
                
                self?.waitingTimerSetting()
                self?.runTimeCode()                                             // update on the way text on customer side
                self?.getOngoingOrders()
                
                // refresh ongoing orders
                
                //                let object = responseValue as? StartedRequest
                
            case .failure(let err):
                
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
    
    func startRide(orderID:String, categoryId:Int, waitingTime: Int) {
        
        stopWaitTimer()
        APIManager.shared.request(with: ServicesEndPoint.start(orderId: Int(/orderID), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), waitingTime: waitingTime)) {
            [weak self] (response) in
            switch response{
                
            case .success(let responseValue):
                
                self?.runTimeCode()                                             // update on the way text on customer side
                self?.getOngoingOrders()                                      // refresh ongoing orders
                
                let object = responseValue as? StartedRequest
                
                guard let dropLat = object?.dropoff_latitude else{return}
                guard let dropLng = object?.dropoff_longitude else{return}
                
                
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    
                    self?.alertBoxWithDoubleAction(message: "Becab wants to open your google map application".localized, title: "Alert".localized, ok: {
                        if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(dropLat),\(dropLng)&directionsmode=driving") {
                            UIApplication.shared.open(url, options: [:])
                        }
                    }, cancel: {
                        print("Cancel")
                    })
                    
                } else {
                    
                    
                    
                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), actionTitle: R.string.localizable.alertInstallGoogleMaps(), viewController: /self
                    ) {
                        
                        let urlStr = "itms-apps://itunes.apple.com/in/app/google-maps-transit-food/id585027354?mt=8"    // Google maps to open in appstore
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                            
                        } else {
                            UIApplication.shared.openURL(URL(string: urlStr)!)
                        }
                    }
                    
                }
                
                //                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), actionTitle: R.string.localizable.alertInstallGoogleMaps(), viewController: /self
                //                    ) {
                //
                //                        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                //                                           if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(dropLat),\(dropLng)&directionsmode=driving") {
                //                                               UIApplication.shared.open(url, options: [:])
                //                                           }
                //                        }
                //                        else {
                //                            let urlStr = "itms-apps://itunes.apple.com/in/app/google-maps-transit-food/id585027354?mt=8"    // Google maps to open in appstore
                //                            if #available(iOS 10.0, *) {
                //                                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                //
                //                            } else {
                //                                UIApplication.shared.openURL(URL(string: urlStr)!)
                //                            }
                //                        }
                //
                //                }
                
            case .failure(let err):
                
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
    
    // MARK: Update driver data and get driver detail ▶︎▶︎▶︎
    
    func updateDriverData(){
        
        APIManager.shared.request(with: CommonEndPoint.updateData(newLanguageId: token.selectedLanguage, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken)) { (response) in
            
            switch response{
            case .success(let response_value ):
                
                DataResponse.driverData = response_value as? DriverModel
                self.buttonSwitch.isSelected = DataResponse.driverData?.data?.online_status == "1"
                if DataResponse.driverData?.data?.approved == "0"{
                    
                    SocketIOManager.shared.closeConnection()
                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertDriverBlocked() , actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
                        
                        self.appDelegate.logout()
                    }
                }
                else{
                    userInfo.userid = "\(/DataResponse.driverData?.data?.user_id)"
                    self.appDelegate.checkUpdate()                                                          //check update version
                }
                
            case .failure(let err):
                Alerts.shared.show(alert:  R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
    
    func checkOnGoingNewRequest(){
        
        if DataResponse.onGoingOrders?.pendingOrders?.count  != 0{
            for item in (DataResponse.onGoingOrders?.pendingOrders)!{
                self.getOrderDetail(orderId: /item.order_id?.toString())
            }
        }
    }
    
    
    func setupNoOngoingOrder(){
        
        self.completeOrderSetupUI()
        orderDirectionResults.arrStepsLocationCoordinates = []
        DataResponse.recentUpdatedOrderId = ""
        // set padding of mapview if there is no order
        let mapInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.mapView.padding = mapInsets
    }
    
    
    func setupMapviewWithOngoingFirstItem(){
        
        let jsonItem  = DataResponse.onGoingOrders?.orders?.reversed().first?.toJSON()
        var dic = [String:Any]()
        
        // manipulate some json keys to match service request model
        dic[LocalKeys.type] = SocketConstants.serRequest
        dic [LocalKeys.order] = jsonItem
        
        let val = Mapper<ServiceRequest>().map(JSONObject: dic)
        DataResponse.onGoingRequest = val
        self.showPolyLineAndDestinationMarker()
    }
    
    
    // MARK: Get driver ongoing orders ▶︎▶︎▶︎
    
    @objc func getOngoingOrders(){
        
        APIManager.shared.request(with: ServicesEndPoint.onGoing()) { [weak self](response) in
            switch response{
                
            case .success(let responseValue ):
                
                DataResponse.onGoingOrders = responseValue as? OnGoingOrders
                
                
                /** This checks if there is currently any order request through api when the app is opened from app icon **/
                self?.checkOnGoingNewRequest()
                
                /** This set up when there is no ongoing order **/
                
                if DataResponse.onGoingOrders?.orders?.count ?? 0 ==  0{
                    self?.setupNoOngoingOrder()
                    return
                }
                
                /** show first item in ongoing order  polyline route when prevoiusly there is no updation any other item of ongoing list and ongoing count is count == 0  **/
                
                if DataResponse.recentUpdatedOrderId == ""{
                    self?.setupMapviewWithOngoingFirstItem()
                }
                
                
                for item in (DataResponse.onGoingOrders?.orders!)! {
                    
                    switch item.order_status{
                        
                    case SocketConstants.dPending:
                        
                        if (item.category_id == 4 || item.category_id == 7){
                            
                            // only one request is processes in case of truck service
                            if /DataResponse.onGoingOrders?.orders?.count == 1{
                                self?.confirmPendingRequest(request: item)
                            }
                            
                        }else{
                            
                            self?.confirmPendingRequest(request: item)
                        }
                        
                    // confirm pending request
                    case SocketConstants.reached, SocketConstants.Ongoing, SocketConstants.confirmed,SocketConstants.dApproved :
                        
                        self?.isRideOngoing = true
                        rideStatus.isRideOngoing = true
                        self?.deliveryStarted = true
                        self?.isDoneCurrentPolyline = true
                        
                        self?.showOngoingOrder()
                        
                        // show latest updated order  polyline route
                        
                        if item.order_id?.toString() == DataResponse.recentUpdatedOrderId {
                            
                            let jsonItem  = item.toJSON()
                            var dic = [String:Any]()
                            dic[LocalKeys.type] = SocketConstants.serRequest
                            dic[LocalKeys.order] = jsonItem
                            
                            let val = Mapper<ServiceRequest>().map(JSONObject: dic)
                            DataResponse.onGoingRequest = val
                            
                        }
                        
                        self?.showPolyLineAndDestinationMarker()
                        //                          if self?.buttonSwitch != nil {
                        //                        self?.buttonSwitch.isHidden = true
                        //                        }
                        if self?.serviceRequest?.order?.bookingType == "RoadPickupPhone" && item.order_status == SocketConstants.confirmed {
                            self?.dropDelivery(orderId: "\(/DataResponse.onGoingOrders?.orders?.first?.order_id)", paymentType:"\(/DataResponse.onGoingOrders?.orders?.first?.payment_type)", myTurn: "\(/DataResponse.onGoingOrders?.orders?.first?.my_turn)", orderStatus: /DataResponse.onGoingOrders?.orders?.first?.order_status, categoryId: /DataResponse.onGoingOrders?.orders?.first?.category_id, organisationCouponUserId: /DataResponse.onGoingOrders?.orders?.first?.organisation_coupon_user_id, order: DataResponse.onGoingOrders?.orders?.first,amount:"0")
                        }
                    default:
                        Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /item.order_status, type: .info)
                    }
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
    
    
    
    // MARK: Complete ongoing order▶︎▶︎▶︎
    
    func dropOngoingOrder(imageSign: UIImage?, orderId: Int , paymentType: String,amount:String?) {
        if let vc = R.storyboard.main.amountViewController(){
            vc.completion = { amount in
                self.endOrder(imageSign: imageSign, orderId: orderId , paymentType: paymentType,amount:"\(amount)")
            }
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
        }
        
    }
    
    
    func endOrder(imageSign: UIImage?, orderId: Int , paymentType: String,amount:String?){
        let distance = rideDistance.dictionaryAllRides["\(orderId)"]
        let distancekm = Double(/distance / 1000)
        
        var arrayImages: [UIImage] = []
        if let img = imageSign {
            arrayImages = [img]
        }
        
        
         let time = self.getEstimatedTimeBetweenLatLongs(lat: Float(Locations.lat.getLoc()) , long: Float(Locations.longitutde.getLoc()) , latitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude) , longitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude))
        Utility.shared.getDistanceBetweenAllPoints { (distanceValue) in
            let distanceExactValue = distanceValue
            //            if distanceExactValue.contains(" m") {
            //                distanceExactValue = "\((Double(distanceValue.replacingOccurrences(of: "m", with: "").trim()) ?? 0)/1000)"
            //            }else if distanceExactValue.contains("km") {
            //                distanceExactValue = distanceValue.replacingOccurrences(of: "km", with: "").trim()
            //            }
            
            
            guard let amountString = amount, let _ = Double(amountString) else {
                Alerts.shared.showOnTop(alert: "Becab", message: "please enter amount".localized, type: .error)
                return
            }
            
            
            APIManager.shared.request(with: ServicesEndPoint.end(orderId: orderId, orderDistance: "\(distanceExactValue)" , latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), paymentType: paymentType, order_time: "\(time)",amount:amountString)) {
                [weak self] (response) in
                guard let self = self else { return }
                
                switch response{
                case .success(let response_value):
                    rideDistance.dictionaryAllRides = [:]
                    
                    if let items =  DataResponse.onGoingOrders?.orders{
                        if items.count > 0 {
                            self.getOngoingOrders()
                        }
                    }else{
                        self.completeOrderSetupUI()
                    }
                    
                    DataResponse.completedRequest = response_value as? CompletedOrderModel
                    self.openInvoiceView(completedOrder: DataResponse.completedRequest)
                    
                case .failure(let err):
                    
                    Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
                }
            }
        }
    }
    
    
    // MARK: Get support services ▶︎▶︎▶︎
    
    func getSupportServices(){
        
        APIManager.shared.request(with: CommonEndPoint.supportServices()) { (response) in
            switch response{
            case .success(let responseValue):
                debugPrint(responseValue ?? "")
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
    
    // MARK: Change Current Ride ▶︎▶︎▶︎
    
    func changeDropToAnotherAcceptedOrder(orderId:String){
    }
    
    
    // Parse JSON data and add it to the heatmap layer.
    func addHeatmap(data:[HeatMapDataEntry])  {
        //        // Set heatmap options.
        heatmapLayer = GMUHeatmapTileLayer()
        heatmapLayer.radius = 80
        heatmapLayer.opacity = 0.8
        heatmapLayer.gradient = GMUGradient(colors: gradientColors,
                                            startPoints: gradientStartPoints!,
                                            colorMapSize: 256)
        var list = [GMUWeightedLatLng]()
        for item in data {
            let lat = item.pickup_latitude
            let lng = item.pickup_longitude
            let coords = GMUWeightedLatLng(coordinate: CLLocationCoordinate2DMake(lat!, lng!), intensity: 1.0)
            list.append(coords)
        }
        // Add the latlngs to the heatmap layer.
        heatmapLayer.weightedData = list
        // Set the heatmap to the mapview.
        heatmapLayer.map = mapView
        
    }
    
    //MARK:-Function to remove heat map layer.
    func removeHeatmap() {
        mapView.clear()
    }
    
    
    // MARK: Get Order Detail ▶︎▶︎▶︎
    
    func getOrderDetail(orderId:String){
        
        NotificationRequest.isNotified = false
        NotificationRequest.orderId = 0
        
        APIManager.shared.request(with: GeneralEndPoint.orderDetails(orderId: orderId)) { (response) in
            switch response{
                
            case .success(let responseValue):
                
                let object  =  Mapper<ServiceRequest>().map(JSONObject: responseValue)
                
                if /object?.order?.order_status == SocketConstants.dApproved || /object?.order?.order_status == SocketConstants.dPending{
                    self.getOngoingOrders()
                }
                else if /object?.order?.order_status == SocketConstants.customerCancel{
                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertsOrderCancelled(), type: .info)
                }
                else{
                    
                    if /object?.order?.order_status == SocketConstants.Searching{
                        if let val = object{
                            
                            self.openRequestPopUp(object: val)
                        }
                    }
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
    
    // MARK: Chnage drop to another ride ▶︎▶︎▶︎
    
    func changeRideToAnotherOrder(orderId:String){
        
        APIManager.shared.request(with: ServicesEndPoint.changeDropToAnotherRide(orderID: Int(orderId))) { (response) in
            switch response{
            case.success(_):
                //refresh list with ongoing order
                self.getOngoingOrders()
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: R.string.localizable.homeDestinationChanged(), type: .info)
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
}

// MARK: -------------------------------------------------------------- MAP VIEW DELEGATES -----------------------------------------------------

extension  MapViewController{
    
    func mapViewsetup(){
        
        // Status button configuration
        let images = self.getToggleStatusImage()
        if self.buttonSwitch != nil {
            buttonSwitch.setImage(images.0, for: .selected)
            buttonSwitch.setImage(images.1, for: .normal)
//            buttonSwitch.isSelected = false
        }
        self.infoWindow = loadNiB()                                                     // Load address info window of marker and map delegates
        mapView.delegate = self
        // mapView.minZoom = Float(15)
        // mapView.setMinZoom(15, maxZoom: 20)
        mapView.settings.tiltGestures = false
        mapView.mapType = .normal
        
        //        mapView.mapStyle(withFilename: "map_style", andType: "json")                     // json file to customise google map styles.
        self.setupLocationPollingTimerFromMap(interval: 5.0)
        segmentMapStyle.setTitle(R.string.localizable.homeNormal(), forSegmentAt: 0)
        segmentMapStyle.setTitle(R.string.localizable.homeSatellite(), forSegmentAt: 1)
        
        UIView.animate(withDuration: 0.0) {
            self.view.setNeedsLayout()
        }
    }
    
    func loadNiB() -> marker_info {
        let infoWindow = marker_info.instanceFromNib() as! marker_info
        return infoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if marker == userMarker{
            infoWindow.removeFromSuperview()
            infoWindow = loadNiB()
            infoWindow.lblAddress.text = /DataResponse.onGoingRequest?.order?.dropoff_address
            
            infoWindow.center = mapView.projection.point(for: userMarker.position)
            infoWindow.center.y = infoWindow.center.y - 20
            self.view.insertSubview(infoWindow, aboveSubview: mapView)
            //            self.view.bringSubview(toFront: infoWindow)
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        if isRideOngoing{
            
            UIView.transition(with: infoWindow, duration: 1, options:.preferredFramesPerSecond60 , animations: {
                self.infoWindow.center = mapView.projection.point(for: self.userMarker.position)
                
                if self.infoWindow.center.x > self.mapView.center.x{
                    self.infoWindow.center.x = self.infoWindow.center.x - 80
                }
                else{
                    self.infoWindow.center.x = self.infoWindow.center.x + 80
                }
                
                if self.infoWindow.center.y + 150 > self.mapView.center.y {
                    self.infoWindow.center.y = self.infoWindow.center.y - 20
                }
                else{
                    self.infoWindow.center.y = self.infoWindow.center.y + 100
                }
                
            }, completion: nil)
            
        }
        
        self.scaleMarkerIcons()
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if btnMyLocation == nil {
            return
        }
        if gesture{ btnMyLocation.isSelected = false }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        //        infoWindow.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        defer {
            self.zoomInitial = self.mapView.camera.zoom
        }
        
        if self.zoomInitial != self.mapView.camera.zoom {
            //            self.scaleMarkerIcons(zoom: self.zoomInitial)
        }
    }
    
    // Scale marker according to map zoom
    
    func scaleMarkerIcons(){
        
        if /mapView.camera.zoom >= minimumZoom {
            
            if previousZoom == /mapView.camera.zoom {
                return
            }
            previousZoom  = /mapView.camera.zoom
            let widthVal = (Float(vehicleSize.width)*(/mapView.camera.zoom).getZoomPercentage)/100
            let heightVal = (Float(vehicleSize.height)*(/mapView.camera.zoom).getZoomPercentage)/100
            
            vehicleCurrentSize =  CGSize(width: Double(widthVal), height: Double(heightVal))
            let Driverimage = getVehicleImage(category: /DataResponse.driverData?.data?.category_id)
            
            if isRideOngoing {
                vehicleCurrentSize = vehicleSize
            }
            
            driverMarker.icon =  Driverimage?.imageWithImage(scaledToSize: vehicleCurrentSize)
            
            if (DataResponse.onGoingRequest?.order?.category_id == 4 || DataResponse.onGoingRequest?.order?.category_id == 7 ){
                
                if DataResponse.onGoingRequest?.order?.order_status == SocketConstants.Ongoing {
                    self.dropOffMarker.icon = #imageLiteral(resourceName: "ic_drop_location_mrkr").imageWithImage(scaledToSize: vehicleCurrentSize)
                }
                else{
                    self.dropOffMarker.icon = #imageLiteral(resourceName: "ic_drop_location_other").imageWithImage(scaledToSize: vehicleCurrentSize)
                }
            } else {
                self.userMarker.icon = #imageLiteral(resourceName: "ic_drop_location_mrkr").imageWithImage(scaledToSize: vehicleCurrentSize)
            }
        }
    }
}


















// MARK: ---------------------------------------------CUSTOM DELEGATE || CALLBACK HANDLERS---------------------------------------------

extension  MapViewController{
    //MARK: Location auth chnaged ▶︎▶︎▶︎
    
    func authorizationStatusChanged(status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse,.authorizedAlways:
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                //make intial api requests if location permission is given
                self.setupController()
            }
            
        case .notDetermined:
            LocationManager.shared.updateUserLocation()
            
        case .restricted,.denied:
            
            Alerts.shared.showAlertWithActionBlock(title: R.string.localizable.alertLocationDisabledAlert(), message: R.string.localizable.alertLocationDisabledMessageMap(), actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            }
        }
    }
    
    //MARK: update driver marker after each call back from location manager.
    ////Rohit for Live Update
    func updateDriverMarker(coordinates: CLLocationCoordinate2D) {
        
        if isRideOngoing{
            self.getSnappedRoadAPiCoordinates(lat: Locations.lat.getLoc(), lng: Locations.longitutde.getLoc()) {
                [weak self](snappedLat, snappedLong) in
                
                if let val = snappedLat {
                    //update driver location and camera
                    self?.setRoadSnappedFocusedCameraView(snappedLat: val, snappedLong: snappedLong!)
                    CurrentSnappedLocation.latitude = val
                    CurrentSnappedLocation.longitude = snappedLong!
                    
                    if /(self?.isRideOngoing) && (  UIApplication.shared.applicationState == .active) {
                        self?.showPolyLineAndDestinationMarker()     //TODO: Show polyline
                    } else {
                        PolylineForCustomer.polyline = ""
                        PolylineForCustomer.distanceText = ""
                        PolylineForCustomer.timeText = ""
                        PolylineForCustomer.distanceValue = 0
                        PolylineForCustomer.timeValue = 0
                    }
                }
            }
        } else {
            
            //update driver location and camera
            self.setRoadSnappedFocusedCameraView(snappedLat: coordinates.latitude, snappedLong: coordinates.longitude)
            CurrentSnappedLocation.latitude = coordinates.latitude
            CurrentSnappedLocation.longitude = coordinates.longitude
        }
    }
    
    ////Rohit for One time one line
    //    func updateDriverMarker(coordinates: CLLocationCoordinate2D) {
    //
    //        CurrentSnappedLocation.latitude = coordinates.latitude
    //        CurrentSnappedLocation.longitude = coordinates.longitude
    //        self.setRoadSnappedFocusedCameraView(snappedLat: coordinates.latitude, snappedLong: coordinates.longitude)
    //
    //    }
    
    // Update driver marker with less accurate Location Horizontal accuracy
    func updateDriverMakerLessAccurate(coordinates: CLLocationCoordinate2D){
        self.setRoadSnappedFocusedCameraView(snappedLat: coordinates.latitude, snappedLong: coordinates.longitude)
    }
    
    // MARK:-- Get Location from kvo of google map using myLocation Property
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let  val:[NSKeyValueChangeKey:Any] = change else {
            return
        }
        
        guard let location = val[NSKeyValueChangeKey.newKey] as? CLLocation else{
            return
        }
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        
        self.driverMarker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        self.driverMarker.rotation = /LocationManager.shared.heading
        self.driverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        if self.btnMyLocation == nil {
            return
        }
        if (self.btnMyLocation.isSelected){
            
            let position = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: (self.mapView.camera.zoom), bearing: /LocationManager.shared.heading, viewingAngle: 0)
            self.mapView.animate(to: position)
        }
        CATransaction.commit()
    }
    
    
    //MARK: Request Accepted ▶︎▶︎▶︎
    
    func acceptedRequest() {
        self.getOngoingOrders()
    }
    
    
    
    //MARK: Confirm Scheduled request
    
    func confirmPendingRequest(request:OrdersList){
        // Present confirm request view controller
        guard let vc = R.storyboard.main.confirmScheduledViewController() else {return}
        vc.serviceRequest = request
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        
        if !(/(self.navigationController?.presentedViewController?.isKind(of: ConfirmScheduledViewController.self))){
            self.navigationController?.visibleViewController?.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Request Started ▶︎▶︎▶︎
    
    func RequestOngoingProcessed(){
        self.getOngoingOrders()
    }
    
    //MARK: Request cancelled ▶︎▶︎▶︎
    
    func RequestOngoingCancelled(){
        self.completeOrderSetupUI()
    }
    
    //MARK: Request confirmed ▶︎▶︎▶︎
    
    func confirmedRequest() {
        self.getOngoingOrders()
    }
    
    //MARK:- Open rating view ▶︎▶︎▶︎
    
    func RequestInvoiceProcessed(){
        guard let vc = R.storyboard.main.rateRideViewController() else {return}
        vc.completedRequest = DataResponse.completedRequest
        vc.delegate = self
        
        openRequestVC(vc: vc)
    }
    
    //MARK: Rating Completed ▶︎▶︎▶︎
    
    func DeliveryRatingCompleted(){
        self.directionalHireButton.isHidden = false
        self.directionalHireButtonHeight.constant = 32
        Alerts.shared.show(alert: R.string.localizable.alertSuccess(), message: R.string.localizable.rateRatedSuccesfully(), type: .success)
        self.updateDriverData()
    }
    
    //MARK: Delegate callback to drop delivery
    
    func dropDelivery(orderId: String , paymentType:String,myTurn:String, orderStatus:String, categoryId: Int, organisationCouponUserId: Int, order: OrdersList?,amount:String?) {
        
        
        //
        DataResponse.recentUpdatedOrderId = orderId
        
        switch orderStatus {
            
        case SocketConstants.confirmed:
            if (categoryId == 7 || categoryId == 4) {
                self.reachedRide(orderID: orderId, categoryId: categoryId)
            } else {
                self.startRide(orderID: orderId, categoryId: categoryId, waitingTime: waitingTimeValue)
            }
            
        case SocketConstants.reached:
            self.startRide(orderID: orderId, categoryId: categoryId, waitingTime: waitingTimeValue)
            
        case SocketConstants.dApproved:
            self.startRide(orderID: orderId,  categoryId: categoryId, waitingTime: waitingTimeValue)
            
        case SocketConstants.Ongoing:
            
            if myTurn == "1"{
                
                // open deliver vc for etoken water delivery
                if  organisationCouponUserId != 0 {
                    guard let vc =   R.storyboard.settingMenu.deliverDrinkingWaterViewController() else{return}
                    vc.item  = order
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    
                    if categoryId == 4 {
                        guard let vc =   R.storyboard.main.dropConfirmSignatureVC() else{return}
                        self.present(vc, animated: false, completion: nil)
                        
                        vc.blockDone = {
                            [weak self] (image) in
                            guard let self = self else { return }
                            self.dropOngoingOrder(imageSign: image, orderId: /Int(/orderId), paymentType: paymentType, amount: amount)
                        }
                        return
                    }
                    
                    // drop ride after comfirmation
                    Alerts.shared.showAlertWithActionBlock(title: R.string.localizable.alertsAlertdropOffLocation(), message: R.string.localizable.alertsAlertdropOffLocationMessage(), actionTitle: "alertTitle.ok".localized, viewController: self) {
                        self.dropOngoingOrder(imageSign: nil, orderId: /Int(/orderId), paymentType: paymentType,amount: amount)
                    }
                }
            }else{
                //Change ride to another customer
                self.changeRideToAnotherOrder(orderId: orderId)
            }
            
        default:
            debugPrint(orderStatus)
        }
    }
    
    
    func breakDown(orderId: String) {
        Alerts.shared.showAlertWithActionBlock(title: "Vehicle Breakdown", message: "Are you sure you want to Break Down the ride", actionTitle: "alertTitle.ok".localized, viewController: self) {
            let distanceInKMeters = (self.getDistanceBetweenLatLongs(lat: Float(Locations.lat.getLoc()) , long: Float(Locations.longitutde.getLoc()) , latitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude) , longitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude))/1000)
            Utility.shared.getDistanceBetweenAllPoints { (distanceValue) in
                let distanceExactValue = distanceValue
                APIManager.shared.request(with: ServicesEndPoint.breakDown(orderId: Int(/orderId) ?? 0, address: "", reason: "", latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), orderDistance: "\(distanceExactValue)")) {
                    [weak self] (response) in
                    switch response{
                        
                    case .success(let responseValue):
                        rideDistance.dictionaryAllRides = [:]
                        
                        if let items =  DataResponse.onGoingOrders?.orders{
                            if items.count > 0 {
                                self?.getOngoingOrders()
                            }
                        }else{
                            self?.completeOrderSetupUI()
                        }
                        
                        DataResponse.completedRequest = responseValue as? CompletedOrderModel
                        self?.openInvoiceView(completedOrder: DataResponse.completedRequest,breakdown: true)
                        
                    case .failure(let err):
                        Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
                    }
                }
            }
        }
    }
    
    func addStops() {
        guard let vc = R.storyboard.main.roadPickaUpLoactionController() else {return}
        vc.isComingFromAddStops = true
        vc.delegate = self
        vc.order = DataResponse.onGoingRequest?.order
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:-Half Way Stopping.
    func halfWayStopping(orderId: String, paymentType: String,halfWayStopReason:String) {
        
        
        Alerts.shared.showAlertWithActionBlock(title: "Alert!", message: "Are you sure you want to stop the ride", actionTitle: "alertTitle.ok".localized, viewController: self) {
            self.stopHalfWay(orderId: orderId, paymentType: paymentType, halfWayStopReason: halfWayStopReason, amount: 0)
        }
    }
    
    func stopHalfWay(orderId: String, paymentType: String,halfWayStopReason:String,amount:Double){
        let distanceInKMeters = (self.getDistanceBetweenLatLongs(lat: Float(Locations.lat.getLoc()) , long: Float(Locations.longitutde.getLoc()) , latitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude) , longitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude))/1000)
        //        let coordinate₀ = CLLocation(latitude: pickupAddress?.lat ?? 0, longitude: pickupAddress?.long ?? 0)
        //        let coordinate₁ = CLLocation(latitude : Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc() )
        //        let distanceInKMeters = (coordinate₀.distance(from: coordinate₁))/1000
        let time = self.getEstimatedTimeBetweenLatLongs(lat: Float(Locations.lat.getLoc()) , long: Float(Locations.longitutde.getLoc()) , latitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude) , longitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude))
        
        Utility.shared.getDistanceBetweenAllPoints { (distanceValue) in
            let distanceExactValue = distanceValue
            APIManager.shared.request(with: ServicesEndPoint.halfWayStopping(orderId: Int(/orderId) ?? 0, paymentType: paymentType, orderDistance: "\(distanceInKMeters)", halfWayStopReason: halfWayStopReason, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(),amount:amount)) {
                [weak self] (response) in
                switch response{
                    
                case .success(let responseValue):
                    rideDistance.dictionaryAllRides = [:]
                    
                    if let items =  DataResponse.onGoingOrders?.orders{
                        if items.count > 0 {
                            self?.getOngoingOrders()
                        }
                    }else{
                        self?.completeOrderSetupUI()
                    }
                    
                    DataResponse.completedRequest = responseValue as? CompletedOrderModel
                    self?.openInvoiceView(completedOrder: DataResponse.completedRequest)
                    
                case .failure(let err):
                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
                }
            }
        }
    }
    
    
    //MARK: DELGATE CALLED TO COLLAPSE ORDER VIEW
    
    func CollapseCollectionView(orderStatus:String,orderID:String,categoryId:Int ) {
        self.actionAdjustDropView(self)
    }
    
    func goToChat(item:OrdersList,name:String,profilePic:String){
        
        guard let vc = R.storyboard.main.chatVC() else{return}
        vc.otherUserDetailId = /item.customer_user_detail_id
        vc.otherUserId = /item.customer_user_id
        vc.name = name
        vc.profilePic = profilePic
        vc.orderId = /item.order_id?.toString()
        self.navigationController?.pushViewController(vc, animated: true)
       // self.pushVC(vc)
    }
    
    
    func CancelCurrentRide(orderStatus:String,orderID:String,categoryId:Int ) {
        
        if orderStatus != SocketConstants.confirmed && orderStatus != SocketConstants.dApproved{
            self.actionAdjustDropView(self)
        }
        else{
            // adjust drop view in truck
            if categoryId == 4{
                if orderStatus == SocketConstants.dApproved{
                    self.actionAdjustDropView(self)
                    return
                }
            }
            // cancel pop alerts in water,tank, gas
            guard let vc = R.storyboard.main.cancellationViewController() else {return}
            vc.orderID = orderID
            vc.reasonDelagate = self
            
            self.openRequestVC(vc: vc)
        }
    }
    
    //MARK: Ride Cancelled by driver after reason
    @objc func nCancelFromBookinScreen(notifcation : Notification) {
        if let _ = notifcation.object as? String {
            cancelRide(reason: "")
        }
    }
    
    
    @objc func showHeatMapNoti(notifcation : Notification) {
        self.removeHeatmap()
        self.fetchHeatMapDataFromBackend()
    }
    
    @objc func removeHeatMapNoti(notifcation : Notification) {
        self.removeHeatmap()
    }
    
    func cancelRide(reason: String) {
        self.getOngoingOrders()  // refresh ongoing orders
    }
}


//MARK: - ----------------------------------------------- COLLECTION VIEW DELEGATES ----------------------------------------------------------
extension MapViewController{
    
    //MARK: Configure Collection View
    func configureCollectionView(){
        self.directionalHireButton.isHidden = true
        self.directionalHireButtonHeight.constant = 0
        if let items = DataResponse.onGoingOrders?.orders{
            let arrDatasource = Array(items.reversed())
            dataSource = CollectionViewDataSource.init(items: arrDatasource, collectionView: collectionView, cellIdentifier: R.reuseIdentifier.onGoingRidesCollectionViewCell.description, headerIdentifier: "", cellHeight: CGFloat(heightDropView), cellWidth: self.view.frame.width , configureCellBlock: { [weak self] (cell, item, indexPath) in
                var data = item as? OrdersList
                (cell as? OnGoingRidesCollectionViewCell)?.item = item as? OrdersList
                (cell as? OnGoingRidesCollectionViewCell)?.delegate = self
                (cell as? OnGoingRidesCollectionViewCell)?.indx = indexPath.row
                (cell as? OnGoingRidesCollectionViewCell)?.vc = self
                (cell as? OnGoingRidesCollectionViewCell)?.friendName = self?.serviceRequest?.order?.friendName
                (cell as? OnGoingRidesCollectionViewCell)?.friendPhoneNumber = self?.serviceRequest?.order?.friendPhoneNumber
                (cell as? OnGoingRidesCollectionViewCell)?.friendCountryCode = self?.serviceRequest?.order?.friendPhoneCode
                (cell as? OnGoingRidesCollectionViewCell)?.bookingtype = self?.serviceRequest?.order?.bookingType
                }, aRowSelectedListener: { (indexPath, item) in
                    debugPrint(indexPath)
            }, willDisplayCell: { (indexpath) in
                debugPrint(indexpath)
            }, scrollViewDelegate: { (scrollView) in
                debugPrint(scrollView)
            },scrollViewDecelerate:{ (scrollView) in
                
                self.OnMultipleRidesScroll()
                
            })
        }
    }
    
    
    
    //MARK:  Reset map and ride details on scrolling multiple rides.
    
    func OnMultipleRidesScroll(){
        
        let x = self.collectionView.contentOffset.x
        let w = self.collectionView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        if currentPage > /DataResponse.onGoingOrders?.orders?.count - 1{
            return
        }
        // Clear map and create new polyline for each scrolled order
        self.mapView.clear()
        if let items = DataResponse.onGoingOrders?.orders{
            
            let ordersReversed = Array(items.reversed())
            let jsonItem  = ordersReversed[currentPage].toJSON()
            var dic = [String:Any]()
            dic[LocalKeys.type] = SocketConstants.serRequest
            dic [LocalKeys.order] = jsonItem
            
            let val = Mapper<ServiceRequest>().map(JSONObject: dic)
            DataResponse.onGoingRequest = val
            self.showPolyLineAndDestinationMarker()
            self.updateDistanceAndDuration(Distance: 0, Time: 0, DistanceInMiles: "", DurationinMinutes: "", orderId: DataResponse.onGoingRequest?.order?.order_id)
        }
    }
    
}
















//MARK: - --------------------------------- NOTIFICATION OBSERVERS AND POLLING TIMERS ----------------------------------------------------

extension MapViewController{
    
    //MARK: Add Observers for order notification and others
    func addObserver(){
        
        let notificationsDic  = [NotificationNames.fcmTokenUpdated.rawValue:#selector(UpdateFcmTokenServer(notification:)),
                                 NotificationNames.openRequestModal.rawValue: #selector(openRequestPopUpNotification(notification:)),
                                 NotificationNames.confirmScheduled.rawValue: #selector(confirmScheduledRequest(notification:)),
                                 NotificationNames.requestCancelled.rawValue : #selector(requestCancelled(notification:)),
                                 NotificationNames.didBecomeActive.rawValue : #selector(didBecomeActive(notification:)),
                                 NotificationNames.internetConnected.rawValue : #selector(internetReachablity),
                                 NotificationNames.internetDisconnected.rawValue : #selector(internetNotReachable),
                                 NotificationNames.readPendingNotifications.rawValue : #selector(openDeliveredAndValidNotifications),
                                 NotificationNames.refreshOrders.rawValue : #selector(getOngoingOrders),
                                 NotificationNames.nCancelFromBookinScreen.rawValue : #selector(nCancelFromBookinScreen(notifcation:)),        NotificationNames.showHeatMapData.rawValue : #selector(showHeatMapNoti(notifcation:)),        NotificationNames.removeHeatMapNoti.rawValue : #selector(removeHeatMapNoti(notifcation:))]
        
        for item in NotificationNames.allCases{
            if let val = notificationsDic[item.rawValue]{
                NotificationCenter.default.addObserver(self, selector: val, name: NSNotification.Name.init(rawValue: item.rawValue), object: nil)
            }
        }
    }
    
    // get order detail if came after tapping on notification screen
    
    @objc  func openDeliveredAndValidNotifications(){
        
        if NotificationRequest.isNotified   {
            if NotificationRequest.orderIds.count > 0{
                for item in NotificationRequest.orderIds{
                    self.getOrderDetail(orderId: "\(item)")
                }
                NotificationRequest.orderIds = []
            }
        }
    }
    
    @objc  func internetReachablity(){
        
        DispatchQueue.main.async {
            Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message:R.string.localizable.alertConnectionEstablished(), type: .success)
        }
        
        self.initialNetworkRequests()                                                           // refresh data from server
    }
    @objc  func internetNotReachable(){
        
        Alerts.shared.showAlertWithActionBlockForce(title:  R.string.localizable.alertConnection(), message:  R.string.localizable.alertConnectionMessage(), actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
        }
    }
    
    @objc func UpdateFcmTokenServer(notification:Notification){
        self.updateDriverData()
    }
    
    @objc func openRequestPopUpNotification(notification:Notification){
        
        if !SocketIOManager.shared.isAuthenticated{
            self.connectToSocket()
        }
        let dic = notification.userInfo as NSDictionary?
        let orderId = dic?.value(forKey: SocketConstants.order_id) as? String
        self.getOrderDetail(orderId: "\(orderId!)")
    }
    
    @objc func confirmScheduledRequest(notification:Notification){
        
        if !SocketIOManager.shared.isAuthenticated{
            self.connectToSocket()
        }
        self.getOngoingOrders()
    }
    
    @objc func requestCancelled(notification:Notification){
        
        if !SocketIOManager.shared.isAuthenticated{
            self.connectToSocket()
        }
        self.getOngoingOrders()
    }
    
    @objc func didBecomeActive(notification:Notification){
        
    }
    
    //MARK: Setup Timers
    
    func setupLocationPollingTimer(interval:Double){
        
        self.registerBackgroundTask()
        socketTimer?.invalidate()
        socketTimer = nil
        socketTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(self.runTimeCode), userInfo: nil, repeats: true)
        socketTimer?.fire()
    }
    
    
    func waitingTimerSetting() {
        self.registerBackgroundTask()
        self.waitngTimerCounter = 0
        waitingTimer?.invalidate()
        waitingTimer = nil
        waitingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.waitDuration), userInfo: nil, repeats: true)
        waitingTimer?.fire()
    }
    
    
    @objc func waitDuration() {
        
        let seconds = fmod(Double(self.waitngTimerCounter), 60)
        let minutes = fmod(trunc(Double(self.waitngTimerCounter / 60)), 60)
        print("Waiting Minuts", minutes)
        self.waitingTimeValue = Int(minutes)
        self.waitngTimerCounter += 1
    }
    
    
    func stopWaitTimer(){
        //shut down timer
        
        waitingTimer?.invalidate()
        waitingTimer = nil
    }
    
    
    
    
    //MARK: Timer for sending location to server via socket after 5 seconds ▶︎▶︎▶︎
    
    @objc func runTimeCode(){
        
        self.updateBearing()                                                              // update bearing each time
        
        //Stop any kind of AvSpeech or notification tone from request model after Request model is dismissed
        if !(/self.navigationController?.presentedViewController?.isKind(of: RequestModelViewController.self)){
            
            SpeechSynthesizer.shared.stopSpeaking()
            SpeechSynthesizer.shared.stopToneAudio()
        }
        
        //send data to socket when there is no ride
        if SocketIOManager.shared.isAuthenticated && !(self.isRideOngoing) {
            self.isDoneCurrentPolyline = true
            SocketIOManager.shared.sendCommonEventData(lat: CurrentSnappedLocation.latitude, lng: CurrentSnappedLocation.longitude)
        }
            //send data to socket when there there is ride ongoing
        else if (self.isRideOngoing) {
            SocketIOManager.shared.sendCommonEventDataWithRide(lat: CurrentSnappedLocation.latitude, lng: CurrentSnappedLocation.longitude)
        }
    }
    
    
    
    func setupLocationPollingTimerFromMap(interval:Double){
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        
    }
}


extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}


extension MapViewController : DirectionalHireDelegate {
    func updateStatus(address:Address) {
        APIManager.shared.request(with: LoginEndpoint.updateHomeAddress(home_address: address.formatted, home_longitude: address.long ?? 0.0, home_latitude: address.lat ?? 0.0 )) { (response) in
            
            switch response {
            case .success(let response_value ):
                
                APIManager.shared.request(with: CommonEndPoint.updateData(newLanguageId: token.selectedLanguage, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: "", fcmId: token.fcmToken)) {
                    [weak self](response) in
                    switch response{
                    case .success(let response_value ):
                        DataResponse.driverData = response_value as? DriverModel
                        self?.setDirectionalStatus(directional_hire:Keys.status_value_on.rawValue )
                        
                    case .failure(let err):
                        Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
                    }
                }
                
                
            case .failure( let str):
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
            }
        }
        
    }
}
extension MapViewController : RoadPickaUpLoactionDelegate {
    func getOrderData() {
        self.getOngoingOrders()
    }
    
    
}


extension MapViewController : CLLocationManagerDelegate {
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 15.0, bearing: /LocationManager.shared.heading, viewingAngle: 0)
        self.mapView?.animate(to: camera)
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManagerVariable.stopUpdatingLocation()
        
    }
    
    
    func updateRideStop(orderId: Int, ride: RideStops) {
        
        var waitingTime = 0.0
        if /ride.stop_status == "ongoing"{
            
            if let startTime = UserDefaults.standard.value(forKey: "RideStopStartTime") as? Int{
                
                let currentTime = Int(Date().timeIntervalSince1970)
                
                waitingTime = Double((currentTime - startTime))/60.0
            }
        }
        
        self.updateStops(orderId: orderId,rideStopId:/ride.ride_stop_id, waitingTime: waitingTime, type: /ride.stop_status == "pending" ? "update" : "end")
    }
    
    
    func updateStops(orderId:Int,rideStopId:Int,waitingTime:Double,type:String){
        
        
        
        APIManager.shared.request(with: ServicesEndPoint.updateStop(orderId: orderId, stopId: rideStopId, waitingTime: "\(waitingTime)", type: type)) { (response) in
            
            switch response {
                
            case .success(let response_value):
                
                if type == "update"{
                    
                    let currentTime = Int(Date().timeIntervalSince1970)
                    UserDefaults.standard.set(currentTime, forKey: "RideStopStartTime")
                    
                } else{
                    
                    UserDefaults.standard.removeObject(forKey: "RideStopStartTime")
                }
                self.getOngoingOrders()
                
            case .failure(let err):
                
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
        
        
        
    }
    
    
}
