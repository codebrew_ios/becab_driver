//
//  DropConfirmSignatureVC.swift
//  Buraq24Driver
//
//  Created by Sandeep Kumar on 04/06/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class DropConfirmSignatureVC: UIViewController {
    
    //MARK:- ======== Outlets ========
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var viewPopUp: UIView!

    //MARK:- ======== Variables ========
    var blockDone: ((UIImage) -> ())?
    var isSigned:Bool = false
    
    //MARK:- ======== LifeCycle ========
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()

        viewPopUp.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
        UIView.animate(withDuration: 0.5) {
            self.viewPopUp.transform = .identity
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }
    }
    
    //MARK:- ======== Actions ========
    @IBAction func didTapCleanSign(_ sender: Any) {
        signatureView.clear()
        isSigned = false
    }
    
    @IBAction func didTapCancel(_ sender: Any) {
        dismiss(animated: true) {
            
        }
    }
    
    @IBAction func didTapComplete(_ sender: Any) {
        
        if isSigned, let signatureImage = self.signatureView.getSignature(scale: 10) {
            
            done(image: signatureImage)
            return
        }
        Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Signature first", type: .error)
    }
    
    //MARK:- ======== Functions ========
    func initalSetup() {
        signatureView.delegate = self
        

    }
    
    func done(image: UIImage) {
        dismiss(animated: true) {
            [weak self] in
            guard let self = self else { return }
            self.blockDone?(image)
        }
    }
    
}

extension DropConfirmSignatureVC: YPSignatureDelegate {
    func didStart(_ view: YPDrawSignatureView) {
        print("Started Drawing")
        isSigned = false
    }
    
    // didFinish(_ view: YPDrawSignatureView) is called rigth after the last touch of a gesture is registered in the view.
    // Can be used to enabe scrolling in a scroll view if it has previous been disabled.
    func didFinish(_ view: YPDrawSignatureView) {
        print("Finished Drawing")
        isSigned = true
    }
}
