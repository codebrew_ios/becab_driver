//
//  MapViewControllerExtension.swift

import UIKit
import GoogleMaps
import ObjectMapper
import SocketIO
import SwiftyJSON
import IBAnimatable
import SideMenu
import Alamofire
import Polyline


let minimumZoom : Float = 50.0
let maximumZoom : Float = 20.0
let vehicleSize : CGSize = CGSize(width: 24.0, height: 38.0)


//MARK: Stuct related to sockets and ongoing orders and request incoming  pop up 

struct orderDirectionResults{
    
    static var arrStepsLocation = [CLLocation]()
    static var arrStepsLocationCoordinates = [CLLocationCoordinate2D]()
    static var arrStepsPolyline = [String]()
    static var orderId = Int()
}


struct CurrentSnappedLocation{
    static var latitude = 0.0
    static var longitude = 0.0
}

struct rideStatus {
    static var isRideOngoing  = false {
        didSet {
            
            print("OK")
        }
    }
}

struct PolylineForCustomer {
    static var polyline = ""
    static var distanceText = ""
    static var distanceValue = Int()
    static var timeText = ""
    static var timeValue = Int()
}

extension MapViewController {
    
    typealias CompletionHandlerRoad = (_ Lat:Double?, _ Lng:Double?) -> Void
    
    func getEstimatedTimeBetweenLatLongs(lat : Float , long : Float , latitude: Float , longitude: Float )  -> Int {
        let userCoordinates = CLLocation(latitude: CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
        
        let strUrl = "http://maps.googleapis.com/maps/api/directions/json?origin=\(lat),\(long)&destination=\(latitude),\(longitude)&sensor=false&mode=\("DRIVING")"
        let url = URL(string: (strUrl as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? "")
        var jsonData: Data? = nil
        if let url = url {
            do {
                try jsonData = Data(contentsOf: url)
            }
            catch {
            }
        }
        if jsonData != nil {
            var error: Error? = nil
            var result: Any? = nil
            do {
                if let jsonData = jsonData {
                    result = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                }
            } catch {
            }
            var arrDistance = (result as? [AnyHashable : Any])?["routes"] as? [AnyHashable]
            if arrDistance?.count == 0 {
                return 0
            } else {
                var arrLeg = (arrDistance?[0] as? [AnyHashable : Any])?["legs"] as? [AnyHashable]
                var dictleg = arrLeg?[0] as? [AnyHashable : Any]
                if let object = (dictleg?["duration"] as? [AnyHashable : Any])?["text"] {
                    return object as? Int ?? 0
                }
            }
        } else {
            return 0
        }
        return 0
    }
    // MARK: Distance between two locations
    func getDistanceBetweenLatLongs(lat : Float , long : Float , latitude: Float , longitude: Float ) -> Double {
        let userCoordinates = CLLocation(latitude: CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
        return userCoordinates.distance(from: CLLocation(latitude: Double(lat) , longitude: Double(long)))
    }
    
    //MARK::- GET POLYLINE BETWEEN TWO LOCATIONS
    func getPolylineRoute(from source: CLLocationCoordinate2D? , to destination: CLLocationCoordinate2D?, toDrop destinationDrop: CLLocationCoordinate2D? , isFirst : Bool, orderID:Int?){
        if isDoneCurrentPolyline{
            if oldCordinates != nil{
                
                let _ = getDistanceBetweenLatLongs(lat: Float((/source?.latitude)) , long: Float(/source?.longitude) , latitude: Float(/oldCordinates?.latitude) , longitude: Float(/oldCordinates?.longitude))
                
                if !(/isFirst){
                    //                    if Int(distance) < 1{   // increase distance to 50 also make sure path is once created for every id orderid
                    //                        return
                    //                    }
                }
            }
            
            deliveryStarted = false   // this variable helps creating first time then its value is passed in in isFirst to check the distance check above
            isDoneCurrentPolyline = false
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            var urlString : String = ""
            if DataResponse.onGoingRequest?.order?.roadStops?.count == 1 {
                urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving&key=\(APIConstants.googleApiKey)&waypoints=via:\(/DataResponse.onGoingRequest?.order?.roadStops?.first?.latitude),\(/DataResponse.onGoingRequest?.order?.roadStops?.first?.longitude)"
            }else if  DataResponse.onGoingOrders?.orders?.first?.roadStops?.count == 2 {
                urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving\(/DataResponse.onGoingRequest?.order?.roadStops?.first?.latitude),\(/DataResponse.onGoingRequest?.order?.roadStops?.first?.longitude)|via:\(/DataResponse.onGoingRequest?.order?.roadStops?.last?.latitude),\(/DataResponse.onGoingRequest?.order?.roadStops?.last?.longitude)"
            } else {
                urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&language=en&mode=driving&key=\(APIConstants.googleApiKey)"
            }
            
            guard let nsString  = NSString.init(string: urlString).addingPercentEscapes(using: String.Encoding.utf8.rawValue),
                let url = URL(string: nsString) else {return}
            
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    self.isDoneCurrentPolyline = true
                }else {
                    do {
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                            
                            let _ = JSON(json)
                            
                            self.isDoneCurrentPolyline = true
                            self.apiCounter += 1
                            
                            DispatchQueue.main.async {
                                //
                                //                            }
                                //                            
                                //                            DispatchQueue.global(qos: .userInteractive).async {
                                [weak self] () -> Void in
                                guard let routes = json["routes"] as? NSArray else {
                                    return
                                }
                                
                                if (routes.count > 0) {
                                    
                                    let overview_polyline = routes[0] as? NSDictionary
                                    let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                    
                                    let arrWayPointsLegs = overview_polyline?["legs"] as? NSArray
                                    let dic = arrWayPointsLegs?.firstObject as? NSDictionary
                                    let arrSteps = dic?.value(forKey: "steps") as? NSArray
                                    
                                    for item in arrSteps!{
                                        let val = item as? NSDictionary
                                        let loc = val?.value(forKey: "start_location") as? NSDictionary
                                        let polylineSteps = val?.value(forKey: "polyline") as? NSDictionary
                                        let pointsSteps = polylineSteps?.value(forKey: "points") as? String
                                        let lat = loc?.value(forKey: "lat") as? Double
                                        let lng = loc?.value(forKey: "lng") as? Double
                                        
                                        let locObj = CLLocation.init(latitude: /lat, longitude: /lng)
                                        orderDirectionResults.arrStepsLocation.append(locObj)
                                        orderDirectionResults.arrStepsPolyline.append(/pointsSteps)
                                    }
                                    
                                    var bearingFirstLat : CLLocationCoordinate2D?
                                    var bearingSecondLat : CLLocationCoordinate2D?
                                    
                                    guard let points = dictPolyline?.object(forKey: "points") as? String else { return }
                                    
                                    let polyline = Polyline(encodedPolyline: points)
                                    let decodedCoordinates: [CLLocationCoordinate2D]? = polyline.coordinates
                                    
                                    orderDirectionResults.arrStepsLocationCoordinates = decodedCoordinates ?? []
                                    
                                    guard let legs  = routes[0] as? NSDictionary , let legsJ = legs["legs"] as? NSArray , let lg = legsJ[0] as? NSDictionary else { return }
                                    
                                    let duration = lg["duration"] as? NSDictionary
                                    let durationLeft1 = duration?.object(forKey: "text") as? String
                                    let durationLeftSeconds = duration?.object(forKey: "value") as? Int
                                    
                                    var durationLeft = ""
                                    var distanceLeft = ""
                                    
                                    durationLeft = /durationLeft1
                                    
                                    let distance = lg["distance"] as? NSDictionary
                                    let distanceLeft1 = distance?.object(forKey: "text") as? String
                                    let distanceLeftMeters = distance?.object(forKey: "value") as? Int
                                    
                                    rideDistance.distance = /distanceLeftMeters
                                    rideDistance.distanceTime = /durationLeftSeconds
                                    
                                    distanceLeft = /distanceLeft1
                                    
                                    //TODO: Update view for distance, eta in ongoing orders view
                                    
                                    
                                    self?.updateDistanceAndDuration(Distance: distanceLeftMeters, Time: durationLeftSeconds, DistanceInMiles: distanceLeft, DurationinMinutes: durationLeft, orderId: DataResponse.onGoingRequest?.order?.order_id)
                                    
                                    let startLocDict = lg["start_location"] as? NSDictionary
                                    let startLat = startLocDict?.object(forKey: "lat") as? Double
                                    let startLong = startLocDict?.object(forKey: "lng") as? Double
                                    
                                    let endLocDict = lg["end_location"] as? NSDictionary
                                    let endLat = endLocDict?.object(forKey: "lat") as? Double
                                    let endLong = endLocDict?.object(forKey: "lng") as? Double
                                    
                                    let steps = lg["legs"] as? NSArray ?? []
                                    if steps.count >= 2{
                                        let step1 = steps[0] as? NSDictionary
                                        let startLocDictStep1 = step1?["start_location"] as? NSDictionary
                                        let startLatStep1 = startLocDictStep1?.object(forKey: "lat") as? Double
                                        let startLongStep1 = startLocDictStep1?.object(forKey: "lng") as? Double
                                        
                                        let step2 = steps[1] as? NSDictionary
                                        let startLocDictStep2 = step2?["start_location"] as? NSDictionary
                                        let startLatStep2 = startLocDictStep2?.object(forKey: "lat") as? Double
                                        let startLongStep2 = startLocDictStep2?.object(forKey: "lng") as? Double
                                        
                                        bearingFirstLat = CLLocationCoordinate2D(latitude: CLLocationDegrees(/startLatStep1) , longitude: CLLocationDegrees(/startLongStep1) )
                                        bearingSecondLat = CLLocationCoordinate2D(latitude: CLLocationDegrees(/startLatStep2) , longitude: CLLocationDegrees(/startLongStep2) )
                                    }
                                    
                                    let bear1 = bearingFirstLat ?? CLLocationCoordinate2D(latitude: CLLocationDegrees(/startLat) , longitude: CLLocationDegrees(/startLong) )
                                    let bear2 = bearingSecondLat ?? CLLocationCoordinate2D(latitude: CLLocationDegrees(/endLat) , longitude: CLLocationDegrees(/endLong) )
                                    
                                    guard let dest = destination else { return }
                                    let degree = self?.degreeBearing(fromCoordinate: self?.oldCordinates ?? dest  , toCoordinate: bear2 )
                                    
                                    //                                    PolylineForCustomer.polyline = points
                                    PolylineForCustomer.distanceText = /distanceLeft
                                    PolylineForCustomer.timeText = /durationLeft
                                    PolylineForCustomer.distanceValue = /distanceLeftMeters
                                    PolylineForCustomer.timeValue = /durationLeftSeconds
                                    
                                    DispatchQueue.main.async { () -> Void in
                                        
                                        self?.updateLatLongOnMap(source: bear1 , to: CLLocationCoordinate2D(latitude: CLLocationDegrees(/endLat) , longitude: CLLocationDegrees(/endLong) ) , points :  /points)
                                        self?.updateMarker(degrees: /degree, duration: 2.0)
                                        self?.oldCordinates = bear1
                                    }
                                }
                            }
                        }
                    }catch {
                        debugPrint("error in JSONSerialization")
                    }
                }
            })
            task.resume()
        }
    }
    
    //MARK: - GET SNAPPED ROAD API COORDINATES
    
    func  getSnappedRoadAPiCoordinates(lat:Double,lng:Double,completionHandler: @escaping CompletionHandlerRoad) {
        
        let path = "https://roads.googleapis.com/v1/snapToRoads?path=\(lat),\(lng)&interpolate=true&key=\(APIConstants.googleApiKey)"
        Alamofire.request(path, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result{
            case .success(let data):
                
                let  dic = data as? NSDictionary
                guard let resp = dic?.value(forKey: "snappedPoints") as? Array<Any> , let snappedDic = resp.first as? NSDictionary , let location = snappedDic.value(forKey: "location") as? NSDictionary ,let latitude = location.value(forKey: "latitude") as? Double , let longitude = location.value(forKey: "longitude") as? Double else {return}
                
                self.apiCounter += 1
                
                completionHandler(latitude,longitude)
                
            case .failure(_):
                completionHandler(lat,lng)
            }
        }
    }
    
    func getStartToEndPath(polyStr :String){
        let path = GMSMutablePath(fromEncodedPath: polyStr)
        polyline = GMSPolyline(path: path)
        freightPolyLineHelper.orderID = /DataResponse.onGoingRequest?.order?.order_id
        if let val = polyline{
            freightPolyLineHelper.polyline = val
        }
    }
    
    //MARK:: Calculate disatance and time eta between two coordinates
    
    func calculateTimeAndDistancefreightHelper(from source: CLLocationCoordinate2D? , to destination: CLLocationCoordinate2D?){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving&key=\(APIConstants.googleApiKey)")!
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
            }else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        
                        DispatchQueue.main.async { [weak self] () -> Void in
                            guard let routes = json["routes"] as? NSArray else {
                                return
                            }
                            if (routes.count > 0) {
                                
                                let overview_polyline = routes[0] as? NSDictionary
                                let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                guard let points = dictPolyline?.object(forKey: "points") as? String else { return }
                                self?.getStartToEndPath(polyStr: points)
                            }
                        }
                        //                        DispatchQueue.global(qos: .userInteractive).async { [weak self] () -> Void in
                        //                            guard let routes = json["routes"] as? NSArray else {
                        //                                return
                        //                            }
                        //                            if (routes.count > 0) {
                        //                                
                        //                                let overview_polyline = routes[0] as? NSDictionary
                        //                                let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                        //                                guard let points = dictPolyline?.object(forKey: "points") as? String else { return }
                        //                                self?.getStartToEndPath(polyStr: points)
                        //                            }
                        //                        }
                    }
                }catch {
                    debugPrint("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    //MARK:-- SHOW PATH
    
    func showPath(polyStr :String){
        
        let path = GMSMutablePath(fromEncodedPath: polyStr)
        polyline = GMSPolyline(path: path)
        polyline?.geodesic = true
        polyline?.strokeWidth = 3.0
        if (DataResponse.onGoingRequest?.order?.category_id == 4 || DataResponse.onGoingRequest?.order?.category_id == 7) {
            polyline?.strokeColor = segmentMapStyle.selectedSegmentIndex == 1 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) :  #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        }
        else{
            polyline?.strokeColor = segmentMapStyle.selectedSegmentIndex == 1 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) :  #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        }
        
        polyline?.map = mapView         //   path not shown 
        routeCreatedOneTime = true
        
        if (DataResponse.onGoingRequest?.order?.category_id == 4 || DataResponse.onGoingRequest?.order?.category_id == 7) && DataResponse.onGoingRequest?.order?.order_id == freightPolyLineHelper.orderID  && DataResponse.onGoingRequest?.order?.order_status != SocketConstants.Ongoing{
            
            polylineSourceToDest = freightPolyLineHelper.polyline
            polylineSourceToDest?.geodesic = true
            polylineSourceToDest?.strokeWidth = 3.0
            polylineSourceToDest?.strokeColor = segmentMapStyle.selectedSegmentIndex == 1 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) :  #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
            polylineSourceToDest?.map = mapView
            
            let dropOff_latitude:Double = (DataResponse.onGoingRequest?.order?.dropoff_latitude ?? 0.0)
            let dropOff_longitude:Double = (DataResponse.onGoingRequest?.order?.dropoff_longitude ?? 0.0)
            self.dropOffMarkerHelper.icon = #imageLiteral(resourceName: "ic_drop_location_mrkr").imageWithImage(scaledToSize: self.vehicleCurrentSize)
            //            self.dropOffMarkerHelper.isFlat = true
            self.dropOffMarkerHelper.map = self.mapView
            let  destinationDropOffCoordinates = CLLocationCoordinate2D(latitude: dropOff_latitude, longitude:dropOff_longitude)
            self.dropOffMarkerHelper.position = destinationDropOffCoordinates
        }
        
        self.isFocus = false
    }
    
    //MARK::- UPDATE MARKER BEARING
    func updateMarker(degrees: Float, duration: Double){
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        driverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        let head = LocationManager.shared.heading
        driverMarker.rotation = head ?? 0
        CATransaction.commit()
    }
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    //MARK:-- Get Bearing between coordinates
    
    func degreeBearing(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        let fLat: Float = /Float(degreesToRadians(degrees: fromLoc.latitude))
        let fLng: Float = /Float(degreesToRadians(degrees: fromLoc.longitude))
        let tLat: Float = /Float(degreesToRadians(degrees: toLoc.latitude))
        let tLng: Float = /Float(degreesToRadians(degrees: toLoc.longitude))
        let degree: Double = radiansToDegrees(radians: /Double(atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))))
        if degree >= 0 {
            return Float(degree)
        } else {
            return Float(360 + degree)
        }
    }
    
    //MARK:-- Update location on map
    
    func updateLatLongOnMap(source: CLLocationCoordinate2D? , to destination: CLLocationCoordinate2D? , points: String? ){
        DispatchQueue.main.async { [ weak self ] in
            
            guard let source = source , let destination = destination   else { return }
            self?.mapView?.clear()
            self?.mapView?.delegate = self
            
            
            // Marker for Truck Service
            if (DataResponse.onGoingRequest?.order?.category_id == 4 || DataResponse.onGoingRequest?.order?.category_id == 7){
                
                var dropOff_latitude:Double  = 0.0
                var dropOff_longitude:Double = 0.0
                
                if DataResponse.onGoingRequest?.order?.order_status == SocketConstants.Ongoing{
                    dropOff_latitude = (DataResponse.onGoingRequest?.order?.dropoff_latitude ?? 0.0)
                    dropOff_longitude = (DataResponse.onGoingRequest?.order?.dropoff_longitude ?? 0.0)
                }
                else{
                    dropOff_latitude =  (DataResponse.onGoingRequest?.order?.pickup_latitude ?? 0.0)
                    dropOff_longitude = (DataResponse.onGoingRequest?.order?.pickup_longitude ?? 0.0)
                }
                
                self?.dropOffMarker.icon = #imageLiteral(resourceName: "ic_drop_location_mrkr").imageWithImage(scaledToSize: self!.vehicleCurrentSize)
                self?.dropOffMarker.map = self?.mapView
                let  destinationDropOffCoordinates = CLLocationCoordinate2D(latitude: dropOff_latitude, longitude:dropOff_longitude)
                self?.dropOffMarker.position = destinationDropOffCoordinates
            }
            else{
                // Markers for All Other Service
                
                
                self?.userMarker.icon = #imageLiteral(resourceName: "ic_drop_location_mrkr").imageWithImage(scaledToSize: self!.vehicleCurrentSize)
                self?.userMarker.map = self?.mapView
                self?.userMarker.position = CLLocationCoordinate2D(latitude: DataResponse.onGoingRequest?.order?.dropoff_latitude ?? 0.0, longitude: DataResponse.onGoingRequest?.order?.dropoff_longitude ?? 0.0)
            }
            
            self?.driverMarker.isFlat = true
            self?.oldCordinates = CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude)
            self?.driverMarker.map = self?.mapView
            CATransaction.begin()
            CATransaction.setAnimationDuration(3.0)
            self?.driverMarker.position = CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude)
            
            if !(/self?.btnMyLocation.isSelected) {
                // uncomment below to fit both marker automatically
                guard let path = GMSMutablePath(fromEncodedPath: /points) else { return }
                let bounds = GMSCoordinateBounds(path: path)
                self?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
            }
            
            CATransaction.commit()
            let mapInsets = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 150, right: 0.0)
            self?.mapView.padding = mapInsets
            self?.showPath(polyStr: /points)
            self?.isDoneCurrentPolyline = true
        }
    }
    
    func sideMenuInitialSetup(){
        
        //TODO: Initialise Side Menu Controller
        guard let vcRight =  R.storyboard.main.supportViewController() else {return}
        guard let vcLeft =  R.storyboard.main.settingViewController()else {return}
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            self.ShowLeftSideMenus(controller: vcRight, leftSide: true)
            self.ShowRightSideMenus(controller: vcLeft, leftSide: false)
        }
        else{
            self.ShowRightSideMenus(controller: vcRight, leftSide: false)
            self.ShowLeftSideMenus(controller: vcLeft, leftSide: true)
        }
    }
    
    func ShowLeftSideMenus(controller: UIViewController, leftSide: Bool){
        
        menuLeftNavigationController = UISideMenuNavigationController(rootViewController: controller)
        menuLeftNavigationController?.navigationBar.isHidden = false
        menuLeftNavigationController?.title = "back".localized
        menuLeftNavigationController?.navigationController?.title = "back".localized
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = self.view.frame.width - 100
        SideMenuManager.default.menuFadeStatusBar = false
    }
    
    func ShowRightSideMenus(controller: UIViewController, leftSide: Bool){
        
        menuRightNavigationController = UISideMenuNavigationController(rootViewController: controller)
        menuRightNavigationController?.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        menuRightNavigationController?.title = "back".localized
        menuRightNavigationController?.navigationController?.title = "back".localized
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        
    }
    
    func openSettingsScreenForLocationPermission(){
        Alerts.shared.showAlertWithActionBlock(title: "alert.locationDisabledAlert".localized, message: "alert.locationDisabledMessage".localized, actionTitle: "alertTitle.ok".localized, viewController: self) {
            UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)}
    }
    
    //MARK: NAV BAR SHADOW STYLE ▶︎▶︎▶︎
    func setupNavigationBarShadow(){
        
        if let font = UIFont(name: LocalKeys.MontBold, size: 19) {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: font]
        }
        self.navigationController?.navigationBar.addShadow()
    }
    
    //MARK: - ---------------------------------------Button Actions-----------------------------------------------
    
    //MARK: COMPLETE RIDE AT DESTINATION ▶︎▶︎▶︎
    
    @IBAction func actionDropCurrentOrder(_ sender: Any) {
        self.dropOngoingOrder(imageSign: nil, orderId: DataResponse.onGoingRequest?.order?.order_id ?? 0, paymentType: "cash", amount: "0")
    }
    
    //MARK: COLLAPSE  ONGOING RIDE ▶︎▶︎▶︎
    
    @IBAction func actionAdjustDropView(_ sender: Any) {
        
        DropAddressView.removeFromSuperview()
        self.CollapsedOngoingOrderView.frame = CGRect.init(x: 18.0 , y: ( self.view.frame.height) - CGFloat(self.heightCollapsedDropView ), width: self.view.frame.width - 36.0, height: CGFloat(self.self.heightCollapsedDropView))
        self.view.addSubview(self.CollapsedOngoingOrderView)
        CollapsedOngoingOrderView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.CollapsedOngoingOrderView.alpha = 1
        }
    }
    
    //MARK: EXPAND COLLAPSED ONGOING RIDE ▶︎▶︎▶︎
    
    @IBAction func actionExpandCollapsedView(_ sender: Any) {
        CollapsedOngoingOrderView.removeFromSuperview()
        self.showOngoingOrder()
    }
    
    //MARK:SET ONLINE/OFFLINE STATUS ▶︎▶︎▶︎
    
    @IBAction func btnSwitchStatus(_ sender: UIButton) {
        sender.bounce()
        let directionalStatus = UserDefaults.standard.value(forKey: "directionalHire_status") as? String ?? "1"
        buttonSwitch.isSelected = !buttonSwitch.isSelected
        let status = buttonSwitch.isSelected ? "1" : "0"
        if self.buttonSwitch != nil {
            if buttonSwitch.isSelected{
//                buttonSwitch.isSelected = false
//                UserDefaults.standard.set(Keys.status_value_off.rawValue, forKey: "online_status")
                setOnlineStatus(status: status, directionalHire: directionalStatus)
            }
            else{
                
                setOnlineStatus(status: status, directionalHire: directionalStatus)
//                buttonSwitch.isSelected = true
//                UserDefaults.standard.set(Keys.status_value_on.rawValue, forKey: "online_status")
            }
        }
        DataResponse.driverData = DataResponse.driverData
    }
    
    //MARK: CALL ONGOING CUSTOMER ▶︎▶︎▶︎
    @IBAction func actionCallCurrentCustomer(_ sender: UIButton) {
        let str: String = "\(/DataResponse.onGoingRequest?.order?.user?.phone_code )\(/DataResponse.onGoingRequest?.order?.user?.phoneNumber )"
        self.callToNumber(number: str)
    }
    
    //MARK: NAVIGATE ACTION ▶︎▶︎▶︎
    
    @IBAction func actionNavigate(_ sender: Any) {
        if LocationManager.shared.currentLoc != nil {
            let currentPos = GMSCameraPosition.camera(withLatitude: self.driverMarker.position.latitude, longitude: /self.driverMarker.position.longitude, zoom: 18.0, bearing: /LocationManager.shared.heading, viewingAngle: 0)
            self.mapView.animate(to: currentPos)
        }
    }
    
    //MARK: CHANGE MAP STYLE  ▶︎▶︎▶︎
    
    @IBAction func actionChangeMapStyle(_ sender: UISegmentedControl) {
        mapView.mapType = sender.selectedSegmentIndex  == 0 ? .normal : .hybrid
        self.showPolyLineAndDestinationMarker()
    }
    
    //MARK: ZOOM DRIVER CURRENT LOCATION ▶︎▶︎▶︎
    
    @IBAction func actionZoomToDriverLocation(_ sender: UIButton) {
        sender.bounce()
        if !sender.isSelected  {
            self.isFocus = true
            sender.isSelected  = true
            
            if isRideOngoing {
                self.mapViewbottomConstraint.constant = -250
            }
        }
        else{
            self.mapViewbottomConstraint.constant = 0
            self.isFocus = false
            sender.isSelected = false
        }
        self.zoomAction()
    }
    
    func zoomAction() {
        
        if let coordinate = LocationManager.shared.currentLoc?.coordinate {
            let currentPos = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15.0, bearing: /LocationManager.shared.heading, viewingAngle: 0)
            
            self.driverMarker.position = coordinate
            //            let type =  DataResponse.driverData?.data?.category_id ??  /(UserSingleton.shared.loggedInUser?.data?.category_id)
            //            self.driverMarker.icon =  self.getVehicleImage(category: type)
            self.driverMarker.map = self.mapView
            
            //            let currentPos = GMSCameraPosition.camera(withLatitude: self.driverMarker.position.latitude, longitude: /self.driverMarker.position.longitude, zoom: 15.0, bearing: /LocationManager.shared.heading, viewingAngle: 0)
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(2.0)
            self.mapView.animate(to: currentPos)
            CATransaction.commit()
        }
    }
    
    
    //MARK: SHOW RIGHT MENU ▶︎▶︎▶︎
    @IBAction func actionShowRightMenu(_ sender: Any) {
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            
            if let val = menuLeftNavigationController{
                //                self.present(/val, animated: true, completion: nil)
            }
        }
        else{
            if let val = menuRightNavigationController{
                //                self.present(/val, animated: true, completion: nil)
            }
        }
    }
}

//MARK:- SIDE MENU DELEGATES  ▶︎▶︎▶︎

extension MapViewController: UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        debugPrint("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        debugPrint("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        debugPrint("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        debugPrint("SideMenu Disappeared! (animated: \(animated))")
    }
}

extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}
