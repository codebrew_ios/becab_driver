//
//  ContactUsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class ContactUsViewController: BaseController, UITextViewDelegate {
    
    //MARK:-----Outlets------
    @IBOutlet weak var lblPhoneText: UILabel!
    @IBOutlet weak var lblMailText: UILabel!
    
    @IBOutlet weak var btnSubmit: AnimatableButton!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    
    @IBOutlet weak var tvMessage: AnimatableTextView!
    @IBOutlet weak var btnArrow: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupController()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func backAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -----Custom Methods------
    
    func setupController(){
        self.rotateButton(button: btnArrow)
        self.localiseController()
        self.navigationController?.navigationBar.removeShadow()

    }
    
    func localiseController(){
        btnBack.title = "back".localized
        self.btnBack.tintColor = UIColor.clear

        self.navigationItem.title = R.string.localizable.sidemenuSettingContact()
        lblMailText.text = "contactus.mail".localized
        lblPhoneText.text = "contactus.phone".localized
        btnSubmit.setTitle("contactus.submit".localized, for: .normal)
        setplaceholder()
       
    }
    
    func setplaceholder(){
        if BundleLocalization.sharedInstance().language == Languages.Arabic {
            tvMessage.text = "contactus.placeholder".localized
        }
        else if BundleLocalization.sharedInstance().language == Languages.Urdu{
            tvMessage.text =  "contactus.placeholder".localized
        }
        else {
            tvMessage.placeholderText = "contactus.placeholder".localized
        }
    }
    
   // MARK: Text view delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "contactus.placeholder".localized {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            setplaceholder()
        }
    }
    
    //MARK: -----Action Buttons------
    
    @IBAction func actionContactViaPhone(_ sender: UIButton) {
        
        // Open call prompt
        self.callToNumber(number: BuraqConstant.contactUsCall)
    }
    
    @IBAction func actionContactViaMail(_ sender: UIButton) {
        
        // Open mail composer
        
        switch sender.tag {
        case 1:
            self.sendEmailToUser(emalAddress: BuraqConstant.contactUsMailTo)
        case 2:
            whatsapp()
        case 3:
            UIApplication.shared.openURL(NSURL(string: "https://youtu.be/IQ-TBJYagRw")! as URL)
        default:
            break
        }
        
        
    }
    
    
    func whatsapp() {
        let phoneNumber =  "+3112900504" // you need to change this number
        let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
        if UIApplication.shared.canOpenURL(appURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL)
            }
        } else {
            // WhatsApp is not installed
        }
        //UIApplication.shared.openURL(URL(string:"https://api.whatsapp.com/send?phone=3112900504")!)
    }
    
    @IBAction func actionSubmitMessage(_ sender: Any) {
        
        if tvMessage.text != ""{
            sendMessage()
        }
        else{
            Alerts.shared.showOnTop(alert: "alert.error".localized, message: "contactus.validateMessage".localized, type: .info)
        }
    }
    
    //MARK: --- APi Request
    
    func sendMessage(){
        APIManager.shared.request(with: CommonEndPoint.contactUsMessage(message: /tvMessage.text)) { [weak self](response) in
            
            switch response{
            case .success(_):
                Alerts.shared.show(alert: "alert.success".localized, message: R.string.localizable.alertMessageSend(), type: .info)
                    self?.setplaceholder()
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
}
