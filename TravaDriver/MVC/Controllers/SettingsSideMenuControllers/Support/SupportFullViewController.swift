//
//  SupportFullViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 01/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class SupportFullViewController: BaseController, UICollectionViewDelegateFlowLayout {
    
    //MARK: Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var btnArrow: UIButton!
    
    //MARK: Properties
    
    var arr:[[String]] = []
    var cellheight = 168.0
    
    var dataSource : CollectionViewDataSource? {
        didSet{
            collectionView.delegate = dataSource
            collectionView.dataSource = dataSource
            collectionView.reloadData()
        }
    }
    
    var titles = ["sidemenu.support.warehouse".localized,"sidemenu.support.spareparts".localized,"sidemenu.support.tyrerepair".localized,"sidemenu.support.oilchange".localized,"sidemenu.support.general".localized,"sidemenu.support.carwash".localized,"sidemenu.support.backupvehicles".localized,"sidemenu.support.custom".localized, "sidemenu.support.ambulance".localized,"sidemenu.support.manpower".localized]
    
    var imageTitles = ["ic_warehouse","ic_spareparts","ic_tyre_repair","ic_oilchange","ic_general","ic_carwash","ic_backup","ic_customc","ic_ambulance","ic_manpower"]
    
    
    //MARK:-- VIEW CONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    //MARK:-- CUSTOM METHODS
    
    func initialSetup(){
        
        btnBack.title = "back".localized
        self.btnBack.tintColor = UIColor.clear

        self.rotateButton(button: btnArrow)
        self.navigationItem.title = R.string.localizable.sidemenuSupport()
        
        // data source  for titles and images
        arr.append(titles)
        arr.append(imageTitles)
        configureCell()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Configure table cell
    
    func configureCell(){
        
        dataSource = CollectionViewDataSource.init(items: DataResponse.driverData?.support, collectionView: collectionView, cellIdentifier: R.reuseIdentifier.supportMenuCollectionViewCell.description, headerIdentifier: "", cellHeight: CGFloat(cellheight), cellWidth: Screen.WIDTH/2, configureCellBlock: {(cell, item, indexPath) in
            
            let object = DataResponse.driverData?.support?[indexPath.row]
            (cell as? SupportMenuCollectionViewCell)?.item = object
            
        }, aRowSelectedListener: { (indexPath, item) in
            debugPrint(indexPath)
        }, willDisplayCell: { (indexpath) in
            debugPrint(indexpath)
        }, scrollViewDelegate: { (scrollView) in
            debugPrint(scrollView)
        },scrollViewDecelerate:{ (scrollView) in
            debugPrint(scrollView)
            
        })
    }
}
