//
//  BookingsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import SwipeMenuViewController


class BookingsViewController: BaseController {
    
    //MARK:-- Variables
    
    
    //MARK: ---------------Variable-------------
    private(set) lazy var bookingViewControllers: [UIViewController] = {
        return [R.storyboard.settingMenu.pastBookingsViewController(),
                R.storyboard.settingMenu.upcomingViewController(),
                ]
        }() as! [UIViewController]
    
    private lazy var controllerTitles = ["booking.past".localized,"booking.upcoming".localized]
    
    //MARK:--OUTLETS
    
    @IBOutlet weak var swipeMenuView: SwipeMenuView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var btnArrow: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UINavigationBar.appearance().backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        self.swipeMenuView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
    }
    
    func initialSetup(){
        self.rotateButton(button: btnArrow)
        
        swipeMenuView.dataSource = nil
        swipeMenuView.delegate = nil
        
        self.navigationController?.navigationBar.removeShadow()
                
        self.navigationItem.title = R.string.localizable.sidemenuSettingBookings()
       // self.btnBack.title = "back".localized
        self.btnBack.tintColor = UIColor.clear
        
        swipeMenuView.dataSource = self
        swipeMenuView.delegate = self
        
        var options: SwipeMenuViewOptions = .init()
        
        options.tabView.itemView.textColor = Colors.swipeTabViewTextColor
        options.tabView.itemView.selectedTextColor = Colors.themeColor
        options.tabView.itemView.margin = 20
        options.tabView.margin = 5
        
        swipeMenuView.reloadData(options: options)
    }
    
    @IBAction func act(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BookingsViewController: SwipeMenuViewDelegate {
    
    // MARK - SwipeMenuViewDelegate
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        
        // Codes
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        // Codes
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        // Codes
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        // Codes
    }
}

extension BookingsViewController: SwipeMenuViewDataSource {
    
    //MARK - SwipeMenuViewDataSource
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return controllerTitles.count
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return controllerTitles[index]
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        let vc = bookingViewControllers[index]
        return vc
    }
}
