//
//  PastSupportViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class PastSupportViewController: UIViewController {

    //MARK:-- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-- Properties
    var dataSource : TableViewDataSource? {
        
        didSet {
            tableView.delegate = dataSource
            tableView.dataSource = dataSource
            tableView.reloadData()
        }
    }
    
    var titles = ["sidemenu.setting.bookings".localized,"sidemenu.setting.support".localized,"sidemenu.setting.earnings".localized,"sidemenu.setting.referal".localized,"sidemenu.setting.contact".localized,"sidemenu.setting.emergency".localized,"sidemenu.setting.setting".localized, "sidemenu.setting.signout".localized]
    
    //MARK:-- View COntroller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    //MARK:-- Custom methods
    func initialSetup(){
        configureTableview()
    }
    
    //MARK:-- Configure cell
    
    func configureTableview(){
        
        dataSource =  TableViewDataSource.init(items: titles, tableView: tableView, cellIdentifier: R.reuseIdentifier.bookingsTableViewCellSupport.description, cellHeight: UITableViewAutomaticDimension, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            
            //            let item = self?.titles[indexPath.row]
            //            (cell as? BookingsTableViewCell)?.title = item
            
            }, aRowSelectedListener: { (indexPath, cell) in
                
                guard let vc = R.storyboard.settingMenu.bookingDetailViewController() else {return }
//                self.navigationController?.pushViewController(vc, animated: true)
                
        }, willDisplayCell: {[weak self] (indexPath, cell) in
            
        })
        
    }
    
    
    

}
