//
//  BookingDetailViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import  IBAnimatable
import ObjectMapper

enum BookingType:String {
    case package = "Package"
    case friend = "Friend"
    case roadPickupPhone = "RoadPickupPhone"
}
protocol reloadBookings:class {
    func reloadBookingData()
}

class BookingDetailViewController: BaseController,reasonCancellation{
    
    var booking :Bookings?
    var orderDetail: ServiceRequest?
    weak var delegate:reloadBookings?
    
    //MARK: Outlets
    
    @IBOutlet weak var baseFareHeadingLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var subTotalAmountLabel: UILabel!
    @IBOutlet weak var serviceChargesLabel: UILabel!
    @IBOutlet weak var bookingFeeLabel: UILabel!
    @IBOutlet weak var normalFareLabel: UILabel!
    @IBOutlet weak var distanceHedaingLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeHeadingLabel: UILabel!
    @IBOutlet weak var timeValueLabel: UILabel!
    @IBOutlet weak var baseFareValueLabel: UILabel!
    
    
    
    
    //User rating
    @IBOutlet weak var ratingByDriverMainView: AnimatableView!
    @IBOutlet weak var toUserRatingLabel: UILabel!
    @IBOutlet weak var backView: AnimatableView!
    @IBOutlet weak var rateUserButton: AnimatableButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var ratingByDriverView: CosmosView!
    @IBOutlet weak var ratingToUserView: CosmosView!
    
    
    @IBOutlet weak var dropLocationLabel: UILabel!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    @IBOutlet weak var imgView: AnimatableImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblStartDateValue: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblEndDateValue: UILabel!
    
    //views
    
    @IBOutlet weak var endDateView: UIStackView!
    @IBOutlet weak var viewPayments: AnimatableView!
    
    @IBOutlet weak var viewCustomer: AnimatableView!
    @IBOutlet weak var viewPaymentsStackView: UIStackView!
    
    //customer
    
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var imgViewCustomer: UIImageView!
    @IBOutlet weak var imgRate: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    
    //invoice
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var lblDropOffText: UILabel!
    
    @IBOutlet weak var lblRateText: UILabel!
    
    let formatter = DateFormatter()
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblBaseFair: UILabel!
    @IBOutlet weak var lblTaxType1: UILabel!
    @IBOutlet weak var lblTaxType2: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var btnCancel: AnimatableButton!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var baseFareHeading: UILabel!
    @IBOutlet weak var serviceChargeLabel: UILabel!
    @IBOutlet weak var bookingFeeHeadingLabel: UILabel!
    @IBOutlet weak var normalFareHeading: UILabel!
    
    //MARK:-- View controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.setText()
        self.navigationController?.navigationBar.isHidden = true
        
        [ratingByDriverView,ratingToUserView].forEach{ v in
            v?.filledBorderColor = .black
            v?.emptyBorderColor = .black
        }
    }
    
    func setText() {
        btnCancel.setTitle("cancel".localized, for: .normal)
        baseFareHeading.text = "basefare".localized
        serviceChargeLabel.text =  "serviceCharge".localized
        bookingFeeHeadingLabel.text = "bookingfee".localized
        lblSubTotal.text = "subTotal".localized
        lblTotal.text = "total".localized
        normalFareHeading.text = "normalfare".localized
    }
    //MARK: Custom Methods
    func initialSetup(){
        self.rateUserButton.setTitle("RatingDriver".localized, for: .normal)
        self.localiseController()   // localise view acc to lang
        
        /booking?.order_status == "Scheduled" ? (btnCancel.isHidden = false) : ( btnCancel.isHidden = true)
        /booking?.order_status == "Scheduled" ? (backView.isHidden = false) : ( backView.isHidden = true)
        
        self.getOrderDetail(orderId: "\(/booking?.order_id)")            //get Order Detail from order id
        
        // update other details
        lblOrderId.text = "Id: \(/booking?.order_token)"
        self.lblAddress.text = /booking?.dropoff_address
        lblStatus.text = getStatus(type: /booking?.order_status)
        
        // static map configuration
        let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=\(/booking?.dropoff_latitude),\(/booking?.dropoff_longitude)&\("zoom=15&size=\(2 * Int(imgView.frame.size.width))x\(2 * Int(imgView.frame.size.height))")&sensor=true&key=\(APIConstants.googleApiKey)"
        let mapUrl: NSURL = NSURL(string: staticMapUrl)!
        self.imgView.kf.setImage(with: mapUrl as URL, placeholder: nil , options: nil, progressBlock: nil, completionHandler: nil)
        
        //update or show date according to scheduled or past orders
        booking?.future == "1" ? SetupScheduledRide(): SetupNormalRide()
        dropLocationLabel.text = booking?.dropoff_address
        pickupLocationLabel.text = booking?.pickup_address
        
        // Calculate and Display fare
        self.CalculateAndDisplayFare()
        
        if booking?.order_status != "SerComplete"{
            ratingByDriverMainView.isHidden = true
            viewCustomer.isHidden = true
            viewPayments.isHidden = true
        }else{
            ratingByDriverMainView.isHidden = false
            viewCustomer.isHidden = false
            viewPayments.isHidden = false
        }
        
    }
    
    // setup pricing view invoice
    
    func CalculateAndDisplayFare(){
        
        //        let _ = calculateFair()
        if booking?.organisation_coupon_user_id != 0{
            lblAmount.text = R.string.localizable.etokenEtoken() + "- \(/booking?.payment?.product_quantity)"
        }
        else{
            lblAmount.text = "\(R.string.localizable.txtCash()) \(/Double(/booking?.payment?.final_charge)?.getTwoDecimalFloat()) \(R.string.localizable.currency())"
        }
        //        self.lblTotalAmount.text =  "\(/Double(/booking?.payment?.final_charge)?.getTwoDecimalFloat()) \(R.string.localizable.currency())"
        //        self.lblfareValue1.text = "\(/Double(/booking?.payment?.initial_charge)?.getTwoDecimalFloat()) \(R.string.localizable.currency())"
        //        self.lblfareValue2.text = "\(/Double(/booking?.payment?.admin_charge)?.getTwoDecimalFloat()) \(R.string.localizable.currency())"
        
        
        
        let paymentInfo = booking?.payment
        
        if booking?.booking_type == "Package" {
            //Base Fare....
            //Package Price
            
            let baseFare = Double(paymentInfo?.distance_price_fixed ?? "") ?? 0.0
            baseFareValueLabel.text = "\(String(format: "%.2f", baseFare)) \(R.string.localizable.currency())"
            baseFareHeadingLabel.text = "Package Price"
            //Time
            let orderTime = Double(paymentInfo?.extra_time ?? "") ?? 0.0
            let timePerHourCharges = Double(paymentInfo?.price_per_min ?? "") ?? 0.0
            let timeCharges = orderTime*timePerHourCharges
            timeHeadingLabel.text = "Extra Time(\((Int(paymentInfo?.price_per_min ?? "") ?? 0)) min)"
            timeValueLabel.text = "\(String(format: "%.2f",timeCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Distance
            let orderDistance = Double(paymentInfo?.extra_distance ?? "") ?? 0.0
            let distancePerHourCharges = Double(paymentInfo?.price_per_km ?? "") ?? 0.0
            let distanceCharges = orderDistance * distancePerHourCharges
            distanceHedaingLabel.text = "Extra Distance (\(orderDistance.roundTo(places: 2)) KM)"
            distanceLabel.text = "\(String(format: "%.2f",distanceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Normal Charges.
            let normalCharges = (baseFare + timeCharges + distanceCharges)
            // let actualValue = Double(paymentInfo?.product_actual_value ?? "") ?? 0.0
            normalFareLabel.text = "\(String(format: "%.2f",normalCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            let serviceCharges = 0.0
            let bookingFee = 0.0
            
            bookingFeeLabel.text = "\(String(format: "%.2f",bookingFee.roundTo(places: 2))) \(R.string.localizable.currency())"
            serviceChargesLabel.text = "\(String(format: "%.2f",serviceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //SubTotal
            subTotalAmountLabel.text = "\(String(format: "%.2f",(normalCharges + bookingFee + serviceCharges).roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Total
            totalAmountLabel.text = "\(String(format: "%.2f",(Double(paymentInfo?.final_charge ?? "") ?? 0.0).roundTo(places: 2))) \(R.string.localizable.currency())"
            
        }else{
            //Base Fare....
            let baseFare = Double(paymentInfo?.product_alpha_charge ?? "") ?? 0.0
            baseFareValueLabel.text = "\(String(format: "%.2f",baseFare)) \(R.string.localizable.currency())"
            
            //Time
            let orderTime = Double(paymentInfo?.order_time ?? "") ?? 0.0
            let timePerHourCharges = Double(paymentInfo?.product_per_hr_charge ?? "") ?? 0.0
            let timeCharges = orderTime*timePerHourCharges
            timeHeadingLabel.text = "\("time".localized)(\((Int(paymentInfo?.order_time ?? "") ?? 0)) min)"
            timeValueLabel.text = "\(String(format: "%.2f",timeCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Distance
            let orderDistance = Double(paymentInfo?.order_distance ?? "") ?? 0.0
            let distancePerHourCharges = Double(paymentInfo?.product_per_distance_charge ?? "") ?? 0.0
            let distanceCharges = orderDistance * distancePerHourCharges
            distanceHedaingLabel.text = "\("distance".localized) (\(orderDistance.roundTo(places: 2)) KM)"
            distanceLabel.text = "\(String(format: "%.2f",distanceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Normal Charges.
            let normalCharges = (baseFare + timeCharges + distanceCharges)
            let actualValue = Double(paymentInfo?.product_actual_value ?? "") ?? 0.0
            normalFareLabel.text = "\(String(format: "%.2f",normalCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Booking fee and service charge
            var serviceCharges = 0.0
            let totalPercentage = Double(paymentInfo?.buraq_percentage ?? 0)/100
            var bookingFee = 0.0
            if normalCharges > actualValue {
                bookingFee = normalCharges * totalPercentage
            }else {
                bookingFee = actualValue * totalPercentage
                serviceCharges = actualValue - normalCharges
            }
            bookingFeeLabel.text = "\(String(format: "%.2f",bookingFee.roundTo(places: 2))) \(R.string.localizable.currency())"
            serviceChargesLabel.text = "\(String(format: "%.2f",serviceCharges.roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //SubTotal
            subTotalAmountLabel.text = "\(String(format: "%.2f",(normalCharges + bookingFee + serviceCharges).roundTo(places: 2))) \(R.string.localizable.currency())"
            
            //Total
            totalAmountLabel.text = "\(String(format: "%.2f",(Double(paymentInfo?.final_charge ?? "") ?? 0.0).roundTo(places: 2))) \(R.string.localizable.currency())"
        }
    }
    
    func SetupScheduledRide(){
        
        if booking?.order_status == SocketConstants.orderCompleted{
            let str = /booking?.created_at
            lblDate.text = str.UTCToLocal(date: str)
            lblStartDateValue.text = /booking?.order_timings?.UTCToLocal(date: str)
            endDateView.isHidden = false
            lblEndDateValue.text = /booking?.updated_at?.UTCToLocal(date: /booking?.updated_at)
        }
        else{
            
            let str = /booking?.order_timings
            lblDate.text = str.UTCToLocal(date: str)
            lblStartDateValue.text = /booking?.order_timings?.UTCToLocal(date: str)
            endDateView.isHidden = true
        }
    }
    
    func SetupNormalRide(){
        
        if booking?.order_status == SocketConstants.orderCompleted  ||  booking?.order_status == SocketConstants.customerCompletedEtoken {
            
            let str = /booking?.payment?.created_at
            lblDate.text = str.UTCToLocal(date: str)
            lblStartDateValue.text = /booking?.order_timings?.UTCToLocal(date: str)
            lblEndDateValue.text = /booking?.updated_at?.UTCToLocal(date: /booking?.payment?.updated_at)
            
        }
        else{
            let str = /booking?.payment?.created_at
            lblDate.text = str.UTCToLocal(date: str)
            endDateView.isHidden = false
            
            lblStartDateValue.text = /booking?.order_timings?.UTCToLocal(date:  /booking?.payment?.updated_at)
            lblStartDate.text = R.string.localizable.bookingUpcomingCancelled()
        }
    }
    
    
    //MARK:  localise controller
    func localiseController(){
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            lblAmount.textAlignment = .left
            lblStatus.textAlignment = .left
        }else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        //        self.lblFareTax.text = "invoiceTax2".localized
        //        self.lblBaseFair.text = "invoiceBaseFair".localized
        //        self.lblTaxType1.text = "invoiceTax1".localized
        //        self.lblTaxType2.text = "invoiceTax3".localized
        //        self.lblTotal.text = "invoiceTotal".localized
        self.lblStartDate.text = R.string.localizable.bookingStartDate()
        self.lblEndDate.text = R.string.localizable.bookingEndDate()
        self.lblDropOffText.text = booking?.category_id == 4 ? R.string.localizable.bookingDeliveryOff() : R.string.localizable.bookingDropOff()
        
        btnCancel.setTitle("request.cancel".localized, for: .normal)
        
    }
    
    //MARK:  calculate fare and update invoice details in screen
    
    func calculateFair() -> String{
        
        let base_price = /booking?.payment?.product_quantity * /Int(/booking?.payment?.product_per_quantity_charge)
        //        self.lblfareValue1.text = "\(/Double(/booking?.payment?.initial_charge)?.getTwoDecimalFloat()) \(R.string.localizable.currency())"
        //
        //        let _ = (Float(/booking?.payment?.buraq_percentage) / 100 ) * 100
        //        self.lblfareValue2.text = "\(/booking?.payment?.admin_charge) \(R.string.localizable.currency())"
        //        self.lblFareValue3.text = "0.0 LKR"
        
        let product_distance_charge = Int(/booking?.payment?.product_per_distance_charge)
        let product_weight_charge = Int(/booking?.payment?.product_per_weight_charge)
        let product_alpha_charge = Int(/booking?.payment?.product_alpha_charge)
        let total_charge = base_price + /product_distance_charge + /product_weight_charge + /product_alpha_charge
        return "\(/total_charge)"
    }
    
    //MARK:  Get Localise Order status according to server status
    func getStatus(type:String) -> String{
        
        switch type {
        case SocketConstants.orderCompleted:
            btnCall.isHidden = true
            lblStatus.textColor = UIColor.green
            return R.string.localizable.bookingCompleted()
            
        case SocketConstants.reached:
            btnCall.isHidden = false
            
            return R.string.localizable.requestReached()
            
        case SocketConstants.SerPending:
            return R.string.localizable.tokenStatusPending()
            
        case SocketConstants.CustomerCancel:
            lblEndDateValue.isHidden = true
            lblEndDate.isHidden = true
            btnCall.isHidden = true
            lblStatus.textColor = R.color.appRed()
            return R.string.localizable.bookingUpcomingCancelled()
            
            //        case SocketConstants.orderUpcoming:
            //            return R.string.localizable.bookingUpcoming()
            
        case SocketConstants.dApproved:
            lblStatus.textColor = R.color.appRed()
            return R.string.localizable.bookingApproved()
            
        case SocketConstants.driverCancel:
            lblEndDateValue.isHidden = true
            lblEndDate.isHidden = true
            btnCall.isHidden = true
            lblStatus.textColor = R.color.appRed()
            return R.string.localizable.bookingUpcomingCancelled()
            
        case SocketConstants.orderTimeout:
            btnCall.isHidden = true
            lblStatus.textColor = R.color.appRed()
            return R.string.localizable.tokenStatusTimeout()
            
            
        default:
            return type
        }
        
        
    }
    
    //MARK:  update remaining data from order detail Api result
    func UpdateData(object:ServiceRequest?){
        
        self.orderDetail = object
        
        lblStatus.text = getStatus(type: /self.orderDetail?.order?.order_status)
        
        
        
        // Brand name according Acc to service (For gas - Brand is default so not shown)
        
        object?.order?.category_id == 1 || object?.order?.category_id == 3  ? (self.lblBrand.text =  getServiceName(category: "\(object?.order?.category_id ?? 1)")) : (self.lblBrand.text = object?.order?.product?.brand_name)
        
        //Product Name Acc to Service (For trucks quantity is not shown)
        
        (object?.order?.category_id == 4 || object?.order?.category_id == 7) ? ( self.lblProduct.text = "\(/object?.order?.product?.product_name)") : ( self.lblProduct.text = "\(/object?.order?.product?.product_name) × \(/object?.order?.payment?.product_quantity)")
        
        
        // Customer, rating, Comments, Name
        
        let url = "\(/object?.order?.user?.profile_pic_url)"
        self.imgViewCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        booking?.order_status == "Scheduled" ? ( self.viewCustomer.isHidden = true) : (  self.viewCustomer.isHidden = false)
        /booking?.order_status == "Scheduled" ? (viewPayments.isHidden = true) : ( viewPayments.isHidden = false)
        ///booking?.order_status == "Scheduled" ? (ratingByDriverMainView.isHidden = true) : ( ratingByDriverMainView.isHidden = false)
        
        if orderDetail?.order?.ratingByUser?.createdAt == nil || orderDetail?.order?.ratingByUser?.createdAt == "" {
            self.viewCustomer.isHidden = true
        }else{
            self.viewCustomer.isHidden = false
            self.lblComment.text = object?.order?.ratingByDriver?.comments
            self.lblRateText.text = "\(/object?.order?.ratingByDriver?.ratings)"
            self.lblCustomerName.text = object?.order?.user?.name
            if /object?.order?.ratingByUser?.ratings != 0 {
                self.imgRate.getRatingImage(rating: "\(/object?.order?.ratingByUser?.ratings )")
                ratingToUserView.rating = Double(object?.order?.ratingByDriver?.ratings ?? 0)
            }else{self.ratingToUserView.isHidden = true}
        }
        ratingByDriverView.isHidden = true
        toUserRatingLabel.isHidden = true
        userNameLabel.isHidden = true
        userImageView.isHidden = true
        if orderDetail?.order?.ratingByDriver?.createdAt == nil || orderDetail?.order?.ratingByDriver?.createdAt == "" {
            rateUserButton.isHidden = false
        }else{
            rateUserButton.isHidden = true
        }
        
        if booking?.order_status != "SerComplete"{
            ratingByDriverMainView.isHidden = true
            viewCustomer.isHidden = true
            viewPayments.isHidden = true
        }else{
            ratingByDriverMainView.isHidden = false
            viewCustomer.isHidden = false
            viewPayments.isHidden = false
        }
    }
    
    //MARK:  Cancel ride delgate protocol
    
    func cancelRide(reason: String) {
        delegate?.reloadBookingData()
        self.navigationController?.popViewController(animated: true)
        
        if let orderId = booking?.order_id {
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name(rawValue: NotificationNames.nCancelFromBookinScreen.rawValue), object: "\(orderId)")
        }
    }
    
    @IBAction func rateUserAction(_ sender: UIButton) {
        guard let vc = R.storyboard.main.rateRideViewController() else {return}
        let data = CompletedOrderModel()
        data.result = CompletedOrderDetail()
        data.result?.CustomerDetails = CustomerDetails()
        data.result?.Customer = Customer()
        data.result?.order_id = self.orderDetail?.order?.order_id
        data.result?.CustomerDetails?.profile_pic_url = self.orderDetail?.order?.user?.profile_pic_url
        data.result?.Customer?.name = self.orderDetail?.order?.user?.name
        vc.completedRequest = data
        vc.delegateOrder = self
        openRequestVC(vc: vc)
    }
    
    
    func openRequestVC(vc:UIViewController){
        
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        
        // To present from any controller
        if self.navigationController?.visibleViewController != nil{
            self.navigationController?.visibleViewController?.present(vc, animated: true, completion: nil)
        }
        else{
            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)
            
        }
    }
    
    //MARK: Action Buttons
    @IBAction func callCustomer(_ sender: Any) {
        
        let phone = "\(/self.orderDetail?.order?.user?.phone_code)" + "\(/self.orderDetail?.order?.user?.phoneNumber)"
        
        self.callToNumber(number: "\(phone)")
    }
    
    @IBAction func actionDone(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCancelScheduledRide(_ sender: Any) {
        
        guard let vc = R.storyboard.main.cancellationViewController() else {return}
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.orderID = booking?.order_id?.toString()
        vc.reasonDelagate = self
        self.navigationController?.visibleViewController?.present(vc, animated: true, completion: nil)
    }
    
    //MArk: OrderDetail from order id
    
    func getOrderDetail(orderId:String){
        
        APIManager.shared.request(with: GeneralEndPoint.orderDetails(orderId: orderId)) { (response) in
            switch response{
            case .success(let responseValue):
                
                let object  =  Mapper<ServiceRequest>().map(JSONObject: responseValue)
                // update data in screen
                self.UpdateData(object: object)
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension BookingDetailViewController: RateCompletedDeliveryOrderDelegate {
    func deliveryRatingCompleted(rating: Int, comment: String) {
        self.ratingByDriverView.rating = Double(rating)
        userNameLabel.text = "\(self.orderDetail?.order?.user?.name ?? "")"
        toUserRatingLabel.text = "\(rating)"
        rateUserButton.isHidden = true
        ratingByDriverView.isHidden = false
        toUserRatingLabel.isHidden = false
    }
}
