//
//  UpcomingViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class UpcomingViewController: UIViewController,reloadBookings {
   
    //MARK:-- Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNpresult: UILabel!
    
    //MARK:-- Properties
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var data : OrderHistory?
    var skip = 0
    var take = 10
    var isLoadingList = false
    var arrDataSource =  [Bookings](){
        didSet{
            dataSource?.items = arrDataSource
            tableView.reloadData()
        }
    }
    
    var dataSource : TableViewDataSource?
    
    //MARK:-- View Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.apiOrderHistory()
        
    }
    
    //MARK:-- Custom Methods
    
    func initialSetup(){
        apiOrderHistory() // fetch orders upcoming or scheduled
        configureTableView()
        lblNpresult.text = R.string.localizable.bookingNoresult() // localise no order label
        
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
    }
    
    // reload booking data
    func reloadBookingData() {
        self.arrDataSource = []
        self.configureTableView()
        self.apiOrderHistory()
    }
    
    
    // refesh control handler
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        skip = 0
        take = 10
        self.reloadBookingData()
        
    }
    
    //MARK:-- Configure table cell
    
    func configureTableView(){
        
        dataSource =  TableViewDataSource.init(items:  self.arrDataSource, tableView: tableView, cellIdentifier: R.reuseIdentifier.bookingsTableViewCellUpcoming.description, cellHeight: UITableViewAutomaticDimension, configureCellBlock: { (cell, item,
            indexPath) in
            
            let object = item as? Bookings
            (cell as? BookingsTableViewCell)?.booking = object
            
            }, aRowSelectedListener: {[weak self] (indexPath, cell) in
                
                guard let arr = self?.arrDataSource else {return}
                guard let vc = R.storyboard.settingMenu.bookingDetailViewController() else {return }
                
                vc.booking =  arr[indexPath.row]
                vc.delegate = self
                let rootController = self?.appDelegate.rootViewController
                rootController?.pushViewController(vc, animated: true)
            }, willDisplayCell: {[weak self] (indexPath, cell) in
                
                if indexPath.row == /self?.arrDataSource.count - 1 && !(/self?.isLoadingList){
                    self?.skip = /self?.take
                    self?.isLoadingList = true
                    self?.apiOrderHistory()
                }
        })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
    }
    
    // MARK: GET ONGOING ORDER HISTORY ▶︎▶︎▶︎
    
    func apiOrderHistory(){
        
        APIManager.shared.request(with: GeneralEndPoint.getOrderHistory(skip: skip, take: take, type:0)) {[weak self] (response) in
            switch response{
                
            case .success(let responseValue):
                guard let responses = responseValue as? OrderHistory, let arr = responses.order else {return}
                
                if self?.skip == 0{
                    self?.tableView.isHidden = arr.count == 0
                }
                
                if arr.count != 0{
                    
                    self?.arrDataSource.append(contentsOf: arr)
                    self?.isLoadingList = false
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
}

