//
//  PastBookingsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class PastBookingsViewController: UIViewController,reloadBookings {
    
    //MARK:-- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoResult: UILabel!
    
    //MARK:-- Properties
    var skip = 0
    var take = 10
    var isLoadingList = false
    var arrDataSource =  [Bookings](){
        didSet{
            
            self.dataSource?.items = arrDataSource
            self.tableView.reloadData()
            
        }
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
       var dataSource : TableViewDataSource?
    
    //MARK:-- View Controller life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:-- Custom methods
    
    func initialSetup(){
        
        // get past order Booking listing
        apiOrderHistory()
        
        // configure TableView
        configureTableView()
        
        self.lblNoResult.text = R.string.localizable.bookingNoresult()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
    }
    
    // refresh api result
    func reloadBookingData() {
        
        self.apiOrderHistory()
    }
    // refesh control handler
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        skip = 0
        take = 10
        arrDataSource = []
        self.reloadBookingData()
        
    }
    
    //MARK:-- Configure table cell
    
    func configureTableView(){
        
        dataSource =  TableViewDataSource.init(items: arrDataSource , tableView: tableView, cellIdentifier: R.reuseIdentifier.bookingsTableViewCellPast.identifier, cellHeight: UITableViewAutomaticDimension, configureCellBlock: {(cell, item,
            indexPath) in
            
            let object = item as? Bookings
            (cell as? BookingsTableViewCell)?.booking = object
            
        }, aRowSelectedListener: {[weak self] (indexPath, cell) in
            
            guard let arr = self?.arrDataSource else {return}
            guard let vc = R.storyboard.settingMenu.bookingDetailViewController() else {return }
            
            vc.booking =  arr[indexPath.row]
            vc.delegate = self
            
            let rootController = self?.appDelegate.rootViewController
            rootController?.pushViewController(vc, animated: true)
            }, willDisplayCell: {[weak self] (indexPath, cell) in
                
                if indexPath.row == /self?.arrDataSource.count - 1 && !(/self?.isLoadingList){
                    self?.skip = /self?.arrDataSource.count
                    self?.isLoadingList = true
                    self?.apiOrderHistory()
                }
        })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        
       
    }
    
    // MARK: GET PAST ORDER HISTORY ▶︎▶︎▶︎
    
    func apiOrderHistory(){
        
        APIManager.shared.request(with: GeneralEndPoint.getOrderHistory(skip: skip, take: take, type:1)) {[weak self] (response) in
            switch response{
                
            case .success(let responseValue):
                guard let responses = responseValue as? OrderHistory, let arr = responses.order else {return}
                
                if self?.skip == 0{
                    self?.tableView.isHidden = arr.count == 0
                }
                
                if arr.count != 0{
                    self?.arrDataSource.append(contentsOf: arr)
                    self?.isLoadingList = false
                }
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
}
