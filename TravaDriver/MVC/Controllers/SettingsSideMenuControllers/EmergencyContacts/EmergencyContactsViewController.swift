//
//  EmergencyContactsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class EmergencyContactsViewController: BaseController {
    
    //MARK:--- Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    //MARK:----Properties
    
    var dataSource : TableViewDataSource?
    
    var titles = ["sidemenu.setting.bookings".localized,"sidemenu.setting.support".localized,"sidemenu.setting.earnings".localized,"sidemenu.setting.referal".localized,"sidemenu.setting.contact".localized,"sidemenu.setting.emergency".localized,"sidemenu.setting.setting".localized, "sidemenu.setting.signout".localized]
    
    var arrContacts:[Contacts]?{
        didSet{
            dataSource?.items = arrContacts
             tableView.reloadData()
        }
        
    }
    
    //MARK:----View Controller Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initialSetup()
    }
    
      //MARK:----Custom Methods
    func initialSetup(){
        self.rotateButton(button: btnArrow)
        
        btnBack.title = "back".localized
        self.btnBack.tintColor = UIColor.clear

        self.navigationItem.title = "emergency.title".localized
        self.configureTableView()
        
        getEmergencyContacts() // fetch contacts from api
    }
    
    @IBAction func actionbtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:----Configure TableView
    
    func configureTableView(){
        
        dataSource =  TableViewDataSource.init(items: self.arrContacts, tableView: tableView, cellIdentifier: R.reuseIdentifier.emergencyContactsTableViewCell.description, cellHeight: UITableViewAutomaticDimension, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            let object = self?.arrContacts?[indexPath.row]
            (cell as? EmergencyContactsTableViewCell)?.contacts = object
            
            }, aRowSelectedListener: { (indexPath, cell) in
                debugPrint(indexPath)
        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        
    }
    
    //MARK:--------- API Requests
    
    // Get emergency contacts
    
    func getEmergencyContacts(){
        
        APIManager.shared.request(with: CommonEndPoint.emergencyContacts) {[weak self] (response) in
            switch response{
            case .success(let responseValue):
                let result = responseValue as? EmergencyContacts
                self?.arrContacts = result?.contacts
                
            case .failure(let err):
                Alerts.shared.showOnTop(alert: "alert.error".localized, message: /err, type: .info)
                
            }
        }
    }
    
}
