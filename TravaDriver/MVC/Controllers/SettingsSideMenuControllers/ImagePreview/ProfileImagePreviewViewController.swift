//
//  ProfileImagePreviewViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class ProfileImagePreviewViewController: UIViewController {
    
    //MARK:----Outlets
    
    @IBOutlet weak var imgViewBackground: AnimatableImageView!
    @IBOutlet weak var imgView: UIImageView!
    
    var image = UIImage()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        imgView.image = image
        imgViewBackground.image = image
        self.navigationItem.title = R.string.localizable.settingProfile()
        // Do any additional setup after loading the view.
    }

   
}
