//
//  ReferalViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class ReferalViewController: BaseController {
    
    
    
    //MARK:---------Outlets-----------
    @IBOutlet weak var lblShareText: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnShare: AnimatableButton!
    @IBOutlet weak var pointsLabel: UILabel!
    //MARK:---------View COntroller life cycle-----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        apiGetCreditpoints()
    }
    
    //MARK:-----CustomMethods-------
    
    func initialSetup(){
        self.localiseController()
    }
    
    func localiseController(){
        lblShareText.text = "Share With your friend \nReferral Code: \(/DataResponse.driverData?.data?.dataUser?.referral_code)"
        btnShare.setTitle("referal.buttonTitle".localized, for: .normal)
    }
    
    //MARK:--------Action Buttons-------
    
    @IBAction func actionShareDriverApp(_ sender: UIButton) {
        
//        let textToShare = R.string.localizable.referalShareableText()
        let textToShare =  "Install Becab Driver Application \nReferral Code: \(/DataResponse.driverData?.data?.dataUser?.referral_code)"
        
        if let appUrl = NSURL(string: APIConstants.appStoreUrl) {
            let objectsToShare = [textToShare, appUrl] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func apiGetCreditpoints() {
        
        
        let endpoint = ServicesEndPoint.getCreditPoints
        
        APIManager.shared.request(with: endpoint) { response in
            switch response {
                
            case .success(let data):
                
                guard let modal = data as? User else {return}
                self.pointsLabel.text = "Your referral points:\(/modal.credit_points)"
//                self?.viewOrderPricing.setCreditPoints(points: modal.credit_points)
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localized, message: /strError , type: .error )
            }
        }
    }
}
