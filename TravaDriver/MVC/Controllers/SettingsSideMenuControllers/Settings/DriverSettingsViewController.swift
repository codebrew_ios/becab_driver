//
//  DriverSettingsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import DropDown
import IBAnimatable

class DriverSettingsViewController: BaseController {
    
    //MARK: Outlets
    
    @IBOutlet weak var languageView: AnimatableView!
    @IBOutlet weak var btnSwitch: UISwitch!
    
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblVersionValue: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblPushNotifications: UILabel!
    @IBOutlet weak var lblTermsAndCOndition: UILabel!
    @IBOutlet weak var lblChangeLangugae: UILabel!
    @IBOutlet weak var lblPrivacy: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var btnImage: UIBarButtonItem!
    
    
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var lblLeft: UILabel!
    @IBOutlet weak var imgAnchor: UIImageView!
    //MARK: Properties
    let dropDown = DropDown()
    
//    let languages = ["txt.english".localized:"en",
//                     "txt.arabic".localized:"ar",
//                     "txt.urdu".localized:"ur",
//                     "txt.chinese".localized:"zh-Hans",
//                     "txt.spanish".localized:"es"]
    
    
    let languages = ["txt.english".localized:"en",
                       "txt.spanish".localized:"es"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.dropDownSetup()
        self.setupController()
    }
    
    //MARK: Custom Methods
    
    func setupController(){
        self.btnBack.title = "back".localized
        self.btnBack.tintColor = UIColor.clear
        let langType = BundleLocalization.sharedInstance().language
        switch langType {
        case Languages.English:
            lblLanguage.text = "English"
            self.dropDown.anchorView = btnLang
        case Languages.Spanish:
            lblLanguage.text = "Español"
            self.dropDown.anchorView = btnLang

        default:
            break;
        }
        
        self.rotateButton(button: btnArrow)
        
        self.lblPushNotifications.text = R.string.localizable.settingsPush()
        self.lblTermsAndCOndition.text = R.string.localizable.settingsTerms()
        self.lblChangeLangugae.text = R.string.localizable.settingsChnageLang()
        self.lblPrivacy.text = R.string.localizable.settingsPrivacy()
        self.lblAboutUs.text = R.string.localizable.settingsAbout()
        self.navigationItem.title = R.string.localizable.sidemenuSettingSetting()
    }
    
    // language drop down setup
    func dropDownSetup(){
        
        let isNotificationOn = /DataResponse.driverData?.data?.notifications
        btnSwitch.isOn = (isNotificationOn == "1")
        let selectedLanguage = UserDefaults.standard.value(forKey: "language") as? String
        self.lblLanguage.text = self.getLanguageFromCode(code: /selectedLanguage)
        
        dropDown.dataSource = Array(languages.keys)
        dropDown.textFont = R.font.montBold(size: 14.0)!
        dropDown.cellHeight = 40
        dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        dropDown.cornerRadius = 10
        dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        dropDown.shadowOpacity = 0.9
        dropDown.shadowRadius = 25
        dropDown.width = languageView.frame.size.width
        dropDown.animationduration = 0.25
        dropDown.textColor = .darkGray
        
        dropDown.bottomOffset = CGPoint(x: 0, y:languageView.frame.size.height + 10)
        
        //drop down selection handler
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//
//            self.lblLanguage.text = item
//            self.dropDown.hide()
//            let langCode = Array(self.languages.values)
//            LanguageFile.shared.changeLanguage(type: langCode[index])
            
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                       
                       self.lblLanguage.text = item
                       self.dropDown.hide()
                       let langCode = Array(self.languages.values)
                       LanguageFile.shared.changeLanguage(type: langCode[index])
//            if selectedLanguage != langCode[index]{
//                LanguageFile.shared.changeLanguage(type: langCode[index])
//            }
        }
    }
    
    //MARK: Action Methods
    
    @IBAction func actionSwitchNotification(_ sender: UISwitch) {
        APIManager.shared.request(with: CommonEndPoint.notificationSettings(notifications: sender.isOn ? "1" : "0")) { [unowned self] (response) in
            switch response{
            case .success(_):
                Alerts.shared.show(alert: "alert.alert".localized, message: R.string.localizable.alertSettingChanged(), type: .info)
                self.updateDriverData()
                
            case .failure(let err):
                sender.isOn = !(sender.isOn)
                Alerts.shared.show(alert: "alert.alert".localized, message: err ?? "", type: .info)
            }
        }
    }
    
    @IBAction func actionBAck(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionTermsConditions(_ sender: Any) {
        
        let stringTermsConditions = "\(APIConstants.TermsConditions)"
        
        var urlComplete = String()
        if BundleLocalization.sharedInstance().language == Languages.English {
            urlComplete = "\(stringTermsConditions)\("/en")"
        } else {
            urlComplete = "\(stringTermsConditions)\("/es")"
        }
        
        guard let vc = R.storyboard.loginSignUp.webViewController() else{ return }
        vc.stringUrl = urlComplete
        vc.type = .terms
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionPrivacy(_ sender: Any) {
        
        let stringTermsConditions = "\(APIConstants.privacyUrl)"
        
        var urlComplete = String()
               if BundleLocalization.sharedInstance().language == Languages.English {
                   urlComplete = "\(stringTermsConditions)\("/en")"
               } else {
                   urlComplete = "\(stringTermsConditions)\("/es")"
               }
        guard let vc = R.storyboard.loginSignUp.webViewController() else{ return }
        vc.stringUrl = urlComplete
        vc.type = .privacy
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func actionAboutUs(_ sender: Any) {
        
//        guard let vc = R.storyboard.settingMenu.aboutUsViewController() else{return}
//
        //        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
        //            vc.version =  "Version \(version)"
        //        }
        //
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        let stringTermsConditions = "\(APIConstants.AboutUs)"
        
        var urlComplete = String()
        if BundleLocalization.sharedInstance().language == Languages.English {
            urlComplete = "\(stringTermsConditions)\("/en")"
        } else {
            urlComplete = "\(stringTermsConditions)\("/es")"
        }
        guard let vc = R.storyboard.loginSignUp.webViewController() else{ return }
        vc.stringUrl = urlComplete
        vc.type = .aboutUs
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func actionChangeLanguage(_ sender: Any) {
        
        let langType = BundleLocalization.sharedInstance().language
        
        switch langType {
        case Languages.Arabic:
            self.dropDown.anchorView = lblLeft
            
        case Languages.Urdu:
            
          self.dropDown.anchorView = lblLeft
            
        case Languages.English:
            self.dropDown.anchorView = btnLang
        
        case Languages.Spanish:
             self.dropDown.anchorView = btnLang
            
        default:
            break;
        }
        
        dropDown.direction = .bottom
        dropDown.dataSource = Array(languages.keys)
        dropDown.show()
    }
    
    //MARK:  NETWORK REQUEST
    
    // MARK: UPDATE DRIVER DATA AND GET DRIVER DETAIL AT HOME MAP ▶︎▶︎▶︎
    
    func updateDriverData(){
        
        APIManager.shared.request(with: CommonEndPoint.updateData(newLanguageId: token.selectedLanguage, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken)) { (response) in
            
            switch response{
            case .success(let response_value ):
                DataResponse.driverData = response_value as? DriverModel
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
}

