//
//  AboutUsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {
    //MARK: ---Outlets
    
    @IBOutlet weak var lblVersion: UILabel!
    
    //MARK: --Properties
    
    var version :String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9751529098, green: 0.635238111, blue: 0, alpha: 1)
        lblVersion.text = /version
    }
  
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
