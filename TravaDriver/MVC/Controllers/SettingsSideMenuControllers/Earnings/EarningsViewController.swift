//
//  EarningsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 02/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import Charts
import FSCalendar

struct  MonthDates {
    var start = ""
    var end = ""
}

class EarningsViewController: UIViewController, UIGestureRecognizerDelegate{
    
    //MARK: ---------------------------OUTLETS-------------------------------------
    
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var weekDaysLabel: UILabel!
    @IBOutlet weak var totalEarnings: UILabel!
    @IBOutlet weak var totalRidesLabel: UILabel!
    @IBOutlet weak var totalUpcomingsLabel: UILabel!
    @IBOutlet weak var totalCancelledRidesLabel: UILabel!
    @IBOutlet weak var chartView: BarChartView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblWeekTotal: UILabel!
    @IBOutlet weak var lblDayTotal: UILabel!
    
    @IBOutlet weak var lblRides: UILabel!
    
    @IBOutlet weak var lblUpcoming: UILabel!
    @IBOutlet weak var lblTotalEarnin: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: PROPERTIES
    var monthDates:[MonthDates] = []
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    var index = 1
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    var dataSource : TableViewDataSource?
    
    var arrEarnings:[RangeData]?{
        didSet{
            dataSource?.items = arrEarnings
            tableView.reloadData()
        }
        
    }
    //      let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    var dicDataIndividualDays:[String:[RangeData]] = [:] // dic contains orders per date
    var dicTotalEarningsIndividualDays :[String:Double] = [:] //dic contain total earned per day
    var days = [String]()
    
    //MARK: ---------------------------VIEW CONTROLLER LIFE CYCLE-------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupViewController()
        self.navigationController?.navigationBar.isHidden = false
        lblRides.text = "Rides".localized
        lblUpcoming.text = "alert.pending".localized
        lblTotalEarnin.text = "TotalEarnings".localized
    }
    
    @IBAction func nextDataAction(_ sender: UIButton) {
        if index < monthDates.count {
            previousButton.isHidden = false
            self.getEarnings(startDate: monthDates[index].start, endDate: monthDates[index].end)
            if index == (monthDates.count - 1) {
                nextButton.isHidden = true
            }else{
                index = index + 1
            }
        }
    }
    @IBAction func previousDataAction(_ sender: UIButton) {
        if index >= 0 {
            nextButton.isHidden = false
            self.getEarnings(startDate: monthDates[index].start, endDate: monthDates[index].end)
            if index == 0 {
                previousButton.isHidden = true
            }else{
                index = index - 1
            }
        }
    }
    //MARK: ---------------------------CUSTOM METHODS-------------------------------------
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true) 
    }
    
    func setupViewController(){
        
        //    self.navigationController?.navigationBar.backItem?.title = "back".localized
        //    self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9751529098, green: 0.635238111, blue: 0, alpha: 1)
        self.titleLabel.text = R.string.localizable.chartTitle()
        self.navigationController?.isNavigationBarHidden = true
        
        tableView.tableFooterView = UIView()
        
        //Calendar View Customisation
        self.setupCalendarView()
        
        // Chart View Customisation
        self.setupCustomChart()
        
        // COnfigure table view
        self.configureTableView()
        
    }
    
    //MARK:----Configure TableView
    
    func configureTableView(){
        
        dataSource =  TableViewDataSource.init(items: self.arrEarnings, tableView: tableView, cellIdentifier: R.reuseIdentifier.chartTableViewCell.description, cellHeight: UITableViewAutomaticDimension, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            let object = self?.arrEarnings?[indexPath.row]
            (cell as? ChartTableViewCell)?.order = object
            
            }, aRowSelectedListener: { (indexPath, cell) in
                debugPrint(indexPath)
        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
    }
    
    //MARK:Setup calendar view and get current week data
    
    func setupCalendarView(){
        
        self.view.addGestureRecognizer(self.scopeGesture)
        self.tableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendar.scope = .week
        self.calendar.scopeHandle.backgroundColor  = UIColor.white
        
        //        self.calendar.firstWeekday = 2
        let dateStart = Date().startOfWeek?.toString(format: "yyyy-MM-dd HH:mm:ss")
        //        let dateStart = self.calendar.currentPage.dateToString(format: "YYYY-MM-dd HH:mm:ss")
        
        let dateEnd  = Date().startOfWeek?.addDays(daysToAdd: 6).dateToString(format: "yyyy-MM-dd HH:mm:ss")
        var firstWeek = MonthDates()
        firstWeek.start = Date().startOfWeek?.toString(format: "yyyy-MM-dd HH:mm:ss") ?? ""
        firstWeek.end =  Date().startOfWeek?.addDays(daysToAdd: 6).dateToString(format: "yyyy-MM-dd HH:mm:ss") ?? ""
        monthDates.removeAll()
        monthDates.append(firstWeek)
        var startDate = Date().startOfWeek?.addDays(daysToAdd: -1)
        for _ in 0...2 {
            let firstDate = startDate?.toString(format: "yyyy-MM-dd HH:mm:ss")
            let secondDate = startDate?.addDays(daysToAdd: -6).dateToString(format: "yyyy-MM-dd HH:mm:ss")
            var firstWeek = MonthDates()
            firstWeek.start = secondDate ?? ""
            firstWeek.end = firstDate ?? ""
            monthDates.insert(firstWeek, at: 0)
            startDate = startDate?.addDays(daysToAdd: -7)
        }
        index = (monthDates.count - 2)
        
        //fetch current week data initially
        self.getEarnings(startDate: dateStart!, endDate: dateEnd!)
    }
    
    //MARK:Setup Custom Chart Settings
    
    func setupCustomChart(){
        
        chartView.delegate = self
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        
        chartView.pinchZoomEnabled = false
        chartView.doubleTapToZoomEnabled = false
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        chartView.drawGridBackgroundEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = false
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = UIColor.clear
        chartView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewBottom.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //        calendar.backgroundColor = Colors.chartBackGround
        chartView.drawBarShadowEnabled = true
        
        chartView.rightAxis.labelTextColor = UIColor.clear
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawAxisLineEnabled = false
        
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.leftAxis.drawAxisLineEnabled = false
        chartView.chartDescription?.text = ""
        self.chartView.xAxis.drawGridLinesEnabled = false
        self.chartView.leftAxis.drawLabelsEnabled = false
        self.chartView.legend.enabled = false
        
        if BundleLocalization.sharedInstance().language == Languages.Spanish {
            days = ["Lu","Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                //["Lun","Dom","Lun","mar", "Mie", "Jue", "Vie", "Se sentó"]
        } else {
            days = ["Mon","Sun","Mon","Tue", "Wed", "Thu", "Fri", "Sat"]
        }
        
        
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:days)
        chartView.xAxis.granularity = 0
        chartView.xAxis.labelTextColor = UIColor.white
        chartView.xAxis.labelFont = UIFont(name: "Mont-Bold", size: 13)!
    }
    
    
    //MARK: Categorise ride data according to each day
    func calculateEarningsAccordingToWeekDays(data:[RangeData]){
        
        //        var arrItems:[RangeData] = []
        //        var arrKeys = [String]()
        dicDataIndividualDays = [:]
        dicTotalEarningsIndividualDays = [:]
        
        for (index,dataDays) in days.enumerated() {
            if index < data.count {
                dicTotalEarningsIndividualDays[dataDays] = data[index].earning
                dicDataIndividualDays[dataDays] = data
            }
        }
        self.createChartData()
    }
    
    //MARK: Calculate ride  earnings according to each day
    
    func createChartData(){
        self.displayWeekResultInChart()
    }
    
    //MARK: Display chart according to earnining on each day
    
    
    func displayWeekResultInChart(){
        
        var values = [Double]()
        for item in days {
            
            if  let val = dicTotalEarningsIndividualDays[item]{
                values.append(val)
            }
            else{
                values.append(0.0)
            }
        }
        
        let unitsSold = values
        let count = dicTotalEarningsIndividualDays.values.count
        if count < 1{
            self.emptyDataSet()
            return
        }
        
        let test = Array(1...7)
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<test.count
        {
            let dataEntry = BarChartDataEntry(x: /Double(test[i]), y: Double(unitsSold[i]))
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "")
        chartDataSet.valueTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        chartDataSet.barShadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        chartDataSet.setColor(UIColor.white)
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartView.data = chartData
        let valuess = dicTotalEarningsIndividualDays[calendar.currentPage.toString(format: "yyyy-MM-dd HH:mm:ss").UTCToLocalShortStyle(date: calendar.currentPage.toString(format: "yyyy-MM-dd HH:mm:ss"))] ?? 0
        self.lblDayTotal.text = "\(R.string.localizable.currency()) \(valuess.roundTo(places: 2))"
    }
    
    //MARK: Chart view when no earnings in given week
    
    func emptyDataSet(){
        
        let chartDataSet = BarChartDataSet(values: [], label: "")
        chartDataSet.valueTextColor = Colors.appColor
        chartDataSet.barShadowColor = Colors.appColor
        chartDataSet.setColor(UIColor.clear)
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartView.data = chartData
    }
    
    //MARK: ---------------------------API REQUEST-------------------------------------
    
    func getEarnings(startDate:String, endDate :String){
        
        APIManager.shared.request(with: GeneralEndPoint.getEarnings(startDate: startDate, endDate: endDate)) {[weak self] (response) in
            switch response{
                
            case .success(let responseValue):
                
                let result = responseValue as? Earnings
                self?.arrEarnings = result?.result?.rangeData?.reversed()
                self?.lblWeekTotal.text = "\(R.string.localizable.currency()) \(result?.result?.weeklyEarnings?.roundTo(places: 2) ?? 0)"
                self?.totalEarnings.text = "\(R.string.localizable.currency()) \(result?.result?.totalEarnings?.roundTo(places: 2) ?? 0)"
                self?.totalRidesLabel.text = "\(result?.result?.totalBooking ?? 0)"
                self?.totalUpcomingsLabel.text = "\(result?.result?.totalUpcomingBooking ?? 0)"
                self?.totalCancelledRidesLabel.text = "\(result?.result?.totalCancelledBooking ?? 0)"
                self?.weekDaysLabel.text = "\(self?.arrEarnings?.last?._id ?? "") - \(self?.arrEarnings?.first?._id ?? "")"
                if let result = result?.result{
                    if result.rangeData?.count == 0 {
                        self?.emptyDataSet()
                        self?.lblWeekTotal?.text = "Total ---"
                        self?.lblDayTotal.text = "0.0 \(R.string.localizable.currency())"
                    }
                    else{
                        self?.calculateEarningsAccordingToWeekDays(data: result.rangeData ?? [])
                    }
                }
                else{
                    
                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /result?.message, type: .info)
                }
                
            case .failure(let err):
                
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
}

//MARK:: CHART DELEGATES

extension EarningsViewController: ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        debugPrint(entry)
        self.lblDayTotal.text = "\(R.string.localizable.currency()) \(entry.y.roundTo(places: 2))"
    }
}


//MARK:: CALENDAR DELEGATES

extension EarningsViewController: FSCalendarDelegate,FSCalendarDataSource {
    
    // MARK:- UIGestureRecognizerDelegate
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        chartView.zoomOut()
        
        return false
        
        let shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top
        
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendar.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstarint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        debugPrint("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        debugPrint("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        
        let dateStart = self.calendar.currentPage.dateToString(format: "yyyy-MM-dd HH:mm:ss")
        let dateEnd  = self.calendar.currentPage.addDays(daysToAdd: 6).dateToString(format: "yyyy-MM-dd HH:mm:ss")
        
        //fetch current week data on week change
        self.getEarnings(startDate: dateStart, endDate: dateEnd)
    }
    
}

extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

extension Date {
    
    var startOfWeek: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
    
    var startOfMonth: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year, .month], from: self))
    }

}

