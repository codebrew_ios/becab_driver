//
//  DeliveryAreasViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 25/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit




class DeliveryAreasViewController: UIViewController, refreshClientDelegate {
    
    //MARK:-- Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-- Properties
    
    var dataSource : TableViewDataSource?
    
    var isClients = false
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

    var organisation_area_id : Int?
    
    var items : [Areas]?{
        didSet{
            dataSource?.items = items
            tableView.reloadData()
        }
    
    }
    var clients : [EtokenClients]?{
        didSet{
            dataSource?.items = clients
            tableView?.reloadData()
        }
    }
    
    //MARK:-- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        isClients ==  true ?  self.configureTableViewClients() : self.configureTableView()
        self.navigationController?.navigationBar.removeShadow()
        isClients ==  true ? self.getAreaClients() : self.getDeliveryAreas()
    }
    
    
    // --------------------Custom Methods-----------------
    
    
    //MARK: Selection handler from Areas
    
    func selectionHandlerArea(indx:Int){
        guard let vc =   R.storyboard.settingMenu.deliveryAreasViewController() else{return}
        vc.isClients = true
        vc.organisation_area_id = self.items?[indx].organisation_area_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Selection handler from clients
    
    func selectionHandlerClient(indx:Int){
        guard let vc =   R.storyboard.settingMenu.deliverDrinkingWaterViewController() else{return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: Configure table views

extension DeliveryAreasViewController{
    
    func configureTableView(){
        
        self.navigationItem.title = R.string.localizable.etokensDeliveryAreas()
        
        dataSource =  TableViewDataSource.init(items: items, tableView: tableView, cellIdentifier: R.reuseIdentifier.deliveryAreaTableViewCell.description, cellHeight: 100.0, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            let item = self?.items?[indexPath.row]
            (cell as? DeliveryAreaTableViewCell)?.title = item?.label
            
            }, aRowSelectedListener: { (indexPath, cell) in
                // move to selected menu
                self.selectionHandlerArea(indx: indexPath.row)
        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
    }
    
    func configureTableViewClients(){
        self.navigationItem.title = R.string.localizable.etokensAreaClients()
        
        dataSource =  TableViewDataSource.init(items: clients, tableView: tableView, cellIdentifier: R.reuseIdentifier.deliveryAreaCustomersTableViewCell.description, cellHeight: 200.0, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            (cell as? DeliveryAreaCustomersTableViewCell)?.item =  self?.clients?[indexPath.row]
            (cell as? DeliveryAreaCustomersTableViewCell)?.delegate = self
            
            }, aRowSelectedListener: { (indexPath, cell) in

        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
    }
}

//MARK: Api Requests

extension DeliveryAreasViewController{
    
    func getDeliveryAreas(){
        
       let day = Date().dayOfWeek()
        
        APIManager.shared.request(with: GeneralEndPoint.getWaterDeliverylocation(day: days.firstIndex(of: "\(/day)") ?? 1, latitude: LocationManager.shared.latitude, longitude: LocationManager.shared.longitude, skip: 0, take: 100)) { [weak self](response) in
            switch response{
            case .success(let responseValue):
                
                let obj = responseValue as? WaterDeliveryAreas
                self?.items = obj?.result

            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
    
    func getAreaClients(){
        
        APIManager.shared.request(with: GeneralEndPoint.getAreaCustomers(organisation_area_id: /self.organisation_area_id, latitude: LocationManager.shared.latitude, longitude: LocationManager.shared.longitude, skip: 0, take: 1000)) { [weak self](response) in
            switch response{
            case .success(let responseValue):
                
                self?.clients = (responseValue as? WaterEtokenClients)?.result
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
    
    // delegate call back after start delivery to refresh
    
    func reloadClients() {
        
        NotificationCenter.default.post(name: Notification.Name(NotificationNames.refreshOrders.rawValue), object: nil, userInfo: nil)
        self.getAreaClients()
        
        
        if let nav = self.navigationController{
            for controller in nav.viewControllers.reversed(){
                if controller.isKind(of: MapViewController.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                }
            }
        }
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
    
    func YearOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
    
    func dayNumberOfWeek() -> Int? {
        
         var calendar = Calendar.current
        calendar.locale = Locale.init(identifier: "en_US")
        return calendar.dateComponents([.weekday], from: self).weekday
        
//        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
}

