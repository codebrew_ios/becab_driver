//
//  DeliverDrinkingWaterViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 26/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import Kingfisher
import ObjectMapper

struct pendingApproval {
    static var arrOrderIds = [Int]()
}

class DeliverDrinkingWaterViewController: UIViewController{
    
    //MARK:: OUTLETS
    
    @IBOutlet weak var imgViewCustomer: UIImageView!
    @IBOutlet weak var imgRating: UIImageView!
    
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblRateValue: UILabel!
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    @IBOutlet weak var tfDeliveredBottles: UITextField!
    @IBOutlet weak var btnDeliver: AnimatableButton!
    
    //MARK:: Properties
    
    var item :OrdersList?
    
    //MARK:: View Controller lIfe Cycyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupController()
        
        // Do any additional setup after loading the view.
    }
    
    func setupController(){
        
        imgViewCustomer.kf.setImage(with: URL.init(string: "\(/item?.user?.profile_pic_url)"), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        
        lblCustomerName.text = /item?.user?.name
        lblDeliveryAddress.text = /item?.dropoff_address
        lblBrand.text = item?.brand?.brand_name
        lblQuantity.text =  "\(/item?.brand?.name) × \(/item?.payment?.product_quantity)"
        tfDeliveredBottles.text = "\(/item?.payment?.product_quantity)"
        
        if pendingApproval.arrOrderIds.contains(/item?.order_id){
            self.btnDeliver.setTitle(R.string.localizable.btndeliverCustConfirmationPending(), for: .normal)
            self.btnDeliver.isEnabled = false
        }
        
        self.getOrderDetail(orderId: /item?.order_id?.toString())
    }
    
    func getOrderDetail(orderId:String){
        
        APIManager.shared.request(with: GeneralEndPoint.orderDetails(orderId: orderId)) { (response) in
            switch response{
                
            case .success(let responseValue):
                
                let object  =  Mapper<ServiceRequest>().map(JSONObject: responseValue)
                
                if object?.order?.order_status == SocketConstants.serCustPending{
                    self.btnDeliver.setTitle(R.string.localizable.btndeliverCustConfirmationPending(), for: .normal)
                    self.btnDeliver.isEnabled = false
                }
                else{
                    self.btnDeliver.setTitle(R.string.localizable.btndeliverDeliver(), for: .normal)
                    self.btnDeliver.isEnabled = true
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertError(), message: /err, type: .info)
            }
        }
    }
    
    //MARK:: Button Action Methods
    
    @IBAction func actionDeliver(_ sender: Any) {
        let val:Int = /Int(tfDeliveredBottles!.text!)
        if val == 0{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message:R.string.localizable.deliverEnterBottles(), type: .info)
            return
        }
        
        
        APIManager.shared.request(with: GeneralEndPoint.endOrderFromEtoken(orderId: /item?.order_id, latitude: /LocationManager.shared.currentLoc?.coordinate.latitude, longitude: /LocationManager.shared.currentLoc?.coordinate.longitude, productQuantity: val, order_distance: nil)) {[weak self ] (response) in
            switch response{
            case .success(let responseValue):
                
                self?.btnDeliver.isEnabled = false
                self?.btnDeliver.setTitle(R.string.localizable.btndeliverCustConfirmationPending(), for: .normal)
                
                
                let obj = responseValue as?  CompletedOrderModel
                pendingApproval.arrOrderIds.append(/obj?.result?.order_id)
                DataResponse.completedRequest = obj
                
            case .failure(let err ):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
            }
        }
    }
    
    @IBAction func actionCallCustomer(_ sender: Any) {
        let number = "\(/item?.user?.phone_code)" + "\(/item?.user?.phone_number)"
        self.callToNumber(number: number)
    }
    
}

extension DeliverDrinkingWaterViewController : UITextFieldDelegate {
    
    //MARK: - ---------------UItextFieldDelegate----------------
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            
            if text == "" && string == "0" {
                return false
            }
            
            let newLength = text.length + string.length - range.length
            return newLength <= 12
            
        } else {
            Alerts.shared.showOnTop(alert: Alert.oops.getLocalised(), message: "alertTitle.PhoneNoEnglish".localized , type: .info)
            return false
        }
    }
}
