//
//  ProfileViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import DropDown
import ActionSheetPicker_3_0
import CoreLocation
import GooglePlaces

class ProfileViewController: BaseController {
    
    //MARK: Outlets
    //@IBOutlet weak var addressTextFiled: AnimatableTextField!
    @IBOutlet weak var btnExpiryDate: UIButton!
    @IBOutlet weak var tfName: AnimatableTextField!
    @IBOutlet weak var tfDrivingLicenseNumber: AnimatableTextField!
     @IBOutlet weak var tfEmail: AnimatableTextField!
     @IBOutlet weak var tfPhoneNumber: AnimatableTextField!
    @IBOutlet weak var tfExpiryDate: UITextField!
    @IBOutlet weak var btnMulkiyaFront: UIButton!
    @IBOutlet weak var btnMulkiyaBack: UIButton!
    @IBOutlet weak var btnUploadProfile: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnUpdate: AnimatableButton!
    @IBOutlet weak var btnRemove1: UIButton!
    @IBOutlet weak var btnRemove2: UIButton!
    @IBOutlet weak var txtSelectExpiryTime: UILabel!
    @IBOutlet weak var txtUploadMulkiya: UILabel!
    @IBOutlet weak var txtUploadDetail: UILabel!
    @IBOutlet weak var txtFront: UILabel!
    @IBOutlet weak var txtBack: UILabel!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var viewCarModel: UIView!
    @IBOutlet weak var txtCarModel: UITextField?
    @IBOutlet weak var viewCarColor: UIView!
    @IBOutlet weak var collectionViewCarColor: UICollectionView! //{
    @IBOutlet weak var tableView: UITableView!{
                didSet {
                    selectedColor = /DataResponse.driverData?.data?.cabColor
                }
            }
    @IBOutlet weak var colorImg: UIImageView!
    @IBOutlet weak var lblColor: UILabel?
    
    @IBOutlet weak var btnSelectColors: UIButton!
    @IBOutlet weak var licenseplate: AnimatableTextField!
    @IBOutlet weak var licensemake: AnimatableTextField!
    

    //MARK: Properties
    var homeAddress:Address?
    var dataSource : TableViewDataSource?
    let dropDownCustom = DropDown()
    var selectedlicenseFace = 0
    var categoryIdsToSend = ""
    var countImage = 2
    let formatter = DateFormatter()
    var dateStringServer = String()
    
    lazy var arr:[String] = []
    lazy var arrIds:[String] = []
    lazy var arrIndxSelected:[Int] = []
    
    @IBOutlet var longGestureProfileImage: UILongPressGestureRecognizer!
    @IBOutlet var longPressMulkiyafront: UILongPressGestureRecognizer!
    @IBOutlet var longPressMulkiyaBack: UILongPressGestureRecognizer!
    
    var dataSourceCollection: SKCollectionViewDataSource?
    var arrayColors: [String] = [
        "FFFFFF"
        ,"808080"
        ,"008000"
        ,"FF0000"
        ,"0000FF"
        ,"FFFF00"
    ]
    
    var arrayColorsName: [String] = [
        "White","Gray","Green","Red","Blue","Yellow"
    ]
    
    var selectedColor: String = ""

    
    //MARK: View Controller life cycle Metyhodas
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup View Controller
        self.setupController()
        self.checkDrivingLicenseValidity()
    }
    
    //MARK: Custom Methods
    
    func checkDrivingLicenseValidity() {
        let validityDate = UserDefaults.standard.value(forKey: "mulkiya_validity") as? String ?? ""
        
       // let newDate = validityDate.toDatee()
        
        let currentDate = Date()
        var difference = 0
        if let newDate = validityDate.toDatee() {
               difference = newDate.days(from: currentDate)
        }
//        if difference <= 0 {
//            self.btnMulkiyaFront.isUserInteractionEnabled = true
//            self.btnMulkiyaBack.isUserInteractionEnabled = true
//            self.tfExpiryDate.isUserInteractionEnabled = true
//            self.btnRemove1.isUserInteractionEnabled = true
//            self.btnRemove2.isUserInteractionEnabled = true
//            self.btnExpiryDate.isUserInteractionEnabled = true
//        }else{
//            self.btnMulkiyaFront.isUserInteractionEnabled = false
//            self.btnMulkiyaBack.isUserInteractionEnabled = false
//            self.tfExpiryDate.isUserInteractionEnabled = false
//            self.btnRemove1.isUserInteractionEnabled = false
//            self.btnRemove2.isUserInteractionEnabled = false
//            self.btnExpiryDate.isUserInteractionEnabled = false
//        }
    }
    
    func setupController(){
        
        self.localiseController() // localise components
        
        [tfName,tfPhoneNumber,tfEmail,tfDrivingLicenseNumber,txtCarModel,licenseplate].forEach { field in
            field?.isUserInteractionEnabled = false
        }
        //update driver details
        tfName.text = DataResponse.driverData?.data?.dataUser?.name
        tfDrivingLicenseNumber.text = "\(/DataResponse.driverData?.data?.mulkiya_number)"
        tfEmail.text = /DataResponse.driverData?.data?.dataUser?.email
        licenseplate.text = /DataResponse.driverData?.data?.vehicle_registration_number
        licensemake.text = /DataResponse.driverData?.data?.vehicle_make
        
        tfPhoneNumber.text = "\(/DataResponse.driverData?.data?.dataUser?.phone_code)" + " " + "\(/DataResponse.driverData?.data?.dataUser?.phone_number)"
        
        if let val:String = DataResponse.driverData?.data?.profile_pic_url {
            self.imgView.kf.setImage(with: URL.init(string: val ), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
            btnUploadProfile.setImage(nil, for: .normal)
        }
        // addressTextFiled.text = DataResponse.driverData?.data?.home_address
        //format expiryDate
        let dateStr = DataResponse.driverData?.data?.mulkiya_validity
        self.dateStringServer = /dateStr
        self.formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.formatter.timeZone = TimeZone.current
        colorImg.backgroundColor = UIColor(hexString: selectedColor)
        
        let dateObj = self.formatter.date(from: /dateStr)
        self.formatter.dateFormat = "dd/MM/yyyy"
        self.formatter.timeZone = TimeZone.current
        
        if dateObj != nil{
            self.tfExpiryDate.text =  self.formatter.string(from: dateObj!)
        }
        
        //add mulkiya images
        let front_mulkiya = APIConstants.imagesBasePath + "\(/DataResponse.driverData?.data?.mulkiya_front)"
        let back_mulkiya = APIConstants.imagesBasePath + "\(/DataResponse.driverData?.data?.mulkiya_back)"
        self.btnMulkiyaFront.kf.setImage(with: URL.init(string: front_mulkiya), for: .normal)
        self.btnMulkiyaBack.kf.setImage(with: URL.init(string: back_mulkiya), for: .normal)
        txtCarModel?.text = /DataResponse.driverData?.data?.cabModel
        configureTableView()
        
    }
    
    // localise controller components
    
    func localiseController(){
        //localizing
        btnBack.title = "back".localized
        txtBack.text = "signup3.Back".localized
        txtFront.text = "signup3.front".localized
        txtUploadMulkiya.text = "signup3.uploadInfo1".localized
        txtUploadDetail.text = "signup3.uploadInfo2".localized
        txtSelectExpiryTime.text  = "signup3.expiryInfo".localized
        self.tfName.placeholder = "signup1.tfNamePlaceholder".localized
        btnEdit.setTitle(R.string.localizable.settingEdit(), for: .normal)
        btnUpdate.setTitle("Update".localized, for: .normal)
        self.tfDrivingLicenseNumber.placeholder = "signup3.tfLicensePlaceholder".localized
        self.navigationItem.title = R.string.localizable.settingProfile()
        
        self.view.setTextFields(mainView: self.view.subviews)
        self.rotateButton(button: btnArrow)
        
    }
    
    // validate new chnages in driver settings
    func validateForm(){
        
        if tfName.text == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertName() , type: .info)
            return
        }
        else if tfDrivingLicenseNumber.text == "" {
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.enterlicense".localized , type: .info)
            return
        }
        else if tfExpiryDate.text == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.enterExpiryDate".localized , type: .info)
            return
        }
        else if tfEmail.text == "" {
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.email".localized, type: .info)
            return
        }
//        else if countImage < 2{
//            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.uploadBoth".localized , type: .info)
//            return
//        }
        
        self.updateProfileImage()
    }
    
    @IBAction func selectHomeAddressAction(_ sender: UIButton) {
        let country = UserDefaults.standard.value(forKey: "current_contry") as? String
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        filter.country = country
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func selctColor(_ sender: UIButton) {
        tableView?.isHidden = false
        
        configureTableView()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackIcon(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionLongPressProfilePic(_ sender: UIButton) {
        
        // Uncomment below to preview image on imageview tap action
        
        if (longGestureProfileImage.state == UIGestureRecognizerState.began) {
            guard let vc = R.storyboard.settingMenu.profileImagePreviewViewController() else{return}
            vc.image = imgView.image!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionLongPressFront(_ sender: UILongPressGestureRecognizer) {
        
        if (longPressMulkiyafront.state == UIGestureRecognizerState.began) {
            guard let vc = R.storyboard.settingMenu.profileImagePreviewViewController() else{return}
            vc.image = /btnMulkiyaFront.imageView?.image
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func actionLongPressBackMulkiya(_ sender: Any) {
        if (longPressMulkiyaBack.state == UIGestureRecognizerState.began) {
            guard let vc = R.storyboard.settingMenu.profileImagePreviewViewController() else{return}
            vc.image = /btnMulkiyaBack.imageView?.image
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //Open Action Sheet Of Library and Camera
    
    func openPhotoOptionsActionSheet(){
        
        CameraImage.shared.captureImage(
            from: self, navigationController: self.navigationController,
            mediaType: nil,
            captureOptions: [.camera, .photoLibrary],
            allowEditting: true) {
                
                [unowned self] (image,fileName) in
                guard let img = image else { return }
                
                if self.selectedlicenseFace == 0 {
                    self.btnMulkiyaFront.setImage(img, for: .normal)
                    self.countImage += 1
                    self.btnRemove1.isHidden = false
                    
                } else{
                    self.btnMulkiyaBack.setImage(img, for: .normal)
                    self.countImage += 1
                    self.btnRemove2.isHidden = false
                }
        }
    }
    
    //MARK: Action Methods
    
    @IBAction func actionBackArraow(_ sender: Any) {
    }
    
    // update driver image
    @IBAction func actionPreviewProfileImage(_ sender: Any) {
        self.view.endEditing(true)
        
        CameraImage.shared.captureImage(
            from: self, navigationController: self.navigationController,
            mediaType: nil,
            captureOptions: [.camera, .photoLibrary],
            allowEditting: true) {
                [unowned self] (image,fileName) in
                guard let img = image else { return }
                self.imgView.image = img
                self.btnUploadProfile.setImage(nil, for: .normal)
                //                self.updateProfileImage()
                //                self.updateDriverData()
                
        }
        
        
    }
    
    // new mulkiya expiry date picker
    @IBAction func actionSelectExpiryDate(_ sender: UIButton) {
        let yearsToAdd = 10
        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.year = yearsToAdd
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        ActionSheetDatePicker.show(withTitle: "Expiry Date", datePickerMode: .date, selectedDate: Date(), minimumDate: Date(), maximumDate: futureDate, doneBlock: { (picker, resp1, resp2) in
            self.formatter.dateFormat = "dd/MM/yyyy"
            
            self.tfExpiryDate.text = self.formatter.string(from: (resp1 as? Date)!)
            self.formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            self.formatter.locale = Locale(identifier: "en_US")
            self.dateStringServer =  self.formatter.string(from: (resp1 as? Date)!)
            
        }, cancel: { (picker) in
            debugPrint("cancelled")
        }, origin: sender)
    }
    
    // update front mulkiya
    @IBAction func actionUploadFrontMulkiya(_ sender: Any) {
        selectedlicenseFace = 0
        self.view.endEditing(true)
        self.openPhotoOptionsActionSheet()
    }
    
    // update back mulkiya
    @IBAction func actionUploadBackMulkiya(_ sender: Any) {
        
        selectedlicenseFace = 1
        self.view.endEditing(true)
        self.openPhotoOptionsActionSheet()
    }
    
    
    // remove mulkiya image action
    @IBAction func actionRemoveMulkiyaFrontBack(_ sender: UIButton) {
        
        if sender.tag == 0{
            
            btnMulkiyaFront.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove1.isHidden = true
                
            }
        }
        else{
            btnMulkiyaBack.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove2.isHidden = true
            }
        }
    }
    // chnage image with edit button below imageview
    @IBAction func actionChangeImage(_ sender: Any) {
        
                self.view.endEditing(true)
                
                CameraImage.shared.captureImage(
                    from: self, navigationController: self.navigationController,
                    mediaType: nil,
                    captureOptions: [.camera, .photoLibrary],
                    allowEditting: true) {
                        [unowned self] (image,fileName) in
                        guard let img = image else { return }
                        self.imgView.image = img
                        self.btnUploadProfile.setImage(nil, for: .normal)
//                        self.updateProfileImage()
                        //                self.updateDriverData()
                        
                }
        
    }
    
    @IBAction func actionUpdateProfile(_ sender: Any) {
        
        self.validateForm()
    }
    
    //MARK: Network Requests
    
    // update profile image only
    func updateProfileImage()  {
        
        var imgs:[UIImage] = []
        
        if let img = self.imgView.image{
            let resized = img.resized(withPercentage: 0.7)
            imgs.append(/resized)
        }
        
        APIManager.shared.request(withImages: LoginEndpoint.updateProfileImage(), images: imgs) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response{
            case .success(_):
                
                self.updateProfileData()
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.alert".localized, message: /err, type: .info)
            }
        }
    }
    
    // update other driver settings mulkiya name etc
    func updateProfileData() {
        
        let strCarModel = /txtCarModel?.text?.trim()
        let strCarColor = selectedColor
        
        var imgs:[UIImage] = []
        if let img = self.btnMulkiyaFront.image(for: .normal) {
//            imgs.append(img)
        }
        
        if let img = self.btnMulkiyaBack.image(for: .normal) {
//            imgs.append(img)
        }
        
        let objR = LoginEndpoint.profileUpdate(name: /tfName.text, mulkiya_number: /tfDrivingLicenseNumber.text, mulkiya_validity: self.dateStringServer, carModel: strCarModel, carColor: strCarColor, home_address:"", home_longitude: Locations.longitutde.getLoc() , home_latitude: Locations.lat.getLoc(),email: /tfEmail.text)
        
        APIManager.shared.request(withImages: objR, images: imgs) { [weak self] (response) in
            
            switch response{
            case .success(_):
                self?.updateDriverData()
                
            case .failure(let err):
                
                Alerts.shared.show(alert: "alert.alert".localized, message: /err, type: .info)
            }
        }
    }
    
    // update driver data after chnages
    func updateDriverData() {
        
        APIManager.shared.request(with: CommonEndPoint.updateData(newLanguageId: token.selectedLanguage, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: "", fcmId: token.fcmToken)) {
            [weak self](response) in
            
            switch response{
            case .success(let response_value ):
                DataResponse.driverData = response_value as? DriverModel
                Alerts.shared.show(alert:"alert.alert".localized , message: "setting.profileDataUpdated".localized, type: .info)
                self?.navigationController?.popViewController(animated: true)
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
            }
        }
    }
}

//MARK:- ======== Collection View ========
extension ProfileViewController {
    func configureTableView(){
        
        dataSource =  TableViewDataSource.init(items: arrayColors, tableView: tableView, cellIdentifier: "ColorsTableViewCell", cellHeight: 16, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            (cell as? ColorsTableViewCell)?.backgroundColor = UIColor.init(hexString: self?.arrayColors[indexPath.row] ?? "")
            (cell as? ColorsTableViewCell)?.colorLabel?.text = self?.arrayColorsName[indexPath.row]

            }, aRowSelectedListener: { (indexPath, cell) in
                // move to selected menu
                self.selectedColor = self.arrayColors[indexPath.row]
                self.colorImg.backgroundColor = UIColor(hexString: self.arrayColors[indexPath.row])
                self.lblColor?.text = self.arrayColorsName[indexPath.row]
                self.tableView?.isHidden = true
        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tableView?.delegate = dataSource
        tableView?.dataSource = dataSource
        tableView?.reloadData()
    }
//    func configureCollectionView() {
//
//        let identifier = SelectColorCollectionCell.identifier
//        collectionViewCarColor.registerCells([identifier])
//
//        dataSourceCollection = SKCollectionViewDataSource(items: arrayColors, collectionView: collectionViewCarColor, cellIdentifier: identifier, cellHeight: 30.0, cellWidth: 30.0)
//
//        dataSourceCollection?.configureCellBlock = {
//            [weak self] (indexPath, cell, item) in
//            guard let self = self else { return }
//
//            if let cell = cell as? SelectColorCollectionCell, let item = item as? String {
//                cell.isSelect = self.selectedColor == item
//                cell.backgroundColor =  UIColor(hexString: item)
//                cell.layer.cornerRadius = 15.0
//                cell.clipsToBounds = true
//            }
//        }
//
//        dataSourceCollection?.aRowSelectedListener = {
//            [weak self] (indexPath, cell) in
//            guard let self = self else { return }
//            debugPrint(indexPath)
//
//            self.selectedColor =  self.arrayColors[indexPath.row]
//            self.collectionViewCarColor.reloadData()
//
//        }
//
//        dataSourceCollection?.reload(items: arrayColors)
//    }
}


extension ProfileViewController : GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        homeAddress = Address(gmsPlace: place)
        //addressTextFiled.text = place.formattedAddress
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}



extension String {
    func convertDateFormater() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "MMM dd,yyyy HH:mm a"
        return  dateFormatter.string(from: date!)
    }
    
    
    func  toDatee() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
      return dateFormatter.date(from:self)
    }
}
