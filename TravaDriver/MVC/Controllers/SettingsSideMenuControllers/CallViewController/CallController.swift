//
//  CallController.swift
//  Buraq24Driver
//
//  Created by Apple on 21/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class CallController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func callButtonAction(_ sender: UIButton) {
        
        if let url = URL(string: "tel://+94117669766"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
