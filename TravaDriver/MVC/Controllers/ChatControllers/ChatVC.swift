//
//  ChatVC.swift
//  Buraq24Driver
//
//  Created by Apple on 05/08/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
enum CallType : String {
    case Audio = "0"
    case Video = "1"
}
enum ChatMessageType : Int {
    case Text  = 1
    case Attachment = 2
    case Audio = 3
    case Video = 4
}

class ChatVC: UIViewController {
    
    //MARK:- Properties
    var dataSource : TableViewDataSource?
    var otherUserId : Int = 0
    var otherUserDetailId : Int = 0
    var chatDta :[Chat]?
    var lblTimeElapsed : UILabel?
    var timeLeft : Int?
    var viewTimeElapsed : UIView?
    var uploadDta : imgeUpload?
    var name : String = ""
    var profilePic : String = ""
    var orderId:String?
    //MARK:- OUTLETS
    
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnOpenGallery: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var btnAudioCall: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtMsg: TextViewplaceholder!{
        didSet{
            txtMsg.placeholder = "typeHere".localized
        }
    }
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtInputBottomConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        imgProfile.sd_setImage(with:URL(string:  "\(APIBasePath.basePath)/" + "images/" + /driverDtaGet.profilePic), placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
        if self.profilePic != "" {
//            if let url = URL(string: profilePic) {
                let urlProfile = URL(string: profilePic)
            self.imgProfile.kf.setImage(with: urlProfile, placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
//            }else{
//                self.imgProfile.image = R.image.ic_profie_bg_ph()
//            }
        }else{
            self.imgProfile.image = R.image.ic_profie_bg_ph()
        }
        
        let firstName = name
//        
//        if name.components(separatedBy: " ") != nil {
//            firstName = /name.components(separatedBy: " ").first
//        }else {
//            firstName = name
//        }
        
        lblName.text = firstName
        configureTableView()
        emitRecieve()
        getChatDta()
        shadowView.layer.cornerRadius = 10
        shadowView.layer.shadowColor = UIColor(white: 0.6, alpha: 1).cgColor
        shadowView.layer.shadowOpacity = 0.9
        shadowView.layer.shadowRadius = shadowView.frame.height/2
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        btnSend.addTarget(self, action: #selector(btnSendAct(_:)), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(btnBackAct(_:)), for: .touchUpInside)
        btnOpenGallery.addTarget(self, action: #selector(btnGalleryAct(_:)), for: .touchUpInside)
        btnAudioCall.addTarget(self, action: #selector(btnAudioAct(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getChatDta), name: Notification.Name(SocketConstants.Chat), object: nil)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
         NotificationCenter.default.post(name: Notification.Name(rawValue:"chat"), object: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: - Update Chat Array
    func updateChat(_ messages: [Chat]) {
        self.dataSource?.items = messages
        self.tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) { [weak self] in
            guard let self = self else { return }
            if self.tblView.contentSize.height < self.tblView.frame.height {
                return
            }
            
             self.scrollToBottom()
          //  self.tblView.scrollTo(direction: .Bottom, animated: true)
        }
    }
    
}
//MARK:- VIDEO CALL VC DELEGATES
//extension ChatVC: VideoCallVcCallStatusDelegates  {
//
//    func didDisconnectCall(callType: Int, isMissedCall: Bool) {
//        //        let chatMessage = ChatVC()
//        //        chatMessage.msg_type = callType
//        //        chatMessage.is_missed = isMissedCall ? 1 : 0
//        //        chatMessage.user_id = /UserSingleton.shared.loggedInUser?.data?.user_id
//        //        chatMessage.created_at = Date().toGlobalTime().dateToString(currentFormat: EnumDateFormat.yyyyMMddHHmmssZ.rawValue, newFormat:  EnumDateFormat.yyyyMMddHHmmss.rawValue)
//        //        arrChatMessages?.append(chatMessage)
//        //        tableView.reloadData()
//        //        tableView.scrollToRow(at: IndexPath(row: /arrChatMessages?.count - 1, section: 0), at: .bottom, animated: true)
//    }
//
//    func consultationTimeOver() {
//
//        Timer.runThisAfterDelay(seconds: 1.0) {
//            self.view.endEditing(true)
//        }
//        //        tableView.reloadData()
//        //        UtilityFunctions.showAlertMessage(alert: R.string.localizable.note(), message: R.string.localizable.yourConsultationTimeGotOver(), viewController: self, buttonText: R.string.localizable.ok())
//
//    }
//
//}
//MARK:- EXTERNAL FUNCTION
extension ChatVC{
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.chatDta?.count != 0{
                let indexPath = IndexPath(row: /self.chatDta?.count-1, section: 0)
                self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    @objc func btnAudioAct(_ sender : UIButton){
        //        let vc = R.storyboard.main.videoCallVC()
        //        vc?.callType = CallType.Audio.rawValue
        //        vc?.receiverId = otherUserId
        //        vc?.name = name
        //        vc?.delegate = self
        //        vc?.timeLeft = self.timeLeft
        //        self.present(vc!, animated: true, completion: nil)
//
//        guard let vc = R.storyboard.main.voipViewController() else { return }
//        vc.caller = Caller(name: /name, userId: "\(/otherUserId)")
//        self.present(vc, animated: true, completion: nil)
    }
    
    
    @objc func btnGalleryAct(_ sender : UIButton){
        
//        CameraImage.shared.captureImage(
//            from: self, At: tblView,
//            mediaType: nil,
//            captureOptions: [.camera, .photoLibrary],
//            allowEditting: true) {
//                [unowned self] (image) in
//                guard let img = image else { return }
//                APIManager.shared.request(withImages: CommonEndPoint.uploadImge(), images: [img]) {
//                    [weak self] (response) in
//                    guard self != nil else { return }
//
//                    switch response{
//                    case .success(let data):
//                        print("yes")
//                        print(data as Any)
//                        self?.uploadDta = data as? imgeUpload
//                        if /self?.txtMsg.text != "Type Message"{
//                            SocketIOManager.shared.sendChatsMessage(text: /self?.txtMsg.text!, to: /self?.otherUserId, sendAt: "2019-01-12 3:27:10", originalIMage: "", thumbnailImage: "",chat_type : "image")
//                        }else{
//                            SocketIOManager.shared.sendChatsMessage(text: "", to: /self?.otherUserId, sendAt: "2019-01-12 3:27:10", originalIMage: "\(APIConstants.socketBasePath)/" + "images/" + /self?.uploadDta?.original, thumbnailImage: APIConstants.socketBasePath + "/images/" + /self?.uploadDta?.thumbnail,chat_type : "image")
//                        }
//                        let dte = Date()
//                        let format = DateFormatter()
//                        format.timeZone = TimeZone.current
//                        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                        let dteGet = format.string(from: dte) as AnyObject
//                        let time = Utility.dteGetConvert(string: dteGet as! String, fromFormat: "yyyy-MM-dd HH:mm:ss", "HH:mm")
//                        let chat = Chat(cid: 0, conversationId: 0, send_to: /self?.otherUserId, send_by: /UserSingleton.shared.loggedInUser?.data?.user_id, text: "", sent_at: time,original : /self?.uploadDta?.original,thumbnail : /self?.uploadDta?.original,chat_type : "image")
//                        self?.chatDta?.append(chat)
//                        self?.updateChat(self?.chatDta ?? [])
//
//                    case .failure(let err):
//                        Alerts.shared.show(alert: "alert.alert".localized, message: /err, type: .info)
//                    }
//                }
//
//        }
    }
    
    //MARK: - Back Action
    @objc func btnBackAct(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func btnSendAct(_ sender : UIButton){
        txtMsg.text = txtMsg.text.trailingSpacesTrimmed
        txtMsg.text = txtMsg.text.removingLeadingSpaces()
        
        let driverId = DataResponse.driverData?.data?.user_detail_id
        
        if txtMsg.text != "" && txtMsg.textColor != UIColor.lightGray && txtMsg.text != txtMsg.placeholder!{
            SocketIOManager.shared.sendChatsMessage(userDetailId: "\(/self.otherUserDetailId)", text: txtMsg.text!, to: otherUserId, sendAt: "2019-01-12 3:27:10", originalIMage: "", thumbnailImage: "",chat_type : "text", orderId: /orderId)
            let dte = Date()
            let format = DateFormatter()
            format.timeZone = TimeZone.current
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dteGet = format.string(from: dte) as AnyObject
            let chat = Chat(cid: 0, conversationId: 0, send_to: otherUserId, send_by: /UserSingleton.shared.loggedInUser?.data?.user_id, text: /self.txtMsg.text, sent_at: dteGet as? String, original : nil,thumbnail : nil,chat_type : "text")
            
            chatDta?.append(chat)
            updateChat(chatDta ?? [])
            
            txtMsg.text = ""
            
        }
        
    }
}
//MARK:- DATA SOURCE FUNCTION
extension ChatVC{
    func configureTableView(){
        
        let  configureCellBlock : ListCellConfigureBlock = { [weak self] ( cell , item , indexpath) in
            guard let object  = self?.chatDta?[indexpath.row] else  {return}
            if /object.send_by == /UserSingleton.shared.loggedInUser?.data?.user_id{
                if /object.chat_type == "text"{
                    if let cell = cell as? chatRightTVC {
                        cell.lblChatMsg.text = /object.text
                        let dteGet = Utility.dteGetConvert(string: /object.sent_at, fromFormat: "yyyy-MM-dd HH:mm:ss", "hh:mm a")
                        cell.lblTime.text = dteGet
                    }
                }else{
                    if let cell = cell as? chatImgeRightTVC {
                        //cell.imgeViewRight.sd_setImage(with:URL(string:  /object.original), placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)//(with:URL(string:  "\(APIConstants.basePath)/" + "images/" + /self?.uploadDta?.original), placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
                        
                        let dteGet = Utility.dteGetConvert(string: /object.sent_at, fromFormat: "yyyy-MM-dd HH:mm:ss", "hh:mm a")
                        cell.lblTime.text = dteGet
                    }
                }
            }else{
                if /object.chat_type == "text"{
                    if let cell = cell as? chatLeftTVC {
                        cell.lblChatMsg.text = /object.text
                        let dteGet = Utility.dteGetConvert(string: /object.sent_at, fromFormat: "yyyy-MM-dd HH:mm:ss", "hh:mm a")
                        cell.lblTime.text = dteGet
                    }
                    
                }else{
                    if let cell = cell as? chatImageLeftCell {
                       // cell.imgeViewLeft.sd_setImage(with:URL(string:  /object.original), placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)//(with:URL(fileURLWithPath: "\(APIConstants.basePath)/" + "images/" + /self?.uploadDta?.original) as URL, placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
                        
                        let dteGet = Utility.dteGetConvert(string: /object.sent_at, fromFormat: "yyyy-MM-dd HH:mm:ss", "hh:mm a")
                        cell.lblTime.text = dteGet
                        
                    }
                    
                }
            }
            
        }
        
        dataSource = TableViewDataSource(items: chatDta, tableView: tblView, cellIdentifier: "chatRightTVC", cellHeight: UITableViewAutomaticDimension, configureCellBlock: nil, aRowSelectedListener: nil, scrollViewScroll: nil, willDisplayCell: nil)
        
        
        dataSource?.identifier1 = ({ indexPath in
            switch indexPath?.row {
            case 0:
                //                return "chatTimeHeaderTVC"
                let obj = self.chatDta?[indexPath?.row ?? 0]
                if /obj?.chat_type == "text"{
                    if /obj?.send_by == /UserSingleton.shared.loggedInUser?.data?.user_id{
                        return "chatRightTVC"
                    }else{
                        return "chatLeftTVC"
                    }
                }else{
                    if /obj?.send_by == /UserSingleton.shared.loggedInUser?.data?.user_id{
                        return "chatImgeRightTVC"
                    }else{
                        return "chatImageLeftCell"
                    }
                }
            default:
                let obj = self.chatDta?[indexPath?.row ?? 0]
                if /obj?.chat_type == "text"{
                    if /obj?.send_by == /UserSingleton.shared.loggedInUser?.data?.user_id{
                        return "chatRightTVC"
                    }else{
                        return "chatLeftTVC"
                    }
                }else{
                    if /obj?.send_by == /UserSingleton.shared.loggedInUser?.data?.user_id{
                        return "chatImgeRightTVC"
                    }else{
                        return "chatImageLeftCell"
                    }
                }
                
                
            }
        })
        
        let viewForHeader : ViewForHeaderInSection = { [weak self] (section) in
            
            let header = R.nib.timeLeftXIB.instantiate(withOwner: nil)[0] as? TimeLeftView
            header?.frame = CGRect(x: 0, y: 0, width: 60, height: 24)
            header?.center.x = (self?.view.center.x)!
            self?.lblTimeElapsed  = header?.lblTimeLeft
            self?.viewTimeElapsed = header?.timeLeftView
            
            let dteGet = Utility.dteGetConvert(string: /self?.chatDta?[0].sent_at, fromFormat: "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd")
            self?.lblTimeElapsed?.text = dteGet
            
            return header
            
        }
        //        dataSource = TableViewDataSourceWithHeader(items: chatDta, tableView: tblView, cellIdentifier: "chatRightTVC", cellHeight: nil, headerHeight: nil, configureCellBlock: configureCellBlock, viewForHeader: viewForHeader, viewForFooter: nil, aRowSelectedListener: nil, willDisplayCell: nil, scrollView: nil)
        
        dataSource?.configureCellBlock = configureCellBlock
        //        dataSource.ViewForHeaderInSection = view
        tblView.delegate = dataSource
        tblView.dataSource = dataSource
        tblView.reloadData()
    }
    func timeString(time:TimeInterval) -> String {
        // let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        // return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i", minutes, seconds)
    }
}

//MARK:- API HIT FUNCTION
extension ChatVC{
    func emitRecieve(){
        SocketIOManager.shared.getChatsMessage { [weak self] (data) in
            guard let self = self, let data = data else { return }
            self.chatDta?.append(data)
            self.updateChat(self.chatDta ?? [])
        }
    }
    
    
    @objc func getChatDta(){
        APIManager.shared.request(with: CommonEndPoint.pssChatListing("1", "\(otherUserId)", 10000, 0,/orderId)) { [weak self] (response) in
            switch response{
            case .success(let responseValue):
                debugPrint("")
                self?.chatDta = responseValue as? [Chat]
                self?.updateChat(self?.chatDta ?? [])
                
            case .failure(let err):
                print(err ?? "Error")
            }
        }
    }
    
}



//MARK: - Keyboard observers
extension ChatVC {
    
    @objc func keyboardDidShowNotification(_ notification: Notification) {
        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: { [weak self] in
              //  self?.tblView.contentInset.bottom = frame.height
                self?.txtInputBottomConstraint.constant = frame.height
                self?.view.layoutIfNeeded()
                }, completion: { [weak self] bool in
                    if bool {
                       // self?.tblView.scrollTo(direction: .Bottom, animated: true)
                        self?.scrollToBottom()
                    }
            })
            
            
        }
    }
    
    @objc func keyboardWillHideNotification(_ notification: Notification) {
        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: { [weak self] in
                self?.tblView.contentInset.bottom = 0.0
                self?.txtInputBottomConstraint.constant = 0.0 // frame.height
                self?.view.layoutIfNeeded()
                }, completion: nil)
        }
    }
    
}
