 //
//  ChatListingVC.swift
//  Buraq24
//
//  Created by Apple on 07/08/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit
//import SDWebImage  // commect this line sd_setImage
class ChatListingVC: UIViewController {
    
    var getChatUser : [UserList]?
    //MARK:- VARIABLES
    var dataSource : TableViewDataSource?
    //MARK:- OUTLETS
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getChatDta()
        configureTableView()
        btnBack.addTarget(self, action: #selector(btnBackAct(_:)), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

}
 //MARK:- TABLEVIEW DELEGATE FUNCTION
 extension ChatListingVC{
    func configureTableView() {
        dataSource =  TableViewDataSource.init(items: getChatUser, tableView: tblView, cellIdentifier: R.reuseIdentifier.tableListingTVC.description, cellHeight: UITableViewAutomaticDimension, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            guard let object = self?.getChatUser?[indexPath.row] else  {return}
            
            if let cell = cell as? TableListingTVC
            {
                let firstName = "\(/object.name)".components(separatedBy: " ").first
                cell.lblName.attributedText = Utility.sendAttString([R.font.montRegular(size: 14)!,R.font.montRegular(size: 12)!], colors: [UIColor.black,UIColor.lightGray], texts: ["\(/firstName)\n",/object.text], align: .left)
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let cDate = formatter.date(from: /object.sent_at)
                if cDate != nil{
                let time = SharedUtil.timeElapsedFromDate(date: cDate!)
                cell.lblTime.font = R.font.montRegular(size: 12)!
                cell.lblTime.text = time
                }
                
                cell.imgProfile.layer.cornerRadius = 24
                if let profilePic = object.profile_pic {
                    if let url = URL(string: profilePic) {
                        let urlProfile = URL(string: "\(APIConstants.socketBasePath)/" + "images/" + /profilePic)
                       // cell.imgProfile.sd_setImage(with: urlProfile , completed: nil)
                    }else{
                        cell.imgProfile.image = R.image.ic_profie_bg_ph()
                    }
                }
            }
            
            }, aRowSelectedListener: { (indexPath, cell) in
                debugPrint(indexPath)
                
                if let cell = cell as? TableListingTVC {
                    guard let object = self.getChatUser?[indexPath.row] else  {return}
                    let firstName = "\(/object.name)".components(separatedBy: " ").first
                    cell.setSelected(false, animated: true)
                    guard let editVC = R.storyboard.main.chatVC() else{return}
                    editVC.otherUserId = /object.oppositionId
                    editVC.profilePic = /object.profile_pic
                    editVC.name = /firstName// /object.name
                    self.navigationController?.pushViewController(editVC, animated: true)
                }
        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tblView.delegate = dataSource
        tblView.dataSource = dataSource
       
        tblView.reloadData()
    }
 }
 //MARK:- API HIT
 extension ChatListingVC{
    func getChatDta(){
        
        let token = /UserSingleton.shared.loggedInUser?.data?.accessToken
       
        APIManager.shared.request(with: CommonEndPoint.getChatList("1", 10, 0)) {[weak self] (response) in
            
            
            switch response{
            case .success(let responseValue):
                debugPrint("")
                self?.getChatUser = responseValue as? [UserList]
                //                self?.chatDta = responseValue as? [Chat]
                self?.configureTableView()//
            case .failure(let err):
                print("yes")
            }
        }
        
    }
 }
 //MARK:- EXTERNAL FUNCTION
 extension ChatListingVC{
    @objc func btnBackAct(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
 }
