//
//  RoadPickUpController.swift
//  Buraq24Driver
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class RoadPickUpController: UIViewController {
    @IBOutlet weak var lblSelectRoad: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSelectRoad.text = "SelectRoad".localized
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:-IBActions.
    //MARK:-Action when user want to start ride with Phone Number.
    @IBAction func continueWithNumberTapped(_ sender: UIButton) {
        guard let vc = R.storyboard.main.raodPickupPhoneNumberController() else {return}
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    //MARK:-Action when user want to start ride after scanning QR Code.
    @IBAction func scanQRCodeTapped(_ sender: UIButton) {
        
        guard let vc = R.storyboard.main.roadPickupQRCodeController() else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:-Action to dismiss Controller
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension RoadPickUpController : RaodPickupPhoneNumberControllerDelegate {
    func hideAndShowPopUp(name: String, phoneCode: String, phoneNumber: String) {
        guard let vc = R.storyboard.main.roadPickaUpLoactionController() else {return}
        vc.name = name
        vc.phoneCode = phoneCode
        vc.phoneNumber = phoneNumber
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
