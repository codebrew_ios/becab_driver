//
//  RoadPickupQRCodeController.swift
//  Buraq24Driver
//
//  Created by Apple on 29/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class RoadPickupQRCodeController: UIViewController {
    
    //MARK:-IBOutlets.
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    @IBOutlet weak var lblScan: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicatorView.startAnimating()
        self.setQRCodeImage()
        self.lblScan.text = "scan".localized
    }
    
    func setQRCodeImage() {
        qrCodeImageView.kf.setImage(with:  URL.init(string: "\(APIConstants.imagesBasePath)\(/DataResponse.driverData?.data?.qrCode)"), placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorView.isHidden = true
        }
        qrCodeImageView.kf.setImage(with: URL.init(string: "\(APIConstants.imagesBasePath)\(/DataResponse.driverData?.data?.qrCode)"), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    //MARK:-IBActions.
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
