//
//  RoadPickaUpLoactionController.swift
//  Buraq24Driver
//
//  Created by Apple on 04/12/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import GooglePlaces
import CoreLocation


enum LocationType {
    case pickUp
    case drop
    case firstStop
    case secondStop
    case thirdStop
    case fourthStop
    
}
protocol RoadPickaUpLoactionDelegate: class {
    func getOrderData()
}
class RoadPickaUpLoactionController: UIViewController {
    
    
    //MARK:-Variables.
    weak var delegate:RoadPickaUpLoactionDelegate?
    var order:Order?
    var name:String = ""
    var phoneCode:String = ""
    var phoneNumber:String = ""
    var pickupAddress:Address?
    var dropAddress:Address?
    var firstStopAddress:Address?
    var secondStopAddress:Address?
    var thirdStopAddress:Address?
    var fourthStopAddress:Address?
    var locationType:LocationType = .pickUp
    var isComingFromAddStops:Bool = false
    var firstViewIsHide = true
    var secondViewIsHide = true
    var thirdViewIsHide = true
    var fourthViewIsHide = true
    var firstViewIsNew = true
    var secondViewIsNew = true
    var thirdViewIsNew = true
    var fourthViewIsNew = true
    
    //MARK:-IBOutlets.
    @IBOutlet weak var firstCrossButton: UIButton!
    @IBOutlet weak var firstStopButton: UIButton!
    
    @IBOutlet weak var secondCrossButton: UIButton!
    @IBOutlet weak var secondStopButton: UIButton!
    
    @IBOutlet weak var thirdCrossButton: UIButton!
    @IBOutlet weak var thirdStopButton: UIButton!
    
    @IBOutlet weak var fourthCrossButton: UIButton!
    @IBOutlet weak var fourthStopButton: UIButton!
    
    @IBOutlet weak var dropOffButton: UIButton!
    @IBOutlet weak var pickupButton: UIButton!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var addNextStopButton: UIButton!
    @IBOutlet weak var pickupLocationTextFiled: AnimatableTextField!
    @IBOutlet weak var dropLocationTextField: AnimatableTextField!
    @IBOutlet weak var firstStopIconHeight: NSLayoutConstraint!
    @IBOutlet weak var firstStopTextField: AnimatableTextField!
    @IBOutlet weak var firstStopViewHeifht: NSLayoutConstraint!
    @IBOutlet weak var secondStopIconHeight: NSLayoutConstraint!
    @IBOutlet weak var secondStopTextField: AnimatableTextField!
    @IBOutlet weak var secondStopViewHeight: NSLayoutConstraint!
    @IBOutlet weak var thirdStopIconHeight: NSLayoutConstraint!
    @IBOutlet weak var thirdStopTextField: AnimatableTextField!
    @IBOutlet weak var thirdStopViewHeifht: NSLayoutConstraint!
    @IBOutlet weak var fourthStopIconHeight: NSLayoutConstraint!
    @IBOutlet weak var fourthStopTextField: AnimatableTextField!
    @IBOutlet weak var fourthStopViewHeifht: NSLayoutConstraint!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isComingFromAddStops {
            self.setUI()
        }else{
          self.currentLocation()
        }
        // Do any additional setup after loading the view.
    }
    
    func currentLocation() {
        LocationManagers.shared.start()
        LocationManagers.shared.success = { address in
            self.pickupAddress = address
            self.pickupLocationTextFiled.text = self.pickupAddress?.formatted
        }
        LocationManagers.shared.failure = { meesage in
        }
    }
    
    
    func showAddButton() {
        if isComingFromAddStops {
            self.addNextStopButton.isHidden = false
            if firstViewIsHide == false && secondViewIsHide == false && thirdViewIsHide == false && fourthViewIsHide == false {
                self.addNextStopButton.isHidden = true
            }else{
                self.addNextStopButton.isHidden = false
            }
        }else{
            self.addNextStopButton.isHidden = false
            self.messageView.isHidden = true
            if firstViewIsHide == false && secondViewIsHide == false {
                self.addNextStopButton.isHidden = true
                self.messageView.isHidden = false
            }else if firstViewIsHide == false || secondViewIsHide == false {
                self.messageView.isHidden = false
            }
        }
    }
    
    //
    func presentLocationPicker() {
        let country = UserDefaults.standard.value(forKey: "current_contry") as? String
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        //filter.type = .address
        //filter.country = country
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK:-IBActions.
    @IBAction func firstStopAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.firstViewIsHide = false
        locationType = .firstStop
        self.presentLocationPicker()
    }
    
    @IBAction func secondStopAction(_ sender: UIButton) {
        self.view.endEditing(true)
        locationType = .secondStop
        self.presentLocationPicker()
    }
    @IBAction func thirdStopAction(_ sender: UIButton) {
        self.view.endEditing(true)
        locationType = .thirdStop
        self.presentLocationPicker()
    }
    
    @IBAction func fourthStopAction(_ sender: UIButton) {
        self.view.endEditing(true)
        locationType = .fourthStop
        self.presentLocationPicker()
    }
    
    @IBAction func firstStopCrossAction(_ sender: UIButton) {
        firstStopTextField.text = ""
        firstStopAddress = Address()
        addNextStopButton.tag = 0
        self.firstViewIsHide = true
        UIView.animate(withDuration: 0.2, animations: {
            self.firstStopIconHeight.constant = 0
            self.firstStopViewHeifht.constant = self.firstStopViewHeifht.constant - 50
        }) { (true) in
            self.showAddButton()
        }
    }
    
    @IBAction func secondStopCrossAction(_ sender: UIButton) {
        addNextStopButton.tag = 1
        secondStopTextField.text = ""
        secondStopAddress = Address()
        self.secondViewIsHide = true
        UIView.animate(withDuration: 0.2, animations: {
            self.secondStopIconHeight.constant = 0
            self.secondStopViewHeight.constant = self.secondStopViewHeight.constant - 50
        }) { (true) in
            self.showAddButton()
        }
    }
    
    @IBAction func thirdStopCrossAction(_ sender: UIButton) {
        thirdStopTextField.text = ""
        thirdStopAddress = Address()
        addNextStopButton.tag = 0
        self.thirdViewIsHide = true
        UIView.animate(withDuration: 0.2, animations: {
            self.thirdStopIconHeight.constant = 0
            self.thirdStopViewHeifht.constant = self.thirdStopViewHeifht.constant - 50
        }) { (true) in
            self.showAddButton()
        }
    }
    @IBAction func fourthStopCrossAction(_ sender: UIButton) {
        fourthStopTextField.text = ""
        fourthStopAddress = Address()
        addNextStopButton.tag = 0
        self.fourthViewIsHide = true
        UIView.animate(withDuration: 0.2, animations: {
            self.fourthStopIconHeight.constant = 0
            self.fourthStopViewHeifht.constant = self.fourthStopViewHeifht.constant - 50
        }) { (true) in
            self.showAddButton()
        }
    }
    @IBAction func dropLocationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        locationType = .drop
        self.presentLocationPicker()
    }
    
    @IBAction func addNextStopButton(_ sender: UIButton) {
        if isComingFromAddStops {
            if firstViewIsHide == true {
                firstViewIsHide = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.firstStopIconHeight.constant = 13
                    self.firstStopViewHeifht.constant = self.firstStopViewHeifht.constant + 50
                }) { (true) in
                    print("")
                }
            }else if secondViewIsHide == true {
                secondViewIsHide = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.secondStopIconHeight.constant = 13
                    self.secondStopViewHeight.constant = self.secondStopViewHeight.constant + 50
                }) { (true) in
                    print("")
                }
            }else if thirdViewIsHide == true {
                thirdViewIsHide = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.thirdStopIconHeight.constant = 13
                    self.thirdStopViewHeifht.constant = self.thirdStopViewHeifht.constant + 50
                }) { (true) in
                    print("")
                }
            }else if fourthViewIsHide == true {
                fourthViewIsHide = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.fourthStopIconHeight.constant = 13
                    self.fourthStopViewHeifht.constant = self.fourthStopViewHeifht.constant + 50
                }) { (true) in
                    print("")
                }
            }
        }else{
            if addNextStopButton.tag == 0 {
                addNextStopButton.tag = 1
                firstViewIsHide = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.firstStopIconHeight.constant = 13
                    self.firstStopViewHeifht.constant = self.firstStopViewHeifht.constant + 50
                }) { (true) in
                    print("")
                }
            }else if addNextStopButton.tag == 1 {
                secondViewIsHide = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.secondStopIconHeight.constant = 13
                    self.secondStopViewHeight.constant = self.secondStopViewHeight.constant + 50
                }) { (true) in
                    print("")
                }
            }
        }
        self.showAddButton()
    }
    @IBAction func pickUpLocationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let country = UserDefaults.standard.value(forKey: "current_contry") as? String
        locationType = .pickUp
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        // filter.type = .establishment
        filter.type = .address
        filter.country = country
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //DataResponse.driverData?.products?.first?.category_brand_product_id
    @IBAction func startRideAction(_ sender: UIButton) {
        if isComingFromAddStops {
            var stops:[[String:Any]] = [[:]]
            stops.removeAll()
            if firstViewIsHide == false && firstViewIsNew == true {
                let firstStopDict : [String : Any] = ["latitude":firstStopAddress?.lat ?? 0.0 ,"longitude":firstStopAddress?.long ?? 0.0,"priority":firstStopTextField.tag,"address":firstStopAddress?.formatted ?? ""]
                stops.append(firstStopDict)
            }
            if secondViewIsHide == false && secondViewIsNew == true {
                let secondStopDict : [String : Any] = ["latitude":secondStopAddress?.lat ?? 0.0 ,"longitude":secondStopAddress?.long ?? 0.0,"priority":secondStopTextField.tag,"address":secondStopAddress?.formatted ?? ""]
                stops.append(secondStopDict)
            }
            if thirdViewIsHide == false && thirdViewIsNew == true {
                let thirdStopDict : [String : Any] = ["latitude":thirdStopAddress?.lat ?? 0.0 ,"longitude":thirdStopAddress?.long ?? 0.0,"priority":thirdStopTextField.tag,"address":thirdStopAddress?.formatted ?? ""]
                stops.append(thirdStopDict)
            }
            if fourthViewIsHide == false && fourthViewIsNew == true {
                let fourthStopDict : [String : Any] = ["latitude":fourthStopAddress?.lat ?? 0.0 ,"longitude":fourthStopAddress?.long ?? 0.0,"priority":fourthStopTextField.tag,"address":fourthStopAddress?.formatted ?? ""]
                stops.append(fourthStopDict)
            }
            
            if isFormValid() {
                APIManager.shared.request(with: ServicesEndPoint.addStops(orderId: order?.order_id ?? 0, stops: toJSONString(data:stops))) { (response) in
                    
                    switch response {
                        
                    case .success( _ ):
                        self.delegate?.getOrderData()
                        self.navigationController?.popViewController(animated: true)
                    case .failure( let str):
                        Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
                    }
                }
            }
            
        }else{
            let coordinate₀ = CLLocation(latitude: pickupAddress?.lat ?? 0, longitude: pickupAddress?.long ?? 0)
            let coordinate₁ = CLLocation(latitude: dropAddress?.lat ?? 0, longitude: dropAddress?.long ?? 0 )
            let distanceInMeters = (coordinate₀.distance(from: coordinate₁))/1000
            var stops:[[String:Any]] = [[:]]
            stops.removeAll()
            if firstViewIsHide == false {
                let firstStopDict : [String : Any] = ["latitude":firstStopAddress?.lat ?? 0.0 ,"longitude":firstStopAddress?.long ?? 0.0,"priority":firstStopTextField.tag,"address":firstStopAddress?.formatted ?? ""]
                stops.append(firstStopDict)
            }
            if secondViewIsHide == false {
                let secondStopDict : [String : Any] = ["latitude":secondStopAddress?.lat ?? 0.0 ,"longitude":secondStopAddress?.long ?? 0.0,"priority":secondStopTextField.tag,"address":secondStopAddress?.formatted ?? ""]
                stops.append(secondStopDict)
            }
            //, Stops: stops
            if isFormValid() {
                APIManager.shared.request(with: ServicesEndPoint.requestApi(categoryId: DataResponse.driverData?.data?.category_id, categoryBrandId: DataResponse.driverData?.data?.category_brand_id, categoryBrandProductId: UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int ?? 0, productQuantity: 1, dropOffAddress: dropAddress?.formatAddress(), dropOffLatitude: dropAddress?.lat, dropOffLongitude: dropAddress?.long, pickupAddress: pickupAddress?.formatAddress(), pickupLatitude: pickupAddress?.lat, pickupLongitude: pickupAddress?.long, orderTimings: "", future: "0", paymentType: "Cash", distance: 50000, couponId: "", organisationCouponUserId: 0, productWeight: 0.0, productDetail: "", orderDistance: Float(distanceInMeters), name: name, phone_number: phoneNumber, phoneCode: phoneCode, bookingType: "RoadPickupPhone", transactionId: "", timezone: TimeZone.current.abbreviation() ?? "", finalCharge: "0", materialDetails: "", details: "", Stops: toJSONString(data: stops))) { (response) in
                    
                    switch response {
                        
                    case .success( _ ):
                        var controllers = self.navigationController?.viewControllers
                        controllers?.removeLast(2)
                        self.navigationController?.setViewControllers(controllers!, animated: true)
                        
                    case .failure( let str):
                        Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
                    }
                }
            }
        }
    }
    
    func isFormValid() -> Bool {
        var message = ""
        if dropLocationTextField.text?.trim() == "" {
            message = "Please enter Drop Location"
        }else if pickupLocationTextFiled.text?.trim() == ""{
            message = "Please enter PickUp Location"
        }else if firstViewIsHide == false && (firstStopTextField.text?.trim() == "") {
            message = "Please enter Stop Location"
        }else if secondViewIsHide == false && (secondStopTextField.text?.trim() == "") {
            message = "Please enter Stop Location"
        } else if thirdViewIsHide == false && (thirdStopTextField.text?.trim() == "") {
            message = "Please enter Stop Location"
        }else if fourthViewIsHide == false && (fourthStopTextField.text?.trim() == "") {
            message = "Please enter Stop Location"
        }
        if message != "" {
            Alerts.shared.showOnTop(alert:  R.string.localizable.alertAlert(), message: message , type: .info)
            return false
        }else{
            return true
        }
    }
    
    func setUI() {
        pickupButton.isUserInteractionEnabled = false
        let pickupAddresss = Address()
        pickupAddresss.formatted = order?.pickup_address
        pickupAddresss.lat = order?.pickup_latitude
        pickupAddresss.long = order?.pickup_longitude
        pickupLocationTextFiled.text = order?.pickup_address
        self.pickupAddress = pickupAddresss
        
        dropOffButton.isUserInteractionEnabled = false
        let dropAddresss = Address()
        dropAddresss.formatted = order?.dropoff_address
        dropAddresss.lat = order?.dropoff_latitude
        dropAddresss.long = order?.dropoff_longitude
        dropLocationTextField.text = order?.dropoff_address
        self.dropAddress = dropAddresss
        
        if let values = self.order?.roadStops, values.count >= 1 {
            for (_,data) in values.enumerated() {
                let priority = Priority(rawValue: data.priority ?? 0)
                let added_with_ride = data.added_with_ride
                let address = Address()
                address.formatted = data.address
                address.lat = data.latitude
                address.long = data.longitude
                switch priority {
                case .first:
                    if added_with_ride == "1" {
                    firstViewIsNew = false
                    }
                    firstStopButton.isUserInteractionEnabled = false
                    firstCrossButton.isUserInteractionEnabled = false
                    firstViewIsHide = false
                    UIView.animate(withDuration: 0.2, animations: {
                        self.firstStopIconHeight.constant = 13
                        self.firstStopViewHeifht.constant = self.firstStopViewHeifht.constant + 50
                    }) { (true) in
                        print("")
                    }
                    firstStopAddress = address
                    firstStopTextField.text = data.address
                case .second:
                    if added_with_ride == "1" {
                        secondViewIsNew = false
                    }
                    secondStopButton.isUserInteractionEnabled = false
                    secondCrossButton.isUserInteractionEnabled = false
                    secondViewIsHide = false
                    UIView.animate(withDuration: 0.2, animations: {
                        self.secondStopIconHeight.constant = 13
                        self.secondStopViewHeight.constant = self.secondStopViewHeight.constant + 50
                    }) { (true) in
                        print("")
                    }
                    secondStopAddress = address
                    secondStopTextField.text = data.address
                case .third:
                    if added_with_ride == "1" {
                        thirdViewIsNew = false
                    }
                    thirdStopButton.isUserInteractionEnabled = false
                    thirdCrossButton.isUserInteractionEnabled = false
                    thirdViewIsHide = false
                    UIView.animate(withDuration: 0.2, animations: {
                        self.thirdStopIconHeight.constant = 13
                        self.thirdStopViewHeifht.constant = self.thirdStopViewHeifht.constant + 50
                    }) { (true) in
                        print("")
                    }
                    thirdStopAddress = address
                    thirdStopTextField.text = data.address
                case .fourth:
                    if added_with_ride == "1"{
                        fourthViewIsNew = false
                    }
                    fourthStopButton.isUserInteractionEnabled = false
                    fourthCrossButton.isUserInteractionEnabled = false
                    fourthViewIsHide = false
                    UIView.animate(withDuration: 0.2, animations: {
                        self.fourthStopIconHeight.constant = 13
                        self.fourthStopViewHeifht.constant = self.fourthStopViewHeifht.constant + 50
                    }) { (true) in
                        print("")
                    }
                    fourthStopAddress = address
                    fourthStopTextField.text = data.address
                default :
                    return
                    
                }
            }
        }
    }
    
    
    func toJSONString(data: [[String:Any]]?,options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = data,
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
    
}

enum Priority:Int {
    case first = 1
    case second = 2
    case third = 3
    case fourth = 4
}


extension RoadPickaUpLoactionController:GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if locationType == .pickUp {
            pickupAddress = Address(gmsPlace: place)
            pickupLocationTextFiled.text = place.formattedAddress
        }else if locationType == .drop {
            dropAddress = Address(gmsPlace: place)
            dropLocationTextField.text = place.formattedAddress
        }else if locationType == .firstStop {
            firstStopAddress = Address(gmsPlace: place)
            firstStopTextField.text = place.formattedAddress
        }else if locationType == .secondStop {
            secondStopAddress = Address(gmsPlace: place)
            secondStopTextField.text = place.formattedAddress
        }else if locationType == .thirdStop {
            thirdStopAddress = Address(gmsPlace: place)
            thirdStopTextField.text = place.formattedAddress
        }else if locationType == .fourthStop {
            fourthStopAddress = Address(gmsPlace: place)
            fourthStopTextField.text = place.formattedAddress
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

