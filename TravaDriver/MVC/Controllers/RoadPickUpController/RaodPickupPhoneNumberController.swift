//
//  RaodPickupPhoneNumberController.swift
//  Buraq24Driver
//
//  Created by Apple on 29/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
protocol RaodPickupPhoneNumberControllerDelegate {
    func hideAndShowPopUp(name:String,phoneCode:String,phoneNumber:String)
}

class RaodPickupPhoneNumberController: UIViewController {
    
    
    var delegate:RaodPickupPhoneNumberControllerDelegate?
    //MARK:-IBOutlets.
    @IBOutlet weak var countryCodeTextField: AnimatableTextField!
    @IBOutlet weak var phoneNumberTextField: AnimatableTextField!
    @IBOutlet weak var nameTextField: AnimatableTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.isOpaque = false
    }
    
    //MARK:-Custom Functions.
    func isFormValid() -> Bool{
        var message = ""
        if nameTextField.text?.trim() == "" {
            message = AlertMessage.emptyName.rawValue
        }else if phoneNumberTextField.text?.trim() == "" {
            message = AlertMessage.emptyPhoneNumber.rawValue
        }else {
            let valid = validateLoginData()
            switch valid {
            case .failure(_, _):
                message = "alert.phoneNumber".localized
            default:
                message == ""
            }
        }
        
        if message != "" {
            Alerts.shared.showOnTop(alert:  R.string.localizable.alertAlert(), message: message , type: .info)
            return false
        }else{
            return true
        }
    }
    func validateLoginData() -> Valid {
        // remove spaces or newline
        return Validation.shared.isValid(phoneNum: phoneNumberTextField.text?.trim())
    }
    //MARK:-IBActions.
    @IBAction func startRideTapped(_ sender: UIButton) {
        if isFormValid() {
            self.delegate?.hideAndShowPopUp(name: nameTextField.text?.trim() ?? "", phoneCode: countryCodeTextField.text?.trim() ?? "", phoneNumber:  phoneNumberTextField.text?.trim() ?? "")
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    @IBAction func countryCodeTapped(_ sender: UIButton) {
        guard let countryPicker = R.storyboard.loginSignUp.countryCodeSearchViewController() else{return}
        countryPicker.delegate = self
        self.present(countryPicker, animated: true, completion: nil)
    }
    
    @IBAction func dismissControllerAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: - Country Picker Delegates
extension RaodPickupPhoneNumberController: CountryCodeSearchDelegate{
    func didTap(onCode detail: [AnyHashable : Any]!) {
        //   imgViewCountry.image = UIImage(named:/(detail["code"] as? String)?.lowercased())
        countryCodeTextField.text = /(detail["dial_code"] as? String)
    }
    
    func didSuccessOnOtpVerification(){
        
    }
}

