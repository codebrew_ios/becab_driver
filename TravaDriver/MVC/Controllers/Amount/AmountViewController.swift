//
//  AmountViewController.swift
//  TravaDriver
//
//  Created by Work on 08/03/21.
//  Copyright © 2021 OSX. All rights reserved.
//

import UIKit

class AmountViewController: UIViewController {

    @IBOutlet weak var amountField: UITextField!
    
    var completion:((Double) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func btn(_ sender: Any) {
        guard let amt = amountField.text, let amount = Double(amt), amount > 0 else{
            Alerts.shared.show(alert: "Becab", message: "please enter amount".localized, type: .error)
            return
        }
        completion?(amount)
        dismiss(animated: true)
    }
    
}
