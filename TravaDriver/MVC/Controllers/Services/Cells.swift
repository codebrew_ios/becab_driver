//
//  CategoryCell.swift
//  TravaDriver
//
//  Created by Work on 18/01/21.
//  Copyright © 2021 OSX. All rights reserved.
//

import Foundation

var servicesTableView:UITableView?

class SelfSizedTableView: UITableView {
  var maxHeight: CGFloat = UIScreen.main.bounds.size.height
  
  override func reloadData() {
    super.reloadData()
    self.invalidateIntrinsicContentSize()
    self.layoutIfNeeded()
  }
  
  override var intrinsicContentSize: CGSize {
    let height = min(contentSize.height, maxHeight)
    return CGSize(width: contentSize.width, height: height)
  }
}

class CategoryCell:UITableViewCell{
    
    @IBOutlet weak var brandTableView: SelfSizedTableView!{
        didSet{
            brandTableView.delegate = self
            brandTableView.dataSource = self
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    var category:Services?{
        didSet{
            nameLabel.text = category?.name
            if #available(iOS 13.0, *) {
                btn.setImage(UIImage(systemName: (category?.isOpen ?? false) ? "chevron.up" : "chevron.down")!, for: .normal)
            }
            brandTableView.reloadData()
        }
    }
    
}

extension CategoryCell:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (category?.isOpen ?? false) ? category?.brands?.count ?? 0 : 0
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandCell", for: indexPath) as!  BrandCell
        cell.brand = category?.brands?[indexPath.row]
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(chevronAction(btn:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let brand = category?.brands?[indexPath.row]
        var height:CGFloat = 44
        if brand?.isOpen ?? false{
            brand?.products?.forEach({ product in
                height += (8 + 8 + 30)
            })
        }
        return height
    }
    
    @objc func chevronAction(btn:UIButton){
        let brand = category?.brands?[btn.tag]
        brand?.isOpen = !(brand?.isOpen ?? true)
        if #available(iOS 13.0, *) {
            if brand?.isOpen ?? false{
                btn.setImage(UIImage(systemName: "chevron.up")!, for: .normal)
            }else{
                btn.setImage(UIImage(systemName: "chevron.down")!, for: .normal)
            }
        }
        servicesTableView?.reloadData()
    }
    
    
}

class BrandCell:UITableViewCell{
    
    @IBOutlet weak var tableView: SelfSizedTableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    var brand:Brands?{
        didSet{
            nameLabel.text = brand?.name
            if #available(iOS 13.0, *) {
                btn.setImage(UIImage(systemName: (brand?.isOpen ?? false) ? "chevron.up" : "chevron.down")!, for: .normal)
            }
            tableView.reloadData()
        }
    }
    
}

extension BrandCell:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (brand?.isOpen ?? false) ? brand?.products?.count ?? 0 : 0
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as!  ProductCell
        cell.product = brand?.products?[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        brand?.products?[indexPath.row].isSelected = !(brand?.products?[indexPath.row].isSelected ?? true)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        46
    }
    
}

class ProductCell:UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    
    var product:Products?{
        didSet{
            nameLabel.text = product?.name
            nameLabel.superview?.backgroundColor = (product?.isSelected ?? false) ? R.color.orange()?.withAlphaComponent(0.5) : UIColor.clear
        }
    }
}


extension UITableViewCell {
    var tbv: UITableView? {
        return parentView(of: UITableView.self)
    }
}

extension UIView {
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}
