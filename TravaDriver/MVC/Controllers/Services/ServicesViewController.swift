//
//  ServicesViewController.swift
//  TravaDriver
//
//  Created by Work on 18/01/21.
//  Copyright © 2021 OSX. All rights reserved.
//

import UIKit


class ServicesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        servicesTableView = self.tableView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "Select Services"
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor:UIColor.white
        ]
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = R.color.orange()
        navigationItem.setHidesBackButton(true, animated: false)
        
    }

    @IBAction func btnContinueAction(_ sender: Any) {
        addBankDetailsAndServices()
    }
}


extension ServicesViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataResponse.driverData?.services?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as!  CategoryCell
        let category = DataResponse.driverData?.services?[indexPath.row]
        cell.category = category
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(chevronAction(btn:)), for: .touchUpInside)
        return cell
    }
    
    
    @objc func chevronAction(btn:UIButton){
        let service = DataResponse.driverData?.services?[btn.tag]
        service?.isOpen = !(service?.isOpen ?? true)
        if #available(iOS 13.0, *) {
            if service?.isOpen ?? false{
                btn.setImage(UIImage(systemName: "chevron.up")!, for: .normal)
            }else{
                btn.setImage(UIImage(systemName: "chevron.down")!, for: .normal)
            }
        }
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let category = DataResponse.driverData?.services?[indexPath.row]
        var height:CGFloat = 44
        if category?.isOpen ?? false{
            category?.brands?.forEach({ brand in
                height += 44
                if brand.isOpen{
                    brand.products?.forEach({ product in
                        height += 8 + 8 + 30
                    })
                }
            })
        }
        return height
    }

    
    func addBankDetailsAndServices(){
        
        var array = [[String:Any]]()
        
        DataResponse.driverData?.services?.forEach({ service in
            service.brands?.forEach({ brand in
                var product_ids = [Int]()
                brand.products?.forEach({ product in
                    if product.isSelected,let id = product.category_brand_product_id{
                        product_ids.append(id)
                    }
                    if let category_id = service.category_id,let brand_id = brand.category_brand_id,!product_ids.isEmpty{
                        let dictionary = [
                            "category_id":category_id,
                            "category_brand_id":brand_id,
                            "category_brand_product_id":product_ids
                        ] as [String:Any]
                        array.append(dictionary)
                    }
                    
                })
            })
        })
        
        
        guard !array.isEmpty else{
            Alerts.shared.show(alert: "Becab", message: "please select services", type: .error)
            return
        }
        
        
        let objR = LoginEndpoint.addServicesToOffer(serviceId: array.toJson(), categoryBrandId: 0, category_brand_product_ids:"")
        
        
        
        APIManager.shared.request(with: objR) {
            [weak self] (resp) in
            
            switch resp {
                
            case .success(let response_value ):
                
                let resp = response_value  as? DriverModel
//                self?.driverData = resp
                DriverDataAtOtp.data = resp
                UserDefaults.standard.set(resp?.products?.first?.category_brand_product_id, forKey: "category_brand_product_id")
                UserDefaults.standard.set(resp?.data?.online_status, forKey: "online_status")
                //MARK:- commented by heeba
               // UserDefaults.standard.set(self?.driverData?.data?.directional_hire, forKey: "directionalHire_status")
                //  self?.moveToUploadLicenseViewController()
                guard let vc = R.storyboard.main.mapViewController() else { return }
                self?.navigationController?.pushViewController(vc, animated: true)
                
            case .failure(let err):
                
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: err ?? "", type: .info)
            }
        }
    }
    
    
}


extension Array where Element : Any{
    
    
    func toJson() -> String {
        do {
            let data = self
            
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
            var string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
            string = string.replacingOccurrences(of: "\n", with: "") as NSString
            debugPrint(string)
            string = string.replacingOccurrences(of: "\\", with: "") as NSString
            debugPrint(string)
            //            string = string.replacingOccurrences(of: "\"", with: "") as NSString
           // string = string.replacingOccurrences(of: " ", with: "") as NSString
            debugPrint(string)
            return string as String
        }
        catch let error as NSError{
            debugPrint(error.description)
            return ""
        }
    }
    
    
    
    func indexOfObject(object : Any) -> NSInteger {
        return (self as NSArray).index(of: object)
    }
    
}

