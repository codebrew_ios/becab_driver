//
//  NTVerificationCodeViewController.swift
//  RoyoRide
//
//  Created by Ankush on 15/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit


class NTVerificationCodeViewController: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet var lblTimer: UILabel!

    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnResend: UIButton!
    
    @IBOutlet weak var stackViewDidntRecieveOtp: UIStackView!
    @IBOutlet var lblMobileNo: UILabel!
    
    @IBOutlet var textFieldCode: UITextField!
    
    @IBOutlet var stackView: UIStackView!
    
    //MARK:- PROPERTIES
    var driverData :DriverModel?
    var phoneCode : String?
    var phoneNumber :String?
    
    var signup_as: LoginSignupType?
    var social_key: String?
    
    var timer : Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    var totalSecondLeft = 120
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            
            self?.setUpUI()
            
        }
        
        textFieldCode.delegate = self
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //  addKeyBoardObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        textFieldCode.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // removeKeyBoardObserver()
    }
    
    func setUpUI() {
        
       /* if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
            
            stackView.semanticContentAttribute = .forceLeftToRight
         //   lblTopDesc.text = R.string.localizable.verifyEnterInfo() + "\n" + /new
            lblTopDesc.text = /new
        }
        else{
            new = /phoneNumber
           // lblTopDesc.text = R.string.localizable.verifyEnterInfo() + "\n" + /new
             lblTopDesc.text = /new
        } */
        
        
        lblMobileNo.text = "otp_text".localized + " " + /phoneCode + "-" + /phoneNumber
        
        stackView.semanticContentAttribute = .forceLeftToRight
        
//        btnResend.setButtonWithTitleColorBtnText()
        
//        btnNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
//        btnNext.setButtonWithTintColorBtnText()
    }
    
    //MARK:- FUNCTIONS
    
    func startTimer() {
        
        totalSecondLeft = 120
        registerBackgroundTask()
        self.timer?.invalidate()
        self.timer = nil
        lblTimer.text = totalSecondLeft.formattedTimer()
        stackViewDidntRecieveOtp.isHidden = true
        lblTimer.isHidden = false
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self , selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        
        if totalSecondLeft > 0 {
            totalSecondLeft = totalSecondLeft - 1
            lblTimer.text = totalSecondLeft.formattedTimer()
        }else {
            endBackgroundTask()
            
            btnResend.isUserInteractionEnabled = true
            self.timer?.invalidate()
            stackViewDidntRecieveOtp.isHidden = false
            lblTimer.isHidden = true
            
            clearTxtFields()
        }
    }
    
    func clearTxtFields() {
        self.view.endEditing(true)
        textFieldCode.text = ""
    }
    
    func registerBackgroundTask() {
        
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    // Move to servicesOffering Screen
    func moveToServicesOfferViewController() {
        
        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
        vc.driverData = self.driverData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Move to map Screen
    func moveToMapViewController() {
        
        let vc = R.storyboard.main.mapViewController()
                  let nav = UINavigationController(rootViewController: vc!)
                  let appDelegate = UIApplication.shared.delegate as! AppDelegate
                  appDelegate.window?.rootViewController = nav
    }
    
    // Move to license Screen
    func moveToUploadLicense() {
        
        guard let vc = R.storyboard.loginSignUp.uploadLicenseViewController() else {return}
        vc.driverData = self.driverData
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Move to SignUp Screen
    func moveToSignUpScreen() {
        
//        guard let formDetailArray = UserSingleton.shared.appSetting?.registration_forum?.form_details else { return }
        
//        let form = formDetailArray.filter({/$0.key_name == "reg_template"}).first
//        let isRequired = /form?.required
        
//        if /isRequired == "0" {
//            guard let vc = R.storyboard.loginSignUp.subscriptionsViewController() else{return}
//            self.navigationController?.pushViewController(vc, animated: true)
            guard let vc = R.storyboard.loginSignUp.registerationDetailsViewController() else {return}
            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
//            guard let vc = R.storyboard.registeration.singlePageRegisteration() else { return }
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
       
        
    }
    
}


//MARK:- ButtonSelector
extension NTVerificationCodeViewController {
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        // 1- Back, 2- Resend, 3- Next
        
        switch sender.tag {
        case 1:
            navigationController?.popViewController(animated: true)
            
        case 2:
            debugPrint("Resend")
            resendOtp()
            
        case 3:
            debugPrint("Next")
            (/textFieldCode.text).isEmpty ? Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "Validation.VerificationValidation".localized, type: .info )  : verifyOtp(otp: /textFieldCode.text)
            
        default:
            break
        }
    }
}

//MARK:- TextField Selector
extension NTVerificationCodeViewController {
    
    @IBAction func textFieldChanged(_ sender: Any) {
        
        if textFieldCode.text?.count == 4 {
            view.endEditing(true)
        }
        
    }
    
}



//MARK:- API
extension NTVerificationCodeViewController {
    
    func verifyOtp(otp:String) {
        
        self.view.endEditing(true)
         
        APIManager.shared.request(with: LoginEndpoint.verifyOtp(otp: otp)) {[weak self] (resp) in
             
             switch resp {
                 
             case .success(let responseValue ):
                
                 let resp = responseValue  as? DriverModel
                 
                 UserDefaults.standard.set(resp?.data?.accountStep, forKey: accountKey)
                 self?.driverData = resp
                 
                 UserDefaults.standard.set(self?.driverData?.products?.first?.category_brand_product_id, forKey: "category_brand_product_id")
                 UserDefaults.standard.set(self?.driverData?.data?.online_status, forKey: "online_status")
                 UserDefaults.standard.set(self?.driverData?.data?.directional_hire, forKey: "directionalHire_status")
                 UserDefaults.standard.set(self?.driverData?.data?.mulkiya_validity, forKey: "mulkiya_validity")
//                  UserDefaults.standard.set(self?.driverData?.data?.pool, forKey: "pool")
                      
                 DataResponse.driverData = resp
                 DataResponse.driverData?.services = DataResponse.driverData?.services?.filter{ $0.category_id == 7 || $0.category_id == 4 }
                 DriverDataAtOtp.data = resp
                 token.access_token = (/resp?.data?.accessToken)
                 UserDefaults.standard.set(self?.driverData?.data?.category_id,forKey: "driver_cat_id")
                 
                 
                 if resp?.data?.dataUser?.name == ""{

                     self?.moveToSignUpScreen()
                 } else {

                     if (UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int) != nil {

                        if (resp?.data?.approved == "0") {

                            Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertDriverBlocked() , actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                                self?.navigationController?.popViewController(animated: true)
                            }
                        } else {
                            self?.moveToMapViewController()

                        }
                     } else {

                         guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
                         let nav = UINavigationController(rootViewController: vc)
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.window?.rootViewController = nav
                     }
                 }
             case .failure(let err):
                 
                 Alerts.shared.showOnTop(alert: "alert.alert".localized, message: err ?? "", type: .info)
             }
         }
    }
    
    func resendOtp() {
        
        guard let accountType = signup_as else {return}
        
        //        var completeNumberArr:[String] = []
        var phoneCode = ""
        var number = ""
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
            
            let  completeNumberArr = (phoneNumber?.components(separatedBy: "-"))
            let reversed = completeNumberArr?.reversed()
            phoneCode = /reversed?.first
            number = /reversed?.last
            
        }
        else{
            
            let  completeNumberArr = phoneNumber?.components(separatedBy: "-")
            phoneCode = /completeNumberArr?.first
            number = /completeNumberArr?.last
        }
        
        APIManager.shared.request(with: LoginEndpoint.sendOtp(languageId: LanguageCode.English.get(), phoneCode: phoneCode, phone_number: number, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken, device_type: "Ios", social_key: social_key, signup_as: accountType.rawValue)) { [weak self](response) in
            
            switch response{
            case .success(let response_value ):
                let resp = response_value  as? OtpModel
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: resp?.message ?? "Otp sent sucessfully" , type: .info)
                
                //start otp expiring timer
                self?.startTimer()
                // add acess token for future reference also chnaged at otp verification
                token.access_token = (/resp?.data?.acessToken)
                
            case .failure( let str):
                Alerts.shared.showOnTop(alert: "alert.alert".localized, message: str ?? "", type: .info)
            }
        }
    }
}

extension NTVerificationCodeViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        return text.count <= 4
    }
}

