//
//  NTSignupViewController.swift
//  RoyoRide
//
//  Created by Ankush on 15/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class NTSignupViewController: UIViewController {
    
    @IBOutlet var viewPhone: UIView!
    
    
    @IBOutlet weak var stackViewUserEmail: UIStackView!
    @IBOutlet weak var stackViewPassword: UIStackView!
    @IBOutlet weak var stackViewConfirmPassword: UIStackView!
    
    @IBOutlet var txtFieldMobileNo: UITextField!
    @IBOutlet weak var textFieldUsernameEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    
    @IBOutlet weak var segmentPhoneEmail: UISegmentedControl!
    
    @IBOutlet weak var buttonNext: UIButton!
    
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblISOCode: UILabel!
    
    
    var otpData : OtpModel?
    var signup_as: LoginSignupType?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
}

extension NTSignupViewController {
    
    func initialSetup() {
        setupUI()
        setupData()
    }
    
    func setupUI() {
        
        stackViewUserEmail.isHidden = segmentPhoneEmail.selectedSegmentIndex == 0
        stackViewPassword.isHidden = segmentPhoneEmail.selectedSegmentIndex == 0
        stackViewConfirmPassword.isHidden = segmentPhoneEmail.selectedSegmentIndex == 0
        
//        segmentPhoneEmail.setTextColorBtnText()
        
//        buttonNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
//        buttonNext.setButtonWithTintColorBtnText()
        
        txtFieldMobileNo.delegate = self
    }
    
    
    func setupData() {
        
        switch segmentPhoneEmail.selectedSegmentIndex {
        case 0:
            lblCountryCode.text = "+57"
            lblISOCode.text = "COL"
            break
        default:
            break
        }
    }
    
    // Move to otp verification  screen (Map view Screen)
    func moveToOtpVerification() {
        
        guard let vc = R.storyboard.newTemplateLoginSignUp.ntVerificationCodeViewController() else {return}
        
        vc.phoneNumber = (/lblCountryCode.text) + "-" + (/txtFieldMobileNo.text)
        vc.signup_as = signup_as
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Button Selector
extension NTSignupViewController {
    
    @IBAction func segmentClicked(_ sender: UISegmentedControl) {
        
        setupUI()
        /* switch sender.selectedSegmentIndex {
         
         case 0:
         viewEmail.removeFromSuperview()
         viewPhone.isHidden = false
         constraintHeightContainerView.constant = 53.0
         
         case 1:
         viewPhone.isHidden = true
         constraintHeightContainerView.constant = 181.0
         view.layoutSubviews()
         
         viewEmail.frame = viewContainer.bounds
         viewContainer.addSubview(viewEmail)
         
         
         default:
         break
         } */
        
        
        
        
    }
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        // 0- Phone, 1- Email
        // 1- Back, 2- Terms, 3- Privacy, 4- Next, 5- Country code
        
        
        switch sender.tag {
        case 1:
            navigationController?.popViewController(animated: true)

        case 4:
            
            let code = /lblCountryCode.text
            let number = txtFieldMobileNo.text ?? ""
            
            switch segmentPhoneEmail.selectedSegmentIndex {
            case 0:
                
                
                if NewTemplateValidations.sharedInstance.validatePhoneNumber(phone: number) {
                    
                    self.sendOtp(code: code, number: number, signup_as: .PhoneNo)
                    
                }
                
            case 1:
                if NewTemplateValidations.sharedInstance.validateSignupUsernameAndPassword(usernameOrEmail: textFieldUsernameEmail.text ?? "", password: textFieldPassword.text ?? "", confirmPassword: textFieldConfirmPassword.text ?? "", phone: txtFieldMobileNo.text ?? "") {
                    
                    debugPrint("Success")
                    sendOtp(code: code, number: number, signup_as: .Email, email: textFieldUsernameEmail.text ?? "", password: textFieldPassword.text ?? "")
                }
                
            default:
                break
            }
            
        case 5:
            
            guard let countryPicker = R.storyboard.loginSignUp.countryCodeSearchViewController() else{return}
            countryPicker.delegate = self
            self.navigationController?.present(countryPicker, animated: true, completion: nil)
        case 2:
            openUrl(urlString: APIConstants.TermsConditions)
            
        case 3:
            openUrl(urlString: APIConstants.privacyUrl)
        default:
            break
        }
        
        
        
        
        
        
    }
    
}

//MARK: - Country Picker Delegates

extension NTSignupViewController: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        
        lblCountryCode.text = /(detail["dial_code"] as? String)
        lblISOCode.text = /(detail["code"] as? String)
    }
    
    func didSuccessOnOtpVerification() {
        
    }
    
    
}

//MARK:- API

extension NTSignupViewController {
    
    func sendOtp(code:String, number:String, signup_as: LoginSignupType?, email: String? = nil, password: String? = nil) {
        
        self.signup_as = signup_as
        guard let accountType = self.signup_as else {return}
        
        APIManager.shared.request(with: LoginEndpoint.sendOtp(languageId: LanguageCode.English.get(), phoneCode: code, phone_number: number, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken , device_type:"Ios", social_key: nil, signup_as: accountType.rawValue, email: email, password: password) ) {[weak self] (response) in
            
            switch response{
            case .success(let response_value ):
                
                let resp = response_value  as? OtpModel
                self?.otpData = resp
                
                // add acess token for future reference also chnaged at otp verification
                token.access_token = (/self?.otpData?.data?.acessToken)
                self?.checkUpdate()
                
            case .failure( let str):
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
            }
        }
    }
    
    func checkUpdate(){
        
        guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return
        }
        
        let force = /self.otpData?.data?.versionForce
        
        if force != currentVersion{
            if currentVersion.compare(force, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending {
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertUpdateApp(), actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
                    
                    if let url = URL(string: APIConstants.appStoreUrl),
                        UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.open(url, options: [:]) { (opened) in
                            if(opened){
                                print("App Store Opened")
                            }
                        }
                    } else {
                        Alerts.shared.showOnTop(alert: "Alert", message: "App store not available.", type: .info)
                    }
                }
                
                return
            }
        }
        
        // otherwise move to otp verfication
        self.moveToOtpVerification()
    }
    
}

//MARK:- UItextfield delegates

extension NTSignupViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            if text == "" && string == "0" {
                return false
            }
            let newLength = text.length + string.length - range.length
            return newLength <= 15
        } else {
            return false
        }
    }
}
