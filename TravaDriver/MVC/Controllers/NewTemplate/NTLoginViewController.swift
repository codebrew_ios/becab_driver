//
//  NTLoginViewController.swift
//  RoyoRide
//
//  Created by Ankush on 15/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit
import DropDown

class NTLoginViewController: BaseController {
    
    //MARK:- Enum
    enum ScreenType: String {
        case phone
        case email
        case emailInstitution
    }
    
    //MARK:- Outlet
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonLoginWithEmailPhone: UIButton!
    
    @IBOutlet weak var constraintHeightContainerView: NSLayoutConstraint!
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet var viewPhone: UIView!
    @IBOutlet var viewEmail: UIView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblISOCode: UILabel!
    
    @IBOutlet var txtFieldMobileNo: UITextField!
    @IBOutlet weak var textFieldUsernameEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    //    @IBOutlet weak var stackLoginWith: UIStackView!
    @IBOutlet weak var tAndcLabel:UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblLanguage:UILabel!
    
    
    //MARK:- Property
    var screenType: ScreenType = .phone
    var otpData :OtpModel?
    var driverData :DriverModel?
    let dropDown = DropDown()
    let languages = ["txt.english".localized:"en",
                     "txt.spanish".localized:"es"]
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:- View LIfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.pushNotificationPermission(application: UIApplication.shared) {
            _ = LocationManager.shared
        }
        initialSetup()
        dropDownSetup()
        
        LocationManager.shared.updateUserLocation()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        openUrl(urlString: APIConstants.TermsConditions)
    }
    @IBAction func privacyPolicyAction(_ sender: Any) {
        openUrl(urlString: APIConstants.AboutUs)
    }
    @IBAction func checkBox(_ sender: UIButton) {
        btnCheckBox.isSelected = !btnCheckBox.isSelected
    }
    @IBAction func languageAction(_ sender: UIButton) {
        
        let langType = BundleLocalization.sharedInstance().language
        
        self.dropDown.anchorView = sender
        
        dropDown.direction = .bottom
        dropDown.dataSource = Array(languages.keys)
        dropDown.show()
        
    }
    
    
    func dropDownSetup(){
        
        let selectedLanguage = UserDefaults.standard.value(forKey: "language") as? String
        self.lblLanguage.text = self.getLanguageFromCode(code: /selectedLanguage)
        
        dropDown.dataSource = Array(languages.keys)
        dropDown.textFont = R.font.montBold(size: 14.0)!
        dropDown.cellHeight = 40
        dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        dropDown.cornerRadius = 10
        dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        dropDown.shadowOpacity = 0.9
        dropDown.shadowRadius = 25
        dropDown.width = 85
        dropDown.animationduration = 0.25
        dropDown.textColor = .darkGray
        
        dropDown.bottomOffset = CGPoint(x: 0, y:10)
        
        //drop down selection handler
        //        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
        //
        //            self.lblLanguage.text = item
        //            self.dropDown.hide()
        //            let langCode = Array(self.languages.values)
        //            LanguageFile.shared.changeLanguage(type: langCode[index])
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.lblLanguage.text = item
            self.dropDown.hide()
            let langCode = Array(self.languages.values)
            LanguageFile.shared.changeLanguage(type: langCode[index])
            //            if selectedLanguage != langCode[index]{
            //                LanguageFile.shared.changeLanguage(type: langCode[index])
            //            }
        }
    }
}


//MARK:- Function
extension NTLoginViewController {
    
    func initialSetup() {
        setupUI()
        setupData()
        
        let text = NSMutableAttributedString()
        let accept = NSAttributedString(string: "Accept ".localized, attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
        text.append(accept)
        let terms = NSAttributedString(string: "terms and condition ".localized, attributes: [NSAttributedString.Key.foregroundColor:R.color.orange() ?? .black])
        text.append(terms)
        
        let and = NSAttributedString(string: "and ".localized, attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
        text.append(and)
        
        let policy = NSAttributedString(string: "privacy policy".localized, attributes: [NSAttributedString.Key.foregroundColor:R.color.orange() ?? .black])
        text.append(policy)
        
        tAndcLabel.attributedText = text
    }
    
    
    
    
    func setupUI() {
        
        switch screenType {
        case .email, .emailInstitution:
            viewPhone.isHidden = true
            constraintHeightContainerView.constant = 153.0
            view.layoutSubviews()
            
            viewEmail.frame = viewContainer.bounds
            viewContainer.addSubview(viewEmail)
            
            
        case .phone:
            viewEmail.removeFromSuperview()
            viewPhone.isHidden = false
            constraintHeightContainerView.constant = 53.0
            
            txtFieldMobileNo.delegate = self
        }
        
        let title = (screenType == .phone ? " " + "Email".localized : "Phone".localized)
        buttonLoginWithEmailPhone.setTitle(title, for: .normal)
        
        //        labelTitle.text = "Login To".localized + " " + "AppName".localized + " " + (screenType == .emailInstitution ? ( "institution".localized) : "")
        
        //        stackLoginWith.isHidden = screenType == .emailInstitution
        
        //        buttonNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
        //        buttonNext.setButtonWithTintColorBtnText()
    }
    
    func setupData() {
        
        switch screenType {
        case .phone:
            lblCountryCode.text = "+57"
            lblISOCode.text = "COL"
            break
            
        default:
            break
        }
    }
    
    // Move to otp verification  screen (Map view Screen)
    func moveToOtpVerification() {
        
        guard let vc = R.storyboard.newTemplateLoginSignUp.ntVerificationCodeViewController() else {return}
        
        vc.phoneNumber = (/lblCountryCode.text) + "-" + (/txtFieldMobileNo.text)
        vc.signup_as = .PhoneNo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Move to map Screen
    func moveToMapViewController() {
        
        let vc = R.storyboard.main.mapViewController()
        let nav = UINavigationController(rootViewController: vc!)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nav
    }
    
    // Move to SignUp Screen
    func moveToSignUpScreen() {
        
        //        guard let formDetailArray = UserSingleton.shared.appSetting?.registration_forum?.form_details else { return }
        
        //        let form = formDetailArray.filter({/$0.key_name == "reg_template"}).first
        //        let isRequired = /form?.required
        
        //        if /isRequired == "0" {
        guard let vc = R.storyboard.loginSignUp.registerationDetailsViewController() else {return}
        self.navigationController?.pushViewController(vc, animated: true)
        //        } else {
        //            guard let vc = R.storyboard.registeration.singlePageRegisteration() else { return }
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }
    }
}


//MARK:- Button Selectors
extension NTLoginViewController {
    
    // 1- Back, LoginWith -2, Next- 3, Country code - 4, 5- RememberMe, 6- Forgot password
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            navigationController?.popViewController(animated: true)
            
        case 2:
            screenType = screenType == .phone ? .email : .phone
            setupUI()
            
        case 3:
            debugPrint("Next")
            
            switch screenType {
            case .phone:
                
                let code = /lblCountryCode.text
                let number = txtFieldMobileNo.text ?? ""
                
                if NewTemplateValidations.sharedInstance.validatePhoneNumber(phone: number) {
                    guard btnCheckBox.isSelected else{
                        Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "please accept terms and conditions".localized , type: .info )
                        return
                    }
                    self.sendOtp(code: code, number: number, login_as: .PhoneNo)
                    
                }
                
            case .email:
                if NewTemplateValidations.sharedInstance.validateLoginUsernameAndPassword(usernameOrEmail: textFieldUsernameEmail.text ?? "", password: textFieldPassword.text ?? "") {
                    guard btnCheckBox.isSelected else{
                        Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "please accept terms and conditions".localized , type: .info )
                        return
                    }
                    debugPrint("Success")
                    emailLogin(login_as: .Email)
                }
                
            case .emailInstitution:
                break
            }
            
        case 4:
            
            guard let countryPicker = R.storyboard.loginSignUp.countryCodeSearchViewController() else{return}
            countryPicker.delegate = self
            self.navigationController?.present(countryPicker, animated: true, completion: nil)
            
            
        case 5:
            debugPrint("Remember me")
            sender.isSelected = !sender.isSelected
            
        case 6:
            debugPrint("Forgot password")
            
            
        default:
            break
        }
    }
}


//MARK: - Country Picker Delegates

extension NTLoginViewController: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        
        lblCountryCode.text = /(detail["dial_code"] as? String)
        lblISOCode.text = /(detail["code"] as? String)
    }
    
    func didSuccessOnOtpVerification() {
        
    }
    
    
}

//MARK:- API

extension NTLoginViewController {
    
    func sendOtp(code:String, number:String, login_as: LoginSignupType?) {
        
        guard let accountType = login_as else {return}
        
        APIManager.shared.request(with: LoginEndpoint.sendOtp(languageId: LanguageCode.English.get(), phoneCode: code, phone_number: number, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken , device_type:"Ios", social_key: nil, signup_as: accountType.rawValue) ) {[weak self] (response) in
            
            switch response{
            case .success(let response_value ):
                
                let resp = response_value  as? OtpModel
                self?.otpData = resp
                
                // add acess token for future reference also chnaged at otp verification
                token.access_token = (/self?.otpData?.data?.acessToken)
                self?.checkUpdate()
                
            case .failure( let str):
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
            }
        }
    }
    
    func checkUpdate(){
        
        guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return
        }
        
        let force = /self.otpData?.data?.versionForce
        
        if force != currentVersion{
            if currentVersion.compare(force, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending {
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertUpdateApp(), actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
                    
                    if let url = URL(string: APIConstants.appStoreUrl),
                        UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.open(url, options: [:]) { (opened) in
                            if(opened){
                                print("App Store Opened")
                            }
                        }
                    } else {
                        Alerts.shared.showOnTop(alert: "Alert", message: "App store not available.", type: .info)
                    }
                }
                
                return
            }
        }
        
        // otherwise move to otp verfication
        self.moveToOtpVerification()
    }
    
    func emailLogin(login_as: LoginSignupType?) {
        
        guard let accountType = login_as else {return}
        
        APIManager.shared.request(with: LoginEndpoint.emailLogin(languageId: LanguageCode.English.get(), timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken , device_type:"Ios", login_as: accountType.rawValue, email: textFieldUsernameEmail.text, password: textFieldPassword.text)) { [weak self] (response) in
            
            switch response {
                
            case .success(let responseValue):
                
                let resp = responseValue  as? DriverModel
                
                UserDefaults.standard.set(resp?.data?.accountStep, forKey: accountKey)
                self?.driverData = resp
                
                UserDefaults.standard.set(self?.driverData?.products?.first?.category_brand_product_id, forKey: "category_brand_product_id")
                UserDefaults.standard.set(self?.driverData?.data?.online_status, forKey: "online_status")
                UserDefaults.standard.set(self?.driverData?.data?.directional_hire, forKey: "directionalHire_status")
                //              UserDefaults.standard.set(self?.driverData?.data?.pool, forKey: "pool")
                UserDefaults.standard.set(self?.driverData?.data?.mulkiya_validity, forKey: "mulkiya_validity")
                
                DriverDataAtOtp.data = resp
                token.access_token = (/resp?.data?.accessToken)
                UserDefaults.standard.set(self?.driverData?.data?.category_id,forKey: "driver_cat_id")
                
                
                if resp?.data?.dataUser?.name == ""{
                    
                    self?.moveToSignUpScreen()
                } else {
                    
                    if (UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int) != nil {
                        
                        if (resp?.data?.approved == "0") {
                            
                            Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertDriverBlocked() , actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                            }
                            return
                        } else {
                            self?.moveToMapViewController()
                        }
                        
                    } else {
                        
                        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
                        let nav = UINavigationController(rootViewController: vc)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = nav
                    }
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localized, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
    
}

//MARK:- UItextfield delegates

extension NTLoginViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            if text == "" && string == "0" {
                return false
            }
            let newLength = text.length + string.length - range.length
            return newLength <= 15
        } else {
            return false
        }
    }
}

