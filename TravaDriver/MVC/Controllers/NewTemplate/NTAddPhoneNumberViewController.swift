//
//  NTAddPhoneNumberViewController.swift
//  RoyoRide
//
//  Created by Ankush on 18/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

import UIKit

class NTAddPhoneNumberViewController: UIViewController {
    
    @IBOutlet var viewPhone: UIView!
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet var txtFieldMobileNo: UITextField!
    
    @IBOutlet weak var buttonNext: UIButton!
    
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblISOCode: UILabel!
    
    var signup_as: LoginSignupType?
    var socialId: String?
    var otpData :OtpModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
}

extension NTAddPhoneNumberViewController {
    
    func initialSetup() {
        setupUI()
        setupData()
    }
    
    func setupUI() {
        
//        buttonNext.setButtonWithBackgroundColorThemeAndTitleColorBtnText()
//        buttonNext.setButtonWithTintColorBtnText()
        
        txtFieldMobileNo.delegate = self
    }
    
    
    func setupData() {
        
        lblCountryCode.text = "+57"
        lblISOCode.text = "COL"
        
    }
    
    // Move to otp verification  screen (Map view Screen)
    func moveToOtpVerification() {
        
        guard let vc = R.storyboard.newTemplateLoginSignUp.ntVerificationCodeViewController() else {return}
        
        vc.phoneNumber = (/lblCountryCode.text) + "-" + (/txtFieldMobileNo.text)
        vc.signup_as = signup_as
        vc.social_key = socialId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Button Selector
extension NTAddPhoneNumberViewController {
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        // 1- Back, 2- Next, 3- Country code
        
        switch sender.tag {
        case 1:
            navigationController?.popViewController(animated: true)
            
        case 2:
            debugPrint("Next")
            let code = /lblCountryCode.text
            let number = txtFieldMobileNo.text ?? ""
            
            if NewTemplateValidations.sharedInstance.validatePhoneNumber(phone: number) {
                
                self.sendOtp(code: code, number: number)
                
            }
            
        case 3:
            debugPrint("Country code")
            guard let countryPicker = R.storyboard.loginSignUp.countryCodeSearchViewController() else{return}
            countryPicker.delegate = self
            self.navigationController?.present(countryPicker, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

//MARK: - Country Picker Delegates

extension NTAddPhoneNumberViewController: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        
        lblCountryCode.text = /(detail["dial_code"] as? String)
        lblISOCode.text = /(detail["code"] as? String)
    }
    
    func didSuccessOnOtpVerification() {
        
    }
    
    
}

//MARK:- API

extension NTAddPhoneNumberViewController {
    
    func sendOtp(code:String, number:String) {
        
        guard let accountType = signup_as else {return}
        
        APIManager.shared.request(with: LoginEndpoint.sendOtp(languageId: LanguageCode.English.get(), phoneCode: code, phone_number: number, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken , device_type:"Ios", social_key: socialId, signup_as: accountType.rawValue)) {[weak self] (response) in
            
            switch response{
            case .success(let response_value ):
                
                let resp = response_value  as? OtpModel
                self?.otpData = resp
                
                // add acess token for future reference also chnaged at otp verification
                token.access_token = (/self?.otpData?.data?.acessToken)
                self?.checkUpdate()
                
            case .failure( let str):
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
            }
        }
    }
    
    
    func checkUpdate(){
        
        guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return
        }
        
        let force = /self.otpData?.data?.versionForce
        
        if force != currentVersion{
            if currentVersion.compare(force, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending {
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertUpdateApp(), actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
                    
                    if let url = URL(string: APIConstants.appStoreUrl),
                        UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.open(url, options: [:]) { (opened) in
                            if(opened){
                                print("App Store Opened")
                            }
                        }
                    } else {
                        Alerts.shared.showOnTop(alert: "Alert", message: "App store not available.", type: .info)
                    }
                }
                
                return
            }
        }
        
        // otherwise move to otp verfication
        self.moveToOtpVerification()
    }
}

//MARK:- UItextfield delegates

extension NTAddPhoneNumberViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            if text == "" && string == "0" {
                return false
            }
            let newLength = text.length + string.length - range.length
            return newLength <= 15
        } else {
            return false
        }
    }
}

