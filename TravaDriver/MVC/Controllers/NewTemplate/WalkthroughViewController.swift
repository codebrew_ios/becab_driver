//
//  WalkthroughViewController.swift
//  TravaDriver
//
//  Created by Work on 01/07/21.
//  Copyright © 2021 OSX. All rights reserved.
//

import UIKit


class WalkthroughCell:UICollectionViewCell{
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var index:Int = 0{
        didSet{
            if index == 0{
                iv.image = #imageLiteral(resourceName: "walk_1")
                titleLabel.text = "Taxi".localized
                subtitleLabel.text = "Register only if you are a taxi driver".localized
            }else{
                iv.image = #imageLiteral(resourceName: "walk_2")
                titleLabel.text = "Optimize".localized
                subtitleLabel.text = "Optimize your time and generate more income with BeCab".localized
            }
        }
    }
    
}


class WalkthroughViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    @IBOutlet weak var segmentControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentControl.currentPage = 0
        segmentControl.numberOfPages = 2
    }

    @IBAction func btnAction(_ sender: Any) {
        guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else {return}
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nav
    }
    
}


extension WalkthroughViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "WalkthroughCell", for: indexPath) as! WalkthroughCell
        cell.index = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.frame.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let index = Int(x/collectionView.frame.width)
        if index >= 0 {
            segmentControl.currentPage = index
        }
    }
    
    
}
