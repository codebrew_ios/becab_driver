//
//  NTLoginSignupTypeViewController.swift
//  RoyoRide
//
//  Created by Ankush on 14/05/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

extension UIViewController{
    func openUrl(urlString:String){
        if let url = URL(string:urlString) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
}

import UIKit
import AuthenticationServices
import SafariServices
import ObjectMapper
import Kingfisher

class WalkthroughCollectionViewCell: UICollectionViewCell {
        
    //MARK:- OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var imageViewScreens: UIImageView!
    
    var model:Any?{
        didSet{
            configureCell()
        }
    }
    
    
    func configureCell(){
        
        guard let _model = model as? Walkthrough else {return}
        
        imageViewScreens?.image = _model.image

        lblTitle?.text = _model.title
        lblSubtitle?.text = _model.description
        lblSubtitle?.setLineSpacing(lineSpacing: 4.0)
        
        
    }
}


enum LoginSignupType: String {
    
    case Normal = "Normal"
    case PhoneNo
    case Email
    case Facebook
    case Apple
    case GmailD
    case PrivateCooperative
}



class NTLoginSignupTypeViewController: UIViewController {
    
    //MARK:- Enum
    enum ScreenType: String {
        case login
        case signup
    }
    
    //MARK:- Outlet
    @IBOutlet var collectionTable: UICollectionView!
    @IBOutlet var arrayIndicatorConstraint: [NSLayoutConstraint]!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAlreadyAccount: UILabel!
    
    @IBOutlet weak var buttonPhoneEmail: UIButton!
    @IBOutlet weak var buttonLoginSignup: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    
    @IBOutlet weak var continueWithAppleButtonStack: UIStackView!
    
    @IBOutlet weak var stackViewTerms: UIStackView!
    
    //MARK:- Properties
    var HeadingCollectionViewDataSource:CollectionViewDataSource?
    var arrayWalkThroughModel:[Walkthrough]? = []
    
    var currentIndex:Int = 0
    var screenType: ScreenType = .login
    var driverData :DriverModel?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        LocationManager.shared.updateUserLocation()
        
    }
}

//MARK:- Function

extension NTLoginSignupTypeViewController {
    
    fileprivate func initialSetup() {
        let item1 = Walkthrough(title: "TAXISTA", description: "Regístrate únicamente si eres conductor de taxi.", image: #imageLiteral(resourceName: "w2"))
        
        let item2 = Walkthrough(title: "OPTIMIZA", description: "Optimiza tu tiempo y genera más ingresos con BeCab.", image: #imageLiteral(resourceName: "w1"))
        arrayWalkThroughModel?.append(item1)
        arrayWalkThroughModel?.append(item2)
        
        setUpUI()

    }
    

    
    fileprivate func setUpUI() {
        
        
        self.setIndicator(selectedIndex: 0)
        self.configureCollectionView()
        
        // App Name
        labelTitle.text = (screenType == .login ? "Login To".localized : "Sign up for".localized)  + " " + "AppName".localized
        labelAlreadyAccount.text = screenType == .login ? "Don't have an account".localized : "Have an account".localized
        
        let buttontitle = (screenType == .login ? "Login".localized : "Signup".localized ) + " " + "with Phone Email".localized
        buttonPhoneEmail.setTitle(buttontitle, for: .normal)
        
        let btnTitle = screenType == .login ? "Signup".localized : "Login".localized
        buttonLoginSignup.setTitle(btnTitle, for: .normal)
        
        stackViewTerms.isHidden = screenType == .login
        buttonCancel.isHidden = screenType == .login
        
    }
    
    fileprivate func configureCollectionView(){ //Configuring collection View cell
        
        let identifier = String(describing: WalkthroughCollectionViewCell.self)
        
        HeadingCollectionViewDataSource = CollectionViewDataSource(items: arrayWalkThroughModel, collectionView: collectionTable, cellIdentifier: identifier, headerIdentifier: nil, cellHeight: collectionTable.bounds.height, cellWidth: UIScreen.main.bounds.width, configureCellBlock: { (cell, item, indexPath) in
            
            let _cell = cell as? WalkthroughCollectionViewCell
            _cell?.model = item
            
        }, aRowSelectedListener: nil, willDisplayCell: nil, scrollViewDelegate: { [weak self] (scrollView) in
            
            guard let indexPath = self?.collectionTable.getVisibleIndexOnScroll() else {return}
            
            if self?.currentIndex != indexPath.item {
                
                self?.setIndicator(selectedIndex: indexPath.item)
                
            }
            
            }, scrollViewDecelerate: nil)
        
        collectionTable.dataSource = HeadingCollectionViewDataSource
        collectionTable.delegate = HeadingCollectionViewDataSource
        collectionTable.reloadData()
    }
    
    func setIndicator(selectedIndex:Int){
        
        for (index,_) in self.arrayIndicatorConstraint.enumerated(){
            
            UIView.animate(withDuration: 0.2) { [unowned self] in
                
                //  self.arrayIndicatorConstraint[index].constant = index == selectedIndex ? self.currentIndicatorWidth : self.indicatorNormalWidth
                
                if index == selectedIndex {
                    self.view.viewWithTag(index + 101)?.backgroundColor = .white
                    // self.view.viewWithTag(index + 1)?.setThemeBackground()
                } else {
                    self.view.viewWithTag(index + 101)?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
                }
                
                // self.view.viewWithTag(index + 1)?.backgroundColor = index == selectedIndex ? R.color.appPurple() : UIColor.lightGray.withAlphaComponent(0.4)
                
                
            }
            
        }
        
        self.currentIndex = selectedIndex
        
        self.view.layoutIfNeeded()
        
    }
    
    // Move to map Screen
    func moveToMapViewController() {
        
        let vc = R.storyboard.main.mapViewController()
                  let nav = UINavigationController(rootViewController: vc!)
                  let appDelegate = UIApplication.shared.delegate as! AppDelegate
                  appDelegate.window?.rootViewController = nav
    }
    
    // Move to SignUp Screen
    func moveToSignUpScreen() {
        
//        guard let formDetailArray = UserSingleton.shared.appSetting?.registration_forum?.form_details else { return }
        
//        let form = formDetailArray.filter({/$0.key_name == "reg_template"}).first
//        let isRequired = /form?.required
        
//        if /isRequired == "0" {
            guard let vc = R.storyboard.loginSignUp.registerationDetailsViewController() else {return}
            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
//            guard let vc = R.storyboard.registeration.singlePageRegisteration() else { return }
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
}


//MARK:- Button Selector

extension NTLoginSignupTypeViewController {
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        // 1 - cancel, 2- QuestionMark, 3- PhoneEmail, 4- Facebook, 5- Google, 6- Apple, 7- Institution Account, 8- Terms, 9- Privacy, 10- LoginSignup
        
        switch sender.tag {
        case 1:
            navigationController?.popViewController(animated: true)
            
        case 2:
            debugPrint("Question")
            
        case 3:
            switch screenType {
                
            case .login:
                guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else {return}
                navigationController?.pushViewController(vc, animated: true)
                
            case .signup:
                guard let vc = R.storyboard.newTemplateLoginSignUp.ntSignupViewController() else {return}
                navigationController?.pushViewController(vc, animated: true)
            }
            
        case 4:
            debugPrint("Facebook")
//            FBLogin.shared.login {[weak self] (userData) in
//
//                debugPrint(userData as Any)
//                self?.checkUserExist(social_key: userData?.id, login_as: LoginSignupType.Facebook)
//            }
            
        case 5:
            debugPrint("Google")
//            GoogleSignIn.shared.openGoogleSigin {[weak self] (userData) in
//
//                debugPrint(userData as Any)
//                self?.checkUserExist(social_key: userData?.id, login_as: LoginSignupType.Gmail)
//            }
            
        case 6:
            debugPrint("Apple")
            
            
        case 7:
            switch screenType {
                
            case .login:
                guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else {return}
                vc.screenType = .emailInstitution
                navigationController?.pushViewController(vc, animated: true)
                
            case .signup:
                break
            }
            
        case 8:
            openUrl(urlString: APIConstants.TermsConditions)
        case 9:
            openUrl(urlString: APIConstants.privacyUrl)
        case 10:
            debugPrint("LoginSignup")
            
            switch screenType {
                
            case .login:
                guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginSignupTypeViewController() else {return}
                vc.screenType = .signup
                navigationController?.pushViewController(vc, animated: true)
                
            case .signup:
                navigationController?.popViewController(animated: true)
            }
        case 11:
            openUrl(urlString: APIConstants.AboutUs)
        default:
            break
        }
    }
    
    
    
    
    
}


//MARK:- API

extension NTLoginSignupTypeViewController {
    
    func socialLogin(social_key: String?, login_as: LoginSignupType?) {
        
        guard let accountType = login_as else {return}
        
        let endpoint = LoginEndpoint.socailLogin(languageId: LanguageCode.English.get(), timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken, device_type: "Ios", social_key: social_key, login_as: accountType.rawValue)
        
        APIManager.shared.request(with: endpoint )  {[weak self] (resp) in
            
            switch resp {
                
            case .success(let responseValue ):
               
                let resp = responseValue  as? DriverModel
                
                UserDefaults.standard.set(resp?.data?.accountStep, forKey: accountKey)
                self?.driverData = resp
                
                UserDefaults.standard.set(self?.driverData?.products?.first?.category_brand_product_id, forKey: "category_brand_product_id")
                UserDefaults.standard.set(self?.driverData?.data?.online_status, forKey: "online_status")
                UserDefaults.standard.set(self?.driverData?.data?.directional_hire, forKey: "directionalHire_status")
                UserDefaults.standard.set(self?.driverData?.data?.mulkiya_validity, forKey: "mulkiya_validity")
//                UserDefaults.standard.set(self?.driverData?.data?.pool, forKey: "pool")
                DriverDataAtOtp.data = resp
                token.access_token = (/resp?.data?.accessToken)
                UserDefaults.standard.set(self?.driverData?.data?.category_id,forKey: "driver_cat_id")
                
                
                if resp?.data?.dataUser?.name == ""{
                   
                    self?.moveToSignUpScreen()
                } else {
                   
                    if (UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int) != nil {
                       
                        if (resp?.data?.approved == "0") {
                           
                            Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertDriverBlocked() , actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                            }
                            return
                        } else {
                            self?.moveToMapViewController()
                        }
                        
                    } else {
                       
                        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
                        let nav = UINavigationController(rootViewController: vc)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = nav
                    }
                }
            case .failure(let err):
                
                Alerts.shared.showOnTop(alert: "alert.alert".localized, message: err ?? "", type: .info)
            }
        }
    }
    
    func checkUserExist(social_key: String?, login_as: LoginSignupType?) {
        
        guard let accountType = login_as else {return}
        
        APIManager.shared.request(with: LoginEndpoint.checkuserExists(social_key: social_key, login_as: accountType.rawValue)) {[weak self] (response) in
            
            switch response{
            case .success(let response_value ):
                
                guard let model = response_value as? CheckUserExistModel else { return }
                
                if /model.AppDetail?.userExists {
                    
                    self?.socialLogin(social_key: social_key, login_as: login_as)
                    
                } else {
                    guard let vc = R.storyboard.newTemplateLoginSignUp.ntAddPhoneNumberViewController() else {return}
                    vc.signup_as = accountType
                    vc.socialId = social_key
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                
            case .failure( let str):
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
            }
        }
    }
}




extension UICollectionView{
  
  func getVisibleIndexOnScroll()-> IndexPath?{
    
    var visibleRect = CGRect()
    visibleRect.origin = self.contentOffset
    visibleRect.size = self.bounds.size
    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
    let indexPath = self.indexPathForItem(at: visiblePoint)
    return indexPath
  }
  
  
}
