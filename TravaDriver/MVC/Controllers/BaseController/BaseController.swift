//
//  BaseController.swift
//  Buraq24Driver
//
//  Created by OSX on 10/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import CoreLocation

class BaseController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func checkForLocationPermission() -> Bool {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
                
            case  .restricted, .denied:
                return false
                
            case .authorizedAlways, .authorizedWhenInUse:
                return true
                
            case .notDetermined:
                LocationManager.shared.updateUserLocation()
                
                return false
            }
        } else {
            
            return false
        }
    }
    
    func getToggleStatusImage()->(UIImage,UIImage){
        
        let langType = BundleLocalization.sharedInstance().language
        
        switch langType {
        case Languages.Arabic:
           return (#imageLiteral(resourceName: "ic_full_online"),#imageLiteral(resourceName: "ic_full_offline"))
        case Languages.Urdu:
            return (#imageLiteral(resourceName: "ic_switch_on_u"),#imageLiteral(resourceName: "ic_switch_off_u"))
        case Languages.English:
             return (#imageLiteral(resourceName: "ic_full_online"),#imageLiteral(resourceName: "ic_full_offline"))
        case Languages.Chinese:
            return(#imageLiteral(resourceName: "ic_switch_on_c"),#imageLiteral(resourceName: "ic_switch_off_c"))
        case Languages.Spanish:
            return(#imageLiteral(resourceName: "ic_switch_on_s"),#imageLiteral(resourceName: "ic_switch_off_s"))
        default:
            return(#imageLiteral(resourceName: "ic_switch_on"),#imageLiteral(resourceName: "ic_switch_off"))

        }
    }
    
    func getLanguageFromCode(code:String) -> String {
        
        switch code {
        case "en":
            return "txt.english".localized
        case "ur":
            return "txt.urdu".localized
        case "ar":
            return "txt.arabic".localized
        case "es":
            return "Español"
        case "zh-Hans":
            return "txt.chinese".localized
        default:
            return "txt.english".localized
        }
    }
    
    func rotateButton(button:UIButton){
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            button.transform = button.transform.rotated(by: .pi)
        }
    }
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        guard let dt = dateFormatter.date(from: date) else {return ""}
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"
        
        return dateFormatter.string(from: dt)
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard let dt = dateFormatter.date(from: date)else {return ""}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM d, h:mm a"
        return dateFormatter.string(from: dt)
    }
    
    func getVehicleImage(category:Int) -> UIImage?{
        
        switch category {
        case 1:
            return #imageLiteral(resourceName: "ic_mini_truck_m_gas")
        case 2:
            return #imageLiteral(resourceName: "ic_mini_truck_water_m")
        case 3:
            return #imageLiteral(resourceName: "ic_truck_m")
        case 4:
            return #imageLiteral(resourceName: "ic_truck_m_freights")
        case 5:
            return #imageLiteral(resourceName: "ic_mini_truck_m_tow")
        case 6:
            return #imageLiteral(resourceName: "ic_heavy_machinery_m")
        case 7:
            return #imageLiteral(resourceName: "ic_mini_cab_m")
        default:
            return #imageLiteral(resourceName: "ic_mini_cab_m")
        }
    }
    
    func getBrandName(category:String,BrandId:String) -> String{
        
        let name = /DataResponse.driverData?.services?.first(where: { $0.category_id == Int(category) })?.brands?.first(where: { $0.category_brand_id == Int(BrandId) })?.name

        
//        if let array = DataResponse.driverData?.services {
//            for item in array.enumerated(){
//                if item.element.category_id == Int(category){
//                    if let itemBrands = item.element.brands{
//                        for brand in itemBrands{
//                            if brand.category_brand_id == Int(BrandId){
//                                return /brand.name
//                            }
//                        }
//                    }
//                }
//            }
//        }
        return name
    }
    
    func getServiceName(category:String) -> String{
        
        let name = DataResponse.driverData?.services?.first(where: { $0.category_id == Int(category) })?.name
        return /name
        
//        switch category {
//            
//        case "1":
//            return R.string.localizable.txtGas()
//        case "2":
//            return R.string.localizable.txtMineralWater()
//        case "3":
//            return R.string.localizable.txtWaterTanker()
//        case "4":
//            return  R.string.localizable.txtTrucks()
//        case "7":
//            return  R.string.localizable.txtCab()
//
//        default:
//            return  "No service"
//        }
    }
}

//MARK:- Add/Remove Child VC
extension BaseController {
    
    func displayContentController(content: UIViewController, parentVc: RegisterationDetailsViewController) {
        
        addChildViewController(content)
        content.view.frame = parentVc.containerView.bounds
        parentVc.containerView.addSubview(content.view)
        
        content.didMove(toParentViewController: self)
    }
    
    func hideContentController(content: UIViewController) {
        content.willMove(toParentViewController: nil)
        content.view.removeFromSuperview()
        content.removeFromParentViewController()
    }
    
}

//MARK:BURAQ EXTENSIONS

extension UIImageView{
    
    func getRatingImage(rating:String){
        
        switch rating {
        case "1":
            self.image = #imageLiteral(resourceName: "ic_1")
        case "2":
            self.image = #imageLiteral(resourceName: "ic_2")
        case "3":
            self.image = #imageLiteral(resourceName: "ic_3")
        case "4":
            self.image = #imageLiteral(resourceName: "ic_4")
        case "5":
            self.image = #imageLiteral(resourceName: "ic_5")
        default:
            self.image = #imageLiteral(resourceName: "ic_1")
        }
    }
}
