//
//  VehicleDetailsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 07/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

import ActionSheetPicker_3_0
class ImageObject {
    var fileName:String?
    var image:UIImage?
}
class VehicleDetailsViewController: BaseController {
    
    
    //MARK:- Enum
    enum Section: Int {
        
        case vehicleCertificate = 0
        case insuranceDocument
        
        func getTitle() -> String {
            
            switch self {
                
            case .vehicleCertificate:
                return "VEHICLE REGISTRATION CERTIFICATE"
                
            case .insuranceDocument:
                return "INSURANCE DOCUMENT"
            }
        }
    }
    
    enum SelectImageType {
        case vehicleFront
        case vehicleBack
       // case vehicleLeft
      // case vehicleRight
        case vehicleInside
        case vehicleCertificate
        case insuranceDocument
    }
    
    //MARK:- Outlet
    @IBOutlet weak var insuranceExpiryDateTextField: AnimatableTextField!
    @IBOutlet weak var rcExpiryDateTextField: AnimatableTextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblLicensePlate: UILabel!
    @IBOutlet weak var txfyear: AnimatableTextField!
    @IBOutlet weak var textFieldVehicleNumber: AnimatableTextField?
    @IBOutlet weak var textFieldVehiclRegeNumber: AnimatableTextField?
    
    @IBOutlet weak var buttonVehicleFront: UIButton?
    @IBOutlet weak var buttonRemoveFront: UIView!
    
    @IBOutlet weak var buttonVehicleBack: UIButton?
    @IBOutlet weak var buttonRemoveBack: UIButton?
    
     @IBOutlet weak var buttonRemoveLeft: UIButton?
     @IBOutlet weak var buttonVehicleLeft: UIButton?
     @IBOutlet weak var buttonRemoveRight: UIButton?
     @IBOutlet weak var buttonVehicleRight: UIButton?
     @IBOutlet weak var buttonRemoveInside: UIButton?
     @IBOutlet weak var buttonVehicleInside: UIButton?
    @IBOutlet weak var vehicleMake: AnimatableTextField!
    @IBOutlet weak var vehicleModel: AnimatableTextField!
    @IBOutlet weak var vehicleColor: AnimatableTextField!
    
    //MARK:- Properties
    var pickerView: DateTimeView?
    var rcExpiryDateString:String = ""
    var insuranceExpirtDateString:String = ""
    var rcExpiryDate:Date = Date()
    var insuranceExpirtDate:Date = Date()
    var imageType: SelectImageType?
    var registrationCertificate:[ImageObject] = []
    var registrationCertificateImages:[UIImage] = []
    var insauranceDocumentImages:[UIImage] = []
    var insauranceDocument:[ImageObject] = []
    var isVehicleFrontUploaded: Bool = false
    var isVehicleBackUploaded: Bool = false
    var isVehicleLeftUploaded: Bool = false
    var isVehicleRightUploaded: Bool = false
    var isVehicleInsideUploaded: Bool = false
    let formatter = DateFormatter()
    var front = Bool()
    var back = Bool()
    var inside = Bool()
    var yearPicker = UIPickerView()
    let currentYear = Calendar.current.component(.year, from: Date())
    
    var dataSource : TableViewDataSourceWithHeader? {
        didSet{
            tableView.delegate = dataSource
            tableView.dataSource = dataSource
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblLicensePlate.text = "LicensePlate".localized
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func rcExpiryDateAction(_ sender: UIButton) {
        self.uiBirthDayPicker(type: .rcExpiry, currentDate: rcExpiryDate)
    }
    
    @IBAction func insuranceExpiryDateAction(_ sender: UIButton) {
        self.uiBirthDayPicker(type: .insuranceExpiry, currentDate: insuranceExpirtDate)
    }
    
    @IBAction func btnYear(_ sender: Any) {
        
        //let yearsToAdd = 10
        let currentDate = Date()
        var dateComponent = DateComponents()
        // dateComponent.year = yearsToAdd
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        ActionSheetDatePicker.show(withTitle: "Year", datePickerMode: .countDownTimer, selectedDate: Date(), minimumDate: Date(), maximumDate: nil, doneBlock: { (picker, resp1, resp2) in
            self.formatter.dateFormat = "yyyy"
            self.txfyear.text = self.formatter.string(from: (resp1 as? Date)!)
        }, cancel: { (picker) in
        }, origin: txfyear)
    }
    
    
    @IBAction func actionLeftMulkiyaImage(_ sender: UIButton) {
       // imageType = .vehicleLeft
        self.openPhotoOptionsActionSheet()
    }
    @IBAction func actionRightMulkiyaImage(_ sender: UIButton) {
        //imageType = .vehicleRight
        self.openPhotoOptionsActionSheet()
    }
    @IBAction func actionInsideMulkiyaImage(_ sender: UIButton) {
        imageType = .vehicleInside
        self.openPhotoOptionsActionSheet()
    }
    
    @IBAction func btnRemoveLeftMulkiya(_ sender: UIButton) {
        
        buttonVehicleLeft?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveLeft?.isHidden = true
        
        isVehicleLeftUploaded = false
    }
    
    @IBAction func btnRemoveRightMulkiya(_ sender: UIButton) {
        
        buttonVehicleRight?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveRight?.isHidden = true
        
        isVehicleRightUploaded = false
    }
    
    @IBAction func btnRemoveInsideMulkiya(_ sender: UIButton) {
        
        buttonVehicleInside?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveInside?.isHidden = true
        
        isVehicleInsideUploaded = false
    }
    
    
}
extension VehicleDetailsViewController : DelegateUpdateDateAndTime {
    
    func updateDateTime(date: Date, type : DateType){
        switch type {
        case .insuranceExpiry:
            insuranceExpirtDate = date
            insuranceExpiryDateTextField.text = date.toString(format: "d-MM-yyyy")
            insuranceExpirtDateString = date.toString(format: "yyyy-MM-dd HH:mm:ss")
        case .rcExpiry:
            rcExpiryDate = date
            rcExpiryDateTextField.text = date.toString(format: "d-MM-yyyy")
            rcExpiryDateString = date.toString(format: "yyyy-MM-dd HH:mm:ss")
        }
        
    }
}
extension VehicleDetailsViewController {
    
    func uiBirthDayPicker(type:DateType,currentDate:Date){
        pickerView =  Bundle.main.loadNibNamed("DateTimeView", owner: self,  options: nil)?.first! as? DateTimeView
        pickerView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        pickerView?.delegate = self
        pickerView?.type = type
        pickerView?.dateTimePicker.datePickerMode = .date
        pickerView?.dateTimePicker.setDate(currentDate, animated: true)
        pickerView?.dateTimePicker.minimumDate = Date()
        UIApplication.shared.keyWindow?.addSubview(pickerView ?? UIView())
    }
    
    func initialSetup() {
        textFieldVehicleNumber?.delegate = self
        textFieldVehiclRegeNumber?.delegate = self
        txfyear.delegate = self
        insuranceExpiryDateTextField.text = Date().toString(format: "d-MM-yyyy")
        insuranceExpirtDateString = Date().toString(format: "yyyy-MM-dd HH:mm:ss")
        rcExpiryDateTextField.text = Date().toString(format: "d-MM-yyyy")
        rcExpiryDateString = Date().toString(format: "yyyy-MM-dd HH:mm:ss")
        //configureTableView()
        txfyear.inputView = yearPicker
        yearPicker.delegate = self
        yearPicker.dataSource = self
        
        
        [textFieldVehicleNumber,textFieldVehiclRegeNumber,rcExpiryDateTextField,vehicleModel,vehicleMake,vehicleColor].forEach { field in
            field?.autocapitalizationType = .allCharacters
        }
    }
    
    func configureTableView(){
        
        tableView.estimatedRowHeight = 50.0
        tableView.register(UINib(nibName: R.reuseIdentifier.uploadedDocCell.identifier, bundle:nil), forCellReuseIdentifier: R.reuseIdentifier.uploadedDocCell.identifier)
        
        tableView.register(UINib(nibName: R.reuseIdentifier.uploadDocHeaderCell.identifier, bundle:nil), forCellReuseIdentifier: R.reuseIdentifier.uploadDocHeaderCell.identifier)
        
        /* tableView.register(UINib(nibName: R.reuseIdentifier.uploadDocHeaderCell.identifier, bundle:nil), forHeaderFooterViewReuseIdentifier: R.reuseIdentifier.uploadDocHeaderCell.identifier) */
        
        dataSource = TableViewDataSourceWithHeader(items: [registrationCertificate,insauranceDocument], tableView: tableView, cellIdentifier: R.reuseIdentifier.uploadedDocCell.identifier , cellHeight: 75, headerHeight: 132.0, configureCellBlock: { (cell, item, indexPath) in
            
            guard let _cell = cell as? UploadedDocTableViewCell else {return}
            _cell.delegate = self
            _cell.labelTitle.isHidden = indexPath.row > 0
            _cell.deleteButton.row = indexPath.row
            _cell.deleteButton.section = indexPath.section
            let type = VehicleSectionType(rawValue: indexPath.section)
            switch type {
            case .vehicleCertificate :
                _cell.uploadedImageView.image = self.registrationCertificate[indexPath.row].image
                _cell.fileNameLabel.text = self.registrationCertificate[indexPath.row].fileName
            case .insuranceDocument :
                _cell.uploadedImageView.image = self.insauranceDocument[indexPath.row].image
                _cell.fileNameLabel.text = self.insauranceDocument[indexPath.row].fileName
            default :
                break
            }
            
        }, viewForHeader: {[weak self] (section) -> UIView? in
            
            guard let cell = self?.tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.uploadDocHeaderCell.identifier ) as? UploadDocHeaderTableViewCell else { return UIView() }
            
            cell.sectionIndex = section
            cell.delegate = self
            cell.labelTitle.text = Section(rawValue: section)?.getTitle()
            cell.buttonUpload.setTitle("Upload Certificate", for: .normal)

            return cell
            
            }, viewForFooter: { (section) -> UIView? in
                return UIView()
        }, aRowSelectedListener: { (indexPath, cell) in
            
            
        }, willDisplayCell: { (indexPath, cell) in
            
            
        }, scrollView: { (scrollView) in
            
            
        })
        
    }
    
    
    
    
    func openPhotoOptionsActionSheet() {
        self.view.endEditing(true)
        
        CameraImage.shared.captureImage(
            from: self, navigationController: self.navigationController,
            mediaType: nil,
            captureOptions: [.camera, .photoLibrary],
            allowEditting: true) {
                
                [weak self] (image,fileName) in
                guard let img = image,
                    let type = self?.imageType else { return }
                let obj = ImageObject()
                obj.image = img
                obj.fileName = fileName
                switch type {
                    
                case .vehicleFront:
                    self?.buttonVehicleFront?.setImage(img, for: .normal)
                    self?.buttonRemoveFront?.isHidden = false
                    self?.front = true
                    self?.isVehicleFrontUploaded = true
                    
                case .vehicleBack:
                    self?.back = true
                    self?.buttonVehicleBack?.setImage(img, for: .normal)
                    self?.buttonRemoveBack?.isHidden = false
                    
                    self?.isVehicleBackUploaded = true
                    
//                case .vehicleLeft:
//                    self?.buttonVehicleLeft?.setImage(img, for: .normal)
//                    self?.buttonRemoveLeft?.isHidden = false
//
//                    self?.isVehicleLeftUploaded = true
//
//                case .vehicleRight:
//                    self?.buttonVehicleRight?.setImage(img, for: .normal)
//                    self?.buttonRemoveRight?.isHidden = false
//
//                    self?.isVehicleRightUploaded = true
                    
                case .vehicleInside:
                    self?.inside = true
                    self?.buttonVehicleInside?.setImage(img, for: .normal)
                    self?.buttonRemoveInside?.isHidden = false
                    
                    self?.isVehicleInsideUploaded = true
                    
                case .vehicleCertificate:
                    debugPrint("vehicleCertificate")
                    //  self?.registrationCertificate.removeAll()
                    self?.registrationCertificate.append(obj)
                    self?.registrationCertificateImages.append(img)
                    self?.reloadTableData()
                case .insuranceDocument:
                    debugPrint("insuranceDocument")
                    //     self?.insauranceDocument.removeAll()
                   
                    self?.insauranceDocument.append(obj)
                    self?.insauranceDocumentImages.append(img)
                    self?.reloadTableData()
                }
        }
    }
    
    func reloadTableData() {
        self.configureTableView()
        self.tableView.reloadData()
    }
    
    func validateForm(completion: @escaping (_ success: Bool?) -> () ) {
        
        if textFieldVehiclRegeNumber?.text?.trim() == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup2.tfVehicleRegPlaceholder".localized , type: .info)
            return
        }
        
        
        if vehicleMake.text?.trim() == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup2.vehicleMake".localized , type: .info)
            return
        }
        
        
        if vehicleModel.text?.trim() == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup2.vehicleModel".localized , type: .info)
            return
        }
        
        if let year = txfyear.text?.trim(), year != "" {
            let currentDate = Date().YearOfWeek()
            let currentYearString = currentDate?.toDate().dateByAddingYears(1) ?? Date()
            let secondDate = year.toDate().dateByAddingYears(1)
            if secondDate > currentYearString {
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please enter valid year (Less than from \(currentDate ?? ""))".localized , type: .info)
                return
            }
        }else{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup2.vehicleYear".localized , type: .info)
            return
        }
        
        
        if vehicleColor.text?.trim() == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup2.vehicleColor".localized , type: .info)
            return
        }
            
            
        else if textFieldVehiclRegeNumber?.text?.trim() == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup2.tfVehicleRegPlaceholder".localized , type: .info)
            return
        }
        
        self.uploadStepSecondData { (success) in
            completion(true)
        }
    }
    
    func uploadStepSecondData(completion: @escaping (_ success: Bool?) -> ()) {
        
        var vehicleImgs:[UIImage] = []
        if let img = self.buttonVehicleFront?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        if let img = self.buttonVehicleBack?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        if let img = self.buttonVehicleRight?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        if let img = self.buttonVehicleLeft?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        if let img = self.buttonVehicleInside?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        
        
        if  !front || !back || !inside {
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please upload the vehicle/document images.".localized , type: .info)
        } else {
            let imagesData:[String:[UIImage]] = ["vehicle_rc_docs":registrationCertificateImages ,"insurance_docs":insauranceDocumentImages ,"vehicle_images":vehicleImgs]
            
            
            
            /*
             (vehicleNumber:String,vehicleRegistrationNumber:String,vehicle_rc_expiry:String,insurance_expiry:String, vehicleMake: String, vehicleModel: String, vehicleColor: String, vehicleYear: String)
             */
            
            APIManager.shared.request(withMultipleImages: LoginEndpoint.uploadProfileStep2(
                vehicleNumber: textFieldVehiclRegeNumber?.text?.trim() ?? "",
                vehicleRegistrationNumber: textFieldVehiclRegeNumber?.text?.trim() ?? "",
                vehicle_rc_expiry: nil,
                insurance_expiry: nil,
                vehicleMake: vehicleMake.text?.trim() ?? "",
                vehicleModel: vehicleModel.text?.trim() ?? "",
                vehicleColor: vehicleColor.text?.trim() ?? "",
                vehicleYear: txfyear.text?.trim() ?? ""),
                                      images: imagesData) { (response) in
                                        completion(true)
            }
            
            
            /*
             [vehicleNumber,vehicleRegistrationNumber, vehicle_rc_expiry, insurance_expiry, vehicleMake, vehicleModel, vehicleColor, vehicleYear]
             */
        }
        
        
        
        
    }
    
    
}


extension VehicleDetailsViewController {
    
    @IBAction func btnRemoveFrontMulkiya(_ sender: UIButton) {
        
        buttonVehicleFront?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveFront?.isHidden = true
        
        isVehicleFrontUploaded = false
    }
    
    @IBAction func btnRemoveBackMulkiya(_ sender: UIButton) {
        
        buttonVehicleBack?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveBack?.isHidden = true
        
        isVehicleBackUploaded = false
    }
    
    @IBAction func actionFrontMulkiyaImage(_ sender: UIButton) {
        imageType = .vehicleFront
        self.openPhotoOptionsActionSheet()
    }
    
    @IBAction func actionBackMulkiyaImage(_ sender: UIButton) {
        imageType = .vehicleBack
        self.openPhotoOptionsActionSheet()
    }
    
    
}

//MARK:- UploadDocHeaderTableViewCellDelegate
extension VehicleDetailsViewController : UploadDocHeaderTableViewCellDelegate {
    
    func uploadCertificateClicked(section: Int?) {
        
        guard let type = Section(rawValue: /section) else {return}
        
        switch type {
            
        case .vehicleCertificate:
            imageType = .vehicleCertificate
            openPhotoOptionsActionSheet()
            
        case .insuranceDocument:
            imageType = .insuranceDocument
            openPhotoOptionsActionSheet()
        }
    }
    
}

extension VehicleDetailsViewController : UploadedDocTableViewCellDelegate {
    func deleteDoc(indexPath: IndexPath) {
        let type = VehicleSectionType(rawValue: indexPath.section)
        switch type {
        case .vehicleCertificate:
            if indexPath.row < self.registrationCertificate.count {
                self.registrationCertificate.remove(at: indexPath.row)
            }
        case .insuranceDocument:
            if indexPath.row < self.insauranceDocument.count {
                self.insauranceDocument.remove(at: indexPath.row)
            }
        default:
            break
        }
        self.reloadTableData()
    }
}
extension VehicleDetailsViewController: UITextFieldDelegate{
    //MARK: - ---------------UItextFieldDelegate----------------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txfyear {
//            let maxLength = 4
//            let newString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
//            return newString.length <= maxLength
//        }
        if textField.isFirstResponder {
            let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
            if (textField == self.textFieldVehicleNumber || textField == self.textFieldVehiclRegeNumber){
                if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                    return false
                }
                if let range = string.rangeOfCharacter(from: validString)
                {
                    print(range)
                    return false
                }
            }
        }
        return true
    }
}

enum VehicleSectionType : Int {
    case vehicleCertificate = 0
    case insuranceDocument = 1
}


//MARK: - Year Picker view delegate
extension VehicleDetailsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
        
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1000
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(currentYear-row)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txfyear.text  = "\(currentYear-row)"
    }
}
