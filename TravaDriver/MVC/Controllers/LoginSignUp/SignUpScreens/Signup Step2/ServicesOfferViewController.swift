//
//  ServicesOfferViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 08/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import DropDown
import IBAnimatable

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

class ServicesOfferViewController: BaseController {
    
    //MARK: ---------------Outlets-------------
    
    @IBOutlet weak var tfBankName: UITextField!
    @IBOutlet weak var tfBankAccountNumber: UITextField!
    
    @IBOutlet weak var btnServices: UIButton!
    @IBOutlet weak var btnBrands: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lblServices: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblCategoryTypes: UILabel!
    
    @IBOutlet weak var anchorViewBrand: CustomView!
    @IBOutlet weak var anchorViewService: CustomView!
    @IBOutlet weak var anchorViewProductType: CustomView!
    @IBOutlet weak var imgView: AnimatableImageView!
    //localised
    @IBOutlet weak var lblserviceinfo: UILabel!
    @IBOutlet weak var lblBrandInfo: UILabel!
    @IBOutlet weak var lblOptionalInfo: UILabel!
    @IBOutlet weak var lblProductType: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tableViewServiceLine: UITableView!
    // tableView
    
    //MARK: ---------------Variables-------------
    
    var dataSource : TableViewDataSource?
    var driverData :DriverModel?
    let dropDown = DropDown()
    let dropDownCustom = DropDown()
    let dropDownCategory = DropDown()
    
    var selectedService = 0
    var categoryIdSelected = 0
    var brandIdSelected = 0
    var arrIndxSelected:[Int] = []
    var arr:[String] = []
    var categoryIdsToSend = ""
    
    var arrCategory:[String] = []
    var arrIds:[String] = []
    var arrIndxSelectedCategory:[Int] = []
    var dummyCatProductId: Int?
    var products :[Products]?
    var arrbrands :[Brands]?
    var selectedRow:Int?
    
    //MARK: ---------------ViewController LifeCycle Functions-------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewServiceLine.allowsMultipleSelection = false
        self.initialSetup()
        self.configureTableView()
    }
    
    //MARK: ---------------Custom Methods-------------
    
    
    func initialSetup(){
        //localised  strings
        
        self.navigationItem.title = "signup2.title".localized
        
        if let font = UIFont(name: LocalKeys.MontserratAlternativesBold, size: 16) {
            let lbl = UILabel()
            // lbl.backgroundColor = UIColor.black
            lbl.text = "signup2.title".localized
            lbl.textColor = UIColor.black
            lbl.font = font
            lbl.sizeToFit()
            navigationItem.titleView = lbl            
        }
        
        self.lblserviceinfo.text = "signup2.txtServicesInfo".localized
        self.lblBrandInfo.text = "signup2.txtBrandInfo".localized
        self.lblProductType.text = "signup3.productInfo".localized
        
        // for semantics
        self.view.setTextFields(mainView: self.view.subviews)
        self.rotateButton(button: btnNext)
        self.rotateButton(button: btnBack)
        
        // show default dropdown values initially
        lblServices.text = DriverDataAtOtp.data?.seletedServices?.first?.name
        categoryIdSelected = (/DriverDataAtOtp.data?.seletedServices?.first?.category_id)
        
        if  DriverDataAtOtp.data?.seletedServices?.first?.default_brands == "1" {
            anchorViewBrand.isHidden = true
        }
        
        self.setupDropDownViewCategory()
        
        arrbrands = DriverDataAtOtp.data?.seletedServices?.first?.brands
        
        //kirti
        //        lblBrand.text = DriverDataAtOtp.data?.seletedServices?.first?.brands?.first?.name
        //        brandIdSelected = (/DriverDataAtOtp.data?.seletedServices?.first?.brands?.first?.category_brand_id)
        //        self.getBrandProducts(CategoryBrandId: brandIdSelected)
        
        arrIndxSelected.append(0)
        
        // anchorViewProductType.isHidden = !(/DriverDataAtOtp.data?.isCabSerive)
        
        guard let obj = DriverDataAtOtp.data?.seletedServices?.first else { return }
        setServices(index: 0, obj: obj)
    }
    
    func setupViewCategoryTypeInitially(){
        lblCategoryTypes.text = ""
        self.arrCategory = []
        self.arrIds = []
        self.arrIndxSelectedCategory = []
        
        for (index, item) in (DriverDataAtOtp.product ?? []).enumerated(){
            arrCategory.append("\(/item.name)")
            arrIds.append("\(/item.category_brand_product_id)")
            //arrIndxSelectedCategory.append(index)
        }
        
        categoryIdsToSend = arrIds.joined(separator: ",")
        categoryIdsToSend.insert("[", at: categoryIdsToSend.startIndex)
        categoryIdsToSend.insert("]", at: categoryIdsToSend.endIndex)
        
        //        let strAllText = arrCategory.joined(separator: ",")
        //        lblCategoryTypes.text = strAllText
        
        setupDropDownViewCategory()
    }
    
    // used to hide dropdown
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dropDown.hide()
        dropDownCategory.hide()
    }
    
    //move to upload licesnse View controller
    func moveToUploadLicenseViewController() {
        
        guard let vc = R.storyboard.loginSignUp.uploadLicenseViewController() else {return}
        vc.driverData = DriverDataAtOtp.data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selectServices(){
        dropDown.anchorView = anchorViewService
        var arr:[String] = []
        for item in (DriverDataAtOtp.data?.seletedServices) ?? []{
            arr.append(/item.name)
        }
        
        dropDown.dataSource = arr
        
        if let font = UIFont(name: LocalKeys.MontBold, size: 14) {
            dropDown.textFont = font
            dropDown.textColor = UIColor.darkText
            
        }
        
        dropDown.show()
        //selection block of drop down
        dropDown.selectionAction = {
            [weak self] (index: Int, item: String) in
            guard let self = self, let obj = DriverDataAtOtp.data?.seletedServices?[index] else { return }
            self.setServices(index: index, obj: obj)
        }
    }
    
    func setServices(index: Int, obj: Services) {
        self.selectedService = index
        //self.categoryIdSelected = 7
        self.categoryIdSelected = /obj.category_id
        self.lblServices.text = obj.name
        
        // automatically select brand first objrct
        
        self.arrIndxSelected = []
        
        self.anchorViewBrand.isHidden = false
        
        if /obj.default_brands == "1" {
            self.anchorViewBrand.isHidden = true
        }
        
        arrbrands = obj.brands
        dataSource?.items = arrbrands
        tableViewServiceLine.reloadData()
        
        //kirti
        //        self.lblBrand.text = (/obj.brands?.first?.name)
        //        self.brandIdSelected = (/obj.brands?.first?.category_brand_id)
        //        self.getBrandProducts(CategoryBrandId: self.brandIdSelected)
        //        self.imgView.kf.setImage(with: URL.init(string: /obj.brands?.first?.image_url), placeholder: nil , options: nil, progressBlock: nil, completionHandler: nil)
        //        self.arrIndxSelected.append(0)
        //        self.dropDown.hide()
        //
        //   self.anchorViewProductType.isHidden = (DriverDataAtOtp.data?.isCabSerive(idCat: self.categoryIdSelected) ?? false)
        
    }
    
    func configureCheckBoxDropDown(){
        
        dropDownCustom.bottomOffset = CGPoint(x: 0, y:(/dropDownCustom.anchorView?.plainView.bounds.height))
        dropDownCustom.direction = .bottom
        
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        dropDownCustom.cellNib = UINib(nibName: "View", bundle: nil)
        dropDownCustom.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropDownCell else { return }
            cell.lblTitle.text = self.arr[index]
            
            if self.arrIndxSelected.contains(index){
                
                cell.btnCheckBox.setImage(#imageLiteral(resourceName: "ic_checkbox_on"), for: .normal)
            }
            else{
                cell.btnCheckBox.setImage(#imageLiteral(resourceName: "ic_checkbox"), for: .normal)
            }
            
            //uncomment below to perform only action on check
            //            cell.btnCheckBox.addTarget(self, action: #selector(self.actionDropDownCheckBox), for: .touchUpInside)
        }
    }
    
    //MARK: setup dropDown view initially
    func setupDropDownViewCategory(){
        
        dropDownCategory.anchorView = anchorViewProductType
        dropDownCategory.dataSource = arrCategory
        dropDownCategory.bottomOffset = CGPoint(x: 0, y:(dropDownCategory.anchorView?.plainView.bounds.height)!)
        dropDownCategory.direction = .bottom
        dropDownCategory.dismissMode = .automatic
        
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        dropDownCategory.cellNib = UINib(nibName: "View", bundle: nil)
        dropDownCategory.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropDownCell else { return }
            cell.lblTitle.text = self.arrCategory[index]
            
            if self.arrIndxSelectedCategory.contains(index){
                cell.btnCheckBox.setImage(#imageLiteral(resourceName: "ic_checkbox_on"), for: .normal)
            }
            else{
                cell.btnCheckBox.setImage(#imageLiteral(resourceName: "ic_checkbox"), for: .normal)
            }
            //uncomment below to add action  only on check box
            //            cell.btnCheckBox.addTarget(self, action: #selector(self.actionDropDownCheckBox), for: .touchUpInside)
        }
    }
    
    func dropDownSelectionHandlerCategory(){
        dropDownCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            if !self.arrIndxSelectedCategory.contains(index){
                self.arrIndxSelectedCategory.append(index)
            }
            else{
                //                if  (self.arrIndxSelectedCategory.count > 1){
                //                    self.arrIndxSelectedCategory = self.arrIndxSelectedCategory.filter { $0 != index }
                //                }
                //                else{
                //                    Alerts.shared.showOnTop(alert: R.string.localizable.signup2ProductType(), message: R.string.localizable.signup2ProductTypeOne(), type: .info)
                //                }
            }
            var arrTextValues = [String]()
            var arrCategoryIdtosend :[String] = []
            
            for (_,val) in self.arrIndxSelectedCategory.enumerated(){
                arrTextValues.append(self.arrCategory[val])
                arrCategoryIdtosend.append(self.arrIds[val])
            }
            
            //self.lblCategoryTypes.text = arrTextValues.joined(separator: ",")
            self.lblCategoryTypes.text = item
            self.categoryIdsToSend = ""
            
            self.categoryIdsToSend = arrCategoryIdtosend.joined(separator: ",")
            self.categoryIdsToSend.insert("[", at: self.categoryIdsToSend.startIndex)
            self.categoryIdsToSend.insert("]", at: self.categoryIdsToSend.endIndex)
            self.dropDownCategory.reloadAllComponents()
            self.dropDownCategory.hide()
        }
        
        dropDownCategory.show()
    }
    
    func dropDownSelectionHandler(){
        
        dropDownCustom.anchorView = anchorViewBrand
        arr = []
        let obj: Services = (DriverDataAtOtp.data?.seletedServices?[selectedService])!
        for item in obj.brands ?? []{
            arr.append(/item.name)
        }
        dropDownCustom.dataSource = arr
        self.configureCheckBoxDropDown()
        
        dropDownCustom.selectionAction = { [unowned self] (index: Int, item: String) in
            self.brandIdSelected = (/obj.brands?[index].category_brand_id)
            self.getBrandProducts(CategoryBrandId: self.brandIdSelected)
            self.setupViewCategoryTypeInitially()
            
            self.arrbrands = obj.brands
            
            //self.lblBrand.text = (/obj.brands?[index].name)
            
            let url = /obj.brands?[index].image_url
            self.imgView.kf.setImage(with: URL.init(string: url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
            self.arrIndxSelected.removeAll()
            self.arrIndxSelected.append(index)
            self.dropDownCustom.reloadAllComponents()
            self.dropDownCustom.hide()
        }
        
        dropDownCustom.show()
    }
    
    @objc func actionDropDownCheckBox(){
        
    }
    
    //MARK: ---------------Button Actions-------------
    
    @IBAction func actionBack(_ sender: Any) {
        
        if let nav = self.navigationController{
            for controller in nav.viewControllers.reversed(){
                if controller.isKind(of: SignUpViewController.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
                if controller.isKind(of: PhoneSignUpViewController.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func actionSelectServices(_ sender: UIButton) {
        
        self.selectServices()
    }
    
    @IBAction func actionSelectBrand(_ sender: UIButton) {
        
        self.dropDownSelectionHandler()
    }
    
    @IBAction func actionCategoryType(_ sender: Any) {
        
        self.dropDownSelectionHandlerCategory()
    }
    
    @IBAction func actionNextSignUpController(_ sender: Any) {
        addBankDetailsAndServices()
        
        //        let vc = R.storyboard.main.mapViewController()
        //        let nav = UINavigationController(rootViewController: vc!)
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //        appDelegate.window?.rootViewController = nav
    }
    
    //MARK: ---------------API REQUESTs -------------
    
    func addBankDetailsAndServices(){
        
        //        if lblCategoryTypes.text == "" {
        //            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.signup2ProductTypeVaildation(), type: .info)
        //            return
        //        }
        
        
        let objR = LoginEndpoint.addServicesToOffer(serviceId: "\(categoryIdSelected)", categoryBrandId: brandIdSelected, category_brand_product_ids: self.categoryIdsToSend)
        
        APIManager.shared.request(with: objR) {
            [weak self] (resp) in
            
            switch resp {
                
            case .success(let response_value ):
                
                let resp = response_value  as? DriverModel
                self?.driverData = resp
                DriverDataAtOtp.data = resp
                UserDefaults.standard.set(self?.driverData?.products?.first?.category_brand_product_id, forKey: "category_brand_product_id")
                UserDefaults.standard.set(self?.driverData?.data?.online_status, forKey: "online_status")
                //MARK:- commented by heeba
               // UserDefaults.standard.set(self?.driverData?.data?.directional_hire, forKey: "directionalHire_status")
                //  self?.moveToUploadLicenseViewController()
                guard let vc = R.storyboard.main.mapViewController() else { return }
                self?.navigationController?.pushViewController(vc, animated: true)
                
            case .failure(let err):
                
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: err ?? "", type: .info)
            }
        }
    }
    
    func getBrandProducts(CategoryBrandId: Int?){
        
        APIManager.shared.request(with: CommonEndPoint.getBrandProdcuts(category_brand_id: CategoryBrandId)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response{
                
            case .success(let response_value):
                let dic = response_value as? BrandProducts
                DriverDataAtOtp.product = dic?.products
                self.products = dic?.products
                
                self.arrIds = []
                
                self.arrIds.append(/self.products?[0].category_brand_product_id?.toString())
                
                print(self.arrIds)
                self.categoryIdsToSend = self.arrIds.joined(separator: ",")
                
                self.categoryIdsToSend.insert("[", at: self.categoryIdsToSend.startIndex)
                self.categoryIdsToSend.insert("]", at: self.categoryIdsToSend.endIndex)
                
                //self.setupViewCategoryTypeInitially()
                
                //                if (DriverDataAtOtp.data?.isCabSerive(idCat: self.categoryIdSelected) ?? false), let id = DriverDataAtOtp.product?.first?.category_brand_product_id {
                //                    self.categoryIdsToSend = "[\(id)]"
                //                    self.lblCategoryTypes.text = /DriverDataAtOtp.product?.first?.name
                //                    self.dummyCatProductId = /DriverDataAtOtp.product?.first?.category_brand_product_id
                //                }
                
            case .failure(let err)  :
                Alerts.shared.showOnTop(alert: "alert.alert".localized, message: /err, type: .info)
            }
        }
    }
}

extension ServicesOfferViewController {
    //MARK:- Configuring table view cell
    func configureTableView(){
        
        dataSource = TableViewDataSource.init(items: arrbrands, tableView: tableViewServiceLine, cellIdentifier: "ServiceLineTableCell", cellHeight: UITableViewAutomaticDimension, configureCellBlock: { [weak self](cell, item, indexPath) in
            
            guard let cell = cell as? ServiceLineTableCell else {return}
            
            if self?.selectedRow != nil {
                if /self?.selectedRow == indexPath.row{
                    cell.isChecked = true
                    cell.btnCheckBox.isSelected = true
                }
                else{
                    cell.isChecked = false
                    cell.btnCheckBox.isSelected = false
                }
            }
            
            
            cell.lblServiceName?.text = self?.arrbrands?[/indexPath.row].name
            cell.btnCheckBox.tag = /indexPath.row
            }, aRowSelectedListener: {[weak self] (indexPath, cell) in
                
                self?.selectedRow = indexPath.row
                self?.tableViewServiceLine.reloadData()
                self?.brandIdSelected = /self?.arrbrands?[/indexPath.row].category_brand_id
                
                print(self?.brandIdSelected)
                self?.getBrandProducts(CategoryBrandId: /self?.brandIdSelected)
                
            }, willDisplayCell: {(indexPath, cell) in
                
        })
        tableViewServiceLine?.delegate = dataSource
        tableViewServiceLine?.dataSource = dataSource
        tableViewServiceLine?.reloadData()
    }
}
