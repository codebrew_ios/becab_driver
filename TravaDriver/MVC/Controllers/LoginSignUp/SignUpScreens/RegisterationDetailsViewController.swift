//
//  RegisterationDetailsViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 07/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
var accountKey = "accountStep"

enum StepController : Int {
    
    case uploadLicenseViewController = 1
    case vehicleDetailsViewController = 2
    case uploadPersonalDocViewController = 3
    
}
class RegisterationDetailsViewController: UIViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var containerView: UIView!
    
    var controllerType : StepController = .uploadLicenseViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
        initialSetup()
    }
}

extension RegisterationDetailsViewController {
    
    func initialSetup() {
        
        if let value = (UserDefaults.standard.value(forKey: accountKey) as? Int) {
            let type = StepController(rawValue: value)
            switch type {
            case .uploadLicenseViewController:
                guard let vc = R.storyboard.loginSignUp.uploadLicenseViewController() else{return}
                displayContentController(content: vc)
                setProgress(1.0/3.0)
                self.controllerType = .uploadLicenseViewController
            case .vehicleDetailsViewController :
                guard let vc = R.storyboard.loginSignUp.vehicleDetailsViewController() else{return}
                displayContentController(content: vc)
                setProgress(1.0 - 1.0/3.0)
                self.controllerType = .vehicleDetailsViewController
            case .uploadPersonalDocViewController :
                guard let vc = R.storyboard.loginSignUp.uploadPersonalDocViewController() else{return}
                displayContentController(content: vc)
                setProgress(1.0)
                self.controllerType = .uploadPersonalDocViewController
            default :
                return
            }
        }else{
            guard let vc = R.storyboard.loginSignUp.uploadLicenseViewController() else{return}
            displayContentController(content: vc)
            setProgress(1.0/3.0)
            self.controllerType = .uploadLicenseViewController
        }
    }
    
    func displayContentController(content: UIViewController) {
        
        addChildViewController(content)
        content.view.frame = containerView.bounds
        containerView.addSubview(content.view)
        
        content.didMove(toParentViewController: self)
    }
    
    func hideContentController(content: UIViewController) {
        content.willMove(toParentViewController: nil)
        content.view.removeFromSuperview()
        content.removeFromParentViewController()
    }
    
    
    func setProgress(_ progress: Float) {
        
        progressView.setProgress(progress, animated: true)
    }
}

//MARK:- Button Selectors
extension RegisterationDetailsViewController {
    
    @IBAction func buttonContinueClicked(_ sender: Any) {
        
        guard let topVC = childViewControllers.last else {return}
        
        switch topVC {
        case is UploadLicenseViewController:
            debugPrint("Move to VehicleDetailsViewController")
            
            (topVC as? UploadLicenseViewController)?.validateForm(completion: { [weak self] (success) in
                
                if /success {
                    // UserDefaults.standard.set(2, forKey: accountKey)
                    guard let vc = R.storyboard.loginSignUp.vehicleDetailsViewController() else{return}
                    self?.displayContentController(content: vc)
                    
                    self?.setProgress(1.0 - 1.0/3.0)
                }
            })
            
        case is VehicleDetailsViewController:
            debugPrint("Move to UploadPersonalDocViewController")
            (topVC as? VehicleDetailsViewController)?.validateForm(completion: { [weak self] (success) in
                
                if /success {
                  //  UserDefaults.standard.set(3, forKey: accountKey)
                    guard let vc = R.storyboard.loginSignUp.uploadPersonalDocViewController() else{return}
                    self?.displayContentController(content: vc)
                    
                    self?.setProgress(1.0)
                }
            })
            
        case is UploadPersonalDocViewController:
            debugPrint("Move to UploadPersonalDocViewController")
            (topVC as? UploadPersonalDocViewController)?.validateForm(completion: { [weak self] (success) in
                if /success {
                    //    UserDefaults.standard.set(4, forKey: accountKey)
//                    guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else{return}
//                    vc.driverData = DataResponse.driverData
//                    self?.navigationController?.pushViewController(vc, animated: true)
                    if let vc = R.storyboard.services.servicesViewController(){
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            })
            
        default:
            debugPrint("No move further")
        }
        
        stopAnimating()
//        guard let vc = R.storyboard.loginSignUp.vehicleDetailsViewController() else{return}
//        displayContentController(content: vc)
//
//        progressView.setProgress((1.0 - 1/3), animated: true)
    }
    
     @IBAction func buttonBackClicked(_ sender: Any) {
        
        guard let topVC = childViewControllers.last else {return}
        
        switch topVC {
        case is UploadPersonalDocViewController:
            debugPrint("Move to back")
            if controllerType == .uploadPersonalDocViewController {
                return
            }
            hideContentController(content: topVC)
            setProgress(1.0 - 1.0/3.0)

            
        case is VehicleDetailsViewController:
            debugPrint("Move to UploadLicenseViewController")
            if controllerType == .vehicleDetailsViewController {
                return
            }
            hideContentController(content: topVC)
            setProgress(1.0/3.0)
            
        case is UploadLicenseViewController:
            debugPrint("Move to home")
            if controllerType == .uploadLicenseViewController {
                
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneSignUpViewController") as? PhoneSignUpViewController
                               else {return}
                           self.navigationController?.pushViewController(vc, animated: true)
                return
            }
             hideContentController(content: topVC)
           
            
        default:
            debugPrint("No move further")
        }
        
        
       /* childViewControllers.forEach { (vc) in
            
            if !(vc is UploadLicenseViewController) {
                hideContentController(content: vc)
            }
        } */
    }
}



