//
//  UploadPersonalDocViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 07/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class UploadPersonalDocViewController: UIViewController {
    
    //MARK:- Enum
    enum Section: Int {
        
        case addressProof = 0
      //  case bilingProof
        case obligationLetter
        
        func getTitle() -> String {
            
            switch self {
                
            case .addressProof:
                return "ADD ADDRESS PROOF"
                
//            case .bilingProof:
//                return "BILLING PROOF FOR ADDRESS"
                
            case .obligationLetter:
                return "OBLIGATION LETTER"
            }
        }
    }
    
    enum SelectImageType {
        case profile
        case IDFront
        case IDBack
        case addressProof
       // case bilingProof
        case obligationLetter
    }
    
    
    //MARK:- Outlet
    @IBOutlet weak var addPicButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonIDFront: UIButton?
    @IBOutlet weak var buttonRemoveFront: UIView!
    
    @IBOutlet weak var buttonIDBack: UIButton?
    @IBOutlet weak var buttonRemoveBack: UIButton?
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var fullNameTextfield: AnimatableTextField!
    @IBOutlet weak var tfEmail: AnimatableTextField!
    @IBOutlet weak var referralField: AnimatableTextField!
    
    //MARK:- Properties
    var imageType: SelectImageType?
    var addressProof:[ImageObject] = []
    var billingProof:[ImageObject] = []
    var obligationLetter:[ImageObject] = []
    var addressProofImages:[UIImage] = []
    var billingProofImages:[UIImage] = []
    var obligationLetterImages:[UIImage] = []
    var isVehicleFrontUploaded: Bool = false
    var isVehicleBackUploaded: Bool = false
    var isProfileUploaded : Bool = false
    var dataSource : TableViewDataSourceWithHeader? {
        didSet{
            tableView.delegate = dataSource
            tableView.dataSource = dataSource
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
}


extension UploadPersonalDocViewController {
    
    func initialSetup() {
        configureTableView()
        
        [fullNameTextfield].forEach { field in
            field?.autocapitalizationType = .allCharacters
        }
    }
    
    func configureTableView(){
        
        tableView.register(UINib(nibName: R.reuseIdentifier.uploadedDocCell.identifier, bundle:nil), forCellReuseIdentifier: R.reuseIdentifier.uploadedDocCell.identifier)
        
        tableView.register(UINib(nibName: R.reuseIdentifier.uploadDocHeaderCell.identifier, bundle:nil), forCellReuseIdentifier: R.reuseIdentifier.uploadDocHeaderCell.identifier)
        
        /* tableView.register(UINib(nibName: R.reuseIdentifier.uploadDocHeaderCell.identifier, bundle:nil), forHeaderFooterViewReuseIdentifier: R.reuseIdentifier.uploadDocHeaderCell.identifier) */
        
        dataSource = TableViewDataSourceWithHeader(items: [addressProof,obligationLetter], tableView: tableView, cellIdentifier: R.reuseIdentifier.uploadedDocCell.identifier , cellHeight: 0.0, headerHeight: 0.0, configureCellBlock: { (cell, item, indexPath) in
            
            guard let _cell = cell as? UploadedDocTableViewCell else {return}
            _cell.delegate = self
            _cell.deleteButton.row = indexPath.row
            _cell.deleteButton.section = indexPath.section
            let type = UploadPersonalSectionType(rawValue: indexPath.section)
            switch type {
            case .addressProof :
                _cell.uploadedImageView.image = self.addressProof[indexPath.row].image
                _cell.fileNameLabel.text = self.addressProof[indexPath.row].fileName
//            case .bilingProof :
//                _cell.uploadedImageView.image = self.billingProof[indexPath.row].image
//                _cell.fileNameLabel.text = self.billingProof[indexPath.row].fileName
            case .obligationLetter :
                _cell.uploadedImageView.image = self.obligationLetter[indexPath.row].image
                _cell.fileNameLabel.text = self.obligationLetter[indexPath.row].fileName
                
            default :
                break
            }
            //   _cell.uploadedImageView.image =
            _cell.labelTitle.isHidden = indexPath.row > 0
            
        }, viewForHeader: {[weak self] (section) -> UIView? in
            
            guard let cell = self?.tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.uploadDocHeaderCell.identifier ) as? UploadDocHeaderTableViewCell else { return UIView() }
            
            cell.sectionIndex = section
            cell.delegate = self
            
            cell.labelTitle.text = Section(rawValue: section)?.getTitle()
            cell.buttonUpload.setTitle("Upload Certificate", for: .normal)
            //            if let section = Section(rawValue: section), section == .addressProof {
            //                if self?.addressProof.count == 0{
            //                    cell.buttonUpload.setImage(UIImage(named: "ic_add_Icon"), for: .normal)
            //                }else{
            //                    cell.buttonUpload.setImage(UIImage(named: "ic_success"), for: .normal)
            //                     cell.buttonUpload.setTitle("Uploaded", for: .normal)
            //                }
            //            }else if let section = Section(rawValue: section), section == .bilingProof  {
            //                if self?.billingProof.count == 0{
            //                    cell.buttonUpload.setImage(UIImage(named: "ic_add_Icon"), for: .normal)
            //                }else{
            //                    cell.buttonUpload.setImage(UIImage(named: "ic_success"), for: .normal)
            //                    cell.buttonUpload.setTitle("Uploaded", for: .normal)
            //                }
            //            }else{
            //                if self?.obligationLetter.count == 0{
            //                    cell.buttonUpload.setImage(UIImage(named: "ic_add_Icon"), for: .normal)
            //                }else{
            //                    cell.buttonUpload.setImage(UIImage(named: "ic_success"), for: .normal)
            //                     cell.buttonUpload.setTitle("Uploaded", for: .normal)
            //                }
            //            }
            // let itemTempArray = (items as? [ScoreData])
            // cell?.labelTitle.text = itemTempArray?[section].title
            
            return cell
            
            }, viewForFooter: { (section) -> UIView? in
                return UIView()
        }, aRowSelectedListener: { (indexPath, cell) in
            
            
        }, willDisplayCell: { (indexPath, cell) in
            
            
        }, scrollView: { (scrollView) in
            
            
        })
        
    }
    
    func reloadData() {
        self.configureTableView()
        self.tableView.reloadData()
    }
    
    func openPhotoOptionsActionSheet() {
        self.view.endEditing(true)
        
        CameraImage.shared.captureImage(
            from: self, navigationController: self.navigationController,
            mediaType: nil,
            captureOptions: [.camera, .photoLibrary],
            allowEditting: true) {
                
                [weak self] (image,fileName) in
                guard let img = image,
                    let type = self?.imageType else { return }
                let obj = ImageObject()
                obj.image = img
                obj.fileName = fileName
                switch type {
                    
                case .profile:
                    self?.addPicButton.setTitle("Change", for: .normal)
                    self?.imgView.image = img
                    self?.isProfileUploaded = true
                case .IDBack:
                    self?.buttonIDBack?.setImage(img, for: .normal)
                    self?.buttonRemoveBack?.isHidden = false
                    self?.isVehicleBackUploaded = true
                    
                case .IDFront:
                    
                    self?.buttonIDFront?.setImage(img, for: .normal)
                    self?.buttonRemoveFront?.isHidden = false
                    self?.isVehicleFrontUploaded = true
                    
                case .addressProof:
                    self?.addressProof.append(obj)
                    self?.addressProofImages.append(img)
                    self?.reloadData()
//                case .bilingProof:
//                    debugPrint("bilingProof")
//                    self?.billingProof.append(obj)
//                    self?.billingProofImages.append(img)
//                    self?.reloadData()
                case .obligationLetter:
                    debugPrint("obligationLetter")
                    self?.obligationLetter.append(obj)
                    self?.obligationLetterImages.append(img)
                    self?.reloadData()
                    
                }
        }
    }
    
    func validateForm(completion: @escaping (_ success: Bool?) -> () ) {
        
        /* let isCabS = /DriverDataAtOtp.data?.isCabSerive
         
         if isCabS {
         let strCarModel = /txtCarModel?.text?.trim()
         if strCarModel.isEmpty {
         Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup3.tfCarModelPlaceholder".localized , type: .info)
         return
         }
         
         if selectedColor.isEmpty {
         Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup3.tfCarColorPlaceholder".localized , type: .info)
         return
         }
         } */
        if isProfileUploaded == false {
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please upload Driver Picture".localized , type: .info)
            return
        }
        if fullNameTextfield?.text?.trim() == "" {
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please enter your Full Name".localized , type: .info)
            return
        }
//        else if tfEmail?.text?.trim() == "" {
//            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.email".localized , type: .info)
//            return
//        }
        
        
        
//        else if !isVehicleFrontUploaded || !isVehicleBackUploaded {
//            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please upload Government ID Card front and back image.".localized , type: .info)
//            return
//        }
//        else if addressProof.count == 0{
//            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please upload Address Proof".localized , type: .info)
//            return
//        }
//        else if billingProof.count == 0{
//            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please upload Billing Proof for Address".localized , type: .info)
//            return
//        }
//        else if obligationLetter.count == 0{
 //           Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please upload Obligation Letter".localized , type: .info)
  //          return
  //      }
        
        self.uploadStepThirdData { (success) in
            completion(true)
        }
    }
    
    func uploadStepThirdData(completion: @escaping (_ success: Bool?) -> ()) {
        
        var vehicleImgs:[UIImage] = []
        if let img = self.buttonIDFront?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        if let img = self.buttonIDBack?.image(for: .normal) {
            vehicleImgs.append(img)
        }
        let imagesData:[String:[UIImage]] = ["address_proof_docs":addressProofImages //,"billing_proof_docs":billingProofImages
            ,"obligated_letter_docs":obligationLetterImages
          // ,"government_id_images":vehicleImgs
            , "profile_pic":[(imgView?.image ?? UIImage())]]
        APIManager.shared.request(withMultipleImages: LoginEndpoint.uploadProfileStep3(name: fullNameTextfield.text?.trim() ?? "", email: tfEmail.text?.trim() ?? "",referral_code:referralField.text), images: imagesData) { (response) in
            completion(true)
        }
    }
}


extension UploadPersonalDocViewController {
    
    @IBAction func buttonRemoveFrontClicked(_ sender: UIButton) {
        buttonIDFront?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveFront?.isHidden = true
        isVehicleFrontUploaded = false
    }
    
    @IBAction func buttonRemoveBackClicked(_ sender: UIButton) {
        
        buttonIDBack?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
        buttonRemoveBack?.isHidden = true
        isVehicleBackUploaded = false
    }
    
    @IBAction func buttonFrontIDClicked(_ sender: UIButton) {
        
        imageType = .IDFront
        self.openPhotoOptionsActionSheet()
    }
    
    
    @IBAction func buttonBackIDClicked(_ sender: UIButton) {
        imageType = .IDBack
        self.openPhotoOptionsActionSheet()
    }
    
    @IBAction func buttonChangeImageClicked(_ sender: Any) {
        
        imageType = .profile
        self.openPhotoOptionsActionSheet()
    }
}

//MARK:- UploadDocHeaderTableViewCellDelegate
extension UploadPersonalDocViewController : UploadDocHeaderTableViewCellDelegate {
    
    func uploadCertificateClicked(section: Int?) {
        
        guard let type = Section(rawValue: /section) else {return}
        
        switch type {
            
        case .addressProof:
            imageType = .addressProof
            openPhotoOptionsActionSheet()
            
//        case .bilingProof:
//            imageType = .bilingProof
//            openPhotoOptionsActionSheet()
            
        case .obligationLetter:
            imageType = .obligationLetter
            openPhotoOptionsActionSheet()
        }
    }
}


extension UploadPersonalDocViewController : UploadedDocTableViewCellDelegate {
    func deleteDoc(indexPath: IndexPath) {
        let type = UploadPersonalSectionType(rawValue: indexPath.section)
        switch type {
        case .addressProof:
            if indexPath.row < self.addressProof.count {
                self.addressProof.remove(at: indexPath.row)
            }
//        case .bilingProof:
//            if indexPath.row < self.billingProof.count {
//                self.billingProof.remove(at: indexPath.row)
//            }
        case .obligationLetter:
            if indexPath.row < self.obligationLetter.count {
                self.obligationLetter.remove(at: indexPath.row)
            }
        default:
            break
        }
        self.reloadData()
        
    }
    
    
}

enum UploadPersonalSectionType : Int {
    
    case addressProof = 0
  ///  case bilingProof = 1
    case obligationLetter = 1
}
