//
//  SelectColorCollectionCell.swift
//  Buraq24Driver
//
//  Created by Sandeep Kumar on 03/06/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class SelectColorCollectionCell: UICollectionViewCell {

    //MARK:- ======== Outlets ========
    @IBOutlet weak var imageTick: UIImageView!

    //MARK:- ======== Variables ========
    var isSelect: Bool  = false {
        didSet {
            imageTick.isHidden = !isSelect
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        isSelect = false
    }

}
