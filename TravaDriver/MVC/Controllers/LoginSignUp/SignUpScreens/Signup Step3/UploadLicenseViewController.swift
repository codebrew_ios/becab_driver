//
//  UploadLicenseViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 08/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import DropDown
import ActionSheetPicker_3_0
import IBAnimatable
import GooglePlaces
import CoreLocation

class UploadLicenseViewController: BaseController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: --Outlets
    
    @IBOutlet weak var addressTextFiled: AnimatableTextField!
    @IBOutlet weak var tfLicenseNumber: UITextField!
    @IBOutlet weak var tfExpirydate: UITextField!
    @IBOutlet weak var viewPan: UIView?
    @IBOutlet weak var viewAadhar: UIView?
    @IBOutlet weak var viewOthers: UIView?
    @IBOutlet weak var labelDrivingLicence: UILabel?
    
    @IBOutlet weak var lblFrontPan: UILabel?
    @IBOutlet weak var lblBackPan: UILabel?
    @IBOutlet weak var lblFrontAadhar: UILabel?
    @IBOutlet weak var lblBackAadhar: UILabel?
    @IBOutlet weak var lblFrontOthers: UILabel?
    @IBOutlet weak var lblBackOthers: UILabel?

    
    @IBOutlet weak var btnLicenseFront: UIButton!
    @IBOutlet weak var btnLicenseBack: UIButton!
    @IBOutlet weak var btnPanFront: UIButton?
    @IBOutlet weak var btnPanBack: UIButton?
    @IBOutlet weak var btnAadharFront: UIButton?
    @IBOutlet weak var btnAadharBack: UIButton?
    @IBOutlet weak var btnOtherDocsFront: UIButton?
    @IBOutlet weak var btnOtherDocsBack: UIButton?
    @IBOutlet weak var btnNext: UIButton?
    @IBOutlet weak var btnRemove2: UIButton!
    @IBOutlet weak var btnRemove1: UIButton!
    
    @IBOutlet weak var btnRemove3: UIButton?
    @IBOutlet weak var btnRemove4: UIButton?
    @IBOutlet weak var btnRemove5: UIButton?
    @IBOutlet weak var btnRemove6: UIButton?
    @IBOutlet weak var btnRemove7: UIButton?
    @IBOutlet weak var btnRemove8: UIButton?
    
    @IBOutlet weak var btnBack: UIButton?
    
    @IBOutlet weak var lblCategoryTypes: UILabel?
    @IBOutlet weak var dropDownAnchorView: CustomView?
    
    @IBOutlet weak var lblInfoType: UILabel?
    
    @IBOutlet weak var tfBankName: UITextField?
    @IBOutlet weak var tfBankAccountNumber: UITextField?
    
    @IBOutlet weak var viewCarModel: UIView?
    @IBOutlet weak var txtCarModel: UITextField?
    @IBOutlet weak var tableView: UITableView?
    
    @IBOutlet weak var viewCarColor: UIView?
//    @IBOutlet weak var collectionViewCarColor: UICollectionView! {
//        didSet {
//            configureCollectionView()
//        }
//    }
    
    //localised
    @IBOutlet weak var lblExpiryInfo: UILabel!
    @IBOutlet weak var lblMulkiyaUpload1: UILabel!
    @IBOutlet weak var lblMulkiyaUpload2: UILabel!
    @IBOutlet weak var lblFront: UILabel!
    @IBOutlet weak var lblBack: UILabel!
    
    @IBOutlet weak var colorImg: UIImageView?
    @IBOutlet weak var lblColor: UILabel?
    
    @IBOutlet weak var btnSelectColors: UIButton?
    //MARK: --Variables
    var homeAddress:Address?
    var dataSource : TableViewDataSource?
    var driverData :DriverModel?
    let picker = UIImagePickerController()
    let dropDownCustom = DropDown()
    var selectedlicenseFace = 0
    enum Documents :String{
        case license;
        case pan;
        case aadhar;
        case otherDocs;
    }
    var documents : Documents = Documents.license
    var categoryIdsToSend = ""
    var countImage = 0
    let formatter = DateFormatter()
    var dateStringServer = String()
    
    
    var arr:[String] = []
    var arrIds:[String] = []
    var arrIndxSelected:[Int] = []
    
    
//    var dataSourceCollection: SKCollectionViewDataSource?
    var arrayColors: [String] = [
        "FFFFFF"
        ,"808080"
        ,"008000"
        ,"FF0000"
        ,"0000FF"
        ,"FFFF00"
        ]
    
    var arrayColorsName: [String] = [
    "White","Gray","Green","Red","Blue","Yellow"
    ]
    
    var selectedColor: String = ""

    //MARK: --ViewController LifeCycle Functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.getCurrentContryCode()
        self.initialSetup()
    }
    
    
    //MARK: --Custom Methods
    
    func initialSetup() {
        tfLicenseNumber.delegate = self
        tfLicenseNumber.autocapitalizationType = .allCharacters
        self.localiseStrings()

        btnRemove1.isHidden = true
        btnRemove2.isHidden = true
        
        // for semantics
        self.view.setTextFields(mainView: self.view.subviews)
      //  self.rotateButton(button: btnNext)
      //  self.rotateButton(button: btnBack)
        
        picker.delegate = self
        
        let isCabS = /DriverDataAtOtp.data?.isCabSerive
        viewCarColor?.isHidden = !isCabS
        viewCarModel?.isHidden = !isCabS
//        dataSource?.items = arrayColors
//        tableView?.reloadData()
        configureTableView()
    }
    
    func localiseStrings(){
        //localised strigs
        
        self.navigationItem.title = "signup3.title" .localized
        
        if let font = UIFont(name: LocalKeys.MontserratAlternativesBold, size: 16) {
            let lbl = UILabel()
            lbl.backgroundColor = UIColor.black
            lbl.text = "signup3.title" .localized
            lbl.textColor = UIColor.white
            lbl.font = font
            lbl.sizeToFit()
            navigationItem.titleView = lbl
        }
        
       // self.tfLicenseNumber.placeholder =  "signup3.tfLicensePlaceholder".localized
        self.lblExpiryInfo.text = "signup3.expiryInfo".localized
      //  self.lblMulkiyaUpload1.text = "signup3.uploadInfo1".localized
      //  self.lblMulkiyaUpload2.text = "signup3.uploadInfo2".localized
        self.lblBack.text = "signup3.Back".localized
        self.lblFront.text = "signup3.front".localized
        self.lblInfoType?.text = "signup2.optionalInfo".localized
        self.tfExpirydate.placeholder = "alert.datePlaceHolder".localized
        self.tfBankAccountNumber?.placeholder = "signup2.tfAccountPlaceholder".localized
        self.tfBankName?.placeholder = "signup2.tfBankPlaceholder".localized
        
    }
    
    func setupViewInitially(){
        
        for (indx, item) in (DriverDataAtOtp.data?.products ?? []).enumerated(){
            arr.append("\(/item.name)")
            arrIds.append("\(/item.category_brand_product_id)")
            arrIndxSelected.append(indx)
        }
        
        categoryIdsToSend = arrIds.joined(separator: ",")
        categoryIdsToSend.insert("[", at: categoryIdsToSend.startIndex)
        categoryIdsToSend.insert("]", at: categoryIdsToSend.endIndex)
        
        let strAllText = arr.joined(separator: ",")
        lblCategoryTypes?.text = strAllText
    }
    
    
    func moveToMapViewController(){
        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
        vc.driverData = self.driverData
        self.navigationController?.pushViewController(vc, animated: true)
        //        let vc = R.storyboard.main.mapViewController()
        //        let nav = UINavigationController(rootViewController: vc!)
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //        appDelegate.window?.rootViewController = nav
    }
    
    func openPhotoOptionsActionSheet(){
        self.view.endEditing(true)
        
        CameraImage.shared.captureImage(
            from: self, navigationController: self.navigationController,
            mediaType: nil,
            captureOptions: [.camera, .photoLibrary],
            allowEditting: true) {
                
                [unowned self] (image,fileName) in
                guard let img = image else { return }
                
                if self.selectedlicenseFace == 0 {
                    switch self.documents {
                    case .license:
                        self.btnLicenseFront.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove1.isHidden = false
                    case .pan:
                        self.btnPanFront?.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove3?.isHidden = false
                    case .aadhar:
                        self.btnAadharFront?.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove5?.isHidden = false
                    case .otherDocs:
                        self.btnOtherDocsFront?.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove7?.isHidden = false
                    }
                    
                } else{
                    switch self.documents {
                    case .license:
                        self.btnLicenseBack.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove2.isHidden = false
                    case .pan:
                        self.btnPanBack?.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove4?.isHidden = false
                    case .aadhar:
                        self.btnAadharBack?.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove6?.isHidden = false
                    case .otherDocs:
                        self.btnOtherDocsBack?.setImage(img, for: .normal)
                        self.countImage += 1
                        self.btnRemove8?.isHidden = false
                    }
                    
                    
                }
        }
    }
    
    func validateForm(completion: @escaping (_ success: Bool?) -> () ) {
        
       /* let isCabS = /DriverDataAtOtp.data?.isCabSerive
        
        if isCabS {
            let strCarModel = /txtCarModel?.text?.trim()
            if strCarModel.isEmpty {
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup3.tfCarModelPlaceholder".localized , type: .info)
                return
            }
            
            if selectedColor.isEmpty {
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup3.tfCarColorPlaceholder".localized , type: .info)
                return
            }
        } */
        
        if tfLicenseNumber.text?.trim() == "" {
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "signup3.tfLicensePlaceholder".localized , type: .info)
            return
        }
        else if tfExpirydate.text?.trim() == ""{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.enterExpiryDate".localized , type: .info)
            return
        }
            
//        else if addressTextFiled.text?.trim() == ""{
//            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "Please enter Home Address".localized , type: .info)
//            return
//        }
            
        else if countImage < 2{
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.uploadBoth".localized , type: .info)
            return
        }
        
        
        //Temporary commented
        self.uploadLicense { (success) in
            completion(true)
        }
    }
    
    
    
    //MARK: --Button Actions
    
    @IBAction func btnPan(_ sender: UIButton) {
        viewPan?.isHidden = !(/viewPan?.isHidden)
    }
    
    @IBAction func btnAadhar(_ sender: UIButton) {
        viewAadhar?.isHidden = !(/viewAadhar?.isHidden)
    }
    
    @IBAction func btnOthers(_ sender: UIButton) {
        viewOthers?.isHidden = !(/viewOthers?.isHidden)
    }
    
    @IBAction func selctColor(_ sender: UIButton) {
        tableView?.isHidden = false
        
       configureTableView()
    }
    @IBAction func actionBack(_ sender: Any) {
        
        if let nav = self.navigationController{
            for controller in nav.viewControllers.reversed(){
                if controller.isKind(of: ServicesOfferViewController.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
                
                
                if controller.isKind(of: PhoneSignUpViewController.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    //MARK:-IBAction to pick Address.
    @IBAction func selectAddress(_ sender: UIButton) {
        let country = UserDefaults.standard.value(forKey: "current_contry") as? String
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        filter.country = country
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func actionPickExpiryTime(_ sender: Any) {
        
        let yearsToAdd = 10
        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.year = yearsToAdd
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        ActionSheetDatePicker.show(withTitle: "Expiry Date", datePickerMode: .date, selectedDate: Date(), minimumDate: Date(), maximumDate: futureDate, doneBlock: { (picker, resp1, resp2) in
            self.formatter.dateFormat = "dd/MM/yyyy"
            self.tfExpirydate.text = self.formatter.string(from: (resp1 as? Date)!)
            self.formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            self.formatter.locale = Locale(identifier: "en_US")
            
            self.dateStringServer =  self.formatter.string(from: (resp1 as? Date)!)
            
        }, cancel: { (picker) in
        }, origin: tfExpirydate)
        
    }
    
    
    @IBAction func actionMoveToMap(_ sender: Any) {
       // self.validateForm()
    }
    
    @IBAction func btnRemoveFrontMulkiya(_ sender: UIButton) {
        if sender.tag == 1 {
            documents = Documents.license
        }else
            if sender.tag == 3 {
                documents = Documents.pan
            }else
                if sender.tag == 5 {
                    documents = Documents.aadhar
                }else{
                    documents = Documents.otherDocs
        }
        switch documents {
        case .license:
            btnLicenseFront.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove1.isHidden = true
            }
        case .pan:
            btnPanFront?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove3?.isHidden = true
            }
        case .aadhar:
            btnAadharFront?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove5?.isHidden = true
            }
        case .otherDocs:
            btnOtherDocsFront?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove7?.isHidden = true
            }
        }
        
    }
    
    @IBAction func btnRemoveBackMulkiya(_ sender: UIButton) {
        if sender.tag == 2 {
            documents = Documents.license
        }else
            if sender.tag == 4 {
                documents = Documents.pan
            }else
                if sender.tag == 6 {
                    documents = Documents.aadhar
                }else{
                    documents = Documents.otherDocs
        }
        switch documents{
        case .license:
            btnLicenseBack.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove2.isHidden = true
                
            }
        case .pan:
            btnPanBack?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove4?.isHidden = true
                
            }
        case .aadhar:
            btnAadharBack?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove6?.isHidden = true
                
            }
        case .otherDocs:
            btnOtherDocsBack?.setImage(#imageLiteral(resourceName: "ic_add_bg"), for: .normal)
            if self.countImage > 0{
                self.countImage -= 1
                btnRemove8?.isHidden = true
                
            }
        }
        
    }
    
    @IBAction func actionFrontMulkiyaImage(_ sender: UIButton) {
        selectedlicenseFace = 0
        
        if sender.tag == 1 {
            documents = Documents.license
        }else
        if sender.tag == 3 {
            documents = Documents.pan
        }else
        if sender.tag == 5 {
            documents = Documents.aadhar
        }else{
            documents = Documents.otherDocs
        }
        self.openPhotoOptionsActionSheet()
    }
    
    @IBAction func actionBackMulkiyaImage(_ sender: UIButton) {
        selectedlicenseFace = 1
        
        if sender.tag == 2 {
            documents = Documents.license
        }else
            if sender.tag == 4 {
                documents = Documents.pan
            }else
                if sender.tag == 6 {
                    documents = Documents.aadhar
                }else{
                    documents = Documents.otherDocs
        }
        self.openPhotoOptionsActionSheet()
    }
    
    @IBAction func btnCategoryType(_ sender: UIButton) {
        // Drop down selection
        //        self.dropDownSelectionHandler()
    }
    
    
    func getCurrentContryCode() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
        if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
            let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{
            
            let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /code} as! [[String : AnyObject]]
            if let val = dict.first{
                let data = val as NSDictionary
                let code = data.value(forKey: "code") as! String
                UserDefaults.standard.set(code, forKey: "current_contry")
            }
        }
    }
    
    //MARK: API Request to upload mulkiya details
    func uploadLicense(completion: @escaping (_ success: Bool?) -> () ) {

        let strCarModel = /txtCarModel?.text?.trim()
        let strCarColor = selectedColor

        var imgs:[UIImage] = []
        if let img = self.btnLicenseFront.image(for: .normal) {
            imgs.append(img)
        }
        if let img = self.btnLicenseBack.image(for: .normal) {
            imgs.append(img)
        }
        let objR = LoginEndpoint.addMulkiyaDetails(mulkiyaNumber: /tfLicenseNumber.text?.trim(),
                                                   mulkiyaValidity: dateStringServer,
                                                   bankName: tfBankName?.text?.trim(),
                                                   bankAccountNumber: tfBankAccountNumber?.text?.trim(),
                                                   carModel: strCarModel,
                                                   carColor: strCarColor,
                                                   home_address: nil,
                                                   home_longitude: Locations.longitutde.getLoc(),
                                                   home_latitude: Locations.lat.getLoc() ?? 0.0)
        
        APIManager.shared.request(withImages: objR, images: imgs) {
            [weak self] (resp) in
            guard let self = self else { return }
            
            switch resp{
            case .success( _):
               //self.sendCategoryToServer()
                completion(true)
                
               /* Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertDriverBlocked(), actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                    self.navigationController?.popToRootViewController(animated: true)
                } */
                 //self.moveToMapViewController()
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: err ?? "", type: .info)
            }
        }
    }
    
    func sendCategoryToServer() {
        let objR = LoginEndpoint.addServicesToOffer(serviceId: "7", categoryBrandId: 23, category_brand_product_ids: "[52]")
        
        APIManager.shared.request(with: objR) {
            [weak self] (resp) in
            switch resp {
            case .success( _ ):
                print("success")
            case .failure( _):
                print("failure")
            }
        }
    }
}

//MARK:- ======== TableView ========

extension UploadLicenseViewController {
    func configureTableView(){
        
        dataSource =  TableViewDataSource.init(items: arrayColors, tableView: tableView, cellIdentifier: "ColorsTableViewCell", cellHeight: 16, configureCellBlock: { [weak self](cell, item,
            indexPath) in
            
            (cell as? ColorsTableViewCell)?.backgroundColor = UIColor.init(hexString: self?.arrayColors[indexPath.row] ?? "")
            (cell as? ColorsTableViewCell)?.colorLabel?.text = self?.arrayColorsName[indexPath.row]
            }, aRowSelectedListener: { (indexPath, cell) in
                // move to selected menu
                self.selectedColor = self.arrayColors[indexPath.row]
                self.colorImg?.backgroundColor = UIColor(hexString: self.arrayColors[indexPath.row])
                self.lblColor?.text = self.arrayColorsName[indexPath.row]
                self.tableView?.isHidden = true
        }, willDisplayCell: { (indexPath, cell) in
            
        })
        
        tableView?.delegate = dataSource
        tableView?.dataSource = dataSource
        tableView?.reloadData()
    }
}


extension UploadLicenseViewController : GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        homeAddress = Address(gmsPlace: place)
        addressTextFiled.text = place.formattedAddress
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
extension UploadLicenseViewController: UITextFieldDelegate{
//    //MARK: - ---------------UItextFieldDelegate----------------
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.isFirstResponder {
            let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
            if (textField == self.tfLicenseNumber){
                if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                    return false
                }
                if let range = string.rangeOfCharacter(from: validString)
                {
                    print(range)
                    return false
                }
            }
        }
        return true
    }
}
