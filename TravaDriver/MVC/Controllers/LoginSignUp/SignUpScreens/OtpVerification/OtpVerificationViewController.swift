//
//  OtpVerificationViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 08/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
//import SVPinView


struct DriverDataAtOtp{
    static var data:DriverModel?
    static var product:[Products]?
}

class OtpVerificationViewController: BaseController , UITextFieldDelegate, MyTextFieldDelegate{
    
    //MARK: ---------------Outlets-------------
//    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var lblTopDesc: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnEditNumber: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var txtFieldFirstDigit: MyTextFieldNew!
    @IBOutlet weak var txtFieldSecondDigit: MyTextFieldNew!
    @IBOutlet weak var txtFieldThirdDigit: MyTextFieldNew!
    @IBOutlet weak var txtFieldFourthDigit: MyTextFieldNew!
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    
    //MARK: ---------------Variables-------------
    var driverData :DriverModel?
    var phoneCode : String?
    var phoneNumber :String?
    var otpTyped = ""
    
    var timer : Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    var totalSecondLeft = 120
    
    //MARK: ---------------ViewController LifeCycle Functions-------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        
        if moveToSignIn.movedSignin{
            self.navigationController?.popViewController(animated: true)
            moveToSignIn.movedSignin = false
        }
        
        txtFieldFirstDigit.becomeFirstResponder()
        txtFieldFirstDigit.text = ""
        txtFieldSecondDigit.text = ""
        txtFieldThirdDigit.text = ""
        txtFieldFourthDigit.text = ""
        
        
        txtFieldFirstDigit.myDelegate = self
        txtFieldSecondDigit.myDelegate = self
        txtFieldThirdDigit.myDelegate = self
        txtFieldFourthDigit.myDelegate = self
        
    }
    //MARK:---------CUstomMethods----
    
    func initialSetup(){
        //start otp expiring timer
        startTimer()
        
        var new = ""
        
//        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
//            new = /phoneNumber?.replacingOccurrences(of: "+", with: "")
//            stackView.semanticContentAttribute = .forceLeftToRight
//            lblTopDesc.text = "\(R.string.localizable.verifyEnterInfo()) \(/new)+"
//
//        }else {
//            new = /phoneNumber
//            lblTopDesc.text = "\(R.string.localizable.verifyEnterInfo()) \(/new)"
//        }
        
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
            
            new = /phoneNumber
            stackView.semanticContentAttribute = .forceLeftToRight
         //   lblTopDesc.text = R.string.localizable.verifyEnterInfo() + "\n" + /new
            lblTopDesc.text = /new
        }
        else{
            new = /phoneNumber
           // lblTopDesc.text = R.string.localizable.verifyEnterInfo() + "\n" + /new
             lblTopDesc.text = /new
        }
        
        
        btnResend.setTitle("verify.resend".localized, for: .normal)
        btnEditNumber.setTitle("verify.edit".localized, for: .normal)
        self.navigationItem.title = "verify.title".localized
        
        self.navigationController?.navigationBar.backItem?.title = "back".localized
        
        if let font = UIFont(name: LocalKeys.MontBold, size: 16) {
            let lbl = UILabel()
           // lbl.backgroundColor = UIColor.black
            lbl.text = "verify.title".localized
            lbl.textColor = UIColor.white
            lbl.font = font
            lbl.sizeToFit()
            navigationItem.titleView = lbl
            
        }

        
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
       
        
        self.rotateButton(button: btnNext)
        
    }
    
    // start otp expiring timer
    
    func startTimer() {
        
        totalSecondLeft = 120
        registerBackgroundTask()
        self.timer?.invalidate()
        self.timer = nil
        lblTimer.text = totalSecondLeft.formattedTimer()
        btnResend.isHidden = true
        lblTimer.isHidden = false
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self , selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        
        if totalSecondLeft > 0 {
            totalSecondLeft = totalSecondLeft - 1
            lblTimer.text = totalSecondLeft.formattedTimer()
        }else {
            endBackgroundTask()
            
            btnResend.isUserInteractionEnabled = true
            self.timer?.invalidate()
            btnResend.isHidden = false
            lblTimer.isHidden = true
            
            self.view.endEditing(true)
            txtFieldFirstDigit.text = ""
            txtFieldThirdDigit.text = ""
            txtFieldSecondDigit.text = ""
            txtFieldFourthDigit.text = ""
        }
    }
    
    
    
    func registerBackgroundTask() {
        
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    // Move to servicesOffering Screen
    func moveToServicesOfferViewController() {
        
//        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
//        vc.driverData = self.driverData
//        self.navigationController?.pushViewController(vc, animated: true)
        if let vc = R.storyboard.services.servicesViewController(){
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // Move to map Screen
    func moveToMapViewController() {
        
        let vc = R.storyboard.main.mapViewController()
                  let nav = UINavigationController(rootViewController: vc!)
                  let appDelegate = UIApplication.shared.delegate as! AppDelegate
                  appDelegate.window?.rootViewController = nav
    }
    
    // Move to license Screen
    func moveToUploadLicense() {
        
        guard let vc = R.storyboard.loginSignUp.uploadLicenseViewController() else {return}
        vc.driverData = self.driverData
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Move to SignUp Screen
    func moveToSignUpScreen(){
        guard let vc = R.storyboard.loginSignUp.registerationDetailsViewController() else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: ---------------Button Actions-------------
    
    @IBAction func btnBack(_ sender: UIButton){
        if let nav = self.navigationController{
            for controller in nav.viewControllers{
                if controller.isKind(of: PhoneSignUpViewController.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func actionResendOtp(_ sender: Any) {
        self.resendOtp()
    }
    
    @IBAction func actionEditNumber(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //  func validateOtpData() -> Valid {
    //        tfPhoneNumber.text = tfPhoneNumber.text?.trim() // remove spaces or newline
    //        return Validation.shared.isValid(phoneNum: tfPhoneNumber.text)
    //  }
    
    
    @IBAction func actionNextToUploadLicense(_ sender: Any) {
        
        verifyOtp()
    }
    
    //MARK: ------------API Request ------
    
    func verifyOtp(){
        
        otpTyped = "\(/txtFieldFirstDigit.text)\(/txtFieldSecondDigit.text)\(/txtFieldThirdDigit.text)\(/txtFieldFourthDigit.text)"
        
        if otpTyped.count < 4 {
            
            Alerts.shared.showOnTop(alert: "alert.alert".localized, message: "alert.invalidOTP".localized , type: .info)
            return
        }
        
        APIManager.shared.request(with: LoginEndpoint.verifyOtp(otp: otpTyped)) {[weak self] (resp) in
            
            switch resp {
                
            case .success(let responseValue ):
                let resp = responseValue  as? DriverModel
                UserDefaults.standard.set(resp?.data?.accountStep, forKey: accountKey)
                self?.driverData = resp
                UserDefaults.standard.set(self?.driverData?.products?.first?.category_brand_product_id, forKey: "category_brand_product_id")
                UserDefaults.standard.set(self?.driverData?.data?.online_status, forKey: "online_status")
                UserDefaults.standard.set(self?.driverData?.data?.directional_hire, forKey: "directionalHire_status")
                UserDefaults.standard.set(self?.driverData?.data?.mulkiya_validity, forKey: "mulkiya_validity")
                if (resp?.data?.approved == "0") && (resp?.data?.mulkiya_number != ""){
                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertDriverBlocked() , actionTitle: R.string.localizable.alertTitleOk(), viewController: /self) {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    return
                }
                
                DriverDataAtOtp.data = resp
                token.access_token = (/resp?.data?.accessToken)
                
//                if let vc = R.storyboard.services.servicesViewController(){
//                    self?.navigationController?.pushViewController(vc, animated: true)
//                }
                
                if resp?.data?.dataUser?.name == ""{
                    self?.moveToSignUpScreen()
                }else{
                    if (UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int) != nil {
                        self?.moveToMapViewController()
                    }else{
                        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
                        let nav = UINavigationController(rootViewController: vc)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = nav
                    }
                }
            case .failure(let err):
                
                Alerts.shared.showOnTop(alert: "alert.alert".localized, message: err ?? "", type: .info)
            }
        }
    }
    
    func resendOtp(){
        //        var completeNumberArr:[String] = []
        var phoneCode = ""
        var number = ""
        
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
            
            let  completeNumberArr = (phoneNumber?.components(separatedBy: "-"))
            let reversed = completeNumberArr?.reversed()
            phoneCode = /reversed?.first
            number = /reversed?.last
            
        }
        else{
            
            let  completeNumberArr = phoneNumber?.components(separatedBy: "-")
            phoneCode = /completeNumberArr?.first
            number = /completeNumberArr?.last
        }
        
        APIManager.shared.request(with: LoginEndpoint.sendOtp(languageId: LanguageCode.English.get(), phoneCode: phoneCode, phone_number: number, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken, device_type: "Ios")) { [weak self](response) in
            
            switch response{
            case .success(let response_value ):
                let resp = response_value  as? OtpModel
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: resp?.message ?? "Otp sent sucessfully" , type: .info)
                
                //start otp expiring timer
                self?.startTimer()
                // add acess token for future reference also chnaged at otp verification
                token.access_token = (/resp?.data?.acessToken)
                
            case .failure( let str):
                Alerts.shared.showOnTop(alert: "alert.alert".localized, message: str ?? "", type: .info)
            }
        }
    }
}

extension OtpVerificationViewController{
    //MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldProcess = false //default to reject
        var shouldMoveToNextField = false //default to remaining on the current field
        let  insertStrLength = string.count
        
        if insertStrLength == 0 {
            
            shouldProcess = true //Process if the backspace character was pressed
            
        } else {
            if /textField.text?.count <= 1 {
                shouldProcess = true //Process if there is only 1 character right now
            }
        }
        
        if shouldProcess {
            
            var mString = ""
            if mString.count == 0 {
                
                mString = string
                shouldMoveToNextField = true
                
            } else {
                //adding a char or deleting?
                if(insertStrLength > 0){
                    mString = string
                    
                } else {
                    //delete case - the length of replacement string is zero for a delete
                    mString = ""
                }
            }
            
            //set the text now
            textField.text = mString
            
            if (shouldMoveToNextField&&textField.text?.count == 1) {
                
                if (textField == txtFieldFirstDigit) {
                    txtFieldSecondDigit.becomeFirstResponder()
                    
                } else if (textField == txtFieldSecondDigit){
                    txtFieldThirdDigit.becomeFirstResponder()
                    
                } else if (textField == txtFieldThirdDigit){
                    txtFieldFourthDigit.becomeFirstResponder()
                    
                } else {
                    txtFieldFourthDigit.resignFirstResponder()
                }
            }
            else{
                self.textFieldDidDelete()
            }
        }
        return false
    }
    
    //MARK: - MyTextField Delegate
    func textFieldDidDelete() {
        
      
        
        if txtFieldSecondDigit.isFirstResponder{
            txtFieldFirstDigit.becomeFirstResponder()
        }
        if txtFieldThirdDigit.isFirstResponder{
            txtFieldSecondDigit.becomeFirstResponder()
            
        }
        
        if txtFieldFourthDigit.isFirstResponder{
            txtFieldThirdDigit.becomeFirstResponder()
        }

        
    }
}

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextFieldNew: UITextField {
    
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
    
}

