//
//  CategoryDetailController.swift
//  TravaDriver
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class CategoryDetailController: UIViewController {

    @IBOutlet weak var categoryTextField: AnimatableTextField!
    @IBOutlet weak var brandTextField: AnimatableTextField!
    @IBOutlet weak var offerTextField: AnimatableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func serviceOfferAction(_ sender: UIButton) {
    }
    
    @IBAction func selectBrandAction(_ sender: UIButton) {
    }
    @IBAction func selectCategoryAction(_ sender: UIButton) {
    }
    @IBAction func contineButtonAction(_ sender: UIButton) {
    }
}
