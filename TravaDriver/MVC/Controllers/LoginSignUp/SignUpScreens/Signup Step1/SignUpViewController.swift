//
//  SignUpViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 07/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import DropDown
struct moveToSignIn{
    static var movedSignin = false
}


class SignUpViewController: BaseController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: ---------------IBOutlets-------------
    
    @IBOutlet weak var btnIndividual: AnimatableButton!
    @IBOutlet weak var btnOrganisation: AnimatableButton!
    @IBOutlet weak var btnOrganisationCodeView: CustomView!
    @IBOutlet weak var btnChooseProfilePic: UIButton!
    @IBOutlet weak var btnMissingOrganisation: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblOrganisationCode: UILabel!
    @IBOutlet weak var lcConstraintsCodeView: NSLayoutConstraint!
    @IBOutlet weak var topNextConstarint: NSLayoutConstraint!
    
    //localised
    @IBOutlet weak var lblSelectFleetCodeInfo: UILabel!
    @IBOutlet weak var lblUploadinfo: UILabel!
    @IBOutlet weak var lblOrganisation: UILabel!
    @IBOutlet weak var lblIndividual: UILabel!
    @IBOutlet weak var lblMissingOrganisation: UILabel!
    @IBOutlet weak var lblAnchorView: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    
    
    //MARK: ---------------variables-------------
    
    var otpData :OtpModel?
    var driverData :DriverModel?
    let picker = UIImagePickerController()
    let dropDown = DropDown()
    
    var arrOrganisationCode:[String] = []
    var arrOrganisationCodeId:[Int] = []
    var  organisationId = 0
    
    var isOrganisation = true {
        didSet {
            if isOrganisation {
                selectOrg()
            } else  {
                selectIndividual()
            }
        }
    }

    
    
    //MARK: ---------------ViewController LifeCycle Functions-------------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initialSetup()
        
        isOrganisation = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {

    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        //        if parent == nil {
        //           moveToSignIn.movedSignin = true
        //            //"Back pressed"
        //        }
        //                if parent == nil {
        //
        //
        //        for controller in self.navigationController!.viewControllers as Array {
        //            if controller.isKind(of: PhoneSignUpViewController.self) {
        //                self.navigationController!.popToViewController(controller, animated: true)
        //                break
        //            }
        //        }
        //    }
    }
    
    //MARK: ---------Custom Methods ----------
    
    func validateLoginData() -> Valid {
        return Validation.shared.isValid(name: tfName.text)
    }
    
    func initialSetup(){
        
        self.navigationItem.title = "signup1.title".localized
        
        if let font = UIFont(name: LocalKeys.MontBold, size: 16) {
            let lbl = UILabel()
            lbl.backgroundColor = UIColor.black
            lbl.text = "signup1.title".localized
            lbl.textColor = UIColor.white
            lbl.font = font
            lbl.sizeToFit()
            navigationItem.titleView = lbl
            
        }
        
        self.lblUploadinfo.text = "signup1.txtUpload".localized
        self.tfName.placeholder = "signup1.tfNamePlaceholder".localized
        self.lblIndividual.text = "signup1.individual".localized
        self.lblOrganisation.text = "signup1.organisation".localized
        self.lblSelectFleetCodeInfo.text = "signup1.selectFleetCode".localized
        self.lblMissingOrganisation.text = "signup1.missingOrganisation".localized
        self.btnMissingOrganisation.setTitle("signup1.click".localized, for: .normal)
        
        
        self.rotateButton(button: btnArrow)
        
        // for semantics
        self.view.setTextFields(mainView: self.view.subviews)
        self.rotateButton(button: btnNext)
        
        picker.delegate = self
        //        self.navigationItem.hidesBackButton = true
        
        //dropdown setup
        
        dropDown.dataSource = arrOrganisationCode
        dropDown.textFont = R.font.montBold(size: 14.0)!
        dropDown.cellHeight = 40
        dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        dropDown.cornerRadius = 10
        dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        dropDown.shadowOpacity = 0.9
        dropDown.shadowRadius = 25
        //        dropDown.width = 100.0
        dropDown.animationduration = 0.25
        dropDown.textColor = .darkGray
        
        //drop down selection handler
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblOrganisationCode.text = item
             self.organisationId = self.arrOrganisationCodeId[index]
            
            self.dropDown.hide()
        }
        
        //get organisation code
        self.getOrganisationCode()
    }
    
    // Move to servicesOffering Screen
    func moveToServicesOfferViewController(){
        guard let vc = R.storyboard.loginSignUp.servicesOfferViewController() else {return}
        vc.driverData =  DriverDataAtOtp.data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: ---------------Button Actions -------------
    
    @IBAction func actionBack(_ sender: UIButton){
        if let nav = self.navigationController{
            for controller in nav.viewControllers.reversed(){
        if controller.isKind(of: PhoneSignUpViewController.self) {
            self.navigationController?.popToViewController(controller, animated: true)
            break
        }
            }}}
    
    @IBAction func actionSelectImage(_ sender: Any) {
        
        self.view.endEditing(true)
        CameraImage.shared.captureImage(
            from: self, navigationController: self.navigationController,
            mediaType: nil,
            captureOptions: [.camera, .photoLibrary],
            allowEditting: true) {
                [unowned self] (image,fileName) in
                guard let img = image else { return }
                self.imgView.image = img
                self.btnChooseProfilePic.setImage(nil, for: .normal)
        }
    }
    
    @IBAction func actionIndividualSelected(_ sender: Any) {
        
        isOrganisation = false

    }
    
    func selectIndividual() {
        
        btnIndividual.borderColor = Colors.themeColor
        btnOrganisation.borderColor = UIColor.init(red: 0.84, green: 0.87, blue: 0.90, alpha: 1.0)
        btnOrganisationCodeView.alpha = 1
        
        UIView.animate(withDuration: 0.3) {
            self.btnOrganisationCodeView.alpha = 0
            self.btnOrganisationCodeView.isHidden = true
            self.lblMissingOrganisation.isHidden = true
            self.btnMissingOrganisation.isHidden = true
            self.lcConstraintsCodeView.constant = 0
            self.topNextConstarint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionOrganisation(_ sender: UIButton) {
        
        isOrganisation = true

    }
    
    func selectOrg() {
        
        btnOrganisationCodeView.alpha = 0
        btnOrganisationCodeView.isHidden = false
        
        UIView.animate(withDuration: 0.3) {
            
            self.lblMissingOrganisation.isHidden = true
            self.btnMissingOrganisation.isHidden = true
            self.btnOrganisationCodeView.alpha = 1
            self.lcConstraintsCodeView.constant = 64
            self.topNextConstarint.constant = 30
            
            self.view.layoutIfNeeded()
        }
        btnOrganisation.borderColor = Colors.themeColor
        btnIndividual.borderColor = UIColor.init(red: 0.84, green: 0.87, blue: 0.90, alpha: 1.0)
    }
    
    @IBAction func actionFleetCodePicker(_ sender: UIButton) {
        
        dropDown.anchorView = lblAnchorView
        dropDown.direction = .any
        dropDown.dataSource = arrOrganisationCode
        dropDown.show()
    }
    
    @IBAction func actionServiceOfferViewController(_ sender: Any) {
        
        self.view.endEditing(true)
        let isValid = validateLoginData()
        switch isValid {
        case .success:
            // created new sign up request
            self.signUpNewUser()
            
        case .failure(_, _):
            Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: "alert.name".localized , type: .info)
        }
    }
    
    @IBAction func actionMissingOrganisation(_ sender: Any) {
        
    }
    
    //MARK: ---------------APIs -------------
    
    func signUpNewUser(){
        
        var imgs:[UIImage] = []
        if let img = self.imgView.image {
            imgs.append(img)
        }
        
        var orgId = 0

        
        if isOrganisation{
            orgId = self.organisationId
        }
        
        APIManager.shared.request(withImages: LoginEndpoint.signup( name: tfName.text, organisationId: orgId), images: imgs) { (response) in
            switch response{
            case .success(let responseValue ):
                
                let resp = responseValue  as? DriverModel
                self.driverData = resp
                DriverDataAtOtp.data = resp
                self.moveToServicesOfferViewController()
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: err ?? "", type: .info)
            }
        }
    }
    
    func getOrganisationCode(){
        
        APIManager.shared.request(with: CommonEndPoint.getOrganisation) { (response) in
            switch response{
            case .success(let responseValue ):
                
                let resp = responseValue  as? organizations
                self.lblOrganisationCode.text = "\(resp?.data.first?.name ?? "")"
                
                self.organisationId = /resp?.data.first?.organisation_id
                
                for item:organizationsData in resp?.data ?? [] {
                    self.arrOrganisationCode.append("\(/item.name)")
                    self.arrOrganisationCodeId.append(/item.organisation_id)
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: err ?? "", type: .info)
            }
        }
    }
}
