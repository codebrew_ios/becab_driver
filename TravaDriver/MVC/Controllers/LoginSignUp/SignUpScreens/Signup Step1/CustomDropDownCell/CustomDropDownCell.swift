//
//  DropDownCell.swift
//  Buraq24Driver
//
//  Created by Apple on 25/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import DropDown

class CustomDropDownCell: DropDownCell {
    
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
}
