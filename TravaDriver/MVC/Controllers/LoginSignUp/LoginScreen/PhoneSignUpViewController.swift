//
//  PhoneSignUpViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 07/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import CoreLocation
import DropDown
import IBAnimatable
import UserNotifications

class PhoneSignUpViewController: BaseController {
    
    //MARK: ---------------IBOutlets-------------
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var tfPhoneNumber: UITextField!
  //  @IBOutlet weak var lbltermsConditionInfo: UILabel!
  //  @IBOutlet weak var lblLanguage: UILabel!
  //  @IBOutlet weak var btnTermsAndConditions: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgViewCountry: UIImageView!
  //  @IBOutlet weak var bottomStackView: UIStackView!
 //   @IBOutlet weak var languageView: AnimatableView!
   // @IBOutlet weak var lblchangeLanguage: UILabel!
  //  @IBOutlet weak var btnConditionsRead: UIButton!
    //MARK: ---------------Variable-------------
    var otpData :OtpModel?
    let dropDown = DropDown()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let des = [["h1": "TAXISTA", "s1": "Regístrate únicamente si eres conductor de taxi."], ["h1": "OPTIMIZA", "s1": "Optimiza tu tiempo y genera más ingresos con BeCab."]]

    
//    let languages = ["txt.english".localized:"en",
//                     "txt.arabic".localized:"ar",
//                     "txt.urdu".localized:"ur",
//                     "txt.chinese".localized:"zh-Hans"]
    
    let languages = ["txt.english".localized:"en",
    "txt.spanish".localized:"zh-Hans"]
    
    
    //MARK: ---------------ViewController LifeCycle Functions-------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupController()
        
        lblTitle.text = des[0]["h1"]
        lblSubTitle.text = des[0]["s1"]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        token.access_token = ""
        
        scrollView.isPagingEnabled = true
        scrollView.delegate = self 
        scrollView.backgroundColor = UIColor.clear
        
    }
    
    override func viewDidLayoutSubviews() {
        let numberOfPages :Int = 1
        let padding : CGFloat = 0
        let viewWidth = scrollView.frame.size.width - 2 * padding
        let viewHeight = scrollView.frame.size.height - 2 * padding
        
        
        let imageArr = [#imageLiteral(resourceName: "w2"), #imageLiteral(resourceName: "w1")]
        
        var x : CGFloat = 0

          for i in 0...numberOfPages {
              let view: UIImageView = UIImageView(frame: CGRect(x: x + padding, y: padding, width: viewWidth, height: viewHeight))
              view.backgroundColor = UIColor.clear
              view.contentMode = .scaleAspectFit
              view.image = imageArr[i]
              scrollView .addSubview(view)

              x = view.frame.origin.x + viewWidth + padding
          }

          scrollView.contentSize = CGSize(width:x+padding, height:scrollView.frame.size.height)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //MARK: - -------------------Custom's Methods-------------------------
    
    func setupController(){
        
        appDelegate.pushNotificationPermission(application: UIApplication.shared)
        
      //  lbltermsConditionInfo.text = "signin.termsInfo".localized
      //  btnTermsAndConditions.setTitle("signin.btnterms".localized, for: .normal)
        tfPhoneNumber.placeholder = "signin.phone".localized
      //  lblchangeLanguage.text = "txt.changeLanguage".localized
        self.navigationItem.title = R.string.localizable.back()
        
        // initial SetUp
        
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
        self.lblCountryCode.text = "+57"
        
        if let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist"),
            let arr:[NSDictionary] = NSArray(contentsOfFile: path) as? [NSDictionary]{
            
            let dict:[[String:AnyObject]] = arr.filter{($0["code"] as! String) == /code} as! [[String : AnyObject]]
//
//            if let val = dict.first{
//                let data = val as NSDictionary
//                let code = data.value(forKey: "code") as! String
//                let dialCode = data.value(forKey: "dial_code") as! String
//
//           //     imgViewCountry.image = UIImage.init(named: /(code.lowercased()))
//                lblCountryCode.text = dialCode
//            }
            self.lblCountryCode.text = "+57"
            
        }
        
        //
        /// Delegate
        tfPhoneNumber.delegate = self
        
        // Dropdown setup
        self.dropDownSetup()
        
        self.view.setTextFields(mainView: self.view.subviews)
        self.rotateButton(button: btnNext)
    }
    
    func dropDownSetup(){
        
        let selectedLanguage = UserDefaults.standard.value(forKey: "language") as? String
      //  self.lblLanguage.text = self.getLanguageFromCode(code: /selectedLanguage)
        
        dropDown.dataSource = Array(languages.keys)
        dropDown.textFont = R.font.montBold(size: 14.0)!
        dropDown.cellHeight = 40
        dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        dropDown.cornerRadius = 10
        dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        dropDown.shadowOpacity = 0.9
        dropDown.shadowRadius = 25
      //  dropDown.width = languageView.frame.size.width
        dropDown.animationduration = 0.25
        dropDown.textColor = .darkGray
        
       // dropDown.bottomOffset = CGPoint(x: 0, y:languageView.frame.size.height + 10)
        
        //drop down selection handler
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
         //   self.lblLanguage.text = item
            self.dropDown.hide()
            
//             let langCode = Array(self.languages.values)
//             LanguageFile.shared.changeLanguage(type: langCode[index])
            
//            if langCode[index] != selectedLanguage{
//                LanguageFile.shared.changeLanguage(type: langCode[index])
//            }
           
        }
    }
    
    // Move to SignUp Screen
    func moveToSignUpScreen() {
        
        guard let vc = R.storyboard.loginSignUp.signUpViewController() else {return}
        vc.otpData = self.otpData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Move to otp verification  screen (Map view Screen)
    func moveToOtpVerification(){
        
        guard let vc = R.storyboard.loginSignUp.otpVerificationViewController() else {return}
        
        vc.phoneNumber = (/lblCountryCode.text) + "-" + (/tfPhoneNumber.text)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //validating Data
    
    func validateLoginData() -> Valid {
        tfPhoneNumber.text = tfPhoneNumber.text?.trim() // remove spaces or newline
        return Validation.shared.isValid(phoneNum: tfPhoneNumber.text)
    }
    
    func checkUpdate(){
        
        guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return
        }
        
        let force = /self.otpData?.data?.versionForce
        
        if force != currentVersion{
            if currentVersion.compare(force, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending {
                
                Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertUpdateApp(), actionTitle: R.string.localizable.alertTitleOk(), viewController: self) {
                    
                    if let url = URL(string: APIConstants.appStoreUrl),
                        UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.open(url, options: [:]) { (opened) in
                            if(opened){
                                print("App Store Opened")
                            }
                        }
                    } else {
                        Alerts.shared.showOnTop(alert: "Alert", message: "App store not available.", type: .info)
                    }
                }
                
                return
            }
        }
        
        // otherwise move to otp verfication
        self.moveToOtpVerification()
    }
    
    //MARK: - -------------------Button Actions-------------------------
    
    
    @IBAction func actionTermsAndCondition(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
        }
        else{
            sender.isSelected = true
        }
    }
    
    
    @IBAction func actionNextAction(_ sender: Any) {
        self.view.endEditing(true)
        
        let isValid = validateLoginData()
        
        let isLocationPermission = checkForLocationPermission()
        if isLocationPermission{
            // validate phone number
            
            switch isValid {
            case .success:
                
                // send otp to phoneNumber
                if LocationManager.shared.currentLoc != nil{
                    
                  /*  if !btnConditionsRead.isSelected{
                        Alerts.shared.showOnTop(alert: "alert.alert".localized, message: R.string.localizable.signinAccept(), type: .info)
                        return
                    } */
                    
                    self.sendOtp()
                }
                else{
                    
                    LocationManager.shared.updateUserLocation()
                    Alerts.shared.showOnTop(alert: "alert.locationDisabledAlert".localized, message: "alert.locationNotFound".localized, type: .info)
                }
                
            case .failure(_, _):
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: "alert.phoneNumber".localized , type: .info)
            }
        }
        else{
            
            Alerts.shared.showAlertWithActionBlock(title: "alert.locationDisabledAlert".localized, message: "alert.locationDisabledMessage".localized, actionTitle: "alertTitle.ok".localized, viewController: self) {
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            }
        }
    }
    
    @IBAction func actionsTermsAndCOnditions(_ sender: Any) {
        
        let stringTermsConditions = "\(APIConstants.TermsConditions)"
        
        var urlComplete = String()
        if BundleLocalization.sharedInstance().language == Languages.English {
            urlComplete = "\(stringTermsConditions)\("/en")"
        } else {
            urlComplete = "\(stringTermsConditions)\("/es")"
        }
        
        
        guard let vc = R.storyboard.loginSignUp.webViewController() else{ return }
        vc.stringUrl = urlComplete
        vc.type = .terms
        vc.isPresentView = true
        
        self.present(vc, animated: true, completion: nil)
        //self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func actionPickCountryCode(_ sender: Any) {
        
        guard let countryPicker = R.storyboard.loginSignUp.countryCodeSearchViewController() else{return}
        countryPicker.delegate = self
        self.navigationController?.present(countryPicker, animated: true, completion: nil)
    }
    
    @IBAction func actionChangeLanguage(_ sender: Any) {
        
        self.view.endEditing(true)
       // dropDown.anchorView = lblLanguage
        dropDown.direction = .bottom
        dropDown.dataSource = Array(languages.keys)
        dropDown.show()
        
    }
    
    // MARK: ---------- API----------
    
    func sendOtp() {
        
        APIManager.shared.request(with: LoginEndpoint.sendOtp(languageId: LanguageCode.English.get(), phoneCode: lblCountryCode.text, phone_number: tfPhoneNumber.text, timeZone: HelperNames.timeZone.get(), latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc(), socketId: nil, fcmId: token.fcmToken , device_type:"Ios")) { (response) in
            
            switch response{
            case .success(let response_value ):
                
                let resp = response_value  as? OtpModel
                self.otpData = resp
                
                // add acess token for future reference also chnaged at otp verification
                token.access_token = (/self.otpData?.data?.acessToken)
                self.checkUpdate()
                
            case .failure( let str):
                
                Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: str ?? "", type: .info)
            }
        }
    }
}

//override UITextFieldDelegate
extension PhoneSignUpViewController : UITextFieldDelegate {
    
    //MARK: - ---------------UItextFieldDelegate----------------
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            
            if text == "" && string == "0" {
                return false
            }
            
            let newLength = text.length + string.length - range.length
            return newLength <= 16
            
        } else {
            Alerts.shared.showOnTop(alert: Alert.oops.getLocalised(), message: "alertTitle.PhoneNoEnglish".localized , type: .info)
            return false
        }
    }
}

//MARK: - Country Picker Delegates
extension PhoneSignUpViewController: CountryCodeSearchDelegate{
    func didTap(onCode detail: [AnyHashable : Any]!) {
        imgViewCountry.image = UIImage(named:/(detail["code"] as? String)?.lowercased())
        lblCountryCode.text = /(detail["dial_code"] as? String)
    }
    
    func didSuccessOnOtpVerification(){
        
    }
}


extension PhoneSignUpViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("END Scrolling \(scrollView.contentOffset.x / scrollView.bounds.size.width)")
        let index = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
        
        lblTitle.text = des[index]["h1"]
        lblSubTitle.text = des[index]["s1"]
    }
}

