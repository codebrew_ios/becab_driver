//
//  WebViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 28/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import WebKit
enum WebType {
    case terms
    case privacy
    case aboutUs
}
class WebViewController: UIViewController , UIWebViewDelegate, WKUIDelegate, WKNavigationDelegate{
    
    //MARK: -- PROPERTIES
    var stringUrl:String?
    
    @IBOutlet weak var webKit: WKWebView!
    @IBOutlet weak var viewForWeb: UIView!
    @IBOutlet weak var navTitle: UILabel!
    
    var webView: WKWebView!
    var type:WebType = .terms
    var isPresentView = Bool()
    
      //MARK: -- VIEW CONTROLLER LIFE CYCLE
    
    override func loadView() {
        
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: viewForWeb.frame, configuration: webConfiguration)
        webView.navigationDelegate = self
        viewForWeb = webView
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.openUrlinWebview()

        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        if isPresentView {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    
    //MARK:-- CUSTOM METHODS

    func openUrlinWebview(){
        
        //startAnimating(nil, message: nil, messageFont: nil, type: .lineScalePulseOutRapid, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9751529098, green: 0.635238111, blue: 0, alpha: 1)
        switch type {
        case .aboutUs:
            self.navTitle.text = "settings.aboutUs".localized
        case .privacy:
            self.navTitle.text  = R.string.localizable.settingsPrivacy()
        case .terms:
            self.navTitle.text = R.string.localizable.settingsTerms()
        }
        
        guard let string = stringUrl else { return }
        
        let url = URL(string: string)
        let requestObj = URLRequest(url: url!)
        webKit.load(requestObj)
 
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       //// self.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        // self.stopAnimating()
    }

}
