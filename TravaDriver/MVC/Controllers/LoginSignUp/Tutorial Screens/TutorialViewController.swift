//
//  TutorialViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 07/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

protocol TutorialViewControllerDelegate {
    func updatePageControl(index : Int)
}

class TutorialViewController: UIPageViewController {
    
    //MARK: ---------------Variable-------------
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        
        return [self.tutorialViewController(vcIdentifier: APIConstants.first),
                self.tutorialViewController(vcIdentifier: APIConstants.second),
                self.tutorialViewController(vcIdentifier: APIConstants.third)]
    }()
    
    private func tutorialViewController(vcIdentifier: String) -> UIViewController {
        
        return UIStoryboard(name: APIConstants.loginSignUp, bundle: nil) .
            instantiateViewController(withIdentifier: "\(vcIdentifier)\(APIConstants.viewController)")
    }
    
    var delegateTutorial : TutorialViewControllerDelegate?
    
    // Track the current index
    var currentIndex: Int?
    private var pendingIndex: Int?
    
    
    //MARK: ---------------ViewController LifeCycle Functions-------------

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self // navi
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

// MARK: ----------- UIPageViewControllerDataSource -------------

extension TutorialViewController: UIPageViewControllerDataSource , UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        currentIndex = viewControllerIndex // navi
        
        let previousIndex = currentIndex! - 1
      
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
        
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        currentIndex = viewControllerIndex
        
        let nextIndex = viewControllerIndex + 1
        
        let orderedViewControllersCount = orderedViewControllers.count
        
    
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    // Navi
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = orderedViewControllers.index(of : /pendingViewControllers.first)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            
            currentIndex = pendingIndex
            
            if let index = currentIndex {
                
                self.delegateTutorial?.updatePageControl(index: index)
            }
        }
    }
    
}
