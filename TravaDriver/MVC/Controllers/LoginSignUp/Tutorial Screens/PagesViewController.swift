//
//  PagesViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class PagesViewController: UIViewController {
    
    //MARK: Outlet Collection
    
    @IBOutlet var lblDetail: [UILabel]!
    @IBOutlet var lblDetailInfo: [UILabel]!
    
    
    let arrHeaders = ["page.header1".localized, "page.header2".localized, "page.header3".localized]
    let arrDetails = ["page.detail1".localized, "page.detail2".localized, "page.detail3".localized]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: Custom Methods
    
    func setupHeadersAndDetails(){
        lblDetail.first?.text = self.arrHeaders[/lblDetail.first?.tag]
        lblDetailInfo.first?.text = self.arrDetails[/lblDetailInfo.first?.tag]
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        self.setupHeadersAndDetails()

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
