//
//  ContainerViewController.swift
//  Buraq24Driver
//
//  Created by OSX on 08/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import CHIPageControl
import DropDown

class ContainerViewController: BaseController , TutorialViewControllerDelegate {
    
    //MARK: ---------------IBOutlets-------------
    @IBOutlet weak var pageControl: CHIPageControlChimayo!
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    let dropDown = DropDown()
    let languages = ["English":"En",
                     "Arabic":"Ar",
                     "Urdu":"Ur",
                     ]
    
    //MARK: ---------------Variable-------------
    
    // perform Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "tutorial" {  
            let tutorialVC = segue.destination as? TutorialViewController
            tutorialVC?.delegateTutorial = self
        }
    }
    
    
    //MARK: ---------------ViewController LifeCycle Functions-------------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // setup view controller
        setupController()
        
    }
    
    func setupController(){
        
        self.navigationController?.isNavigationBarHidden = true
        
        dropDown.dataSource = Array(languages.keys)
        dropDown.textFont = R.font.montBold(size: 14.0)!
        dropDown.cellHeight = 40
        dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        dropDown.cornerRadius = 10
        dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        dropDown.shadowOpacity = 0.9
        dropDown.shadowRadius = 25
        dropDown.width = 200.0
        dropDown.animationduration = 0.25
        dropDown.textColor = .darkGray
        
        //drop down selection handler
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.lblLanguage.text = item
            self.dropDown.hide()
            
        }
        
        self.rotateButton(button: btnNext)
    }
    
    //MARK: - -------------------Button Actions-------------------------
    
    @IBAction func btnNext(_ sender: Any) {
        
        guard let vc = R.storyboard.loginSignUp.phoneSignUpViewController() else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionChangeLanguagePicker(_ sender: UIButton) {
        
        dropDown.anchorView = lblLanguage
        dropDown.direction = .top
        dropDown.dataSource = Array(languages.keys)
        dropDown.show()
        
    }
    
    //MARK: -------------------
    func updatePageControl(index : Int) {
        
        pageControl.progress = Double(index)
    }
    
}
