//
//  RequestSubmitController.swift
//  Buraq24Driver
//
//  Created by Apple on 15/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class RequestSubmitController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.redirectToNextScreen()
    }
    
    
    //MARK:-Function hide controller fater two second and redirest to Home(Dashboard)
    func redirectToNextScreen() {
        UIView.animate(withDuration: 2.0, animations: {
        }, completion: { (true) in
            guard let vc = R.storyboard.main.mapViewController() else{ return }
            self.navigationController?.pushViewController(vc, animated: true)
        })
    }
    
}
