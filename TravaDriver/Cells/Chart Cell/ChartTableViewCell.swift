//
//  ChartTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 05/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class ChartTableViewCell: UITableViewCell {
    
    //MARK:: OUTLETS
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantityInfo: UILabel!
    @IBOutlet weak var lblProductDetails: UILabel!
    
    
    //MARK:: PROPERTIES
    
    var order : RangeData? {
        
        didSet {
            configureCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    private func configureCell() {
       // lblQuantityInfo.text = R.string.localizable.chartQuantity()
        lblDate.text = order?._id
        lblPrice.text = "\(R.string.localizable.currency()) \(order?.earning?.roundTo(places: 2) ?? 0)"
        lblProductDetails.text = "\(order?.bookings ?? 0)"
        
    }
}
