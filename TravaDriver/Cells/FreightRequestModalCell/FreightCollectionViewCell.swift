//
//  FreightCollectionViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 05/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable


class FreightCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: AnimatableImageView!
    
    
    var indexPath: IndexPath?
    var item: imagesArray? {
        
        didSet {
            configureCell()
        }
    }
    
    private func configureCell() {
        imgView.kf.setImage(with: URL.init(string: /item?.image_url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
    }
}
