//
//  OnGoingRidesCollectionViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 17/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import CoreLocation
import ObjectMapper

protocol DropViewActionDelegate : class {
    
    func dropDelivery(orderId:String, paymentType:String, myTurn:String, orderStatus:String,categoryId:Int,organisationCouponUserId:Int, order :OrdersList?,amount:String?)
    func CollapseCollectionView(orderStatus:String, orderID:String,categoryId:Int)
    func CancelCurrentRide(orderStatus:String, orderID:String,categoryId:Int)
    func halfWayStopping(orderId:String, paymentType:String, halfWayStopReason:String)
    func breakDown(orderId:String)
    func addStops()
    func goToChat(item:OrdersList,name:String,profilePic:String)
     func updateRideStop(orderId:Int,ride:RideStops)
}

class OnGoingRidesCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var addressHeadingLabel: UILabel!
    //MARK: Outlets
    @IBOutlet weak var halfWayStopBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgViewCustomer: UIImageView!
    @IBOutlet weak var imgRate: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var halfWayStopMainView: UIView!
    @IBOutlet weak var breakDownMainView: UIView!
    @IBOutlet weak var btnDropRide: UIButton!
    @IBOutlet weak var btnCollapse: AnimatableButton!
    @IBOutlet weak var btnNavigate: UIButton!
    @IBOutlet weak var btnNavigateWidth: NSLayoutConstraint!
    @IBOutlet weak var btnCollapseArrow: UIButton!
    @IBOutlet weak var halfWayStopingBtn: AnimatableButton!
    @IBOutlet weak var addStopsButton: AnimatableButton!
    @IBOutlet weak var halfWayStopingView: AnimatableView!
    @IBOutlet weak var endrideButtonHeight: NSLayoutConstraint!
   
    
    @IBOutlet weak var addStopsButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var amountField: UITextField!
    
    //MARK: Variables
    weak var delegate:DropViewActionDelegate?
    var timer:Timer = Timer()
    var startRideIfDriverReached = false
    var vc:UIViewController?
    var friendName:String?
    var friendPhoneNumber:String?
    var friendCountryCode:String?
    var bookingtype:String?
    var friendPicUrl:String?
    var chat :[Chat]?
    var item: OrdersList? {
        didSet {
            configureCell()
        }
    }
    
    var indx = 0
    
    func getServiceName(category:String) -> String{
        
        switch category {
            
        case "1":
            return R.string.localizable.txtGas()
        case "2":
            return R.string.localizable.txtMineralWater()
        case "3":
            return R.string.localizable.txtWaterTanker()
        case "4":
            return  R.string.localizable.txtTrucks()
            
        default:
            return  "No service"
        }
    }
    
    //MARK: Custom Methods
    
    private func configureCell() {
        amountField.isHidden = true
        ratingView.filledBorderColor = .black
        ratingView.emptyBorderColor = .black
        NotificationCenter.default.removeObserver(self, name:  Notification.Name(rawValue:"chat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleBadge(notification:)), name: Notification.Name(rawValue:"chat"), object: nil)
        if bookingtype == "RoadPickupPhone" {
            if item?.roadStops?.count ?? 0 >= 1 || DataResponse.onGoingOrders?.orders?.first?.roadStops?.count ?? 0 >= 1{
                addStopsButton?.setTitle("Add Stops", for: .normal)
                addStopsButton?.isHidden = false
                addStopsButtonHeight?.constant = 48
            }else{
                addStopsButton?.setTitle("", for: .normal)
                addStopsButton?.isHidden = true
                addStopsButtonHeight?.constant = 0
            }
        }else{
            addStopsButton?.setTitle("", for: .normal)
            addStopsButton?.isHidden = true
            addStopsButtonHeight?.constant = 0
        }
        halfWayStopBtnHeight.constant = 0
        halfWayStopMainView.isHidden = true
        breakDownMainView.isHidden = true
        //UPDATE CUSTOMER DETAILS
        if bookingtype == "Friend" {
            lblCustomerName.text = friendName
            imgViewCustomer.kf.setImage(with: URL.init(string: friendPicUrl ?? ""), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
            ratingView.isHidden = true
            imgRate.isHidden = true
        }else {
            lblCustomerName.text = item?.user?.name
            imgRate.getRatingImage(rating: /item?.user?.ratings_avg)
            ratingView.rating = Double(item?.user?.ratings_avg ?? "0") ?? 0
            lblRateCount.text = "\(/item?.user?.ratings_avg)"
            imgViewCustomer.kf.setImage(with: URL.init(string: "\(/item?.user?.profile_pic_url)"), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        if item?.order_status == "Confirmed"  { //|| item?.order_status == "Onreached"
            self.btnCollapse.isHidden = false
        }else{
            self.btnCollapse.isHidden = true
        }
        if item?.order_status == "Confirmed" {
            addressHeadingLabel.text = "request.pickuplocation".localized
            lblAddress.text = item?.pickup_address
        }else{
            addressHeadingLabel.text = "request.dropLocation".localized
            lblAddress.text = item?.dropoff_address
        }
        self.addTimerToHideShowEndRideButton()
        
        //UPDATE  DISTANCE FROM MODEL(DATA IS ADDED IN MODEL IN DIRECTION API AT MAPSCREEN)
        
        
        
        lblTime.text = "\(/item?.etaTimeGoogleApi)"
        lblDistance.text = "\(/item?.distanceGoogleApi)"
        
        
        print("aaya \(lblTime.text )")
        
        
        //CALULATE FARE
        let _ = calculateFair()
        
        if item?.organisation_coupon_user_id != 0{
            lblPrice.text = "E-Tokens - " + "\(/item?.payment?.product_quantity)"
        }
        else{
            let valDouble = Double(/item?.payment?.final_charge)?.getTwoDecimalFloat()
            lblPrice.text = "\(/valDouble) \(R.string.localizable.currency())"
        }
        
        
        
        //TODO: initial setup when order is confirmed
        
        if /item?.order_status == SocketConstants.reached || /item?.order_status == SocketConstants.confirmed || /item?.order_status == SocketConstants.dApproved {
            
            //TODO: Setup button title Truck, breakdown ,recovery etc(4,5,6)
            if  (item?.category_id == 4 || item?.category_id == 7){
                
                // set collapse button title if approved otherwise set title cancel
                
                if /item?.order_status == SocketConstants.confirmed {
                    //       btnCollapse.setTitle(R.string.localizable.requestCancel(), for: .normal)
                }
                else{
                    //     btnCollapse.setTitle(R.string.localizable.alertsCollapse(), for: .normal)
                }
                
                // Check driver distance at pickup location if driver is within 1000 meters change title to "START RIDE NOW"
                
                let distance = getDistanceBetweenLatLongs(lat: Float(LocationManager.shared.latitude) , long: Float(LocationManager.shared.longitude) , latitude: Float(/item?.pickup_latitude) , longitude: Float(/item?.pickup_longitude))
                
                if Int(distance) < 10000{
                    btnDropRide.setTitle(R.string.localizable.requestStart(), for: .normal)
                    
                    if (/item?.order_status == SocketConstants.confirmed && (item?.category_id == 7 || item?.category_id == 4)) {
                        btnDropRide.setTitle(R.string.localizable.requestReached(), for: .normal)
                    }
                    startRideIfDriverReached = true
                    
                    //                    btnNavigate.isHidden = true
                    //                    btnNavigateWidth.constant = 0
                    
                    btnNavigate.isHidden = false
                    btnNavigateWidth.constant = 48
                    if /item?.order_status != SocketConstants.confirmed{
                        checkForStopsStartStop(btn: btnDropRide)
                    }
                    return
                }
                
                // set button title "START AT PICKUP LOCATION" when driver is not near pickup location
                btnDropRide.setTitle("Start At Pickup Location", for: .normal)
                if (/item?.order_status == SocketConstants.confirmed && (item?.category_id == 7 || item?.category_id == 4)) {
                    btnDropRide.setTitle(R.string.localizable.requestReached(), for: .normal)
                }
                btnNavigate.isHidden = false
                btnNavigateWidth.constant = 48
                startRideIfDriverReached = false
                
            }
                //TODO: Setup button title for gas,mineral water,tanker.
                
            else{
                
                // order status confirmed
                
                print("Turn  \(/item?.my_turn)")
                
                // Both button titles in gas,mineral water,tanker.
                btnDropRide.setTitle("request.start".localized, for: .normal)
            
                btnCollapseArrow.isHidden = true
                
                btnNavigateWidth.constant = 0
                btnNavigate.isHidden = true
                
                
                //   btnCollapse.setTitle("request.cancel".localized, for: .normal)
            }
        }
        else{
            
            //TODO: initial setup when order is ready for dropoff (final status)
            
            // collapse button title
            //    btnCollapse.setTitle(R.string.localizable.alertsCollapse(), for: .normal)
            
            // Current ride button title which is currently  moving to drop delivery
            
            print("Turn  \(/item?.my_turn)")
            
            if /item?.my_turn == "1"{
                
                if (item?.category_id == 4 || item?.category_id == 7){
                    if rideDistance.distance > 1000{
                        
                        if !rideDistance.dictionaryAllRides.keys.contains("\(/item?.order_id)"){
                            print("distance when enterd --\(rideDistance.distance)")
                            rideDistance.dictionaryAllRides["\(item!.order_id!)"] = rideDistance.distance
                        }
                    }
                }
                halfWayStopBtnHeight.constant = 48
                //halfWayStopMainView.isHidden = false
                //breakDownMainView.isHidden = false
                btnDropRide.setTitle("alerts.drop".localized, for: .normal)
                btnNavigate.isHidden = false
                btnCollapseArrow.isHidden = false
                btnNavigateWidth.constant = 48
                
            }
            else{
                // Other  rides button title.
                btnDropRide.setTitle("alert.changeRide".localized, for: .normal)
                btnNavigate.isHidden = true
                btnNavigateWidth.constant = 0
            }
        }
        
//        if item?.order_status == SocketConstants.Ongoing && getOngoingStops() == nil{
//            amountField.isHidden = false
//        }else{
//            amountField.isHidden = true
//        }
        
        checkForStopsStartStop(btn: btnDropRide)
       
    }
    
    func checkForStopsStartStop(btn:UIButton){
        
        guard let item = item,let roadStops = item.roadStops else{return}
        
        
        
        for roadStop in roadStops{
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .ordinal
            
            let string = numberFormatter.string(from: NSNumber(value: /roadStop.priority))
            
            if roadStop.stop_status == "pending"{
                
                btn.setTitle("Start \(/string) stop ride", for: .normal)
                break
            }
            else if roadStop.stop_status == "ongoing"{
                
                btn.setTitle("End \(/string) stop ride", for: .normal)
                break
            }
        }
        
    }
    
    
    @objc func handleBadge(notification:NSNotification){
        
        if /(notification.object as? Bool){
            
            btnChat.setImage(R.image.ic_chat_active(), for: .normal)
        }
        else{
            
             btnChat.setImage(R.image.chat_black(), for: .normal)
        }
    }
    
    func addTimerToHideShowEndRideButton(){
        if item?.order_status == "Confirmed" {
            self.endrideButtonHeight?.constant = 0
            self.btnDropRide.isHidden = true
            timer.invalidate()
            self.addTimer()
        }else{
            timer.invalidate()
            endrideButtonHeight?.constant = 48
            btnDropRide.isHidden = false
            
        }
    }
    
    func addTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (timer) in
            let distance = (self.getDistanceBetweenLatLongs(lat: Float(Locations.lat.getLoc()) , long: Float(Locations.longitutde.getLoc()) , latitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude) , longitude: Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude)))
         //   if distance <= 500 {
                self.timer.invalidate()
                self.endrideButtonHeight?.constant = 48
                self.btnDropRide.isHidden = false
//            }else{
//                self.endrideButtonHeight.constant = 0
//                self.btnDropRide.isHidden = true
//            }
        })
    }
    
    func getDistanceBetweenLatLongs(lat : Float , long : Float , latitude: Float , longitude: Float ) -> Double {
        let userCoordinates = CLLocation(latitude: CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
        return userCoordinates.distance(from: CLLocation(latitude: Double(lat) , longitude: Double(long)))
    }
    
    //NOTE: CALCULATE FARE
    
    func calculateFair() -> String{
        var base_price = 0.0
        
        base_price = /Double(/item?.payment?.product_quantity) * /Double(/item?.payment?.product_per_quantity_charge)
        
        let product_distance_charge = /Double(/item?.payment?.product_per_distance_charge) * /Double(/item?.payment?.order_distance)
        let product_weight_charge = /Double(/item?.payment?.product_per_weight_charge)
        let product_alpha_charge = /Double(/item?.payment?.product_alpha_charge)
        
        let total_charge = /base_price + product_distance_charge + product_weight_charge + product_alpha_charge
        
        
        let buraqPercentage = item?.payment?.buraq_percentage
        var share = 0.0
        
        if let anInt = buraqPercentage as? Int{
            share  = Double(anInt)/100 * total_charge
        }
        if let aDouble = buraqPercentage as? Double{
            share  = aDouble/100 * total_charge
        }
        if let aString = buraqPercentage as? String {
            share  = /Double(aString)/100 * total_charge
        }
        
        let Final = total_charge + share
        return "\(/Final.getTwoDecimalFloat())"
    }
    
    //MARK:-
    @IBAction func actionCallCustomer(_ sender: Any) {
        if bookingtype == "Friend" {
           self.callToNumber(number: "\(/friendCountryCode)\(/friendPhoneNumber)")
        }else{
        self.callToNumber(number: "\(/item?.user?.phone_code)\(/item?.user?.phone_number)")
        }
    }
    
    
    @IBAction func btnChatAction(_ sender: Any) {
           
           guard let item = item else{return}
         btnChat.setImage(R.image.chat_black(), for: .normal)
           if bookingtype == "Friend" {
           self.delegate?.goToChat(item: item,name: /friendName,profilePic: /friendPicUrl)
           }
           else{
               
               self.delegate?.goToChat(item: item,name: /item.user?.name,profilePic: /item.user?.profile_pic_url)
           }
           
       }
    
    @IBAction func addStopsAction(_ sender: AnimatableButton) {
        self.delegate?.addStops()
        //ADDSTOPS
    }
//    @IBAction func actionDropAtAddress(_ sender: Any) {
//
//        //TODO: IF STATUS IS NOT ONGOING CHECK IF DRIVER IS NEAR TO PICKUP LOCATION IN CASE OF FREIGHT SERVICES (TO BE USED IN 4,5,6 CATEGORY) TO START
//
//        if item?.order_status != SocketConstants.Ongoing {
//
//            if  (item?.category_id == 4 || item?.category_id == 7) {
//                if !startRideIfDriverReached {
//                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertReachPickupToStart(), type: .info)
//                    //                    return//TODO: Uncomment Sandeep
//                }
//            }
//
//            //passing delegate to map view controller to drop rides or make current ride according to status
//
//            delegate?.dropDelivery(orderId: "\(/item?.order_id)", paymentType:"\(/item?.payment?.payment_type)", myTurn: "\(/item?.my_turn)", orderStatus: /item?.order_status, categoryId: /item?.category_id, organisationCouponUserId: /item?.organisation_coupon_user_id, order: item)
//        } else {
//
//            //            let distance = getDistanceBetweenLatLongs(lat: Float(LocationManager.shared.latitude) , long: Float(LocationManager.shared.longitude) , latitude: Float(/item?.dropoff_latitude) , longitude: Float(/item?.dropoff_longitude))
//            //
//            // check if ride status is ongoing and is current ride (myTurn == 1) and check distance condition to enable drop request
//            //            if self.item?.order_status == SocketConstants.Ongoing && self.item?.my_turn == "1"{
//            //                if Int(distance) > 100000{
//            //                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertReachDropoffToDrop(), type: .info)
//            //                    return
//            //                }
//            //            }
//
//            //passing delegate to map view controller to drop rides or make current ride according to status
//
//            self.delegate?.dropDelivery(orderId: "\(/self.item?.order_id)", paymentType:"\(/self.item?.payment?.payment_type)", myTurn: "\(/self.item?.my_turn)", orderStatus: /self.item?.order_status, categoryId: /self.item?.category_id, organisationCouponUserId: /self.item?.organisation_coupon_user_id, order: self.item)
//        }
//    }
    
    @IBAction func actionDropAtAddress(_ sender: Any) {
        
        //TODO: IF STATUS IS NOT ONGOING CHECK IF DRIVER IS NEAR TO PICKUP LOCATION IN CASE OF FREIGHT SERVICES (TO BE USED IN 4,5,6 CATEGORY) TO START
        
        if item?.order_status != SocketConstants.Ongoing {
            
            if  (item?.category_id == 4 || item?.category_id == 7 || item?.category_id == 10) {
                if !startRideIfDriverReached {
                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertReachPickupToStart(), type: .info)
                    //                    return//TODO: Uncomment Sandeep
                }
            }
            
            //passing delegate to map view controller to drop rides or make current ride according to status
            
            if let rideStop = getOngoingStops(){
                
               
                    
                    delegate?.updateRideStop(orderId: (/item?.order_id), ride: rideStop)
                    
                    
              
            }
            else{
//                guard let amount = amountField.text, let _ = Double(amount) else {
//                    Alerts.shared.showOnTop(alert: "Becab", message: "please enter amount".localized, type: .error)
//                    return
//                }
                
                delegate?.dropDelivery(orderId: "\(/item?.order_id)", paymentType:"\(/item?.payment?.payment_type)", myTurn: "\(/item?.my_turn)", orderStatus: /item?.order_status, categoryId: /item?.category_id, organisationCouponUserId: /item?.organisation_coupon_user_id, order: item, amount: amountField.text)
                    
            }
            

            let distance = getDistanceBetweenLatLongs(lat: Float(LocationManager.shared.latitude) , long: Float(LocationManager.shared.longitude) , latitude: Float(/item?.dropoff_latitude) , longitude: Float(/item?.dropoff_longitude))
            
             //check if ride status is ongoing and is current ride (myTurn == 1) and check distance condition to enable drop request
            if self.item?.order_status == SocketConstants.Ongoing && self.item?.my_turn == "1"{
                if Int(distance) * 1000 < 200 { //100000
                   //under 200 meters
                } else {
                    Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertReachDropoffToDrop(), type: .info)
                    return
                }
            }

            
        } else {
            
                //passing delegate to map view controller to drop rides or make current ride according to status
                if let rideStop = getOngoingStops(){
                    
                    delegate?.updateRideStop(orderId: (/item?.order_id), ride: rideStop)
                    
                } else{
                    
                    
                    self.delegate?.dropDelivery(orderId: "\(/self.item?.order_id)", paymentType:"\(/self.item?.payment?.payment_type)", myTurn: "\(/self.item?.my_turn)", orderStatus: /self.item?.order_status, categoryId: /self.item?.category_id, organisationCouponUserId: /self.item?.organisation_coupon_user_id, order: self.item,amount:amountField.text)
                }
            }
    }
    
    func getOngoingStops()->RideStops?{
        
         guard let item = item,let roadStops = item.roadStops else{return nil}
        
        var  rideStop:RideStops? = nil
        
        

         for roadStop in roadStops{
          
             
             if roadStop.stop_status == "pending"{
                if roadStop.priority == 1{
                    
                    break
                }
                 rideStop = roadStop
                break
                 
             }
             else if roadStop.stop_status == "ongoing"{
                 
                rideStop = roadStop
                 break
             }
         }
        
        return rideStop
        
    }
    
    @IBAction func ationHalfWayStopping(_ sender: UIButton) {
        self.delegate?.halfWayStopping(orderId: "\(/self.item?.order_id)", paymentType:"\(/self.item?.payment?.payment_type)",halfWayStopReason:"")
    }
    
    @IBAction func actionBreakDown(_ sender: UIButton) {
       self.delegate?.breakDown(orderId: "\(/self.item?.order_id)")
    }
    
    
    @IBAction func actionCollapse(_ sender: UIButton) {
        
        if sender.tag == 1{
            delegate?.CollapseCollectionView(orderStatus: /item?.order_status,orderID: /item?.order_id?.toString(),categoryId: /item?.category_id)
        }
        else{
            
            // delegate passed to collapse the ride collection view to small view
            delegate?.CancelCurrentRide(orderStatus: /item?.order_status,orderID: /item?.order_id?.toString(),categoryId: /item?.category_id)
        }
    }
    
    @IBAction func actionNavigate(_ sender: Any) {
        
        var dropOff_latitude:Double = (item?.dropoff_latitude ?? 0.0)
        var dropOff_longitude:Double = (item?.dropoff_longitude ?? 0.0)
        
        if /item?.order_status == SocketConstants.confirmed || /item?.order_status == SocketConstants.dApproved {
            
            //TODO: Setup button title Truck, breakdown ,recovery etc(4,5,6)
            if  (item?.category_id == 4 || item?.category_id == 7){
                
                // set collapse button title if approved otherwise set title cancel
                
                if /item?.order_status == SocketConstants.confirmed {
                    dropOff_latitude = /item?.pickup_latitude
                    dropOff_longitude = /item?.pickup_longitude
                }
            }
        }
        
        let alert = UIAlertController(title: "Alert".localized, message: "Becab wants to open your google map application".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { (action) in
            
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(dropOff_latitude),\(dropOff_longitude)&directionsmode=driving") {
                    UIApplication.shared.open(url, options: [:])
                }
            }else{
                
                if let controller = self.vc{
                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), actionTitle: R.string.localizable.alertInstallGoogleMaps(), viewController: controller) {
                        
                        let urlStr = "itms-apps://itunes.apple.com/in/app/google-maps-transit-food/id585027354?mt=8"    // Google maps to open in appstore
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                            
                        } else {
                            UIApplication.shared.openURL(URL(string: urlStr)!)
                        }
                    }
                }
                else{
                    Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), type: .error)
                }
            }
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "No".localized, style: .cancel, handler: { (action) in
                   
               }))
        
        vc!.present(alert, animated: true)
        
        
        
     /*
        Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), actionTitle: R.string.localizable.alertInstallGoogleMaps(), viewController: /vc
            ) {
                
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(dropOff_latitude),\(dropOff_longitude)&directionsmode=driving") {
                        UIApplication.shared.open(url, options: [:])
                    }
                }else{
                    
                    if let controller = self.vc{
                        Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), actionTitle: R.string.localizable.alertInstallGoogleMaps(), viewController: controller) {
                            
                            let urlStr = "itms-apps://itunes.apple.com/in/app/google-maps-transit-food/id585027354?mt=8"    // Google maps to open in appstore
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                                
                            } else {
                                UIApplication.shared.openURL(URL(string: urlStr)!)
                            }
                        }
                    }
                    else{
                        Alerts.shared.showOnTop(alert: R.string.localizable.alertAlert(), message: R.string.localizable.alertGoogleMapNotInstalled(), type: .error)
                    }
                }
        } */
    
    }
    //MARK:- added by heeba
    func chatStatus() {
        let chatMsg = chat?[0].text ?? ""
        if chatMsg != "" {
            btnChat.setImage(#imageLiteral(resourceName: "ic_chat_active") , for: .normal)
        } else {
            btnChat.setImage(#imageLiteral(resourceName: "ic_chat"), for: .normal)
        }
    }
}
