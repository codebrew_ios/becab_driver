//
//  EmergencyContactsTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class EmergencyContactsTableViewCell: UITableViewCell {
    
    //MARK--------------------outlets -------------------
    @IBOutlet weak var imgView: AnimatableImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    
    //MARK::- PROPERTIES
    var contacts : Contacts?{
        didSet {
            configureCell()
        }
    }
    var cont : String?{
        didSet {
            configureCell()
        }
    }
    
    //MARK::- FUNCTIONS
    private func configureCell() {
        
        lblName.text = contacts?.name
        lblNumber.text = "\(/contacts?.phone_number)"
       
    }

    //MARK::- ACTIONS
    @IBAction func callContactSelected(_ sender: UIButton) {
        
        let str: String = "\(/contacts?.phone_number)"
        self.callToNumber(number: str)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
