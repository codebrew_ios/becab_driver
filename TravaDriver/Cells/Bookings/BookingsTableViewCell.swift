//
//  BookingsTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable

class BookingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRideId: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgView: AnimatableImageView!
    
    var booking : Bookings? {
        
        didSet {
            configureCell()
        }
    }
    
    let formatter = DateFormatter()
    
    private func configureCell() {
        lblRideId.text = "Id: \(/booking?.order_token)"
        let finalCharge = String(format: "%.02f", /Double(/booking?.payment?.final_charge))
        
        if booking?.organisation_coupon_user_id != 0{
            
            lblAmount.text = "E-Tokens- \(/booking?.payment?.product_quantity)"
        }
        else{
            lblAmount.text = "\(R.string.localizable.txtCash())- \(finalCharge) \(R.string.localizable.currency())"
        }
        
        // let dateutc = /booking?.payment?.updated_at
        let dateutc = /booking?.order_timings
        lblDate.text = dateutc.UTCToLocal(date: dateutc)
        
        if booking?.order_status == SocketConstants.customerCancel{
            lblStatus.text = R.string.localizable.bookingUpcomingCancelled()
            lblStatus.textColor = R.color.appRed()
        }
        else if booking?.order_status == SocketConstants.orderCompleted ||  booking?.order_status == SocketConstants.customerCompletedEtoken{
            lblStatus.text = R.string.localizable.bookingCompleted()
              lblStatus.textColor = UIColor.green
             
        }
        else if booking?.order_status == SocketConstants.orderUpcoming{
            lblStatus.text = booking?.order_status
//            lblStatus.text = R.string.localizable.bookingUpcoming()
            let dateutc = /booking?.order_timings
            lblDate.text = dateutc.UTCToLocal(date: dateutc)
        }
        else if booking?.order_status == SocketConstants.dApproved{
            lblStatus.text = R.string.localizable.bookingApproved()
            let dateutc = /booking?.order_timings
            lblDate.text = dateutc.UTCToLocal(date: dateutc)
        }
        else if booking?.order_status == SocketConstants.driverCancel || booking?.order_status == SocketConstants.customerCancelEtoken{
            lblStatus.text = R.string.localizable.bookingUpcomingCancelled()
            lblStatus.textColor = R.color.appRed()
        }
        else if  booking?.order_status == SocketConstants.orderTimeout{
             lblStatus.text = "Timeout"
        }
            
        else if  booking?.order_status == SocketConstants.serCustPending{
             lblStatus.text = "Approval Pending"
        }
        else if  booking?.order_status == SocketConstants.reached {
            lblStatus.text = R.string.localizable.requestReached()
        }
        else{
            
            lblStatus.text = booking?.order_status
        }
        
        let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=\(/booking?.dropoff_latitude),\(/booking?.dropoff_longitude)&\("zoom=15&size=\(2 * Int(imgView.frame.size.width))x\(2 * Int(imgView.frame.size.height))")&sensor=true&key=\(APIConstants.googleApiKey)"
        
        let mapUrl: NSURL = NSURL(string: staticMapUrl)!
        
        self.imgView.kf.setImage(with: mapUrl as URL, placeholder: nil , options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    
    func calculateFair() -> String{
        
        let base_price = /booking?.payment?.product_quantity * /Int(/booking?.payment?.product_per_quantity_charge)
        
//        let shareBuraq = (Float(/booking?.payment?.buraq_percentage) / 100 ) * 100
      
        let product_distance_charge = Int(/booking?.payment?.product_per_distance_charge)
        let product_weight_charge = Int(/booking?.payment?.product_per_weight_charge)
        let product_alpha_charge = Int(/booking?.payment?.product_alpha_charge)
        let total_charge = base_price + /product_distance_charge + /product_weight_charge + /product_alpha_charge
        return "\(/total_charge)"
    }
    
    func getStatus(type:String) -> String {
        
        switch type {
        case "SerComplete":
            return "Completed"
        case "Serpending":
            return "Pending"
        case "CustCancel":
            return "Cancelled"
        default:
            return ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        


        // Initialization code
    }
    
    
}
