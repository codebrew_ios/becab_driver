//
//  DeliveryAreaTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 25/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class DeliveryAreaTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    var title : String? {
        
        didSet {
            configureCell()
        }
    }
    
    let formatter = DateFormatter()
    
    private func configureCell() {
        
        lblName.text = title
        
    }
  

}
