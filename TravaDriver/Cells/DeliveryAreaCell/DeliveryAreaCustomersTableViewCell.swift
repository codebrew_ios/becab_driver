//
//  DeliveryAreaCustomersTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 25/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import Kingfisher

enum EtokenClientStatus : String{
    case NoOrder = "NoOrder"
    case SerCustCancel = "SerCustCancel"
    case Confirmed = "Confirmed"
    case SerCustConfirm = "SerCustConfirm"
    case CustCancel = "CustCancel"
    case Ctimeout = "CTimeout"
    case DriverCancel = "DriverCancel"
    
    func getValue() -> String {
        
        switch self {
        case .NoOrder:
            return R.string.localizable.tokenStatusStartDelivery()
            
        case .SerCustCancel:
            return R.string.localizable.tokenStatusCancelled()
            
        case .Confirmed:
            return R.string.localizable.tokenStatusConfirmed()
            
        case .SerCustConfirm:
            return R.string.localizable.tokenStatusCompletedOrder()
            
        case .CustCancel :
            return R.string.localizable.tokenStatusCancelled()
            
        case .Ctimeout :
            return R.string.localizable.tokenStatusTimeout()
            
        case .DriverCancel :
            return R.string.localizable.tokenStatusCancelled()
        }
    }
    
}

protocol refreshClientDelegate {
    func reloadClients()
}


class DeliveryAreaCustomersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgClient: AnimatableImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgRating: UIImageView!
    @IBOutlet weak var lblRateCOunt: UILabel!
    @IBOutlet weak var lblProductDetail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var btnDelivery: AnimatableButton!
    
    var item : EtokenClients? {
        
        didSet {
            configureCell()
        }
    }
    
    var isOngoing: Bool?{
        didSet{
            
            if /isOngoing{
                self.btnDelivery.backgroundColor = UIColor.white
                self.btnDelivery.setTitleColor(Colors.themeColor, for: .normal)
                self.btnDelivery.isEnabled = false
                self.btnDelivery.borderColor = Colors.themeColor
                self.btnDelivery.borderWidth = 2.0
                
            }
            else{
                self.btnDelivery.backgroundColor = Colors.themeColor
                self.btnDelivery.setTitleColor(UIColor.white, for: .normal)
                self.btnDelivery.isEnabled = true
                self.btnDelivery.borderColor = Colors.themeColor
                self.btnDelivery.borderWidth = 2.0
            }
        }
    }
    
    var delegate:refreshClientDelegate?
    
    let formatter = DateFormatter()
    
    private func configureCell() {
        
        lblName.text = /item?.name
        lblAddress.text = /item?.address
        lblProductDetail.text =  "\(/item?.product_name) × \(/item?.bottle_quantity)"
        imgClient.kf.setImage(with: URL.init(string: /item?.profile_pic_url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
        
        self.lblRateCOunt.text = "\(/item?.rating_count)"
        self.imgRating.getRatingImage(rating: "\(/item?.ratings_avg)")
        
        self.isOngoing = false
        
        switch /item?.todayOrderStatus{
        case EtokenClientStatus.NoOrder.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.NoOrder.getValue(), for: .normal)
            
        case EtokenClientStatus.SerCustCancel.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.SerCustCancel.getValue(), for: .normal)
            
        case EtokenClientStatus.Confirmed.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.Confirmed.getValue(), for: .normal)
            self.isOngoing = true
            
        case EtokenClientStatus.SerCustConfirm.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.SerCustConfirm.getValue(), for: .normal)
            self.isOngoing = true
            
        case EtokenClientStatus.CustCancel.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.CustCancel.getValue(), for: .normal)
            
        case EtokenClientStatus.DriverCancel.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.CustCancel.getValue(), for: .normal)
            
        case EtokenClientStatus.Ctimeout.rawValue:
            self.btnDelivery.setTitle(EtokenClientStatus.Ctimeout.getValue(), for: .normal)
            
        default:
            self.btnDelivery.setTitle(/item?.todayOrderStatus, for: .normal)
        }
    }
    
    @IBAction func actionCallClient(_ sender: Any) {
        
        let number = "\(/item?.phone_code)\(/item?.phone_number)"
        self.callToNumber(number: number)
    }
    
    @IBAction func actionStartRide(_ sender: Any) {
        
        APIManager.shared.request(with: GeneralEndPoint.initiateOrderFromEtoken(organisation_coupon_user_id: /item?.organisation_coupon_user_id, latitude: /LocationManager.shared.currentLoc?.coordinate.latitude, longitude: /LocationManager.shared.currentLoc?.coordinate.longitude)) { [weak self ](response) in
            switch response{
                
            case .success(_):
                self?.delegate?.reloadClients()
            case .failure(let err):
                Alerts.shared.show(alert: R.string.localizable.alertAlert(), message: /err, type: .info)
                
            }
        }
        
    }
    
    
}
