//
//  UploadDocHeaderTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 07/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

protocol UploadDocHeaderTableViewCellDelegate {
    func uploadCertificateClicked(section: Int?)
}

extension UploadDocHeaderTableViewCellDelegate {
    
    func uploadCertificateClicked(section: Int?) {}
}

class UploadDocHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var borderImageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonUpload: UIButton!
    
    var delegate: UploadDocHeaderTableViewCellDelegate?
    var sectionIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension UploadDocHeaderTableViewCell {
    
    @IBAction func buttonUploadCertclicked(_ sender: Any) {
        delegate?.uploadCertificateClicked(section: sectionIndex)
    }
    
}
