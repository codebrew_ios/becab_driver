//
//  ColorsTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 23/07/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit

class ColorsTableViewCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView?
    @IBOutlet weak var colorLabel: UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
