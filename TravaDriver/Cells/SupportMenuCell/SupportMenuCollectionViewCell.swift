//
//  SupportMenuCollectionViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class SupportMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var indexPath: IndexPath?
    var item: Support? {
        didSet {
            configureCell()
        }
    }
    
    private func configureCell() {
        lblName.text = item?.name
        imgView.kf.setImage(with: URL.init(string: /item?.image_url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
    }
}
