

import UIKit
protocol ServiceLineDelegate: class {
    func selectBrand(index: Int)
}

class ServiceLineTableCell: UITableViewCell {
    
    // MARK:- Outlets
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblServiceName: UILabel!
    
    //MARK: -Properties
    weak var delegate: ServiceLineDelegate?
    let checkedImage = UIImage(named: "ic_checkbox_on")! as UIImage
    let uncheckedImage = UIImage(named: "ic_checkbox")! as UIImage

    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                
                btnCheckBox.setImage(checkedImage, for: .selected)
            } else {
                btnCheckBox.setImage(uncheckedImage, for: .normal)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func btnCheckBoxAct(_ sender: UIButton) {
//        if (sender.isSelected){
//            isChecked = false
//            btnCheckBox.isSelected = false
//        }else{
//            isChecked = true
//             btnCheckBox.isSelected = true
//        }
//        
//        delegate?.selectBrand(index: /sender.tag)
    }
}

