//
//  ServiceRequestCollectionViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 19/10/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import IBAnimatable
import CoreLocation


protocol requestCallBacks : class{
    func removeItemAfterTimeOut(indx:IndexPath)
}

class ServiceRequestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var lblDeliveryLocationKey: UILabel!
    @IBOutlet weak var lblDeliveryLocationValue: UILabel!
    @IBOutlet weak var lblCapacityKey: UILabel!
    @IBOutlet weak var lbltotalKey: UILabel!
    @IBOutlet weak var lblCapacityValue: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblCustomerRating: UILabel!
    @IBOutlet weak var lblScheduledTime: UILabel!
    @IBOutlet weak var lblDistanceLeft: UILabel!
    @IBOutlet weak var lbldurationLeft: UILabel!
    @IBOutlet weak var lblScheduleServiceTitleKey: UILabel!
    @IBOutlet weak var lblScheduleTimeKey: UILabel!
    
    @IBOutlet weak var imgViewCustomer: UIImageView!
    @IBOutlet weak var imgRating: UIImageView!
    
    
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: AnimatableButton!
    @IBOutlet weak var viewScheduled: UIView!
    
    @IBOutlet weak var popHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var bottomButtonStackHeight: NSLayoutConstraint!
    
    var count = 0
    var progress :Int?
    let formatter = DateFormatter()
    var currentIndex: IndexPath?
    weak var delegate : requestCallBacks?
   weak  var timer:Timer?
    
    var serviceRequest: ServiceRequest? {
        didSet {
            setupViewController()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViewController()
        // Initialization code
    }
    
    func setupViewController(){
        
        //schedule timer to dismiss  this controller after time
        if timer  == nil{
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector( self.update), userInfo: nil, repeats: true)

        }
        
        self.updateDistanceAndPopupConstaints() // update request distance and pop up height constarints
        self.localiseController() // localise controller strings
        
        //Set Request data in Pop Up Alert
        let fair = calculateFair()
        lblTotalValue.text = "\(fair) \(R.string.localizable.currency())"
        lblCapacityValue.text = "\(/serviceRequest?.order?.product?.brand_name) , \(/serviceRequest?.order?.product?.name) × \(/serviceRequest?.order?.payment?.product_quantity)"
        lblCustomerName.text = "\(/serviceRequest?.order?.user?.name)"
        lblCustomerRating.text = "\(/serviceRequest?.order?.user?.ratings_count)"
        imgRating.getRatingImage(rating: "\(/serviceRequest?.order?.user?.ratings_avg)")
        lblDeliveryLocationValue.text = serviceRequest?.order?.dropoff_address
        
        //Speech  request text over siri
        SpeechSynthesizer.shared.speakTextOverVoice(text: "\(/lblCustomerName.text) has requested \(/serviceRequest?.order?.product?.name) at \(/lblDeliveryLocationValue.text)")
        
        // update image profile
        let url = "\(/serviceRequest?.order?.user?.profile_pic_url)"
        imgViewCustomer.kf.setImage(with: URL.init(string: url), placeholder: #imageLiteral(resourceName: "ic_profie_bg_ph"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func updateDistanceAndPopupConstaints(){
        
        let destinationCoordinate = CLLocationCoordinate2D.init(latitude: /serviceRequest?.order?.dropoff_latitude, longitude: /serviceRequest?.order?.dropoff_longitude)
        
        self.calculateTimeAndDistance(from: LocationManager.shared.currentLoc?.coordinate, to: destinationCoordinate)
        
        //set localised streing
        
        if serviceRequest?.order?.future == "1"{
            self.lblScheduledTime.text = self.UTCToLocal(date: /serviceRequest?.order?.order_timings)
            viewScheduled.isHidden = false
//            popHeightConstarint.constant = 430
            bottomButtonStackHeight.constant = 156
        }
        else{
//            popHeightConstarint.constant = 385
            viewScheduled.isHidden = true
            bottomButtonStackHeight.constant = 115
        }
    }
    
    // localise strings
    
    func localiseController(){
        
        self.lblDeliveryLocationKey.text = R.string.localizable.requestLocation()
        self.lblCapacityKey.text =  R.string.localizable.requestCapacity()
        self.lbltotalKey.text = R.string.localizable.requestTotal()
        self.btnAccept.setTitle(R.string.localizable.requestAccept(), for: .normal)
        self.btnReject.setTitle(R.string.localizable.requestReject(), for: .normal)
    }
    
    //MARK:--- Timer selector method
    
    @objc func update() {
        if(count < /APIConstants.timerProgress) {
            let fractionalProgress = Float(count) / Float(/APIConstants.timerProgress)
            progressView.setProgress(fractionalProgress, animated: true)
            count += 1
        }
        else{
            
            if count == 45 {
                
                if /timer?.isValid {
                    delegate?.removeItemAfterTimeOut(indx: /currentIndex)
                    timer?.invalidate()
                }
            }
        }
    }
    
    //MARK:-- Calculate fair
    
    func calculateFair() -> String{
        
        // Calculation of Base price
        var base_price = /Double(/serviceRequest?.order?.payment?.product_quantity) * /Double(/serviceRequest?.order?.payment?.product_per_quantity_charge)
        
        if base_price == 0{
            base_price = /Double(/serviceRequest?.order?.payment?.product_quantity) * /Double(/serviceRequest?.order?.payment?.product_per_quantity_charge_str)
        }
        
        // Additional Charges
        let product_distance_charge = /Double(/serviceRequest?.order?.payment?.product_per_distance_charge)
        let product_weight_charge = /Double(/serviceRequest?.order?.payment?.product_per_weight_charge)
        let product_alpha_charge = /Double(/serviceRequest?.order?.payment?.product_alpha_charge)
        
        let total_charge = base_price + product_distance_charge + product_weight_charge + product_alpha_charge
        
        return "\(/total_charge)"
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard let dt = dateFormatter.date(from: date)else {return ""}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM d, h:mm a"
        return dateFormatter.string(from: dt)
    }
    
    
    @IBAction func actionAcceptRequest(_ sender: Any) {
        
        APIManager.shared.request(with: ServicesEndPoint.accept(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) { [weak self] (response) in
            
            switch response{
            case .success(_):
                
                if self?.serviceRequest?.order?.future == "1"{
                    
//                    self?.dismiss(animated: true, completion: {
//                        Alerts.shared.show(alert: "alert.alert".localized, message: R.string.localizable.requestScheduledSuccessFully(), type: .info)
//                    })
                }
                else{
//                    self?.dismiss(animated: true, completion: {
//                        self?.delegate?.acceptedRequest()
//                    })
                }
                
            case .failure(let err):
                Alerts.shared.show(alert: "alert.error".localized, message: /err, type: .info)
//                self?.dismiss(animated: true, completion: nil)
                
            }
        }
        
        
    }
    
    @IBAction func actionRejectRequest(_ sender: Any) {
        
        
        APIManager.shared.request(with: ServicesEndPoint.reject(orderId: serviceRequest?.order?.order_id, latitude: Locations.lat.getLoc(), longitude: Locations.longitutde.getLoc())) {[weak self] (response) in
//            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    //MARK: ---------------To dismiss service request view when tapped outside-------------
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch: UITouch? = touches.first
//        if touch?.view == self.view {
//            //            self.dismiss(animated: true, completion: nil)
//        }
//    }
    
    func calculateTimeAndDistance(from source: CLLocationCoordinate2D? , to destination: CLLocationCoordinate2D?){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving&key=\(APIConstants.googleApiKey)")!
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        DispatchQueue.global(qos: .userInteractive).async { [weak self] () -> Void in
                            guard let routes = json["routes"] as? NSArray else {
                                return
                            }
                            
                            if (routes.count > 0) {
                                
                                guard let legs  = routes[0] as? NSDictionary , let legsJ = legs["legs"] as? NSArray , let lg = legsJ[0] as? NSDictionary else { return }
                                
                                let duration = lg["duration"] as? NSDictionary
                                let durationLeft1 = duration?.object(forKey: "text") as? String
                                
                                let distance = lg["distance"] as? NSDictionary
                                let distanceLeft1 = distance?.object(forKey: "text") as? String
                                DispatchQueue.main.async {
                                    self?.lbldurationLeft.text = "\(/durationLeft1)"
                                    self?.lblDistanceLeft.text = "\(/distanceLeft1)"
                                }
                            }
                        }
                    }
                }catch {
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }

    
    
}
