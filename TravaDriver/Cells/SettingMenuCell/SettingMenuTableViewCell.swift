//
//  SettingMenuTableViewCell.swift
//  Buraq24Driver
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class SettingMenuTableViewCell: UITableViewCell {
    
    //MARK:- Outlets

    @IBOutlet weak var lblName: UILabel!
    var indexPath: IndexPath?
    var title: String? {
        didSet {
            configureCell()
        }
    }
    
    private func configureCell() {
        lblName.text = title
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
