//
//  Extension.swift
//  Idea
//
//  Created by Dhan Guru Nanak on 2/16/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import NVActivityIndicatorView
import SafariServices
//import EZSwiftExtensions
//import SDWebImage

extension NSObject {
    class var identifier: String {
        return String(describing: self)
    }
}

extension UILabel{
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}

extension UIViewController : NVActivityIndicatorViewable {
    
    func startAnimateLoader(){
        self.startAnimating(CGSize.init(width: 24, height: 24), message: nil, messageFont: nil, type: .ballPulse, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil)
    }
    
    //    func openHyperLink(link:String?){
    //        var addUrl = link
    //        if addUrl?.lowercased().hasPrefix("http://")==false{
    //            addUrl = "http://" + /addUrl
    //        }
    //
    //        guard let url = URL(string: /addUrl) else {
    //            return //be safe
    //        }
    //        let safariVC = SFSafariViewController(url:url)
    //        safariVC.delegate = self as? SFSafariViewControllerDelegate
    //        ez.topMostVC?.presentVC(safariVC)
    //
    //    }
    
    
    func alertBox(message:String,title:String, ok:@escaping ()->()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            ok()
        }))
        alert.addAction(UIAlertAction(title:"Cancel", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    func alertBoxWithDoubleAction(message:String,title:String,okTitle:String = "Ok",cancelTitle:String = "Cancel".localized, ok:@escaping ()->(),cancel:@escaping ()->()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: okTitle, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            ok()
        }))
        alert.addAction(UIAlertAction(title:cancelTitle, style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
            cancel()
            //  alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openVideoPlayer(url:String){
        
        if let url = URL(string: url){
            
            let player = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func alertBoxOk(message:String,title:String, ok:@escaping ()->()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            ok()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //    func showShare(shareLink : [Any]?){
    //
    //        if let linkToShare = shareLink, !linkToShare.isEmpty{
    //
    //            let objectsToShare = [link] as [Any]
    //
    //            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
    //
    //            ez.topMostVC?.present(activityVC, animated: true, completion: nil)
    //
    //        }
    //    }
    
}

extension NSObject {
    
    func callToNumber(number : String?){
        
        if let url = URL(string: "tel://\(number ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func sendEmailToUser(emalAddress : String?){
        
        let email = emalAddress ?? "foo@foo.com"
        if let url = URL(string: "mailto:\(email)") ,UIApplication.shared.canOpenURL(url)  {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}


//MARK:- UIImage Extension
extension UIImage {
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0.1
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    var png: Data? { return UIImagePNGRepresentation(self) }
    
    //    func reduceSize(_ quality: JPEGQuality) -> UIImage {
    //
    //        switch quality {
    //
    //        case .low, .lowest:
    //            return self.kf.image(withRoundRadius: 0.0, fit: CGSize(width: 100.0, height: 100.0))
    //
    //        default :
    //            return self.kf.image(withRoundRadius: 0.0, fit: CGSize(width: 200.0, height: 200.0))
    //        }
    //    }
    
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    
}

//MARK: - UIImageView extension
extension UIImageView {
    
    func addBlackGradientLayer(frame: CGRect, colors:[UIColor]){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors.map{$0.cgColor}
        self.layer.addSublayer(gradient)
    }
    
    //    func loadImage(url: String) {
    //        if url != "" {
    //            if url.contains("/") {
    //                if let url = URL(string: url) {
    //                    self.sd_setShowActivityIndicatorView(true)
    //                    self.sd_setIndicatorStyle(.gray)
    //                    self.sd_setImage(with: url, completed: nil)
    //                }
    //            }
    //        }
    //    }
    
    
    func isNull() -> Bool{
        if self.image == nil{
            Toast.show(text: "Please select a picture", type: .info)
            return true
        }else{
            return false
        }
    }
    
    var roundedImage: UIImageView {
        
        let maskLayer = CAShapeLayer(layer: self.layer)
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x:0, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:self.bounds.size.height))
        
        bezierPath.addQuadCurve(to: CGPoint(x:0, y:self.bounds.size.height), controlPoint: CGPoint(x:self.bounds.size.width/2, y:self.bounds.size.height + self.bounds.size.height*0.3))
        
        bezierPath.addLine(to: CGPoint(x:0, y:0))
        
        bezierPath.close()
        
        maskLayer.path = bezierPath.cgPath
        
        maskLayer.frame = self.bounds
        
        maskLayer.masksToBounds = true
        
        self.layer.mask = maskLayer
        
        return self
    }
    
    
    
}

//MARK: - Int extension
extension Int {
    
    func toString() -> String{
        return "\(self)"
        
    }
    
    
    func formattedTimer() -> String {
        
        let mintues = self/60
        let seconds = self%60
        return String(format: "%02d : %02d", mintues ,seconds )
    }
    
}

extension UIImage{
    func imageWithImage( scaledToSize newSize:CGSize) -> UIImage?{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let img = newImage else {
            return self
        }
        return img
    }
    
}

extension Float {
    
    var getZoomPercentage : Float {
        
        let range = maximumZoom - minimumZoom
        let startValue = self - minimumZoom
        return (startValue*100)/range
    }
    
}
extension Double{
    func getTwoDecimalFloat() -> String {
        return  String(format: "%f" ,self)
        // "%.02f"
    }
}


extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension Date {
   
    
    /// EZSE: Calculates how many days passed from now to date
    public func daysInBetweenDate(_ date: Date) -> Double {
        var diff = self.timeIntervalSince1970 - date.timeIntervalSince1970
        diff = fabs(diff/86400)
        return diff
    }
    
    /// EZSE: Calculates how many hours passed from now to date
    public func hoursInBetweenDate(_ date: Date) -> Double {
        var diff = self.timeIntervalSince1970 - date.timeIntervalSince1970
        diff = fabs(diff/3600)
        return diff
    }
    
    /// EZSE: Calculates how many minutes passed from now to date
    public func minutesInBetweenDate(_ date: Date) -> Double {
        var diff = self.timeIntervalSince1970 - date.timeIntervalSince1970
        diff = fabs(diff/60)
        return diff
    }
    
    /// EZSE: Calculates how many seconds passed from now to date
    public func secondsInBetweenDate(_ date: Date) -> Double {
        var diff = self.timeIntervalSince1970 - date.timeIntervalSince1970
        diff = fabs(diff)
        return diff
    }
    
}

//MARK: - String extension
extension String {
    
    
    func removingLeadingSpaces() -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: .whitespaces) }) else {
            return self
        }
        return String(self[index...])
    }
    var trailingSpacesTrimmed: String {
        var newString = self
        
        while newString.hasSuffix(" ") {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
    
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    // Localised String
    var localized: String {
        return NSLocalizedString(self , comment:"")
    }
    
    // Trim String
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func capitalizingFirstLetter() -> String {
        
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst()).lowercased()
        return first + other
    }
    
    
    
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    
    //    func isEmptyText(withErrorMessage:String?) -> Bool{
    //        if self.trimmed().isEmpty{
    //            Toast.show(text: withErrorMessage, type: .error)
    //            return true
    //        } else {
    //            return false
    //        }
    //    }
    
    //    func isNull() ->String? {
    //        if self.trimmed().isEmpty{
    //            return nil
    //        }
    //        return self
    //    }
    
    func validateUrl () -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
    
    public func separate(withChar char : String) -> [String]{
        var word : String = ""
        var words : [String] = [String]()
        for chararacter in self.characters {
            if String(chararacter) == char && word != "" {
                words.append(word)
                word = char
            }else {
                word += String(chararacter)
            }
        }
        words.append(word)
        return words
    }
    
    func replace(myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString)     // gets an array of characters
        chars[index] = newChar
        let modifiedString = String(chars)
        return modifiedString
    }
    
    
}




extension UIPickerView {
    
    func addKeyboardToolBar() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("donePicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("donePicker")))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
    }
}

extension UIButton{
    
    func bounce(){
        self.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.transform = .identity
            },
                       completion: nil)
    }
    
    func transformContent(){
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
    
    func setInsetsWithImage(){
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: self.frame.size.width - 24, bottom: 0, right: 0)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.frame.size.width/2 + 16)
    }
    
    func addGradient(topColor:UIColor,bottomColor:UIColor,statPoint:CGPoint,endPoint:CGPoint){
        let gradient:CAGradientLayer = CAGradientLayer()
        let colorTop = topColor
        let colorBottom = bottomColor
        
        gradient.colors = [colorTop, colorBottom]
        gradient.startPoint = statPoint
        gradient.endPoint = endPoint
        gradient.frame = self.bounds
        gradient.cornerRadius = 5
        self.layer.insertSublayer(gradient, at: 0)
    }
}
extension UITextView{
    
    func resolveHashTags() -> (NSAttributedString,[String]){
        var length : Int = 0
        let text:String = self.text
        let words:[String] = self.text.separate(withChar: " ")
        let hashtagWords = words.flatMap({$0.separate(withChar: "#")})
        let attrs = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16.0)]
        let attrString = NSMutableAttributedString(string: text, attributes:attrs)
        var hashWord = [String]()
        var iterateIndex = 0
        for word in hashtagWords {
            if word.hasPrefix("#") {
                
                let matchRange:NSRange = NSMakeRange(length, word.characters.count)
                let stringifiedWord:String = word
                hashWord.append(word)
                attrString.addAttribute(NSAttributedStringKey.link, value: "hash:\(stringifiedWord)", range: matchRange)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HASHTAG"), object: nil, userInfo: [iterateIndex:word])
                iterateIndex += 1
            }
            length += word.characters.count
        }
        return (attrString,hashWord)
    }
    
}


@IBDesignable class HeaderViewWithShadow: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.lightGray.cgColor
    }
}


@IBDesignable class SmallerSwitch: UISwitch {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        transform = CGAffineTransform(scaleX: 0.774, y: 0.774)
        layer.cornerRadius = self.frame.height / 2.0
    }
}


@IBDesignable class TTNavigationBar: UINavigationBar {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.barTintColor = UIColor.white
        self.titleTextAttributes = [ NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 14)!]
    }
}


extension UINavigationBar{
    
    func addShadow(){
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 5
        
    }
    
    func removeShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 0
    }
    
}

extension UITextView{
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
    
    
    
}

extension UIPageViewController {
    
    func goToCenterVC(viewController:UIViewController?){
        
        guard let seconVC = viewController else { return }
        setViewControllers([seconVC], direction: .forward, animated: false, completion: nil)
        
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x:(labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,y:
            (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x,y:
            locationOfTouchInLabel.y - textContainerOffset.y);
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension NSLayoutConstraint {
    
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        
        return NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
        
    }
}

extension UITableView{
    
    func registerNibTableCell(nibName:String){
        let nib = UINib(nibName: nibName, bundle: nil)
        self.register(nib, forCellReuseIdentifier: nibName)
    }
    
}

extension UICollectionView{
    
    func registerNibCollectionCell(nibName:String,reuseIdentifier:String){
        let nib = UINib(nibName: nibName, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    func deselectAllItems(animated: Bool = false) {
        for indexPath in self.indexPathsForSelectedItems ?? [] {
            self.deselectItem(at: indexPath, animated: animated)
        }
    }
    
}


extension NSMutableAttributedString {
    
    //    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
    //      let attrs: [NSAttributedStringKey: Any] = [(NSFontAttributeName as NSString) as NSAttributedStringKey: UIFont(name:R.font.sfuiDisplayBold.fontName, size: 13)!]
    //        let boldString = NSMutableAttributedString(string:text, attributes: attrs as [String : Any])
    //        append(boldString)
    //
    //        return self
    //    }
    //
    //    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
    //      let attrs: [NSAttributedStringKey: Any] = [(NSFontAttributeName as NSString) as NSAttributedStringKey: UIFont(name:R.font.sfuiDisplayRegular.fontName, size: 13)!]
    //        let boldString = NSMutableAttributedString(string:text, attributes: attrs as [String : Any])
    //        append(boldString)
    //
    //        return self
    //    }
}

extension URL{
    
    static var documentsDirectory: URL {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        return try! documentsDirectory.asURL()
    }
    
    static func urlInDocumentsDirectory(with filename: String) -> URL {
        return documentsDirectory.appendingPathComponent(filename)
    }
    
    
    func getMediaDuration() -> Float64{
        
        let asset : AVURLAsset = AVURLAsset.init(url: self) as AVURLAsset
        let duration : CMTime = asset.duration
        return CMTimeGetSeconds(duration)
        
    }
    
    func generateThumbnail() -> UIImage? {
        do {
            let asset = AVURLAsset(url: self)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: kCMTimeZero, actualTime: nil)
            
            return UIImage(cgImage: cgImage)
        } catch {
            print(error.localizedDescription)
            
            return nil
        }
    }
}


//extension NSObject{
//
//    func uploadMedia(mediaData: Data,fileName:String,mediaType:media,completion:@escaping(_ url:String)->())-> (Uploading){
//
//        let set = S3BucketHelper.shared.uploadRequest(data: mediaData,
//                                                      fileName: fileName,
//                                                      mediaType: .image,mimeType : mediaType == .video ? "mp4" : nil) { (Url) in
//                                                        guard let url = Url else { return }
//                                                        completion(url.absoluteString)
//                                                        print("--------------------\(url)")
//        }
//
//        return (Uploading(name: fileName, uploadRequest: set.0, transferManager: set.1, isAlreadyUploaded: false))
//
//    }
//
//    func setExclusiveTouchToButtons(from view: UIView) {
//        for subview: UIView in view.subviews {
//            if subview.subviews.count > 0 {
//                setExclusiveTouchToButtons(from: subview)
//            }
//            else if (subview is UIButton) {
//                subview.isExclusiveTouch = true
//            }
//        }
//    }
//
//}


extension UIButton {
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}

@IBDesignable
class CustomView: UIView {
    
    @IBInspectable var borderWidth: CGFloat = 0.0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    func addShadow(){
        
        self.layer.shadowColor = UIColor.red.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
}
