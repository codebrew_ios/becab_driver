//
//  Date+Extentions.swift
//  Auttle
//
//  Created by CodeBrew on 9/18/17.
//  Copyright © 2017 CodeBrew. All rights reserved.
//

import Foundation


//MARK: - DATE Extention
extension Date {
    
    
    func dateByAddingYears(_ years : Int ) -> Date {
        let calendar = Calendar.current
        var dateComponent = DateComponents()
        dateComponent.year = years
        return (calendar as NSCalendar).date(byAdding: dateComponent, to: self, options: NSCalendar.Options.matchNextTime)!
    }
    
    
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == .orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == .orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == .orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    //    func isdateEqual(toDate:Date)->Bool{
    //
    ////      return self.year == toDate.year && self.day == toDate.day && self.month == toDate.month ? true : false
    //    }
    
    func subtractDays(daysToAdd: Int) -> Date {
        
        let secondsInDays: TimeInterval = -(Double(daysToAdd) * 60 * 60 * 24)
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays) as Date
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addDays(daysToAdd: Int) -> Date {
        
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays) as Date
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> Date {
        
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours) as Date
        
        //Return Result
        return dateWithHoursAdded
    }
    
    //date to string
    //    func dateToString(format:String) -> String {
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.dateFormat = format //Your New Date format as per requirement change it own
    //        let newDate = dateFormatter.string(from: self)
    //        
    //        return newDate
    //    }
    
    
    
    func dateToString(format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //Your New Date format as per requirement change it own
        dateFormatter.timeZone = TimeZone.current
        
        if BundleLocalization.sharedInstance()?.language == Languages.Spanish {
            dateFormatter.locale = Locale(identifier: "es_PA")
        } else {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        
        
        let newDate = dateFormatter.string(from: self)
        
        return newDate
    }
    
    
    
    func dateToStringWithShortStyle() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.short
        let newDate = dateFormatter.string(from: self)
        return newDate
    }
    
    
    
    func toString(format:String)->String{
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeZone = NSTimeZone.local
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func toStringWithoutTimeZone(format:String)->String{
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeZone = NSTimeZone.default
        
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func addTime(time: Date) -> Date? {
        
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = dateComponents.month!
        mergedComponments.day = dateComponents.day!
        mergedComponments.hour = timeComponents.hour!
        mergedComponments.minute = timeComponents.minute!
        mergedComponments.second = timeComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    var millisecondsSince1970:Double {
        return Double((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    
    public func timeAgoSince() -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: self, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            return "\(year) years ago"
        }
        
        if let year = components.year, year >= 1 {
            return "Last year"
        }
        
        if let month = components.month, month >= 2 {
            return "\(month) months ago"
        }
        
        if let month = components.month, month >= 1 {
            return "Last month"
        }
        
        if let week = components.weekOfYear, week >= 2 {
            return "\(week) weeks ago"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return "Last week"
        }
        
        if let day = components.day, day >= 2 {
            return "\(day) days ago"
        }
        
        if let day = components.day, day >= 1 {
            return "Yesterday"
        }
        
        if let hour = components.hour, hour >= 2 {
            return "\(hour) hours ago"
        }
        
        if let hour = components.hour, hour >= 1 {
            return "An hour ago"
        }
        
        if let minute = components.minute, minute >= 2 {
            return "\(minute) min ago"
        }
        
        if let minute = components.minute, minute >= 1 {
            return "a min ago"
        }
        
        if let second = components.second, second >= 3 {
            return "\(second) sec ago"
        }
        return "Just now"
    }
    
    public func timePending(endTime : Date) -> String {
        
        let calendar = Calendar.current
        let now = endTime
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: self, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            return "\(year) years"
        }
        
        if let year = components.year, year >= 1 {
            return "year"
        }
        
        if let month = components.month, month >= 2 {
            return "\(month) months"
        }
        
        if let month = components.month, month >= 1 {
            return "\(month) month"
        }
        
        if let week = components.weekOfYear, week >= 2 {
            return "\(week) weeks"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return "\(week) week"
        }
        
        if let day = components.day, day >= 2 {
            return "\(day) days ago"
        }
        
        if let day = components.day, day >= 1 {
            return "\(day) day"
        }
        
        if let hour = components.hour, hour >= 2 {
            return "\(hour) hours"
        }
        
        if let hour = components.hour, hour >= 1 {
            return "\(hour) hours"
        }
        
        if let minute = components.minute, minute >= 2 {
            return "\(minute) minutes"
        }
        
        if let minute = components.minute, minute >= 1 {
            return "\(minute) minute"
        }
        
        if let second = components.second, second >= 3 {
            return "\(second) seconds"
        }
        //        return "\(second) second"
        return "second"
        
    }
    
}


extension String{
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        guard  let dt = dateFormatter.date(from: date)else{return ""}
        
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"
        
        return dateFormatter.string(from: dt)
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if BundleLocalization.sharedInstance()?.language == Languages.Spanish {
            dateFormatter.locale = Locale(identifier: "es_PA")
        } else {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        
        guard  let dt = dateFormatter.date(from: date)else{return ""}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM d, h:mm a"
        
        return dateFormatter.string(from: dt)
    }
    
    func UTCToLocalShortStyle(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard  let dt = dateFormatter.date(from: date)else{return ""}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "YYYY-MM-dd"
        
        return dateFormatter.string(from: dt)
    }
    
    func timeDiffrenceBetweenUTCDates(date:String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard  let dt = dateFormatter.date(from: date)else{return 0}
        
        let currentDate = Date()
        
        //        Alerts.shared.showAlertWithActionBlockForce(title: "CurrentTime \(currentDate)", message: "CreationDate \(dt)", actionTitle: "Ok", viewController: /UIApplication.shared.keyWindow?.topMostWindowController() ) {
        //            print("gerghe")
        //        }
        
        //        let timeDifference = dt.timeIntervalSince(currentDate)
        let timeDifference = currentDate.timeIntervalSince(dt)
        
        
        return Int(timeDifference)
    }
}

extension Double {
    func toDate() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self / 1000))
    }
}


extension String {
    func  toDate( dateFormat format  : DateFormat = .preDefined) -> Date {
        let dateFormatter = DIDateFormator.format(dateFormat: format)
        if let date = dateFormatter.date(from: self){
            return date
        }
        print("Invalid arguments ! Returning Current Date . ")
        return Date()
    }
}


enum DateFormat {
    case preDefined
    case display
    
    var format: String {
        switch self {
        case .preDefined: return "yyyy"
        case .display: return "MMMM d, yyyy"
        }
    }
}


class DIDateFormator:NSObject {
    class func format(dateFormat:DateFormat = .preDefined) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.format
        return dateFormatter
    }
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
