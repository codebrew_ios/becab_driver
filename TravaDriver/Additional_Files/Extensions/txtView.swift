//
//  txtView.swift
//  Buraq24
//
//  Created by Apple on 06/08/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

//MARK: -----> TextView With Placeholder
typealias  TextViewShouldChangeBlock = (String) -> ()
class TextViewplaceholder: UITextView, UITextViewDelegate {
    
    @IBInspectable var placeholder: String?
    @IBInspectable var placeholderColor: UIColor?
    @IBInspectable var maxLimit: Int = 10000000
    
    @IBInspectable var textValue: String {
        get {
            if self.text == placeholder {
                return ""
            }
            return self.text
        }
        set {
            text = newValue
        }
    }
    
    var textViewShouldChangeBlock: TextViewShouldChangeBlock?
    
    override func awakeFromNib() {
        delegate = self
        textColor = UIColor.lightGray
        text = (textValue == "") ? placeholder : textValue
        textColor = (placeholderColor != nil) ? placeholderColor : UIColor.lightGray
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if text == placeholder {
            text = ""
            textColor = UIColor.black
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if text == "" {
            text = placeholder
            textColor = (placeholderColor != nil) ? placeholderColor : UIColor.lightGray
            
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if let str = textView.text, let textRange = Range(range, in: str) {
            let updatedText = str.replacingCharacters(in: textRange, with: text)
            
            if let block = textViewShouldChangeBlock {
                block(updatedText)
            }
            
            return updatedText.count < maxLimit
        }
        
        return true
    }
}
