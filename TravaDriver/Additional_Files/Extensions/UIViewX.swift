//

//  UIViewX.swift

//  DesignableX

//

//  Created by Mark Moeykens on 12/31/16.

//  Copyright © 2016 Mark Moeykens. All rights reserved.

//



import UIKit



@IBDesignable

class UIViewX: UIView {
    
    
    
    // MARK: - Gradient
    
    
    
    @IBInspectable var firstColor: UIColor = UIColor.white {
        
        didSet {
            
            updateView()
            
        }
        
    }
    
    
    
    @IBInspectable var secondColor: UIColor = UIColor.white {
        
        didSet {
            
            updateView()
            
        }
        
    }
    
    
    
    @IBInspectable var horizontalGradient: Bool = false {
        
        didSet {
            
            updateView()
            
        }
        
    }
    
    
    
    override class var layerClass: AnyClass {
        
        get {
            
            return CAGradientLayer.self
            
        }
        
    }
    
    
    
    func updateView() {
        
        let layer = self.layer as! CAGradientLayer
        
        layer.colors = [ firstColor.cgColor, secondColor.cgColor ]
        
        
        
        if (horizontalGradient) {
            
            layer.startPoint = CGPoint(x: 0.0, y:1.0)
            
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
        } else {
            
            layer.startPoint = CGPoint(x: 0, y: 1.0 )
            
            layer.endPoint = CGPoint(x: 0, y: 1)
            
        }
        
    }
    
    
    
    
    
    

    @IBInspectable public var cornerRadius: CGFloat = 0 {

        didSet {

            layer.cornerRadius = cornerRadius

        }

    }
    
    
    
    // MARK: - Shadow
    
    
    
    @IBInspectable public var shadowOpacity: CGFloat = 0{
        
        didSet {
            
            layer.shadowOpacity = Float(shadowOpacity)
            
        }
        
    }
    
    
    
    @IBInspectable public var shadowColor: UIColor = UIColor.clear {
        
        didSet {
            
            layer.shadowColor = shadowColor.cgColor
            
        }
        
    }
    
    
    
    @IBInspectable public var shadowRadius: CGFloat = 0 {
        
        didSet {
            
            layer.shadowRadius = shadowRadius
            
        }
        
    }
    
    
    
    @IBInspectable public var shadowOffsetY: CGFloat = 0 {
        
        didSet {
            
            layer.shadowOffset.height = shadowOffsetY
            
        }
        
    }
    
}



extension UIView{
    
    
    
    func makeRoundCorner(withBorder radius:CGFloat , color:UIColor , width: CGFloat) {
        
        self.layer.masksToBounds = true
        
        self.layer.cornerRadius = radius
        
        self.layer.borderColor = color.cgColor
        
        self.layer.borderWidth = width
        
        self.clipsToBounds = true
        
    }
    
    
    
    
    
    func animateView(withDirection direction:Int, toframe: CGRect) {
        
        
        
        if direction == 0 {
            
            self.frame.origin.x = -self.frame.maxX
            
        }else if direction == 1{
            
            self.frame.origin.x = self.frame.maxX
            
        }else if direction == 3{
            
            self.frame.origin.y = self.frame.maxY
            
        }else if direction == 4{
            
            self.frame.origin.y = -toframe.origin.y
            
        }
        
        
        
        UIView.animate(withDuration: 1.2, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.50, options: .curveEaseIn, animations: {
            
            self.frame = toframe
            
            self.layoutIfNeeded()
            
        }, completion: nil)
        
    }
    
    
    
    
    
    func animateAlpha() {
        
        
        
        self.isHidden = false
        
        
        
        UIView.animate(withDuration: 0.6) {
            
            self.alpha = 1.0
            
        }
        
    }
    
    
    
    func animateAlpha(alpha:CGFloat) {
        
        
        
        self.isHidden = false
        
        
        
        UIView.animate(withDuration: 0.3) {
            
            self.alpha = alpha
            
        }
        
    }
    
    
    
    func hideWithAlpha() {
        
        
        
        UIView.animate(withDuration: 0.6, animations: {
            
            self.alpha = 0.0
            
        }) { (done) in
            
            self.isHidden = true
            
        }
        
}

}


extension UIView {
    func addDashedBorder() {
        let viewBorder = CAShapeLayer()
        viewBorder.strokeColor = UIColor.blue.cgColor
        viewBorder.lineDashPattern = [2, 2]
        viewBorder.frame = self.bounds
        viewBorder.fillColor = nil
        viewBorder.path = UIBezierPath(rect: viewBorder.bounds).cgPath
        self.layer.addSublayer(viewBorder)
    }
}
