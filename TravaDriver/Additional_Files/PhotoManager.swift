//
//  PhotoManager.swift
//  Buraq24Driver
//
//  Created by Apple on 26/11/19.
//  Copyright © 2019 OSX. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

//typealias PhotoTakingHelperCallback = ((UIImage?) -> Void)
typealias CallbackwithURL = ((UIImage?) -> Void)
typealias PhotoTakingHelperCallback = ((UIImage?,URL?) -> Void)

enum ImagePickerOptions: Int {
    case Camera
    case Gallery
}

private struct constant {
    
    static let actionTitle = ""
    static let actionMessage = ""
    static let cameraBtnTitle = "takePicture".localized
    static let galeryBtnTitle = "selectImageFromGalary".localized
    static let videogaleryBtnTitle = "selectVideoFromGalary".localized
    
    static let cancelBtnTitle = "alertCancel".localized
}

@objc class PhotoManager:NSObject {
     var fromVC: UIViewController?
    let imagePicker = UIImagePickerController()
    internal var navController: UINavigationController!
    internal var callback: PhotoTakingHelperCallback!
    
    
    /*
     Intialize the navController from give reference of navigationcontroller while creating Photomanager class object.
     Callback: Callback will be call after the picking image.
     */
    @objc init(from vc: UIViewController,navigationController:UINavigationController, VideoOption shouldShow:Bool, callback:@escaping PhotoTakingHelperCallback) {
        
        self.navController = navigationController
        self.callback = callback
        super.init()
        
        self.presentActionSheet(withVideo: true)
        //  checkCameraPermission()
        //  checkPhotoLibraryPermission()
        
        //        if checkPhotoLibraryPermission() == true {
        //            presentActionSheet(withVideo: shouldShow)
        //        }else{
        //         // UtilityClass.getInstance().showAlert(titleStr: "Alert!", messageStr: "Camera permission is required, Please allow camera  in settings.", actiontitleStr: "OK", withAction: false, viewController: navigationController.topViewController!)
        //        }
    }
    
    internal func openSettings()
    {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl)
        {
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        
       }
    
    //MARK: ImagePicker Custom Functions
    /// Presenting sheet with option to select image source
    private func presentActionSheet(withVideo:Bool) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraButton = UIAlertAction(title: constant.cameraBtnTitle, style: .default, handler: { (action) -> Void in
            self.checkCameraPermission()
            
        })
        
        let  galleryButton = UIAlertAction(title: constant.galeryBtnTitle, style: .default, handler: { (action) -> Void in
            self.checkPhotoLibraryPermission()
        })
        
        let  videoGalleryButton = UIAlertAction(title: constant.videogaleryBtnTitle, style: .default, handler: { (action) -> Void in
            self.checkPhotoLibraryPermission(withVideoSatatus: true)
        })
        
        let cancelButton = UIAlertAction(title: constant.cancelBtnTitle, style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cameraButton)
        alertController.addAction(galleryButton)
        
        if withVideo{
            alertController.addAction(videoGalleryButton)
        }
        alertController.addAction(cancelButton)
        
        navController.present(alertController, animated: true, completion: nil)
        // alertController.view.tintColor = appThemeColor
    }
    
    
    
    private func checkCameraPermission() {
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                
                if granted {
                    self.presentUIimagePicker(type: .camera)
                } else {
                     Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "alert.photoPermissionDenied".localized, actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.fromVC) {
                                                 self.openSettings()
                                             }
                }
            }
        //  return true
        case .authorized:
            self.presentUIimagePicker(type: .camera)
            break
            
        case .denied, .restricted:
           Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "alert.photoPermissionDenied".localized, actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.fromVC) {
                                         self.openSettings()
                                     }
            
        }
    }
    
    
    func checkPhotoLibraryPermission(withVideoSatatus videoStatus:Bool = false) {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
            
        case .authorized:
            if videoStatus {
                self.presentUIimagePicker(witMedidaType: "public.movie", type: .photoLibrary)
                return
            }
            self.presentUIimagePicker(type: .photoLibrary)
            break
            
        case .denied, .restricted :
              Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "alert.photoPermissionDenied".localized, actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.fromVC) {
                                         self.openSettings()
                                     }
            // Utility.openSettingsOfApp(navigationController: self.navController, message: "Change app settings to enable the access of media.")
            break
            
        //handle denied status
        case .notDetermined:
            // ask for permissions
            
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized:
                    if videoStatus {
                        self.presentUIimagePicker(witMedidaType: "public.movie", type: .photoLibrary)
                        return
                    }
                    self.presentUIimagePicker(type: .photoLibrary)
                    break
                case .denied, .restricted , .notDetermined:
                     Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "alert.photoPermissionDenied".localized, actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.fromVC) {
                                                 self.openSettings()
                                             }
                    
                default:
                    break
                }
            }
        default:
            break
        }
    }
    
    
    /*
     presentUIimagePicker will present the UIImagePicker with give type
     type: Camera or Gallery
     controller: UINavigationcontroller, navigationcontroller on with uiimagepicker will present.
     */
    private func presentUIimagePicker(type: UIImagePickerControllerSourceType){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = type
        imagePicker.delegate = self
        
        navController.present(imagePicker, animated: true, completion: nil)
    }
    
    
    /*
     presentUIimagePicker will present the UIImagePicker with give type
     type: Camera or Gallery
     controller: UINavigationcontroller, navigationcontroller on with uiimagepicker will present.
     */
    private func presentUIimagePicker(witMedidaType mediaType:String, type: UIImagePickerControllerSourceType){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = type
        imagePicker.delegate = self
        imagePicker.mediaTypes = [mediaType]
        
        navController.present(imagePicker, animated: true, completion: nil)
    }
    
}
//MARK: ------------------------Class End------------------------------------





//MARK: ------------------------Class Extension------------------------------------

/*Extension for UIImagePickerControllerDelegate & UINavigationControllerDelegate
 
 
 */
extension PhotoManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL{
            print(videoURL)
            thumbnailForVideoAtURL(videoURL: videoURL as URL)
            return
        }
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            callback(pickedImage,nil)
        }
        
        navController.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        navController.dismiss(animated: true, completion: nil)
    }
    
    
    func thumbnailForVideoAtURL(videoURL: URL){
        let asset: AVAsset = AVAsset(url: videoURL )
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 5) , actualTime: nil)
            let finalImage = UIImage(cgImage: thumbnailImage)
            callback(finalImage,videoURL as URL)
        } catch let error {
            print(error)
            callback(nil,videoURL as URL)
            
        }
        navController.dismiss(animated: true, completion: nil)
    }
}






