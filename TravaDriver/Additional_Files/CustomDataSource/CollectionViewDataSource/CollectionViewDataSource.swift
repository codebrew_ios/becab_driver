//
//  CollectionViewDataSource.swift
//  SafeCity
//
//  Created by Aseem 13 on 29/10/15.
//  Copyright © 2015 Taran. All rights reserved.
//


import UIKit

typealias ScrollViewScrolled = (UIScrollView) -> ()
typealias WillDisplay = (_ indexPath: IndexPath) -> ()
typealias ScrollViewDecelerated = (UIScrollView) -> ()

class CollectionViewDataSource: NSObject  {
    
    var items: Array<Any>?
    var cellHeight: CGFloat = 0.0
    var cellWidth : CGFloat = 0.0
    var cellIdentifier  : String?
    var headerIdentifier: String?
    var collectionView  : UICollectionView?
    var scrollViewListener  : ScrollViewScrolled?
    var configureCellBlock  : ListCellConfigureBlock?
    var aRowSelectedListener: DidSelectedRow?
    var willDisplay         : WillDisplay?
    var scrollViewDecelertedListener  : ScrollViewDecelerated?
    
    
    init (items: Array<Any>?  , collectionView: UICollectionView? , cellIdentifier: String? , headerIdentifier: String? , cellHeight: CGFloat , cellWidth: CGFloat , configureCellBlock: ListCellConfigureBlock? , aRowSelectedListener:   DidSelectedRow? , willDisplayCell: WillDisplay? , scrollViewDelegate: ScrollViewScrolled?, scrollViewDecelerate:ScrollViewDecelerated?)  {
        
        self.collectionView = collectionView
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.cellWidth  = cellWidth
        self.cellHeight = cellHeight
        
        self.configureCellBlock     = configureCellBlock
        self.aRowSelectedListener   = aRowSelectedListener
        self.willDisplay            = willDisplayCell
        self.scrollViewListener     = scrollViewDelegate
        self.scrollViewDecelertedListener = scrollViewDecelerate
        
    }
    
    override init() {
        super.init()
    }
    
    //MARK:- ======== Functions ========
    func reload(items: [Any]?) {
        
        self.items = items ?? []
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.reloadData()
    }
}

extension CollectionViewDataSource: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let identifier = cellIdentifier else {
            fatalError("Cell identifier not provided")
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as UICollectionViewCell
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row] {
            block(cell , item , indexPath)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let block = self.aRowSelectedListener, let item: Any = self.items?[(indexPath).row]{
            block(indexPath ,item)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let block = willDisplay {
            block(indexPath)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(scrollView)
        
        if let block = self.scrollViewDecelertedListener {
            block(scrollView)
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let block = self.scrollViewListener {
            block(scrollView)
        }
    }
    
    
}

extension CollectionViewDataSource: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

//MARK:- ======== Extension UICollectionView ========
extension UICollectionView {
    
    func registerCells(_ nibNames:[String]) {
        nibNames.forEach({
            [weak self] in
            let nib = UINib(nibName: $0, bundle: nil)
            self?.register(nib, forCellWithReuseIdentifier: $0)
        })
    }
}
