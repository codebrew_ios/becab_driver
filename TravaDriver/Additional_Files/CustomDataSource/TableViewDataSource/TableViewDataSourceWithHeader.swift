//
//  TableViewDataSourceWithHeader.swift
//  Auttle
//
//  Created by CodeBrew on 8/30/17.
//  Copyright © 2017 CodeBrew. All rights reserved.
//

import UIKit

class TableViewDataSourceWithHeader: TableViewDataSource  {
    
    var viewforHeaderInSection: ViewForHeaderInSection?
    var viewForFooterInSection: ViewForHeaderInSection?
    var headerHeight: CGFloat?
    
    
    
    required init (items: Array<Any>? , tableView: UITableView? , cellIdentifier: String?,cellHeight:CGFloat?,headerHeight:CGFloat?,configureCellBlock: ListCellConfigureBlock?,viewForHeader:ViewForHeaderInSection?,viewForFooter:ViewForHeaderInSection?, aRowSelectedListener: DidSelectedRow?,willDisplayCell: WillDisplayCell? , scrollView : ScrollViewScroll?) {
        
        self.viewforHeaderInSection = viewForHeader
        self.headerHeight = headerHeight
        self.viewForFooterInSection = viewForFooter
        
        
        super.init(items: items, tableView: tableView, cellIdentifier: cellIdentifier, cellHeight: cellHeight, configureCellBlock: configureCellBlock, aRowSelectedListener: aRowSelectedListener , scrollViewScroll : scrollView, willDisplayCell : willDisplayCell)
    }
    
    
    override init() {
        super.init()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier else {
            fatalError("Cell identifier not provided")
        }
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if let block = self.configureCellBlock , let itemsArray = self.items?[indexPath.section]  as? AnyObject, let item:Any = itemsArray[indexPath.row] {
            block(cell , item , indexPath)
        }
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return /items?.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (items?[section] as AnyObject).count
        // return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let block = viewForFooterInSection else { return nil }
        return block(section)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension //cellHeight ?? 150
    }
    
    
    //MARK: - SCrollView Delegate Method's
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollViewScroll?(scrollView)
    }
    
    
    
    
}
