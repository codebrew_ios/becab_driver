//
//  Alerts.swift
//  Grintafy
//
//  Created by Sierra 4 on 14/07/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ISMessages

typealias AlertBlock = (_ success: AlertTag) -> ()

struct alertGlobal{
static var alert = UIAlertController()
}


enum Alert : String {
    
    case success = "Success"
    case oops = "Oops"
    case login = "Login Successfull"
    case alert = "Alert"
    case ok = "Ok"
    case cancel = "Cancel"
    case error = "Error"
    case logout = "Are you sure you want to Logout?"
    case confirm = "Confirm"
    
    func getLocalised() -> String {
        
        switch self {
        case .success:  return "Success"
            
        case .oops: return "Oops"
            
        case .login: return "Login Successfull"
            
        case .alert: return "Alert"
            
        case .ok: return "Ok"
            
        case .cancel: return "Cancel"
            
        case .error: return "Error"
            
        case .logout: return "Are you sure you want to Logout?"
            
        case .confirm: return "Confirm"
       
        }
    }
}

enum AlertTag {
    
    case done
    case yes
    case no
}

class Alerts: NSObject {
    
    static let shared = Alerts()
    
    //MARK: - Show ISMessages Alert
    func show(alert title : Alert , message : String , type : ISAlertType){
        
        ISMessages.showCardAlert(withTitle: title.rawValue, message: message, duration: 3.0, hideOnSwipe: true, hideOnTap: true, alertType: type, alertPosition: .bottom, didHide: nil)
    }
    
    func show(alert title : String , message : String , type : ISAlertType){
        
        ISMessages.showCardAlert(withTitle: title , message: message, duration: 3.0, hideOnSwipe: true, hideOnTap: true, alertType: type, alertPosition: .bottom, didHide: nil)
    }
    
    func showOnTop(alert title : String , message : String , type : ISAlertType){
        
        ISMessages.showCardAlert(withTitle: title , message: message, duration: 3.0, hideOnSwipe: true, hideOnTap: true, alertType: type, alertPosition: .top, didHide: nil)
    }
    
    
    func showAlertView(alert Title: String , message: String , buttonTitles : [String] , viewController : UIViewController){
        
        let alert = UIAlertController(title: Title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitles[0]  , style: UIAlertActionStyle.default , handler: { action in
            
            switch action.style{
                
            case .default:
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, options: [:], completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                }
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            }}))
        
        if buttonTitles.count != 1 {
            alert.addAction(UIAlertAction(title: buttonTitles[1]  , style: UIAlertActionStyle.cancel , handler: nil))
        }
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithActionBlock (title : String , message : String , actionTitle : String , viewController : UIViewController , action1: @escaping () -> ()) {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title:actionTitle  , style: UIAlertActionStyle.default , handler: { action in
            
            action1()
        }))
        
        alert.addAction(UIAlertAction(title: R.string.localizable.alertCancel()  , style: UIAlertActionStyle.cancel , handler: nil))
       // alert.view.tintColor = Colors.themeColor
        
        viewController.present(alert, animated: true, completion: nil)
        
        alertGlobal.alert = alert
        
    }
    
    func showAlertWithActionBlockForce (title : String , message : String , actionTitle : String , viewController : UIViewController , action1: @escaping () -> ()) {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title:actionTitle  , style: UIAlertActionStyle.default , handler: { action in
            
            action1()
        }))
        
       // alert.view.tintColor = Colors.themeColor
        viewController.present(alert, animated: true, completion: nil)
    }
    
}
