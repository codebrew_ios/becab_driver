//
//  AlertMessages.swift
//  Idea
//
//  Created by Dhan Guru Nanak on 2/16/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import Foundation

enum AlertMessage:String{
    
    case emptyPassword = "Please enter password"
    case emptyPhoneNumber = "Please enter contact number"
    case passwordNotMatch = "Password and retype password does not match"
    case notValidPassword = "Password must be at least 8 characters long"
    case notValidPhoneNumber = "Please enter valid contact number"
    case emptyQualification = "Please enter your qualifications"
    case notValidLink = "Please enter valid web link"
    case emptyAddress = "Please select your address"
    case emptyName = "Please enter name"
    case resetPasswordLink = "We've sent a password reset link to this email address."
    case emailVerification = "Please verify your account and then login to continue"
    case selectBreadToContinue = "Please Select bread to contintue."
    
}
