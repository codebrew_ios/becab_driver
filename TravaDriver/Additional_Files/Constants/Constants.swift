//
//  Constants.swift
//  Grintafy
//
//  Created by Sierra 4 on 20/07/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import UIKit

enum MediaType  {
    
    case camera
    case photoLibrary
    case videoGallery
    case takeAPhoto
    
    func get() -> String{
        
        switch self {
            
        case .camera:
            return "Camera"
            
        case .photoLibrary:
            return "PhotoLibrary"
            
        case .videoGallery:
            return "VideoGallery"
            
        case .takeAPhoto:
            return "TakeAPhoto"
        }
    }
}


struct Colors {
    static let themeColor = UIColor.white //UIColor(red:0.22, green:0.29, blue:0.34, alpha:1)
    static let swipeTabViewTextColor = UIColor(red:0.22, green:0.29, blue:0.34, alpha:0.7)
    static let chartBackGround = UIColor(red:0.27, green:0.59, blue:0.84, alpha:1)
    static let appColor = UIColor.init(hexString: "#697BF5")
}

enum driverType: Int{
    
    case gasService = 1
    case drinkingWater = 2
    case waterTanker = 3
    case freight = 4
}

enum OrderEventTypes : String {
    case serReached = "SerOReached"

    case resuestIncoming = "SerRequest"
    case requestCancelled = "SerCancel"
    case requestAcceptedOther = "SerOAccept"
    case requestRated = "CustRatedService"
    case About2Start = "About2Start"
    case DPending = "DPending"
    case SerHalfWayStop = "SerHalfWayStop"
    case SerHalfWayStopCancelled = "SerHalfWayStopCancelled"
    case EtokenTimeout = "CTimeout"
    case EtokenCustomerConfirm = "SerCustConfirm"
    case EtokenCustomerCancel  = "SerCustCancel"
    case EtokenCustomerComplete  = "SerComplete"
    case SerBreakdown = "SerBreakdown"
    case SerBreakdownCancelled = "SerBreakDownCancelled"
}


struct rideDistance {
    static var distance = 0
    static var distanceTime = 0
    static var dictionaryAllRides = [String:Int]()
}


struct DataResponse {
    
    //TODO: Current OnGoing Orders Model Data
    static var onGoingOrders:OnGoingOrders?
    
    //TODO: Current Driver Detail Model Data
    static var driverData:DriverModel?
    
    //TODO: Current Service Model Data
    static var onGoingRequest:ServiceRequest?
    
    //TODO: Recently Completed request
    static var completedRequest:CompletedOrderModel?
    
    //TODO: order id of ride whose status is changed recently, used to show polyline of
    static var recentUpdatedOrderId = ""
    
}

struct NetworkConnection{
    
    static var isConnected = false
}


//Internal App Notification Used
enum NotificationNames : String, CaseIterable  {
    
    case updateDistanceTime = "updateRideDistancetime"
    case fcmTokenUpdated = "fcmTokenUpdated"
    case openRequestModal = "openRequestModal"
    case confirmScheduled = "confirmScheduled"
    case requestCancelled = "requestCancelled"
    case newServiceRequest = "newServiceRequest"
    case didBecomeActive = "didBecomeActive"
    case particularOrderCancel = "particularOrderCancel"
    case internetConnected = "internetConnected"
    case internetDisconnected = "internetDisconnected"
    case readPendingNotifications = "readPendingNotifications"
    case refreshOrders = "refreshOrders"
    case cancelled = "Cancelled"
    case halfWay = "HalfWayStop"
    case OtherAccepted = "OtherAccepted"
    case nCancelFromBookinScreen = "nCancelFromBookinScreen"
    case showHeatMapData = "showHeatMapData"
    case removeHeatMapNoti = "removeHeatMapNoti"
}

//Internal App Driver Category

enum DateTimePickerMode  {
    
    case date
    case time
    
    func get() -> String{
        
        switch self {
            
        case .date:
            return "date"
            
        case .time:
            return "time"
        }
    }
}

enum ImageType {
    
    case profilePic
    case coverPic
    
    func get() -> String{
        
        switch self {
            
        case .profilePic:
            return "ProfilePic"
            
        case .coverPic:
            return "CoverPic"
            
        }}}


// USER Location
enum Locations : Double {
    
    case lat
    case longitutde
    
    func getLoc () -> Double {
        
        switch self {
        case .lat:
            return LocationManager.shared.latitude
        case .longitutde :
            return LocationManager.shared.longitude
        }
    }
}


enum HelperNames {
    
    case deviceType
    case deviceToken
    case deviceId
    case timeZone
    case phoneCode
    case countryCode
    case countryName
    case accessToken
        
    func get() -> String{
        
        switch self {
        case .deviceType:
            return "IOS"
            
        case .deviceToken:
            return UserDefaults.standard.string(forKey: "deviceToken") ?? ""
            
        case .deviceId:
            return /UIDevice.current.identifierForVendor?.uuidString
            
        case .timeZone:
            return TimeZone.current.identifier
            
        case .phoneCode:
            return "+971"
            
        case .countryCode:
            return "ARE"
            
        case .countryName:
            return "United Arab Emirates"
            
        case .accessToken:
            return /(UserSingleton.shared.loggedInUser?.data?.accessToken)
        }
    }
}


//Language Enums

enum LanguageCode {
    
    case English
    
    case Arabic
    
    case Urdu
    
    case Chinese
    
    case Spanish
    
    func get() -> String{
        switch self {
            
        case .English:
            return "1"
            
        case .Urdu:
            return "3"
            
        case .Arabic:
            return "5"
            
        case .Chinese:
            return "4"
        
        case .Spanish:
            return "6"
            
        }
    }
}

// Current Screen Size
struct Screen {
    
    static let HEIGHT = UIScreen.main.bounds.size.height
    static let WIDTH = UIScreen.main.bounds.size.width
    static let HEIGHTRATIO = WIDTH * 0.5625
}


enum languageType {
    
    case en
    case ur
    case ar
    
    func get() -> String{
        switch self {
            
        case .en:
            
            return "English"
            
        case .ar:
            
            return "Arabic"
            
        case .ur:
            
            return "Urdu"
        }
    }
}

//Device Check
extension UIDevice {
    
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    enum ScreenType: String {
        
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    
    var screenType: ScreenType {
        
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}



