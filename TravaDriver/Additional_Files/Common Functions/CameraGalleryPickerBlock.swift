////
////  ImagePicker.swift
////  Grintafy
////
////  Created by Sierra 4 on 16/08/17.
////  Copyright © 2017 com.example. All rights reserved.
////
//
//import UIKit
////import EZSwiftExtensions
//import MobileCoreServices
//import AVKit
//import AVFoundation
//
//
//typealias onPicked = (UIImage , Data , String) -> ()
//
//class CameraGalleryPickerBlock: NSObject , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
//    
//    typealias videoThumbnailBlock = (_ thumbnail: UIImage?) -> ()
//    typealias onCanceled = () -> ()
//    
//    
//    var pickedListner : onPicked?
//    var canceledListner : onCanceled?
//    var isRectangularCropingEnable = false
//
//    
//    static let sharedInstance = CameraGalleryPickerBlock()
//    
//    override init() {
//        super.init()
//    }
//    
//    deinit{
//    }
//    
//    func pickerImageVideo(pickedListner : @escaping onPicked , canceledListner : @escaping onCanceled){
//        
//        UtilityFunctions.showActionSheetWithStringButtons(buttons: [MediaType.camera.get(), MediaType.photoLibrary.get(), MediaType.videoGallery.get()], success: {[unowned self] (str) in
//            
//            if str ==  MediaType.camera.get(){
//                
//                if !UtilityFunctions.isCameraPermission()  {
//
//                    UtilityFunctions.show(alert: "alertTitle.enableCameraAccess".localized, message: "alert.TurnOnCameraPermission".localized, buttonText: "buttonTitle.settings".localized, buttonOk: {
//
//                        self.openSettings()
//                    })
//
//                    UtilityFunctions.show(alert: "MicroPhoneAccess".localized, message: "AllowMicrophoneAccess".localized, buttonText: "buttonTitle.settings".localized, buttonOk: {
//
//                        self.openSettings()
//                    })
//
//                    return
//                }
//                
//            } else {
//                
//                if !UtilityFunctions.accessToPhotos(){
//                    
//                    UtilityFunctions.show(alert: "alertTitle.enablePhotosAccess".localized, message: "alert.photoPermission".localized, buttonText: "buttonTitle.settings".localized, buttonOk: {
//                        
//                        self.openSettings()
//                    })
//                    
//                    return
//                }
//            }
//            
//            self.pickedListner   = pickedListner
//            self.canceledListner = canceledListner
//            
//            self.showCameraOrGallery(type: str)
//        })
//    }
//    
//    func pickerImage(pickedListner : @escaping onPicked , canceledListner : @escaping onCanceled) {
//        
//        UtilityFunctions.showActionSheetWithStringButtons(buttons: [ MediaType.takeAPhoto.get() ,  MediaType.photoLibrary.get()], success: {[unowned self] (str) in
//            
//            if str ==  MediaType.takeAPhoto.get().localized {
//                
//                
//                CheckPermission.shared.permission(For: PermissionType.camera, completion: { (isGranted) in
//                    
//                    if isGranted {
//                        
//                            self.pickedListner   = pickedListner
//                            self.canceledListner = canceledListner
//                        
//                            self.showCameraOrGallery(type: str)
//                        
//                    } else {
//                        
//                        UtilityFunctions.show(alert: "alertTitle.enableCameraAccess".localized, message: "alert.TurnOnCameraPermission".localized, buttonText: "buttonTitle.settings".localized, buttonOk: {
//                            
//                            self.openSettings()
//                        })
//                        
//                        return
//                    }
//                })
//                
//            } else {
//                
//                CheckPermission.shared.permission(For: PermissionType.photos, completion: { (isGranted) in
//                    
//                    if isGranted {
//                        
//                        self.pickedListner   = pickedListner
//                        self.canceledListner = canceledListner
//                        
//                        self.showCameraOrGallery(type: str)
//                        
//                    } else {
//                        
//                        UtilityFunctions.show(alert: "alertTitle.enablePhotosAccess".localized, message: "alert.photoPermission".localized, buttonText: "buttonTitle.settings".localized, buttonOk: {
//                            
//                            self.openSettings()
//                        })
//                    }
//                })
//            }
//
//        })
//    }
//    
//    
//    
//    func showCameraOrGallery(type : String){
//        
//        let picker : UIImagePickerController = UIImagePickerController()
//        
//        switch /type {
//            
//        case MediaType.camera.get():
//            
//            picker.sourceType = UIImagePickerControllerSourceType.camera
//            picker.mediaTypes = [kUTTypeMovie as String , kUTTypeImage as String]
//            picker.cameraCaptureMode = .video
//            picker.videoQuality = UIImagePickerControllerQualityType(rawValue: 1)!
//            picker.videoMaximumDuration = 60
//            picker.allowsEditing = false
//
//            
//        case MediaType.takeAPhoto.get():
//            
//            picker.sourceType = UIImagePickerControllerSourceType.camera
//            picker.mediaTypes = [kUTTypeImage as String]
//            picker.cameraCaptureMode = .photo
//            picker.allowsEditing = false
//
//            
//        case MediaType.photoLibrary.get():
//            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            picker.allowsEditing = false
//
//            
//        case MediaType.videoGallery.get():
//            
//            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            picker.mediaTypes = [kUTTypeMovie as String , kUTTypeMPEG4 as String , kUTTypeVideo as String , kUTTypeAVIMovie as String]
//            picker.videoQuality = UIImagePickerControllerQualityType(rawValue: 4)!
//            picker.videoMaximumDuration = 60
//            picker.allowsEditing = true
//
//            
//        default:
//            break
//        }
//        
//        picker.delegate = self
//        ez.topMostVC?.present(picker, animated: true, completion: nil)
//    }
//    
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        
//        picker.dismiss(animated: true, completion: nil)
//        if let listener = canceledListner {
//            listener()
//        }
//    }
//    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        
////        let data = Data()
//        
//        if let image : UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
//            self.cropImage(image: image)
//        }
//        
//        if let  videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
//            
//            var thumbnail = UIImage()
//            getVideoThumbanil(video: videoUrl.absoluteURL, responseBlock: { (img) in
//                
//                guard let thumb = img else {return}
//                thumbnail = thumb
//                
//                do{
//                    let videoData = try Data(contentsOf: videoUrl)
//                    if let listener = self.pickedListner {
//                        
//                        listener(thumbnail , videoData , "video")
//                    }
//                }
//                catch let error {
//                    print("*** Error generating thumbnail: \(error.localizedDescription)")
//                }
//            })
//        }
//        
//        ez.runThisInMainThread {
//            picker.dismiss(animated: true, completion: nil)
//        }
//        
//        
//    }
//    
//    func openSettings(){
//        
//        let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)! as URL
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
//        } else {
//            
//            // Fallback on earlier versions
//            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
//            
//            if let url = settingsUrl {
//                UIApplication.shared.openURL(url)
//            }
//        }
//    }
//    
//    //Mark: - Generate thumbnail of video
//    
//    func getVideoThumbanil(video : URL , responseBlock : @escaping videoThumbnailBlock) {
//        
//        var thumbnail : UIImage?
//        DispatchQueue.main.async(execute: { () in
//            
//            let asset = AVAsset.init(url: video)
//            let assetImageGenerator = AVAssetImageGenerator(asset: asset)
//            assetImageGenerator.appliesPreferredTrackTransform = true
//            
//            var time = asset.duration
//            time.value = min(time.value, 2)
//            
//            do {
//                let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
//                thumbnail =  UIImage(cgImage: imageRef)
//                
//            } catch {
//                print("error")
//            }
//            responseBlock(thumbnail)
//        })
//    }
//    
//    func cropImage(image : UIImage){
//        
//        let vc = TOCropViewController(image:image )
//        vc.delegate = self
//        
//        if(isRectangularCropingEnable == true) {
//            
//            vc.cropView.aspectRatio = CGSize.init(width: Screen.WIDTH, height: Screen.HEIGHT/3.2)
//            vc.cropView.cropBoxResizeEnabled = false
//            vc.aspectRatioPickerButtonHidden = true
//            vc.resetAspectRatioEnabled = false
//            vc.rotateButtonsHidden = true
//            
//        }else {
//            vc.cropView.aspectRatio = CGSize.init(width: Screen.WIDTH, height: Screen.HEIGHT - 60.0)
//    
//            vc.cropView.cropBoxResizeEnabled = true
//            vc.aspectRatioPickerButtonHidden = false
//            vc.resetAspectRatioEnabled = true
//            vc.rotateButtonsHidden = false
//        }
//        
//        ez.runThisAfterDelay(seconds: 0.80) {
//            ez.topMostVC?.presentVC(vc)
//        }
//    }
//}
//
//
//extension CameraGalleryPickerBlock : TOCropViewControllerDelegate {
//    
//    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
//        
//        let data = Data()
//        if let listener = pickedListner{
//            
//            listener(image, data , "image")
//            cropViewController.dismissVC(completion: nil)
//        }
//    }
//}
//
//
//
//
