////
////  UtilityFunctions.swift
////  Grintafy
////
////  Created by Sierra 4 on 16/08/17.
////  Copyright © 2017 com.example. All rights reserved.
////
//
//import UIKit
//import Foundation
////import EZSwiftExtensions
//import AVFoundation
//import Photos
//import PhotosUI
//
//class UtilityFunctions {
//
//
//    class func appendOptionalStrings(withArray array : [String?]) -> String {
//        return array.flatMap{$0}.joined(separator: " ")
//    }
//
//
//
//    //MARK: - Different Alerts
//    class func show(alert title:String , message:String ,buttonText: String , buttonOk: @escaping () -> ()  ){
//
//        let alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
//
//        alertController.addAction(UIAlertAction(title: "alertTitle.cancel".localized , style: UIAlertActionStyle.default, handler: nil))
//
//        alertController.addAction(UIAlertAction(title: buttonText , style: UIAlertActionStyle.default, handler: {  (action) in
//            buttonOk()
//        }))
//
//        ez.topMostVC?.present(alertController, animated: true, completion: nil)
//    }
//
//    class func showWithCancelAction(alert title:String , message:String ,buttonText: String , buttonOk: @escaping (Bool) -> ()  ){
//
//        let alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
//
//        alertController.addAction(UIAlertAction(title: "alertTitle.cancel".localized , style: UIAlertActionStyle.default, handler: {  (action) in
//            buttonOk(false)
//        }))
//
//        alertController.addAction(UIAlertAction(title: buttonText , style: UIAlertActionStyle.default, handler: {  (action) in
//            buttonOk(true)
//        }))
//
//        ez.topMostVC?.present(alertController, animated: true, completion: nil)
//    }
//
//
//    class func showWithOk(alert title:String , message:String, buttonTitle : String , buttonOk: @escaping () -> ()){
//
//        let alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
//        alertController.addAction(UIAlertAction(title: buttonTitle , style: UIAlertActionStyle.default, handler: {  (action) in
//            buttonOk()
//        }))
//
//        ez.topMostVC?.present(alertController, animated: true, completion: nil)
//    }
//
//
//    class func showActionSheetWithStringButtons(  buttons : [String] , success : @escaping (String) -> ()) {
//
//        let controller = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//
//        for button in buttons{
//            let action = UIAlertAction(title: button.localized , style: .default, handler: { (action) -> Void in
//                success(button)
//            })
//            controller.addAction(action)
//        }
//
//        let cancel = UIAlertAction(title: "buttonTitle.cancel".localized , style: UIAlertActionStyle.cancel) { (button) -> Void in}
//        controller.addAction(cancel)
//        ez.topMostVC?.present(controller, animated: true) { () -> Void in
//
//        }
//    }
//
//
//    //MARK: - Check for camera permission
//    class func isCameraPermission() -> Bool {
//
//        let cameraMediaType = AVMediaType.video
//        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
//
//        switch cameraAuthorizationStatus {
//
//        case .authorized: return true
//
//        case .restricted: return false
//
//        case .denied: return false
//
//        case .notDetermined:
//            AVCaptureDevice.requestAccess(for: cameraMediaType, completionHandler: { (granted) in
//
//            })
//
//            return true
//        }
//    }
//
//
//    //MARK: - convert arabic number to English
//    class func toEnglishNumber(number: String) -> String {
//
//        var result:NSNumber = 0
//
//        let numberFormatter = NumberFormatter()
//        numberFormatter.locale = Locale(identifier: "EN")
//
//        if let finalText = numberFormatter.number(from: number) {
//
//            //            print("Intial text is: ", number)
//            //            print("Final text is: ", finalText)
//            result =  finalText
//        }
//        return result.stringValue
//    }
//
//
//    //MARK : - Check For Micro Phone Permission
//    class func isMicroPhonePermission () -> Bool {
//
//        let AVAudioSessionPermission = AVAudioSession.sharedInstance().recordPermission()
//
//        switch AVAudioSessionPermission {
//
//        case AVAudioSessionRecordPermission.granted: return true
//
//        case AVAudioSessionRecordPermission.denied: return false
//
//        case AVAudioSessionRecordPermission.undetermined : return false
//
//        }
//    }
//
//    class func accessToPhotos() -> Bool {
//
//        let status = PHPhotoLibrary.authorizationStatus()
//
//        switch status {
//
//        case .authorized: return true
//        case .restricted: return false
//
//        case .denied: return false
//
//        case .notDetermined:
//            PHPhotoLibrary.requestAuthorization({ (newStatus) in
//
//                if (newStatus == PHAuthorizationStatus.authorized) {
//
//                } else {
//
//                }
//            })
//            return true
//        }
//    }
//
//
//    //MARK: - Get Attributed String
//    class func GetAttributedString(arrStrings : [String],arrColor : [UIColor], arrFont: [String] , arrSize: [CGFloat], arrNextLineCheck: [Bool]) -> NSMutableAttributedString {
//
//        var attriString : NSMutableAttributedString?
//        let combination = NSMutableAttributedString()
//
//        for index in 0..<arrStrings.count {
//
//            let yourAttributes = [NSAttributedStringKey.foregroundColor: arrColor[index], NSAttributedStringKey.font:UIFont.init(name: arrFont[index], size: arrSize[index])]
//
//            if arrNextLineCheck[index] == true {
//
//                attriString = NSMutableAttributedString(string:"\n\(arrStrings[index])" , attributes: yourAttributes)
//
//            }else{
//
//                attriString = NSMutableAttributedString(string:" \(arrStrings[index])"  , attributes: yourAttributes) }
//            combination.append(attriString!)
//        }
//
//        return combination
//    }
//
//
//    //MARK: - Conversion of UIView to its Image // i.e screenshot
//    class func imageWithView(view: UIView) -> UIImage {
//
//        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
//        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
//        let img = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return img!
//    }
//
//
//
//    //MARK: - convert array Model To json object
//    class func convertArrayIntoJson(array: [String]?) -> String? {
//
//        do {
//
//            let jsonData = try JSONSerialization.data(withJSONObject: array ?? [], options: JSONSerialization.WritingOptions.prettyPrinted)
//
//            var string = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
//            string = string.replacingOccurrences(of: "\n", with: "") as String
//            string = string.replacingOccurrences(of: "\\\"", with: "\"") as String
//            string = string.replacingOccurrences(of: "\\", with: "") as String // removes \
//            // string = string.replacingOccurrences(of: " ", with: "") as String
//            string = string.replacingOccurrences(of: "/", with: "") as String
//
//            return string as String
//        }
//
//        catch let error as NSError {
//
//            print(error.description)
//            return ""
//        }
//    }
//
//}
//
//
//
//
