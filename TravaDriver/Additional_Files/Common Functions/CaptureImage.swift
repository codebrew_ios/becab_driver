

//
//  CaptureImage.swift
//  Untap
//
//  Created by Sierra 3 on 19/04/17.
//  Copyright © 2017 CB Neophyte. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import Photos

class CameraImage: NSObject {
    
    static var shared = CameraImage()
    var fromVC: UIViewController?
    var navigationController:UINavigationController?
    var complete:((_ image: UIImage?,_ fileName:String?) -> Void)?
    var completeVideoCallback:((_ videoUrl: URL?,_ videoData:Data?,_ thumbnail:UIImage?) -> Void)?
    
    
    internal func openSettings()
    {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl)
        {
            
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        
    }
    
    func captureImage(from vc: UIViewController,navigationController:UINavigationController?,mediaType:[String]?, captureOptions sources: [UIImagePickerControllerSourceType], allowEditting crop: Bool, callBack: ((_ image: UIImage?,_ fileName:String?) -> Void)?) {
        self.fromVC = vc
        self.complete = callBack
        self.completeVideoCallback = nil
        self.navigationController = navigationController
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = crop
        imagePicker.modalPresentationStyle = .popover
        imagePicker.popoverPresentationController?.sourceView = vc.view
        if mediaType != nil{
            imagePicker.mediaTypes = mediaType ?? []
        }
        
        if sources.count > 1 {
            openActionSheet(with: imagePicker, sources: sources)
        }
        else {
            let source = sources[0]
            if source == .camera && cameraExists {
                imagePicker.allowsEditing = false
                imagePicker.sourceType = source
            }
            DispatchQueue.main.async {
                vc.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func captureVideo(from vc: UIViewController,mediaType:[String]?, captureOptions sources: [UIImagePickerControllerSourceType], allowEditting crop: Bool, completeVideoCallback:((_ videoUrl: URL?,_ videoData:Data?,_ thumbnail:UIImage?) -> Void)?) {
        self.fromVC = vc
        self.completeVideoCallback = completeVideoCallback
        self.complete = nil
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = crop
        if mediaType != nil{
            imagePicker.mediaTypes = mediaType ?? []
        }
        
        if sources.count > 1 {
            openActionSheet(with: imagePicker, sources: sources)
        }
        else {
            let source = sources[0]
            if source == .camera && cameraExists {
                imagePicker.sourceType = source
            }
            DispatchQueue.main.async {
                vc.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func openActionSheet(with imagePicker: UIImagePickerController, sources: [UIImagePickerControllerSourceType]) {
        
        let actionSheet = UIAlertController(title: "alert.selectOption".localized, message: nil, preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor.darkGray
        
        actionSheet.modalPresentationStyle = .popover
        actionSheet.popoverPresentationController?.sourceView = self.fromVC?.view
        
        for source in sources {
            let action = UIAlertAction(title: source.name, style: .default, handler: { (action) in
                
                switch source{
                case .camera:
                    DispatchQueue.main.async {
                        CheckPermission.shared.permission(For: .camera, completion: { (success) in
                            if success{
                                DispatchQueue.main.async {
                                    imagePicker.allowsEditing = false
                                    imagePicker.sourceType = source
                                    //   self.navigationController?.present(imagePicker, animated: true, completion: nil)
                                    self.fromVC?.present(imagePicker, animated: true, completion: nil)
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "alert.cameraPermissionDenied".localized, actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.fromVC) {
                                        self.openSettings()
                                    }
                                    //                            Alerts.shared.show(alert: "alert.alert".localized, message: "alert.photoPermissionDenied".localized, type: .info)
                                }
                            }
                        })
                    }
                case .photoLibrary:
                    DispatchQueue.main.async {
                        CheckPermission.shared.permission(For: .photos, completion: { (success) in
                            if success{
                                DispatchQueue.main.async {
                                    imagePicker.sourceType = source
                                    //   self.navigationController?.present(imagePicker, animated: true, completion: nil)
                                    self.fromVC?.present(imagePicker, animated: true, completion: nil)
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    Alerts.shared.showAlertWithActionBlockForce(title: R.string.localizable.alertAlert(), message: "alert.photoPermissionDenied".localized, actionTitle: R.string.localizable.alertTitleOk(), viewController: /self.fromVC) {
                                        self.openSettings()
                                    }
                                    //                            Alerts.shared.show(alert: "alert.alert".localized, message: "alert.photoPermissionDenied".localized, type: .info)
                                }
                            }
                        })
                    }
                default:
                    break
                }
                
            })
            if source == .camera {
                if cameraExists{  actionSheet.addAction(action) }
            }
            else {
                actionSheet.addAction(action)
            }
        }
        let cancel = UIAlertAction(title: "alert.cancel".localized, style: .cancel) { (action) in
        }
        if #available(iOS 10.0, *) {
            cancel.setValue(UIColor(displayP3Red: 0, green: 122/255, blue: 255/255, alpha: 1), forKey: "titleTextColor")
        } else {
            // Fallback on earlier versions
        }
        actionSheet.addAction(cancel)
        DispatchQueue.main.async {
            self.fromVC?.present(actionSheet, animated: true, completion: nil)
        }
    }
    
}

extension CameraImage: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var cameraExists: Bool {
        let front = UIImagePickerController.isCameraDeviceAvailable(.front)
        let rear = UIImagePickerController.isCameraDeviceAvailable(.rear)
        return front || rear
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        guard let callBack = complete else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        callBack(nil,"")
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        var nameOfFile = "Camera_IMG.jpg"
        if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
            let fileName = asset?.value(forKey: "filename")
            nameOfFile = fileName as? String ?? "Camera_IMG.jpg"
        }
        
        var image: UIImage? = nil
        
        if let edittedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = edittedImage
        }
        else if let fullImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            image = fullImage
        }
        
        
        if complete == nil && completeVideoCallback == nil{
            picker.dismiss(animated: true, completion: nil)
            return
        }else if complete == nil{
            if let callBackVideo = completeVideoCallback {
                let url: URL = info[UIImagePickerControllerMediaURL] as! URL
                let chosenVideo = info[UIImagePickerControllerMediaURL] as! URL
                let videoData = try! Data(contentsOf: chosenVideo, options: [])
                //        let thumbnail = url.generateThumbnail()
                //        callBackVideo(url, videoData, thumbnail)
            }
        }else if completeVideoCallback == nil{
            if let callBack = complete {
                callBack(image,nameOfFile)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}

extension UIImagePickerControllerSourceType {
    
    var name: String {
        
        switch self {
        case .camera:
            return "alert.fromCamera".localized
            
        case .photoLibrary:
            return "alert.fromPhotos".localized
            
        case .savedPhotosAlbum:
            return "Album"
            
        }
    }
}





