import Foundation
import SwiftMessages

class Toast {
  class func show(text: String?,type: Theme){
    let view = MessageView.viewFromNib(layout: .messageView)
    
    // Theme message elements with the warning style.
    view.configureTheme(type)
//    let isUserLoggedIn = UserSingleton.shared.loggedInUser?.data?.accessToken != nil
//    view.configureTheme(backgroundColor: UIColor.colorDefaultApp(), foregroundColor: UIColor.white)
    view.preferredHeight = 80
//    view.titleLabel?.font = UIFont(name: R.font.colfaxMedium.fontName, size: 17)!
//    view.bodyLabel?.font = UIFont(name: R.font.colfaxMedium.fontName, size: 14)!
    
    view.button?.isHidden = true
    view.configureContent(title:type == Theme.error ? R.string.localizable.alertAlert() : R.string.localizable.alertSuccess(), body: /text, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: R.string.localizable.alertTitleOk()) { (button) in
      SwiftMessages.hide()
    }
    SwiftMessages.defaultConfig.presentationStyle = .top
    
    SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
    
    // Disable the default auto-hiding behavior.
    SwiftMessages.defaultConfig.duration = .automatic
    
    // Dim the background like a popover view. Hide when the background is tapped.
    //SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
    SwiftMessages.defaultConfig.dimMode = .none
    
    // Disable the interactive pan-to-hide gesture.
    SwiftMessages.defaultConfig.interactiveHide = true
    
    // Specify a status bar style to if the message is displayed directly under the status bar.
    SwiftMessages.defaultConfig.preferredStatusBarStyle = .default
    // Show message with default config.
    SwiftMessages.show(view: view)
    
    // Customize config using the default as a base.
    var config = SwiftMessages.defaultConfig
    config.duration = .forever
    
    // Show the message.
    SwiftMessages.show(config: config, view: view)
  }
}
