import CoreLocation

class Utility: NSObject {
    
    //MARK: - --------------Variables-----------------
    static let shared = Utility()
    
    
    static func dteGetConvert(string : String,fromFormat : String,_ toFormat : String) -> String{
           let formatter = DateFormatter()
           formatter.locale = Locale(identifier: "en_US_POSIX")
           formatter.dateFormat = fromFormat
           let cDate = formatter.date(from: string)
           
           if cDate == nil{
               return "01 May 1800"
           }
           formatter.dateFormat = toFormat
           
           let dt = formatter.date(from: formatter.string(from: cDate!))
           formatter.timeZone = TimeZone.current
           formatter.dateFormat = toFormat
           return formatter.string(from: cDate!)
       }
    
    func getDistanceBetweenAllPoints(completion: @escaping(Double) -> Void) {
        let array = SocketIOManager.shared.fullTrackList
        var distance:Double = 0
        var index = 0
        
        let dispatchRouteCalulator = DispatchGroup()
        
        while index < array.count {
            
            let fistLatLong = array[index]
            if index == array.count - 1 {
                dispatchRouteCalulator.enter()
                distance += getDistanceBetweenLatLongs(lat: Float(fistLatLong.latitude), long: Float(fistLatLong.longitude), latitude: Float(fistLatLong.latitude), longitude: Float(fistLatLong.longitude))
                print("fistLat: \(fistLatLong.latitude) fistLong: \(fistLatLong.longitude)")
                print("distance : \(distance)")
                dispatchRouteCalulator.leave()
            }else{
                dispatchRouteCalulator.enter()
                let secondLatLong = array[1+index]
                distance += getDistanceBetweenLatLongs(lat: Float(fistLatLong.latitude), long: Float(fistLatLong.longitude), latitude: Float(secondLatLong.latitude), longitude: Float(secondLatLong.longitude))
                print("fistLat: \(fistLatLong.latitude) fistLong: \(fistLatLong.longitude)")
                print("secondLat: \(secondLatLong.latitude) secondLong: \(secondLatLong.longitude)")
                print("distance : \(distance)")
                dispatchRouteCalulator.leave()
            }
            index += 1
        }
        dispatchRouteCalulator.notify(queue: .main) {
            print("finalDistance : \(distance)")
            completion(distance)
        }
    }
    
    // MARK: Distance between two locations
    func getDistanceBetweenLatLongs(lat : Float , long : Float , latitude: Float , longitude: Float ) -> Double {
        let userCoordinates = CLLocation(latitude: CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
        return userCoordinates.distance(from: CLLocation(latitude: Double(lat) , longitude: Double(long)))/1000
    }
    
    func getDistanceWithDynamicUrl(completion: @escaping(String) -> Void) {
        let lat = Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_latitude)
        let long = Float(/DataResponse.onGoingOrders?.orders?.first?.pickup_longitude)
        var waypoints = "waypoints="
        for (index,data) in SocketIOManager.shared.fullTrackList.enumerated() {
            if index == SocketIOManager.shared.fullTrackList.count-1{
                waypoints = "\(waypoints)\(data.latitude),\(data.longitude)"
            }else{
                waypoints = "\(waypoints)\(data.latitude),\(data.longitude)|"
            }
        }
        var dist = ""
        let str : NSString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(Float(Locations.lat.getLoc())),\(Float(Locations.longitutde.getLoc()))&destination=\(lat),\(long)&sensor=false&mode=driving&alternatives=true&units=metric&key=\(googleKey)&\(waypoints)" as NSString
        guard let strUrl = str.addingPercentEscapes(using: String.Encoding.utf8.rawValue) else { return }
        if let url = URL(string: strUrl) {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "")
                    completion(dist)
                    return
                }
                if let result = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String:Any],
                    let routes = result["routes"] as? [[String:Any]], let route = routes.first,
                    let legs = route["legs"] as? [[String:Any]], let leg = legs.first,
                    let distance = leg["distance"] as? [String:Any], let distanceText = distance["text"] as? String {
                    
                    dist = distanceText
                    completion(dist)
                }else{
                     completion("0")
                }
                
            })
            task.resume()
        }
    }
    
    static func sendAttString(_ fonts: [UIFont], colors: [UIColor], texts: [String], align : NSTextAlignment, lineSpacing : CGFloat) -> NSMutableAttributedString{
          let paragraphStyle = NSMutableParagraphStyle()
          paragraphStyle.alignment = align
          paragraphStyle.lineSpacing = lineSpacing
          let attString : NSMutableAttributedString = NSMutableAttributedString(string: "")
          
          for (num,_) in fonts.enumerated(){
              let attributes = [NSAttributedString.Key.font: fonts[num], NSAttributedString.Key.foregroundColor: colors[num]]
              let myAttrString = NSAttributedString(string: texts[num], attributes: attributes)
              attString.append(myAttrString)
          }
          
          attString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attString.length))
          return attString
      }
      
      // for making attributed string with different font and colors
      static func sendAttString(_ fonts: [UIFont], colors: [UIColor], texts: [String], align : NSTextAlignment) -> NSMutableAttributedString{
          return sendAttString(fonts, colors: colors, texts: texts, align: align, lineSpacing: 4)
      }
}
