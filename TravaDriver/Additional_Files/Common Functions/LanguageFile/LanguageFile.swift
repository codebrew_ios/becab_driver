//
//  LanguageFile.swift
//  Wade7Student
//
//  Created by OSX on 22/12/17.
//  Copyright © 2017 OSX. All rights reserved.
//

import UIKit

internal struct Languages {
    
    static let Arabic = "ar"
    static let English = "en"
    static let Urdu = "ur"
    static let Chinese = "zh-Hans"
    static let Spanish = "es"
}

class LanguageFile: NSObject {
    
    static let shared = LanguageFile()
    
    //MARK: SetLanguage
    func changeLanguage(type:String){
        
        switch type {
            
        case Languages.English:
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.English
            token.selectedLanguage = LanguageCode.English.get()
            
        case Languages.Urdu:
            
            BundleLocalization.sharedInstance().language = Languages.Urdu
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            token.selectedLanguage = LanguageCode.Urdu.get()
            
        case Languages.Arabic:
            
            BundleLocalization.sharedInstance().language = Languages.Arabic
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            token.selectedLanguage = LanguageCode.Arabic.get()
            
        case Languages.Chinese:
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.Chinese
            token.selectedLanguage = LanguageCode.Chinese.get()
            
        case Languages.Spanish:
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.Spanish
            token.selectedLanguage = LanguageCode.Spanish.get()
            
        default:
            
            print("example")
            
            //            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            //            BundleLocalization.sharedInstance().language = Languages.English
            //             token.selectedLanguage = LanguageCode.English.get()
        }
        
        UserDefaults.standard.set(BundleLocalization.sharedInstance().language, forKey: "language")
        initializeStoryboard()
    }
    
    func initializeStoryboard() {
        
        if token.access_token != "" {
            //if (UserDefaults.standard.value(forKey: "category_brand_product_id") as? Int) != nil {
                let vc = R.storyboard.main.mapViewController()
                let nav = UINavigationController(rootViewController: vc!)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = nav
            //}
        /*else{
                let vc = R.storyboard.loginSignUp.phoneSignUpViewController()
                let nav = UINavigationController(rootViewController: vc!)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = nav
            }*/
        }else{
//            guard let vc = R.storyboard.newTemplateLoginSignUp.ntLoginViewController() else {return}
            guard let vc = R.storyboard.newTemplateLoginSignUp.walkthroughViewController() else {return}
            let nav = UINavigationController(rootViewController: vc)
            nav.isNavigationBarHidden = true
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nav
//            let vc = R.storyboard.loginSignUp.phoneSignUpViewController()
//            let nav = UINavigationController(rootViewController: vc!)
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.window?.rootViewController = nav
        }
    }
}

extension UIView {
    
    func setTextFields(mainView : [UIView]) {
        
        for view in mainView{
            if view is UITextField {
                let txtField = view as? UITextField
                txtField?.textAlignment = setTexts()
            }
            else if view is UITextView {
                
                let txtView = view as? UITextView
                txtView?.textAlignment = setTexts()
            }
                
            else if view is UIButton{
                if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
                    
                    scaleButton(button: view as? UIButton)
                }
            }else{
                
                setTextFields(mainView: view.subviews)}
        }
    }
    
    func scaleButton(button : UIButton?) {
        
        if let widthTitle = button?.titleLabel?.frame.size.width, let widthImage = button?.imageView?.frame.size.width{
            button?.titleEdgeInsets = UIEdgeInsetsMake(0, -widthTitle - 4, 0, -widthTitle)
            button?.imageEdgeInsets = UIEdgeInsetsMake(0, -widthImage, 0, -widthImage - 4)
            
        }
    }
    
    func setTexts() -> NSTextAlignment {
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu {
            return .right
        }else {return .left}
    }
    
    func setViewLeftAlign(mainView : [UIView]) {
        
        for view in mainView {
            if view is UITextField {
                let txtField = view as? UITextField
                txtField?.textAlignment = .left
            }
            else if view is UITextView {
                
                let txtView = view as? UITextView
                txtView?.textAlignment = .left
            }
            else if view is UIButton {
                let btn = view as? UIButton
                btn?.setLeftAlign()
            }else{
                setViewLeftAlign(mainView: view.subviews)
            }
        }
    }
    
}

extension UIButton {
    
    func setAlignment() {
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            self.contentHorizontalAlignment = .right
        }else {
            self.contentHorizontalAlignment = .left
        }
    }
    
    func setLeftAlign() {
        self.contentHorizontalAlignment = .left
    }
    
    func setImageRotation() {
        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
            self.setImage(self.imageView?.image?.withHorizontallyFlippedOrientation(), for: .normal)
        }
    }
    
}

