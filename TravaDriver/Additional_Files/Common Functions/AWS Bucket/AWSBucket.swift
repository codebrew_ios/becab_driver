////
////  AWSBucket.swift
////  Kabootz
////
////  Created by Sierra 4 on 08/06/17.
////  Copyright © 2017 Sierra 4. All rights reserved.
////
//
//import UIKit
//import Foundation
//import AWSS3
//
//struct AWSConstants {
//    
//    static let S3BucketName = "bhiveuser"
//    static let accessKey = "AKIAICRDJMBD4Q4Y5ULQ"
//    static let secretKey = "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"
//}
//
//enum media {
//  
//    case image
//    case video
//    case document
//  
//    func key() -> String {
//        switch self {
//        case .image: return "image.jpeg"
//        case .video: return "video.mp4"
//        case .document: return "document.pdf"
//        }
//    }
//  
//    func contentType() -> String {
//        switch self {
//        case .image: return "image/jpeg"
//        case .video: return "video/mp4"
//        case .document: return "application/pdf"
//        }
//    }
//}
//
//typealias returnReponse = (URL?) -> Void
//typealias transferUtility = (AWSS3TransferManagerUploadRequest?, AWSS3TransferManager?) -> Void
//
//
//class S3BucketHelper: NSObject {
//  
//    static let shared = S3BucketHelper()
//    private override init() {}
//  
//    //MARK: - Save Image -> return Path
//    func returnImageUrl(data: Data) -> URL {
//      
//        let fileManager = FileManager.default
//        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("image\(arc4random()).jpeg")
//      
//        fileManager.createFile(atPath: path as String, contents: data, attributes: nil)
//        let fileUrl = URL(fileURLWithPath: path)
//        return fileUrl
//    }
//  
//  func returnVideoUrl(data: Data) -> URL {
//    
//    let fileManager = FileManager.default
//    let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("image\(arc4random()).mp4")
//    
//    fileManager.createFile(atPath: path as String, contents: data, attributes: nil)
//    let fileUrl = URL(fileURLWithPath: path)
//    return fileUrl
//  }
//  
//  
//    //MARK: - Create Upload Request Object
//    func uploadRequest(data: Any? , fileName: String, mediaType: media,mimeType : String?) -> AWSS3TransferManagerUploadRequest {
//      
//        let uploadRequest = AWSS3TransferManagerUploadRequest()!
//      
//      
//      
//      
//        switch mediaType {
//          
//        case .image:
//            guard let imageData = data as? Data else {
//                return uploadRequest
//            }
//            uploadRequest.body = self.returnImageUrl(data: imageData)
//            uploadRequest.key = "\(fileName)"
//            uploadRequest.bucket = AWSConstants.S3BucketName
//            uploadRequest.contentType = mediaType.contentType()
//            //  uploadRequest.acl = .publicRead
//            return uploadRequest
//          
//        case .video:
//            guard let videoUrl = data as? Data else {
//                return uploadRequest
//            }
//            print(videoUrl)
//            uploadRequest.body = self.returnImageUrl(data : videoUrl)
//            uploadRequest.key = "\(fileName)"
//            uploadRequest.bucket = AWSConstants.S3BucketName
//            uploadRequest.contentType = mediaType.contentType()
//            //   uploadRequest.acl = .publicRead
//            return uploadRequest
//        case .document :
//            guard let documentUrl = data as? URL else {
//                return uploadRequest
//            }
//            uploadRequest.body = documentUrl
//            uploadRequest.key = "\(fileName)"
//            uploadRequest.bucket = AWSConstants.S3BucketName
//            uploadRequest.contentType = /mimeType
//            //   uploadRequest.acl = .publicRead
//            return uploadRequest
//        }
//    }
//  
//    //MARK: - Setup S3Bucket
//    func setupForS3Bucket() {
//        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: AWSConstants.accessKey, secretKey: AWSConstants.secretKey)
//        let configuration = AWSServiceConfiguration(region: AWSRegionType.USWest2, credentialsProvider: credentialsProvider)
//        AWSServiceManager.default().defaultServiceConfiguration = configuration
//    }
//  
//    //MARK: - Upload Images/Videos
//    func uploadRequest(data: Any?, fileName:String, mediaType: media ,mimeType : String?, oncompletion:@escaping returnReponse) -> (AWSS3TransferManagerUploadRequest, AWSS3TransferManager) {
//        setupForS3Bucket()
//      
//        let uploadRequest = self.uploadRequest(data: data,  fileName: fileName, mediaType: mediaType,mimeType:mimeType)
//        let transferManager = AWSS3TransferManager.default()
//        transferManager.upload(uploadRequest).continueWith(block: { (task: AWSTask) -> Any? in
//            //Loader.shared.stop()
//            if let error = task.error {
//                print("Upload failed with error: (\(error.localizedDescription))")
//            }
//          print(fileName)
//          print(mediaType)
//          
//            let url = AWSS3.default().configuration.endpoint.url
//            let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(/uploadRequest.key)
//            //print("Uploaded to:\(publicURL)")
//          
//            if task.result != nil {
//                
//                let url = AWSS3.default().configuration.endpoint.url
//                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(/uploadRequest.key)
//                //                print("Uploaded to:\(publicURL)")
//                oncompletion(publicURL)
//            }
//            return nil
//        })
//        return (uploadRequest, transferManager)
//    }
//  
//    //MARK: - Delete FIle from Bucket
//    func deleteFile(name: String) {
//        setupForS3Bucket()
//        let transferManager = AWSS3.default()
//        let deleteObjectRequest = AWSS3DeleteObjectRequest()
//        deleteObjectRequest?.bucket = AWSConstants.S3BucketName
//        
//        deleteObjectRequest?.key = "\(name)"
//        
//        transferManager.deleteObject(deleteObjectRequest!).continueWith { (task:AWSTask) -> AnyObject? in
//            if let error = task.error {
//                print("Error occurred: \(error)")
//                return nil
//            }
//            print("Deleted successfully.")
//            return nil
//        }
//    }
//  
//    //MARK: - Remove Image
//    func abortUpload(imageData:Uploading?) {
//        
//        guard let obj = imageData else { return }
//        guard let uploadRequest = obj.uploadRequest else { return }
//        switch uploadRequest.state {
//        case .completed:
//            self.deleteFile(name:/obj.fileName)
//        default:
//            self.cancelUpload(obj.transferManager)
//        }
//    }
//  
//    ////MARK: - Delete from AWS Bucket
//    //func deleteFromBucket(_ fileName: String?) {
//    //    guard let name = fileName else { return }
//    //    S3BucketHelper.shared.deleteFile(name: name)
//    //}
//  
//    //MARK: - Cancel AWS Upload
//    func cancelUpload(_ transferManager: AWSS3TransferManager?) {
//        transferManager?.cancelAll()
//    }
//}
//
