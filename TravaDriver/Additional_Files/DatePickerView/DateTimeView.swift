//
//  DateTimeView.swift
//  Gameplan
//
//  Created by Sierra 4 on 21/09/17.
//  Copyright © 2017 Code-brew. All rights reserved.
//


// MARK: - MODULES
import UIKit

enum DateType {
    case rcExpiry
    case insuranceExpiry
}
// MARK: - PROTOCOLS
protocol DelegateUpdateDateAndTime {
    func updateDateTime(date: Date , type : DateType )
}

// MARK: - CLASS
class DateTimeView: UIView {
    
    // MARK: - OUTLETS
    @IBOutlet var dateTimePicker: UIDatePicker!
    @IBOutlet weak var pickerBottomView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    // MARK: - PROPERTIES
    var delegate : DelegateUpdateDateAndTime?
    var type : DateType = .rcExpiry
    var datePickerDateOfBirth = false
    var hideView = true
    
    override func awakeFromNib() {
    }
    
    // MARK: - ACTIONS
    @IBAction func btnActionDone(_ sender: UIButton) {
        delegate?.updateDateTime(date: dateTimePicker.date, type : self.type)
        self.removeFromSuperview()
    }
    
    @IBAction func btnActionCancel(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    @IBAction func btnActionReset(_ sender: UIButton) {
        //delegate?.updateDateTime(date: nil , field : self.field)
    }
    
    @IBAction func btnActionPicker(_ sender: UIDatePicker) {
        //getDateAndTime()
    }
    @IBAction func datePickerAction(_ sender: UIDatePicker) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if hideView {
            let touch = touches.first
            if touch?.view?.tag != 99 {
                self.removeFromSuperview()
            }
        }
    }
}

// MARK: - UPDATE TIME
extension DateTimeView {
    
    func getDateAndTime(){
        delegate?.updateDateTime(date: dateTimePicker.date, type : self.type)
        
    }
}



